--Compares the alternate source (ISD) station list to the master point station lists and finds the closest location to the ISD station.
--TODO: Split into batches
--TODO: exclude buoys
select * from
	(select  N.UniqueID,N.Station_Name,N.Distance,
		ROW_NUMBER() OVER (PARTITION BY N.UniqueID ORDER BY N.Distance ASC) as DistanceRank 
		From (
			Select s.UniqueID,l.station_name
				, geography::STGeomFromText(('POINT(' + s.Longitude + ' ' + s.Latitude + ')'), 4326).STDistance(L.SpatialMeasureGeog)/1000 as Distance 
			From [AlternateWeatherSources].[dbo].[ISDCodes] S, WT_Processing..Master_WT_PointStations L
			where isnumeric(s.Longitude)=1
			and isnumeric(S.Latitude)=1
			and POWER(power(abs(cast(S.Longitude as real)-L.Longitude),2)+power(abs(cast(S.Latitude as real)-L.Latitude),2),.5) <1
			and L.LocationID<70000000
			) N
	) M
 where distancerank = 1




