/****** DOT-618  ******/
--0-5 km Analysis
--Find non-matching AWIS IDs with a WMO code that also match either wmo1 or wmo2 codes in the AWIS table
--Comparing Gladstone WMO code to the nearest station wmo codes resulted in 0 matches for both wmo 1 and wmo 2, which is why I searched the entire AWIS list
SELECT [Station] as GStationID
      ,[WMOID] as GWMOCode
      ,[StationNam] as GStationName
      ,[Latitude] as GLat
      ,[Longitude] as GLong
      ,[Elevation_] as GElev
      ,[Distance_k] as DistanceToNearestAWISStation
      ,[AWIS_ID] as NearestAWISID
      ,[AWIS_Stati] as NearestAWISStationName
      ,[AWIS_Latit] as NearestAWISLat
      ,[AWIS_Longi] as NearestAWISLong
      ,[AWIS_Eleva] as NearestAWISElev
	  ,wmo_id1 as NearestAWISWMO1
	  , wmo_id2 as NearestAWISWMO2
  FROM [AlternateWeatherSources].[dbo].[GladstoneActiveStations_0To5Km]
  where station <> awis_id
  and wmoid not like 'null'
  and (wmoid in (
	Select wmo_id2
  From [AlternateWeatherSources].[dbo].[GladstoneActiveStations_0To5Km]
  )
  or wmoid in (
  	Select wmo_id1
	  From [AlternateWeatherSources].[dbo].[GladstoneActiveStations_0To5Km]
	  ))


-- 5-27 km Analysis
SELECT [Station] as GStationID
      ,[WMOID] as GWMOCode
      ,[StationNam] as GStationName
      ,[Latitude] as GLat
      ,[Longitude] as GLong
      ,[Elevation_] as GElev
      ,[Distance_km] as DistanceToNearestAWISStation
      ,[AWIS_ID] as NearestAWISID
      ,[AWIS_Stati] as NearestAWISStationName
      ,[AWIS_Latit] as NearestAWISLat
      ,[AWIS_Longi] as NearestAWISLong
      ,[AWIS_Eleva] as NearestAWISElev
	  ,wmo_id1 as NearestAWISWMO1
	  , wmo_id2 as NearestAWISWMO2
  FROM [AlternateWeatherSources].[dbo].[GladstoneActiveStations_5To25Km]
  where station <> awis_id
  and wmoid not like 'NA'
  and (wmoid in (
	Select distinct wmo_id2
  From [AlternateWeatherSources].[dbo].[GladstoneActiveStations_5To25Km]
  )
  or wmoid in (
  	Select wmo_id1
	  From [AlternateWeatherSources].[dbo].[GladstoneActiveStations_5To25Km]
	  ))

-- 0-5 km; Find all WMO1/2 codes that match GWMOID, don't filter on Station IDs that match AWIS
SELECT [Station] as GStationID
      ,[WMOID] as GWMOCode
      ,[StationNam] as GStationName
      ,[Latitude] as GLat
      ,[Longitude] as GLong
      ,[Elevation_] as GElev
      ,[Distance_k] as DistanceToNearestAWISStation
      ,[AWIS_ID] as NearestAWISID
      ,[AWIS_Stati] as NearestAWISStationName
      ,[AWIS_Latit] as NearestAWISLat
      ,[AWIS_Longi] as NearestAWISLong
      ,[AWIS_Eleva] as NearestAWISElev
	  ,wmo_id1 as NearestAWISWMO1
	  , wmo_id2 as NearestAWISWMO2
  FROM [AlternateWeatherSources].[dbo].[GladstoneActiveStations_0To5Km]
  where wmoid not like 'null'
	and (wmoid in(
		Select wmo_id1
		from [AlternateWeatherSources].[dbo].[GladstoneActiveStations_0To5Km]
		)
	or wmoid in (
		Select wmo_id2
		from [AlternateWeatherSources].[dbo].[GladstoneActiveStations_0To5Km]
		))
	and station not like awis_id
  
  --5-27 km; Find all WMO1/2 codes that match GWMOID, don't filter on Station IDs that match AWIS
  SELECT [Station] as GStationID
      ,[WMOID] as GWMOCode
      ,[StationNam] as GStationName
      ,[Latitude] as GLat
      ,[Longitude] as GLong
      ,[Elevation_] as GElev
      ,[Distance_km] as DistanceToNearestAWISStation
      ,[AWIS_ID] as NearestAWISID
      ,[AWIS_Stati] as NearestAWISStationName
      ,[AWIS_Latit] as NearestAWISLat
      ,[AWIS_Longi] as NearestAWISLong
      ,[AWIS_Eleva] as NearestAWISElev
	  ,wmo_id1 as NearestAWISWMO1
	  , wmo_id2 as NearestAWISWMO2
  FROM [AlternateWeatherSources].[dbo].[GladstoneActiveStations_5To25Km]
  where wmoid not like 'na'
	and (wmoid in(
		Select wmo_id1
		from [AlternateWeatherSources].[dbo].[GladstoneActiveStations_5To25Km]
		)
	or wmoid in (
		Select wmo_id2
		from [AlternateWeatherSources].[dbo].[GladstoneActiveStations_5To25Km]
		))
	and station not like awis_id



-- 0-5 km; Find all WMO1/2 codes that match station
SELECT [Station] as GStationID
      ,[WMOID] as GWMOCode
      ,[StationNam] as GStationName
      ,[Latitude] as GLat
      ,[Longitude] as GLong
      ,[Elevation_] as GElev
      ,[Distance_k] as DistanceToNearestAWISStation
      ,[AWIS_ID] as NearestAWISID
      ,[AWIS_Stati] as NearestAWISStationName
      ,[AWIS_Latit] as NearestAWISLat
      ,[AWIS_Longi] as NearestAWISLong
      ,[AWIS_Eleva] as NearestAWISElev
	  ,wmo_id1 as NearestAWISWMO1
	  , wmo_id2 as NearestAWISWMO2
  FROM [AlternateWeatherSources].[dbo].[GladstoneActiveStations_0To5Km]
  where station in(
		Select wmo_id1
		from [AlternateWeatherSources].[dbo].[GladstoneActiveStations_0To5Km]
		)
	or station in (
		Select wmo_id2
		from [AlternateWeatherSources].[dbo].[GladstoneActiveStations_0To5Km]
		)

-- 5-27 km; Find all WMO1/2 codes that match station
SELECT [Station] as GStationID
      ,[WMOID] as GWMOCode
      ,[StationNam] as GStationName
      ,[Latitude] as GLat
      ,[Longitude] as GLong
      ,[Elevation_] as GElev
      ,[Distance_km] as DistanceToNearestAWISStation
      ,[AWIS_ID] as NearestAWISID
      ,[AWIS_Stati] as NearestAWISStationName
      ,[AWIS_Latit] as NearestAWISLat
      ,[AWIS_Longi] as NearestAWISLong
      ,[AWIS_Eleva] as NearestAWISElev
	  ,wmo_id1 as NearestAWISWMO1
	  , wmo_id2 as NearestAWISWMO2
  FROM [AlternateWeatherSources].[dbo].[GladstoneActiveStations_5To25Km]
  where station in(
		Select wmo_id1
		from [AlternateWeatherSources].[dbo].[GladstoneActiveStations_5To25Km]
		)
	or station in (
		Select wmo_id2
		from [AlternateWeatherSources].[dbo].[GladstoneActiveStations_5To25Km]
		)