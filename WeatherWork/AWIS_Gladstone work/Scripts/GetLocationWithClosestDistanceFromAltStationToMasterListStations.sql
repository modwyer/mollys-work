--Compares the alternate source (ISD) station list to the master point station lists and finds the closest location to the ISD station.
--Split work into batches for faster processing

Declare @Location table(LocationID int,RunID int identity,ClassBreak int, Station_Name varchar(100), Longitude float, Latitude float, SpatialMeasureGeog geography)
Insert @Location(LocationID, Station_Name, Longitude, Latitude, SpatialMeasureGeog)
	Select LocationID, Station_Name, Longitude, Latitude, SpatialMeasureGeog
	From WT_Processing.[dbo].[Master_WT_PointStations]

Declare @MaxRunID int = (Select max(RunID) from @Location)
Declare @ClassBreaks int = 10
Declare @ClassBreakSize float = @MaxRunID/@ClassBreaks
Declare @StartTime as datetime2(7)
Declare @EndTime as datetime2(7)

Update @Location
Set ClassBreak = RunId/@ClassBreakSize

Declare @ClassBreakMin int = 0

While @ClassBreakMin <=@ClassBreaks
	Begin
		Print ('Now working on class break ' + cast(@ClassBreakMin as varchar(10)) + '.')
		Set @StartTime = SYSDATETIME()
		Print ('Class break ' + cast(@ClassBreakMin as varchar(10)) + ' was started at ' + cast(@StartTime as varchar(19)) + '.')
		
		Select * from
			(select  N.UniqueID,N.Station_Name,N.Distance,
				ROW_NUMBER() OVER (PARTITION BY N.UniqueID ORDER BY N.Distance ASC) as DistanceRank 
				From (
					Select s.UniqueID,l.station_name
						, geography::STGeomFromText(('POINT(' + s.Longitude + ' ' + s.Latitude + ')'), 4326).STDistance(L.SpatialMeasureGeog)/1000 as Distance 
					From [AlternateWeatherSources].[dbo].[ISDCodes] S, @Location L
					where isnumeric(s.Longitude)=1
					and isnumeric(S.Latitude)=1
					and POWER(power(abs(cast(S.Longitude as real)-L.Longitude),2)+power(abs(cast(S.Latitude as real)-L.Latitude),2),.5) <1
					and L.LocationID<70000000
					and ClassBreak = @ClassBreakMin
					) N
			) M
		where distancerank = 1

		Set @EndTime = SYSDATETIME()
		Print ('Class break ' + cast(@ClassBreakMin as varchar(10)) + ' was ended at ' + cast(@EndTime as varchar(19)) + '.')
		Set @ClassBreakMin = @ClassBreakMin + 1
	end

