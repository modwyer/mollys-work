
--***List all Gladstone stations where the names or station id matches and distance from AWIS station is between 0 and .5 km
SELECT g.Station, g.StationName, g.Latitude, g.Longitude, g.Elevation_m, g.NEAR_FID, g.Distance_km, m.awis_id, m.awis_station_name, m.awis_latitude, m.awis_longitude, m.awis_elevation
  FROM [AlternateWeatherSources].[dbo].[GladstoneActiveStations] G, WT_Processing.[dbo].[Master_WT_PointStations_Lookup] M
  Where (g.Station = M.AWIS_Id 
  or (m.awis_station_name like g.stationname or m.awis_station_name = g.stationname))
  and Distance_km between 0 and .5
  --5390

--***List all Gladstone stations where the name or station id matches and distance from AWIS station is between .51 and 1 km
SELECT g.Station, g.StationName, g.Latitude, g.Longitude, g.Elevation_m, g.NEAR_FID, g.Distance_km, m.awis_id, m.awis_station_name, m.awis_latitude, m.awis_longitude, m.awis_elevation
  FROM [AlternateWeatherSources].[dbo].[GladstoneActiveStations] G, WT_Processing.[dbo].[Master_WT_PointStations_Lookup] M
  Where (g.Station = M.AWIS_Id 
  or (m.awis_station_name like g.stationname or m.awis_station_name = g.stationname))
  and Distance_km between .51 and 1
  order by distance_km desc




--List all Gladstone stations with a matching AWIS_ID
SELECT [Station],[StationName],[Latitude],[Longitude],[Elevation_m],[NEAR_FID],[Distance_km]
  FROM [AlternateWeatherSources].[dbo].[GladstoneActiveStations]
  where Station in (
	Select distinct AWIS_ID
	From WT_Processing.[dbo].[Master_WT_PointStations_Lookup]
	where AWIS_ID is not null
	)
--6116

--List all Gladstone stations with a matching AWIS_ID or AWIS station names that are in the exact same spot
SELECT g.Station, g.StationName, g.Latitude, g.Longitude, g.Elevation_m, g.NEAR_FID, g.Distance_km, m.awis_id, m.awis_station_name, m.awis_latitude, m.awis_longitude, m.awis_elevation
  FROM [AlternateWeatherSources].[dbo].[GladstoneActiveStations] G, WT_Processing.[dbo].[Master_WT_PointStations_Lookup] M
  Where (g.Station = M.AWIS_Id 
  or (m.awis_station_name like g.stationname or m.awis_station_name = g.stationname))
  and Distance_km = 0
  --1398




