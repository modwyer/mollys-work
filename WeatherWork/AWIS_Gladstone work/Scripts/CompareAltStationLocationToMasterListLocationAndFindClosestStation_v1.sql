--Declare @Lat1 float, @Lat2 float, @Long1 float, @Long2 float

--Create table #ISDDistances (RunId , USAF_Codes varchar(100), Lat1 float, Long1 float, AWIS_ID varchar(10), Lat2 float, Long2 float
--Select @Lat1 = Latitude, @Long1 = Longitude
--From AlternateWEatherSources..ISDcodes

--Select @Lat2 = Latitude, @Long2 = Longitude
--from WT_Processing..Master_WT_PointStations_Lookup

--[dbo].[GetDistanceWithPassedCoordinates] @Lat1 @Long1 @Lat2 @Long2

drop table #PointGeometry, #AllDistances, #MinDistances
USE [AlternateWeatherSources]
GO
Create table #PointGeometry (UniqueId int, AWIS_ID varchar(10), Geom1 geometry, Geom2 geometry, LatDiff float, LongDiff float)
Insert into #PointGeometry (UniqueId, AWIS_ID, Geom1, Geom2, LatDiff, LongDiff)
	SELECT I.UniqueId,[AWIS_ID], 
			awmDatabase.dbo.GetGeometryForCoordinate(I.Latitude, I.Longitude) as Geom1
			,awmDatabase.dbo.GetGeometryForCoordinate(L.[AWIS_Latitude], L.[AWIS_Longitude]) as Geom2
			, abs(I.Latitude - L.AWIS_Latitude) as LatDiff
			, abs(I.Longitude - L.AWIS_Longitude) as LongDiff
		  FROM AlternateWEatherSources..ISDcodes I, WT_Processing..Master_WT_PointStations_Lookup L
		  Where L.[AWIS_ID] is not null
		  and abs(I.Latitude - L.AWIS_Latitude)<.15
		  and abs(I.Longitude - L.AWIS_Longitude)<.15
		  and I.Station_Code not in 
		  (
			Select AWIS_ID
			From WT_Processing..Master_WT_PointStations_Lookup
			where AWIS_ID is not null
			)

Select * from #PointGeometry
--18867

Create table #AllDistances (UniqueId int, AWIS_Id varchar(10), Distance float)
Insert into #AllDistances (UniqueId, AWIS_Id, Distance)
	Select UniqueId, AWIS_ID, Geom1.STDistance(Geom2) as Distance
	From #PointGeometry

Select * from #allDistances	
--18867

Create table #MinDistances (UniqueID int, AWIS_Id varchar(10), MinDistance float)
Insert into #MinDistances (UniqueID, AWIS_Id, MinDistance)
	Select uniqueId, AWIS_ID, min(Distance) as MinDistance
	From #AllDistances
	Group By UniqueId, AWIS_ID
	order by uniqueid

Select * from #MinDistances



Select i.uniqueid, a.awis_id, a.distance, i.Station_Name, i.Station_Code, i.Latitude as ISDLat, i.Longitude as ISDLong, i.elevation_m, i.End_date
From [AlternateWeatherSources].[dbo].[ISDCodes] I
inner join
#MinDistances M
on i.uniqueid = m.uniqueid
Inner Join
WT_Processing..WT_Processing..Master_WT_PointStations_Lookup L
On m.UniqueId = l.UniqueId
order by i.uniqueid

--------------------------------------------------
WITH AllDistances (UniqueId, AWIS_Id, Distance)
as
(
Select UniqueId, AWIS_ID, Geom1.STDistance(Geom2) as Distance
From 
	(
	SELECT I.UniqueId,[AWIS_ID], 
		awmDatabase.dbo.GetGeometryForCoordinate(I.Latitude, I.Longitude) as Geom1
		,awmDatabase.dbo.GetGeometryForCoordinate(L.[AWIS_Latitude], L.[AWIS_Longitude]) as Geom2
		, abs(I.Latitude - L.AWIS_Latitude) as LatDiff
		, abs(I.Longitude - L.AWIS_Longitude) as LongDiff
	  FROM AlternateWEatherSources..ISDcodes I, WT_Processing..Master_WT_PointStations_Lookup L
	  Where L.[AWIS_ID] is not null
	  and abs(I.Latitude - L.AWIS_Latitude)<.15
	  and abs(I.Longitude - L.AWIS_Longitude)<.15
	  and I.Station_Code not in 
	  (
		Select AWIS_ID
		From WT_Processing..Master_WT_PointStations_Lookup
		where AWIS_ID is not null
		)
	) J
),
MinDistances as 
	(
	Select uniqueId, min(Distance) as MinDistance
	From AllDistances
	Group By UniqueId
	)

Select A.UniqueId, A.AWIS_Id, A.Distance
From AllDistances A
Inner Join
MinDistances M
On A.UniqueId = M.UniqueId
Where A.Distance = M.MinDistance








