/****** Scripts to isolate ICAO, WMO, USAF, etc stations ******/
--basic query
SELECT [StationId]
      ,[NCDC_Station_Number]
      ,[WMO_Station_Number]
      ,[Station_Name]
      ,[Latitude]
      ,[Longitude]
      ,[Elevation_Meters]
  FROM [WT_Processing].[dbo].[worldstas_Import]
  where len([WMO_Station_Number])=5
  and len(StationID) = 4
  and StationID not like '%[0-9]%'

  --To pull out ICAO stations
  Select STationID, WMO_Station_Number, STation_Name, Latitude, Longitude, (Latitude-.15) as MinLat
  , Latitude+.15 as MaxLat, Longitude-.15 as MinLong, Longitude+.15 as MaxLong
  From [WT_Processing].[dbo].[worldstas_Import]
  where len(StationID) = 4
  and StationID not like '%[0-9]%'
  and WMO_Station_Number = 999999
  and len(WMO_station_number) = 6

  --To pull out non-ICAO stations
   Select StationID, WMO_Station_Number, STation_Name, Latitude, Longitude, (Latitude-.15) as MinLat
  , Latitude+.15 as MaxLat, Longitude-.15 as MinLong, Longitude+.15 as MaxLong
  From [WT_Processing].[dbo].[worldstas_Import]
  where StationID like '%[0-9]%'
  and WMO_Station_Number <> 999999
  and len(WMO_station_number) = 6
 
  --To find unaccounted for stations
   Select StationID, WMO_Station_Number, STation_Name, Latitude, Longitude, (Latitude-.15) as MinLat
  , Latitude+.15 as MaxLat, Longitude-.15 as MinLong, Longitude+.15 as MaxLong
  From [WT_Processing].[dbo].[worldstas_Import]
  where stationid not in (
	Select STationID
	  From [WT_Processing].[dbo].[worldstas_Import]
	  where len(StationID) = 4
	  and StationID not like '%[0-9]%'
	  )
	and stationid not in (
		Select StationID
		  From [WT_Processing].[dbo].[worldstas_Import]
		  where StationID like '%[0-9]%'
		  )

--To find which stations in [AlternateWeatherSources].[dbo].[MadisStationInfo] are in worldstas_Import
Select *
From [AlternateWeatherSources].[dbo].[MadisStationInfo]
where Station in (
	Select AWIS_ID
	From WT_Processing.[dbo].[Master_WT_PointStations_Lookup]
	)