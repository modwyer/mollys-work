--1. Valid WMOs (#ValidWMO)
Select *
From #ValidWMO
where wmo_id not in (
	Select wmo_id
	from #ValidWMO
	group by wmo_id
	having count(*) >1
	)
order by wmo_id

--2. Duplicate WMO from Valid WMO list above
Select *
From #ValidWMO
where wmo_id in (
	Select wmo_id
	from #ValidWMO
	group by wmo_id
	having count(*) >1
	)
	order by wmo_id

--3. Invalid WMOs - matching 999999 or nulls or mix (#AWISInvalidWMO)
	select Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2
	from WT_Processing..Master_WT_PointStations_Lookup
	where WMO_ID1 is null
	and WMO_ID2 is null
	and awis_id is not null
	union all
	select Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2
	from WT_Processing..Master_WT_PointStations_Lookup
	where WMO_ID1 = 999999
	and WMO_ID2 = 999999
	and awis_id is not null
	union all
	Select locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2 
	from WT_Processing..Master_WT_PointStations_Lookup
	where awis_id is not null
	and locationid not in (
		Select Locationid from #AWISInvalidWMO)
	and locationid not in (
		Select Locationid from #TroubleWMO)
	and locationid not in (
		Select Locationid from #ValidWMO)

--4. Matching WMO issues - mismatched valid WMO codes or matching WMO codes (#TroubleWMO)
	Select Locationid, wmo_id1, wmo_id2, awis_id, awis_station_name, awis_latitude, awis_longitude
	from WT_Processing..Master_WT_PointStations_Lookup
	where awis_id is not null
	and wmo_id1 <> 999999
	and wmo_id2 <> 999999
	and wmo_id2 is not null
	and (wmo_id1 = wmo_id2
	or wmo_id1 <> wmo_id2)
	and locationid not in (
		Select Locationid from #AWISInvalidWMO)



-----------------------------------------------------------------------------------------------------
/** 1. Queries to populate the temp tables **/

--A. Find WMOs with matching nulls 
	Create table #AWISNulls (Locationid int, awis_id varchar(50), awis_station_name varchar(50), awis_latitude real, awis_longitude real, wmo_id1 int, wmo_id2 int)
	Insert into #AWISNulls (Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2)
	select Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2
	from WT_Processing..Master_WT_PointStations_Lookup
	where WMO_ID1 is null
	and WMO_ID2 is null
	and awis_id is not null

--B. Find WMOs with matching 999999
	Create table #AWIS999999 (Locationid int, awis_id varchar(50), awis_station_name varchar(50), awis_latitude real, awis_longitude real, wmo_id1 int, wmo_id2 int)
	Insert into #AWIS999999 (Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2)
	select Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2
	from WT_Processing..Master_WT_PointStations_Lookup
	where WMO_ID1 = 999999
	and WMO_ID2 = 999999
	and awis_id is not null

--C. Find all invalid WMOs: matching null or 999999 
	Create table #AWISInvalidWMO (Locationid int, awis_id varchar(50), awis_station_name varchar(50), awis_latitude real, awis_longitude real, wmo_id1 int, wmo_id2 int)
	Insert into #AWISInvalidWMO (Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2)
	Select Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2 from #AWISNulls
	union all
	Select Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2 from #AWIS999999

--D. Find WMOs with match issues: WMO codes match or are valid mismatched codes 
	Create table #TroubleWMO (Locationid int, wmo_id1 int, wmo_id2 int, awis_id varchar(50), awis_station_name varchar(50), awis_latitude real, awis_longitude real)
	Insert into #TroubleWMO (Locationid, wmo_id1, wmo_id2, awis_id, awis_station_name, awis_latitude, awis_longitude)
	Select Locationid, wmo_id1, wmo_id2, awis_id, awis_station_name, awis_latitude, awis_longitude
	from WT_Processing..Master_WT_PointStations_Lookup
	where awis_id is not null
	and wmo_id1 <> 999999
	and wmo_id2 <> 999999
	and wmo_id2 is not null
	and (wmo_id1 = wmo_id2
	or wmo_id1 <> wmo_id2)
	and locationid not in (
		Select Locationid from #AWISInvalidWMO)

--E. Find valid WMOs: combined wmo code column from ID1 and ID2
	Create table #ValidWMO (Locationid int, wmo_id int, awis_id varchar(50), awis_station_name varchar(50), awis_latitude real, awis_longitude real)
	--wmo_id2 unique
	Insert into #ValidWMO (Locationid, wmo_id, awis_id, awis_station_name, awis_latitude, awis_longitude)
	Select Locationid, wmo_id2, awis_id, awis_station_name, awis_latitude, awis_longitude
	from WT_Processing..Master_WT_PointStations_Lookup
	where awis_id is not null
	and wmo_id2 is not null
	and wmo_id2 <> 999999
	and (wmo_id1 is null
	or wmo_id1 = 999999)
	and locationid not in (
		Select Locationid from #AWISInvalidWMO) --IDs are invalid
	and locationid not in (
		Select Locationid from #TroubleWMO)  --IDs have match issue
	union all
	--wmo_id1 unique
	Select Locationid, wmo_id1, awis_id, awis_station_name, awis_latitude, awis_longitude
	from WT_Processing..Master_WT_PointStations_Lookup
	where awis_id is not null
	and wmo_id1 is not null
	and wmo_id1 <> 999999
	and (wmo_id2 is null
	or wmo_id2 = 999999)
	and locationid not in (
		Select Locationid from #AWISInvalidWMO)
	and locationid not in (
		Select Locationid from #TroubleWMO)

--F. Find 55 missing locations to go in #AWISInvalidWMO table
	Insert into #AWISInvalidWMO (Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2)
	Select locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2
	from WT_Processing..Master_WT_PointStations_Lookup
	where awis_id is not null
	and locationid not in (
		Select Locationid from #AWISInvalidWMO)
	and locationid not in (
		Select Locationid from #TroubleWMO)
	and locationid not in (
		Select Locationid from #ValidWMO)

----------------------------------------------------------------------------------------
/** 2. Identify duplicate WMO codes in Valid WMO list above and fix **/

--A. Identify duplicates 1145	
	Select wmo_id
	From #ValidWMO
	where wmo_id in (
		Select wmo_id
		from #ValidWMO
		group by wmo_id
		having count(*) >1
		)
	
--B. Find duplicates marked as "do not use for distance"  524
	Select *
	From #ValidWMO
	where wmo_id in (
		Select wmo_id
		from #ValidWMO
		group by wmo_id
		having count(*) >1
		)
	and locationid in (
		Select Locationid
		From WT_Processing..Master_WT_PointStations
		where  UseForDistance = 0 --0 = do not use; 1 = use
		)

--C. Find duplicate marked as "use for distance" 621
	Select *
	From #ValidWMO
	where wmo_id in (
		Select wmo_id
		from #ValidWMO
		group by wmo_id
		having count(*) >1
		)
	and locationid in (
		Select Locationid
		From WT_Processing..Master_WT_PointStations
		where  UseForDistance = 1 --0 = do not use; 1 = use
		)
	order by wmo_id

--D. Add codes from the duplicate list that are marked do not use to the invalid table 524
	Insert into #AWISInvalidWMO (Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2)
	Select locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2
	from WT_Processing..Master_WT_PointStations_Lookup
	where awis_id is not null
	and (wmo_id1 in (
		Select wmo_id
		from #ValidWMO
		group by wmo_id
		having count(*) >1
		)
	or wmo_id2 in (
		Select wmo_id
		from #ValidWMO
		group by wmo_id
		having count(*) >1
		))
	and locationid in (
		Select Locationid
		From WT_Processing..Master_WT_PointStations
		where UseForDistance = 0 --0 = do not use; 1 = use
		)
	and locationid not in (
		Select locationid from #TroubleWMO)

--E. Delete codes from the valid table that were moved to the invalid table 524
	Delete from #ValidWMO
	where locationid in (
		Select locationid
	from WT_Processing..Master_WT_PointStations_Lookup
	where awis_id is not null
	and (wmo_id1 in (
		Select wmo_id
		from #ValidWMO
		group by wmo_id
		having count(*) >1
		)
	or wmo_id2 in (
		Select wmo_id
		from #ValidWMO
		group by wmo_id
		having count(*) >1
		))
	and locationid in (
		Select Locationid
		From WT_Processing..Master_WT_PointStations
		where UseForDistance = 0 --0 = do not use; 1 = use
		)
	and locationid not in (
		Select locationid from #TroubleWMO)
		)

--F. ::Note:: there are 124 duplicates in the valid table but they all are marked to use for distance
	Select *
	From #ValidWMO
	where wmo_id in (
		Select wmo_id
		from #ValidWMO
		group by wmo_id
		having count(*) >1
		)
	and locationid in (
		Select Locationid
		From WT_Processing..Master_WT_PointStations
		where  UseForDistance = 1 --0 = do not use; 1 = use
		)
	order by wmo_id

------------------------------------------------------------------------------
/** 3. Address match issues **/
--A. Add match issue codes marked as do not use to #AWISInvalidWMO *524 24
	Insert into #AWISInvalidWMO (Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2)
	Select Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2
	from WT_Processing..Master_WT_PointStations_Lookup
	where awis_id is not null
	and wmo_id1 <> 999999
	and wmo_id2 <> 999999
	and wmo_id2 is not null
	and (wmo_id1 = wmo_id2
	or wmo_id1 <> wmo_id2)
	and locationid not in (
		Select Locationid from #AWISInvalidWMO) --Invalid IDs list
	and locationid  in (
		Select Locationid
		From WT_Processing..Master_WT_PointStations
		where  UseForDistance = 0 --0 = do not use; 1 = use
		)
	
--B. Delete invalid match codes from #TroubleWMO that were added to the invalid list *524 24
	Delete From #TroubleWMO
	where locationid  in (
		Select locationid from #AWISInvalidWMO
		)

--C. Add match issue codes marked as do use and with matching wmo codes to #ValidWMO 12
	Insert into #ValidWMO (Locationid, wmo_id, awis_id, awis_station_name, awis_latitude, awis_longitude)
	Select Locationid, wmo_id1, awis_id, awis_station_name, awis_latitude, awis_longitude
	from #TroubleWMO
	where wmo_id1 = wmo_id2
	and locationid in (
		Select Locationid
		From WT_Processing..Master_WT_PointStations
		where  UseForDistance = 1 --0 = do not use; 1 = use
		)
	
--D. Delete match codes from #TroubleWMO that were added to the Valid list 12
	Delete From #TroubleWMO
	where locationid  in (
		Select locationid from #validWMO
		)

--E-1. Note there are 514 locations with valid mismatched WMO codes that are marked as "use"
	Select * from #TroubleWMO
	where locationid in (
		Select Locationid
		From WT_Processing..Master_WT_PointStations
		where  UseForDistance = 1 --0 = do not use; 1 = use
		)
--E-2. Insert results from E above into the valid table, using wmo_id2 for the wmo_id code. 514
	Insert into #ValidWMO (Locationid, wmo_id, awis_id, awis_station_name, awis_latitude, awis_longitude)
	Select Locationid, wmo_id2, awis_id, awis_station_name, awis_latitude, awis_longitude from #TroubleWMO
	where locationid in (
		Select Locationid
		From WT_Processing..Master_WT_PointStations
		where  UseForDistance = 1 --0 = do not use; 1 = use
		)
	and locationid not in (
		Select locationid from #ValidWMO)

--E-3. Delete match issue locations that were added to the valid table above 514
	Delete From #TroubleWMO
	where locationid in (
		Select Locationid
		From WT_Processing..Master_WT_PointStations
		where  UseForDistance = 1 --0 = do not use; 1 = use
		)

---------------------------------------------------------------------------
/** 4. Readdress duplicates in #ValidWMO table **/
--A-1. ::Note:: there are 126 duplicates in the valid table but they all are marked to use for distance
	Select *
	From #ValidWMO
	where wmo_id in (
		Select wmo_id
		from #ValidWMO
		group by wmo_id
		having count(*) >1
		)
	and locationid in (
		Select Locationid
		From WT_Processing..Master_WT_PointStations
		where  UseForDistance = 1 --0 = do not use; 1 = use
		)
	order by wmo_id

--A-2. ::Note:: there is 1 duplicate in the valid table that is marked do not use for distance
	Select *
	From #ValidWMO
	where wmo_id in (
		Select wmo_id
		from #ValidWMO
		group by wmo_id
		having count(*) >1
		)
	and locationid in (
		Select Locationid
		From WT_Processing..Master_WT_PointStations
		where  UseForDistance = 0 --0 = do not use; 1 = use
		)
	order by wmo_id

--A-3. Insert WMO code that is marked do not use in the valid table to the invalid table 1
	Insert into #AWISInvalidWMO (Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2)
	Select Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2
	from WT_Processing..Master_WT_PointStations_Lookup
	where wmo_id1 in (
		Select wmo_id
		From #ValidWMO
		where wmo_id in (
			Select wmo_id
			from #ValidWMO
			group by wmo_id
			having count(*) >1
			)
		and locationid in (
			Select Locationid
			From WT_Processing..Master_WT_PointStations
			where  UseForDistance = 0 --0 = do not use; 1 = use
			))

--A-4. Delete invalid code from the valid table 1
	Delete From #ValidWMO
	where wmo_id in (
		Select wmo_id
		from #ValidWMO
		group by wmo_id
		having count(*) >1
		)
	and locationid in (
		Select Locationid
		From WT_Processing..Master_WT_PointStations
		where  UseForDistance = 0 --0 = do not use; 1 = use
		)

--A-5. Search Gladstone for matching awis codes to try and find which of the duplicates match; 50 matches
	--Keep these in the valid list
	Select Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2
	from WT_Processing..Master_WT_PointStations_Lookup v, AlternateWeatherSources..GladstoneWithMatchingAWIS g
	--Select v.Locationid, v.wmo_id, v.awis_id, v.awis_latitude, v.awis_longitude, g.g_StationID, g.g_wmoid, g.g_Lat, g.g_Long
	--from AlternateWeatherSources..GladstoneWithMatchingAWIS g, #ValidWMO v
	where g.g_StationID in (
		Select awis_id
		From #ValidWMO
		where wmo_id in (
			Select wmo_id
			from #ValidWMO
			group by wmo_id
			having count(*) >1
			))
	and locationid in (
			Select Locationid
			From WT_Processing..Master_WT_PointStations
			where  UseForDistance = 1 --0 = do not use; 1 = use
			)
	and g.g_StationID = v.awis_id
	--order by wmo_id
	
--A-6. Insert these codes from the duplicate list to the invalid list; 76
	Insert into  #AWISInvalidWMO (Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2)
	Select Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2
	from WT_Processing..Master_WT_PointStations_Lookup
	where (wmo_id1 in (
		Select wmo_id
		from #ValidWMO
		group by wmo_id
		having count(*) >1
		) or
		wmo_id2 in (
		Select wmo_id
		from #ValidWMO
		group by wmo_id
		having count(*) >1
		))
	and locationid in (
		Select Locationid
		From WT_Processing..Master_WT_PointStations
		where  UseForDistance = 1 --0 = do not use; 1 = use
		)
	and awis_id is not null
	Except
	Select Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2
	from WT_Processing..Master_WT_PointStations_Lookup v, AlternateWeatherSources..GladstoneWithMatchingAWIS g
	where g.g_StationID in (
		Select awis_id
		From #ValidWMO
		where wmo_id in (
			Select wmo_id
			from #ValidWMO
			group by wmo_id
			having count(*) >1
			))
	and locationid in (
			Select Locationid
			From WT_Processing..Master_WT_PointStations
			where  UseForDistance = 1 --0 = do not use; 1 = use
			)
	and g.g_StationID = v.awis_id

--A-7. Delete those 76 duplicates from the valid table
	Delete
	from #ValidWMO 
	where Locationid in (
		Select Locationid
		from WT_Processing..Master_WT_PointStations_Lookup
		where (wmo_id1 in (
			Select wmo_id
			from #ValidWMO
			group by wmo_id
			having count(*) >1
			) or
			wmo_id2 in (
			Select wmo_id
			from #ValidWMO
			group by wmo_id
			having count(*) >1
			))
		and locationid in (
			Select Locationid
			From WT_Processing..Master_WT_PointStations
			where  UseForDistance = 1 --0 = do not use; 1 = use
			)
		and awis_id is not null
		Except
		Select Locationid
		from WT_Processing..Master_WT_PointStations_Lookup v, AlternateWeatherSources..GladstoneWithMatchingAWIS g
		where g.g_StationID in (
			Select awis_id
			From #ValidWMO
			where wmo_id in (
				Select wmo_id
				from #ValidWMO
				group by wmo_id
				having count(*) >1
				))
		and locationid in (
				Select Locationid
				From WT_Processing..Master_WT_PointStations
				where  UseForDistance = 1 --0 = do not use; 1 = use
				)
		and g.g_StationID = v.awis_id
		)

--B-1. ::Note:: There are 10 duplicate wmo_id in the #ValidWMO table remaining
	Select *
	From #ValidWMO
	where wmo_id in (
		Select wmo_id
		from #ValidWMO
		group by wmo_id
		having count(*) >1
		)
		order by wmo_id

--B-2. Are those duplicates in the ISD observed table? Yes, 5 wmo codes
	Select distinct usaf
	from AlternateWeatherSources..ISDStationObservations_pivoted
	where usaf in (
		Select distinct wmo_id--, Locationid, awis_id,  awis_station_name, awis_latitude, awis_longitude
		From #ValidWMO
		where wmo_id in (
			Select wmo_id
			from #ValidWMO
			group by wmo_id
			having count(*) >1
			)
		and locationid in (
			Select Locationid
			From WT_Processing..Master_WT_PointStations
			where  UseForDistance = 1 --0 = do not use; 1 = use
			)
		)

--B-3. Figure out which locationid is active by looking through the federated tables
	--6 locations
	Select locationid, max(Temporalid) as LastDataDate
	from WT_Asia_Federated..vasia_point_daily
	where locationid in (
		Select Locationid
		From #ValidWMO
		where wmo_id in (
			Select wmo_id
			from #ValidWMO
			group by wmo_id
			having count(*) >1
			)
		and locationid in (
			Select Locationid
			From WT_Processing..Master_WT_PointStations
			where  UseForDistance = 1 --0 = do not use; 1 = use
			)
		)
		group by locationid

	--2 locations
	Select locationid, max(Temporalid) as LastDataDate
	from WT_NAM_Federated..vnam_point_daily
	where locationid in (
		Select Locationid
		From #ValidWMO
		where wmo_id in (
			Select wmo_id
			from #ValidWMO
			group by wmo_id
			having count(*) >1
			)
		and locationid in (
			Select Locationid
			From WT_Processing..Master_WT_PointStations
			where  UseForDistance = 1 --0 = do not use; 1 = use
			)
		)
		group by locationid

	--1 location, 1 location with no data
	Select locationid, max(Temporalid) as LastDataDate
	from WT_sAM_Federated..vsam_point_daily
	where locationid in (
		Select Locationid
		From #ValidWMO
		where wmo_id in (
			Select wmo_id
			from #ValidWMO
			group by wmo_id
			having count(*) >1
			)
		and locationid in (
			Select Locationid
			From WT_Processing..Master_WT_PointStations
			where  UseForDistance = 1 --0 = do not use; 1 = use
			)
		)
		group by locationid

--B-5. Insert invalid duplicates to invalid table; 5
	Insert into  #AWISInvalidWMO (Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2)
	Select Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2
	from WT_Processing..Master_WT_PointStations_Lookup
	where locationid in (21037559, 50929447, 60277239, 21036413, 21036657)

--B-6. Delete invalid duplicates from the valid table; 5
	Delete 
	From #ValidWMO
	where Locationid in (21037559, 50929447, 60277239, 21036413, 21036657)

--C. Create and insert into tables to hold valid AWIS stations
	Create table AlternateWeatherSources..AWISValidWMO (Locationid int, wmo_id int, awis_id varchar(50), awis_station_name varchar(50), awis_latitude real, awis_longitude real)
	--wmo_id2 unique
	Insert into AlternateWeatherSources..AWISValidWMO (Locationid, wmo_id, awis_id, awis_station_name, awis_latitude, awis_longitude)
	Select * from #ValidWMO


--D. Create and insert into tables to hold valid AWIS stations
	Create table AlternateWeatherSources..AWISInvalidWMO (Locationid int, awis_id varchar(50), awis_station_name varchar(50), awis_latitude real, awis_longitude real, wmo_id1 int, wmo_id2 int)
	Insert into  AlternateWeatherSources..AWISInvalidWMO (Locationid, awis_id, awis_station_name, awis_latitude, awis_longitude, wmo_id1, wmo_id2)
	Select * from #AWISInvalidWMO
---------------------------------------------------------
Truncate table #AWISNulls
Truncate table #AWISInvalidWMO
Truncate table #ValidWMO
Truncate table #TroubleWMO
Truncate table #AWIS999999