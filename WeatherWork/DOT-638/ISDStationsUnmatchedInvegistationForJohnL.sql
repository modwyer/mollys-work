/****** Script for SelectTopNRows command from SSMS  ******/
SELECT distinct locationid
  FROM [AlternateWeatherSources].[dbo].[AWISObservationCounts]
  --11760

Select AWIS_ID, WMOCOde, AWISName
from AlternateWeatherSources..AWIS_Wmocodes
where Locationid in (
	SELECT distinct locationid
	FROM [AlternateWeatherSources].[dbo].[AWISObservationCounts]
  )
  --10315 WMO station codes with observations

SELECT distinct usaf
from AlternateWeatherSources..ISDStationObservations_pivoted
where usaf not in (
Select WMOCOde
from AlternateWeatherSources..AWIS_Wmocodes
)
--8351 ISD stations match awis WMO codes

SELECT distinct usaf
from AlternateWeatherSources..ISDStationObservations_pivoted
where usaf  in (
	Select WMOCOde
	from AlternateWeatherSources..AWIS_Wmocodes
	where Locationid in (
		SELECT distinct locationid
		FROM [AlternateWeatherSources].[dbo].[AWISObservationCounts]
	  )
	)
--18674 isd not in awis
--7227 isd in awis


SELECT distinct usaf
from AlternateWeatherSources..ISDStationObservations_pivoted
where usaf in (
	Select distinct WMOCOde
	from AlternateWeatherSources..AWIS_Wmocodes
	where Locationid in (
		SELECT distinct locationid
		FROM [AlternateWeatherSources].[dbo].[AWISObservationCounts]
		where Dateyear >= 2008
	  )
	)
and observed_year >= 2008
--6892 ISD stations after 2008 match AWIS after 2008

--reverse of above
SELECT distinct locationid
from AlternateWeatherSources..AWISObservationCounts
where locationid in (
	Select distinct locationid
	from AlternateWeatherSources..AWIS_Wmocodes
	where wmocode in (
		SELECT distinct usaf
		FROM AlternateWeatherSources..ISDStationObservations_pivoted
		where observed_year >= 2008
	  )
	)
and Dateyear >= 2008
--6973 awis stations after 2008 have an ISD match after 2008
