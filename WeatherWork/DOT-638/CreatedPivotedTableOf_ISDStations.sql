Create table ISDStationObservations_pivoted (USAF int, WBAN int, Observed_Year int, Observed_Month varchar(3), ObservationsCount int)

Insert into ISDStationObservations_pivoted (USAF, WBAN, Observed_Year, Observed_Month, ObservationsCount)
SELECT USAF, WBAN, Year, Observed_Month, ObservationsCount
FROM 
   (SELECT USAF, WBAN, Year, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug,Sep, Oct, Nov, Dec
   FROM [AlternateWeatherSources].[dbo].[ISDStationObservations]) p
UNPIVOT
   (ObservationsCount FOR Observed_Month IN 
      (Jan, Feb, Mar, Apr, May, Jun, Jul, Aug,Sep, Oct, Nov, Dec)
)AS unpvt
order by usaf asc