/****** Get observations for ISD and AWIS and compare  ******/
--Get ISD observations between 2006 and 2015 for each station that has an AWIS match; 949116 rows; 8843 stations
	SELECT  [USAF]
		  ,[Observed_Year]
		  ,[Observed_Month]
		  ,[ObservationsCount]
	  FROM [AlternateWeatherSources].[dbo].[ISDStationObservations_pivoted]
	  where usaf in (
		Select ISDWMO
		From AlternateWeatherSources.. AWIS_ISD_WMOCrosswalk
		)
	and observed_year between 2006 and 2015

--Get AWIS observations between 2006 and 2015 for each station that has an ISD match; 662681 rows; 7658 stations; (4261 stations not included; 284682 rows)
	SELECT distinct Locationid
		  --, DateYear
		  --, DateMonth
		  --, ObservedCounts
	  FROM AlternateWeatherSources..AWISObservationCountsv2
	  where Locationid not in (
		Select locationid
		From AlternateWeatherSources.. AWIS_ISD_WMOCrosswalk
		)
	and DateYear between 2006 and 2015
	and locationid in (
		Select locationid
		from AlternateWeatherSources..AWISInvalidWMO
		)


	Create table #SuspectedNoDataStation (locationid int, awiswmo int, isdwmo int, awiscode varchar(50), awisname varchar(150), awislatitude real, awislongitude real)
	Insert into #SuspectedNoDataStation (locationid, awiswmo, isdwmo, awiscode, awisname, awislatitude, awislongitude)
		Select locationid, awiswmo, isdwmo, awiscode, awisname, Latitude, longitude
		From AlternateWeatherSources.. AWIS_ISD_WMOCrosswalk
		where locationid not in (
			SELECT distinct Locationid
			FROM AlternateWeatherSources..AWISObservationCounts
			where Locationid in (
				Select locationid
				From AlternateWeatherSources.. AWIS_ISD_WMOCrosswalk
				)
			and DateYear between 2006 and 2015
			)

	Select * 
	from AlternateWeatherSources..AWISObservationCounts
	where locationid in (
		Select * 
		from #SuspectedNoDataStation
		)

--compare new awis observation list to the valid locations
Select distinct locationid 
from AlternateWeatherSources..AWISObservationCountsv2
where locationid in (
	Select locationid
	from AlternateWeatherSources..AWIs_ISD_WMOCrosswalk
	)

Select locationid
from AlternateWeatherSources..AWISValidWMO
where locationid not in (
	Select distinct locationid 
	from AlternateWeatherSources..AWISObservationCountsv2
	)

Select distinct locationid, awiswmo, isdwmo, awiscode, awisname
from AlternateWeatherSources..AWIs_ISD_WMOCrosswalk 
where isdwmo in (
	Select usaf
	from AlternateWeatherSources..ISDStationObservations_pivoted
	)
and locationid in (
	Select distinct locationid 
	from AlternateWeatherSources..AWISObservationCountsv2
	where locationid in (
		Select locationid
		from AlternateWeatherSources..AWIs_ISD_WMOCrosswalk
		)
	)

--join example of above
Select i.usaf, i.observed_year,i.observed_month,i.observationsCount
		, a.locationid, a.dateyear, a.datemonth, a.observedcounts
		, c.locationid, c.awiswmo, c.isdwmo, c.awiscode, c.awisname, c.latitude, c.longitude
from AlternateWeatherSources..ISDStationObservations_pivoted i
inner join
AlternateWeatherSources..AWIs_ISD_WMOCrosswalk c
on i.usaf = c.isdwmo
inner join
AlternateWeatherSources..AWISObservationCountsv2 a
on c.locationid = a.locationid
where i.observed_year = a.dateyear
and i.observed_month = a.datemonth
order by a.locationid asc, a.dateyear asc, datepart(mm,cast(a.datemonth+ ' 1900' as datetime)) asc


--Do the observed stations not in the valid list show up in the invalid list?
Select *
from AlternateWeatherSources..AWISInvalidWMO
where locationid in (
	Select distinct locationid 
	from AlternateWeatherSources..AWISObservationCountsv2
	where locationid not in (
		Select locationid
		from AlternateWeatherSources..AWISValidWMO
		)
	)
