/****** Get observation counts for ISD and AWIS where the locationID, Year and Month match ******/
--1. Total comparison of observation counts per month per year for AWIS and ISD side by side
	Select i.usaf, i.observed_year,i.observed_month,i.observationsCount
			, a.locationid, a.dateyear, a.datemonth, a.observedcounts
			, c.locationid, c.awiswmo, c.isdwmo, c.awiscode, c.awisname, c.latitude, c.longitude
	from AlternateWeatherSources..ISDStationObservations_pivoted i
	inner join
	AlternateWeatherSources..AWIs_ISD_WMOCrosswalk c
	on i.usaf = c.isdwmo
	inner join
	AlternateWeatherSources..AWISObservationCountsv2 a
	on c.locationid = a.locationid
	where i.observed_year = a.dateyear
	and i.observed_month = a.datemonth
	order by a.locationid asc, a.dateyear asc, datepart(mm,cast(a.datemonth+ ' 1900' as datetime)) asc

--1-b. Find total distinct stations
	Select distinct i.usaf
			,a.locationid
			,c.locationid
	from AlternateWeatherSources..ISDStationObservations_pivoted i
	inner join
	AlternateWeatherSources..AWIs_ISD_WMOCrosswalk c
	on i.usaf = c.isdwmo
	inner join
	AlternateWeatherSources..AWISObservationCountsv2 a
	on c.locationid = a.locationid
	where i.observed_year = a.dateyear
	and i.observed_month = a.datemonth

--2. Find AWIS months with count greater than ISD count
	Select i.usaf, i.observed_year,i.observed_month,i.observationsCount
			, a.locationid, a.dateyear, a.datemonth, a.observedcounts
			, c.locationid, c.awiswmo, c.isdwmo, c.awiscode, c.awisname, c.latitude, c.longitude
	from AlternateWeatherSources..ISDStationObservations_pivoted i
	inner join
	AlternateWeatherSources..AWIs_ISD_WMOCrosswalk c
	on i.usaf = c.isdwmo
	inner join
	AlternateWeatherSources..AWISObservationCountsv2 a
	on c.locationid = a.locationid
	where i.observed_year = a.dateyear
	and i.observed_month = a.datemonth
	and a.ObservedCounts > i.ObservationsCount
	order by a.locationid asc, a.dateyear asc, datepart(mm,cast(a.datemonth+ ' 1900' as datetime)) asc

--2-b. Find total distinct stations
	Select distinct i.usaf
			,a.locationid
			,c.locationid
	from AlternateWeatherSources..ISDStationObservations_pivoted i
	inner join
	AlternateWeatherSources..AWIs_ISD_WMOCrosswalk c
	on i.usaf = c.isdwmo
	inner join
	AlternateWeatherSources..AWISObservationCountsv2 a
	on c.locationid = a.locationid
	where i.observed_year = a.dateyear
	and i.observed_month = a.datemonth
	and a.ObservedCounts > i.ObservationsCount
	
--3. Find AWIS months with count greater than ISD 0 count
	Select i.usaf, i.observed_year,i.observed_month,i.observationsCount
			, a.locationid, a.dateyear, a.datemonth, a.observedcounts
			, c.locationid, c.awiswmo, c.isdwmo, c.awiscode, c.awisname, c.latitude, c.longitude
	from AlternateWeatherSources..ISDStationObservations_pivoted i
	inner join
	AlternateWeatherSources..AWIs_ISD_WMOCrosswalk c
	on i.usaf = c.isdwmo
	inner join
	AlternateWeatherSources..AWISObservationCountsv2 a
	on c.locationid = a.locationid
	where i.observed_year = a.dateyear
	and i.observed_month = a.datemonth
	and a.ObservedCounts > i.ObservationsCount
	and i.ObservationsCount = 0
	order by a.locationid asc, a.dateyear asc, datepart(mm,cast(a.datemonth+ ' 1900' as datetime)) asc

--3-b. Find distinct stations
	Select distinct i.usaf
			,a.locationid
			,c.locationid
	from AlternateWeatherSources..ISDStationObservations_pivoted i
	inner join
	AlternateWeatherSources..AWIs_ISD_WMOCrosswalk c
	on i.usaf = c.isdwmo
	inner join
	AlternateWeatherSources..AWISObservationCountsv2 a
	on c.locationid = a.locationid
	where i.observed_year = a.dateyear
	and i.observed_month = a.datemonth
	and a.ObservedCounts > i.ObservationsCount
	and i.ObservationsCount = 0