/***************** List out all the ISD codes that have a matching code in the AWIS list **************/
Create table AlternateWeatherSources..AWIS_ISD_Stations (wmocode int, awis_id varchar(10), awisname varchar(200))

--1-A. Find AWIS stations with a matching ISD WMO ID. 8846
		--match IDs with length 6; 7608
		Create table #Length6 (usaf int, wmo_id int, locationid int, awis_id varchar(10), awis_station_name varchar(150), awis_latitude real, awis_longitude real)
		insert into #Length6 (usaf, wmo_id, locationid, awis_id, awis_station_name, awis_latitude, awis_longitude)
		Select distinct usaf, wmo_id, locationid, awis_id, awis_station_name, awis_latitude, awis_longitude from AlternateWeatherSources..ISDStationObservations_pivoted i 
		inner join
		AlternateWeatherSources..AWISValidWMO a
		on i.usaf = a.wmo_id
		where len(a.wmo_id) = 6
		and len(i.usaf) = 6

		--match IDs with length 5; 1216
		Create table #Length5 (usaf int, wmo_id int, locationid int, awis_id varchar(10), awis_station_name varchar(150), awis_latitude real, awis_longitude real)
		insert into #Length5 (usaf, wmo_id, locationid, awis_id, awis_station_name, awis_latitude, awis_longitude)
		Select distinct usaf, wmo_id, locationid, awis_id, awis_station_name, awis_latitude, awis_longitude from AlternateWeatherSources..ISDStationObservations_pivoted i  
		inner join
		AlternateWeatherSources..AWISValidWMO a
		on i.usaf = a.wmo_id
		where len(a.wmo_id) = 5
		and len(i.usaf) = 5

		--match IDs with ISD length 5 plus 0 and AWIS length 6; 22
		Create table #RemainingLength (usaf int, wmo_id int, locationid int, awis_id varchar(10), awis_station_name varchar(150), awis_latitude real, awis_longitude real)
		insert into #RemainingLength (usaf, wmo_id, locationid, awis_id, awis_station_name, awis_latitude, awis_longitude)
		Select distinct usaf, wmo_id, locationid, awis_id, awis_station_name, awis_latitude, awis_longitude from AlternateWeatherSources..ISDStationObservations_pivoted i  --match IDs with ISD length 5 plus 0 and AWIS length 6
		inner join
		AlternateWeatherSources..AWISValidWMO a
		on cast(i.usaf as varchar(10)) + '0' = cast(a.wmo_id as varchar(10))
		where len(usaf) = 5
		and len(wmo_id) =6
		and wmo_id not in (
			Select wmo_id from #Length6
			)	
		and wmo_id not in (
			Select wmo_id from #Length5
			)

		--Combine all 3 lists above; 8846
		Create table #EntireList (usaf int, wmo_id int, locationid int, awis_id varchar(10), awis_station_name varchar(150), awis_latitude real, awis_longitude real)
		insert into #EntireList (usaf, wmo_id, locationid, awis_id, awis_station_name, awis_latitude, awis_longitude)
		Select * from #Length6
		union all
		Select * from #Length5
		union all
		Select * from #RemainingLength

		--find duplicates; 3
		Select * 
		from #Entirelist
		where usaf in (
		Select usaf
		From #EntireList
		group by usaf
		having count(*) >1)
		and len(usaf) <> len(wmo_id)
		order by usaf

--1-B. Create a crosswalk table between AWIS and ISD wmo codes
	truncate table AlternateWeatherSources..AWIS_ISD_WMOCrosswalk 
	Create table AlternateWeatherSources..AWIS_ISD_WMOCrosswalk 
			(Locationid int, AWISWMO int, ISDWMO int, AWISCode varchar(10), AWISName varchar(150), Latitude real, Longitude real)
	Insert into AlternateWeatherSources..AWIS_ISD_WMOCrosswalk (Locationid, AWISWMO, ISDWMO, AWISCode, AWISName, Latitude, Longitude)
	Select locationid, wmo_id, usaf, awis_id, awis_station_name, awis_latitude, awis_longitude from #Entirelist
	
	--Remove the duplicates
	Delete
	from AlternateWeatherSources..AWIS_ISD_WMOCrosswalk
	where ISDWMO in (
	Select ISDWMO
	From AlternateWeatherSources..AWIS_ISD_WMOCrosswalk
	group by ISdWMO
	having count(*) >1)
	and len(ISDWMO) <> len(AWISWMO)
	

/**********  Compare AWIS stations to USAF stations using WMO codes *************/
--0. ISD stations observation counts per year per month 
	SELECT  [USAF]
		  ,[Observed_Year]
		  , Observed_Month
		  ,[ObservationsCount] as Observations
	  FROM [AlternateWeatherSources].[dbo].[ISDStationObservations_pivoted]
	  --group by USAF, [Observed_Year], Observed_Month
	  order by USAF, [Observed_Year] asc, Observed_Month asc

--1-B. Total number of observations for all ISD stations across all available time
	Declare @SumofObservations bigint
	SELECT  @SumofObservations = sum(cast([ObservationsCount] as bigint))
	  FROM [AlternateWeatherSources].[dbo].[ISDStationObservations_pivoted]

	Select @SumofObservations
	--2,742,971,648 observations

--1-C. Total number of observations for all AWIS matched ISD stations in specified time
	Declare @SumofObservations bigint
	SELECT  @SumofObservations = sum(cast([ObservationsCount] as bigint))
	  FROM [AlternateWeatherSources].[dbo].[ISDStationObservations_pivoted]
	  where Observed_year between 2006 and 2015
	  and USAF in (
		Select wmo_id
		From [AlternateWeatherSources]..AWISValidWMO
		)

	Select @SumofObservations
	--614,304,205 observations to compare with AWIS



/*********** Create an AWIS list that includes both WMO_ID1 and _ID2 codes for comparison against ISD.  **************/
--Combine WMO_ID1 and WMO_ID2 into 1 column with a comparable AWIS_ID and station name 
--drop table #WMOCodes
Create table #AWIS_WMOCodes (WMOCode int, AWIS_ID varchar(10), AWISName varchar(200), locationid int)

--1. add WMO_ID1 codes
	Insert into #AWIS_WMOCodes (WMOCode, AWIS_ID, AWISName, locationid)
	Select distinct WMO_ID1, wmo_id2, awis_id, awis_station_name, Locationid
	from [WT_Processing].[dbo].[Master_WT_PointStations_Lookup]
	where awis_id is not null
	and wmo_id1 <> 999999
	order by wmo_id1

--2. add WMO_ID2 codes
	Insert into #AWIS_WMOCodes (WMOCode, AWIS_ID, AWISName)
	Select WMO_ID2, awis_id, awis_station_name
	from [WT_Processing].[dbo].[Master_WT_PointStations_Lookup]
	where awis_id is not null
	and wmo_id2 <> 999999
	and awis_id not in (Select AWIS_ID from #AWIS_WMOCodes)

	Select * from #AWIS_WMOCodes
	--12144 awis codes with WMO code

Use AlternateWeatherSources

--3. Create table in AlternateWeatherSources DB and insert values from #AWIS_WMOCodes
	Create table AWIS_WMOCodes (WMOCode int, AWIS_ID varchar(10), AWISName varchar(200))
	Insert into AWIS_WMOCodes (WMOCode, AWIS_ID, AWISName)
	Select * from #AWIS_WMOCodes








