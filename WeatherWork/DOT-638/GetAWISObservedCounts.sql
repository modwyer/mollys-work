/****** Pull Observation counts from AWIS per stations per year per month  ******/ --952187
SELECT [LocationID]
      ,TemporalID/1000000 as DateYear
	  ,(TemporalID-(TemporalID/1000000)*1000000)/10000 as DateMonth
	  ,count([AttributeID]) as ObservedCounts
  FROM [WT_Processing].[dbo].[PointData]
  where activeflag = 1
  group by locationid, TemporalID/1000000, (TemporalID-(TemporalID/1000000)*1000000)/10000
  order by locationid, TemporalID/1000000, (TemporalID-(TemporalID/1000000)*1000000)/10000

--Pull data for specific time frame
SELECT distinct [LocationID]
   --   ,TemporalID/1000000 as DateYear
	  --,(TemporalID-(TemporalID/1000000)*1000000)/10000 as DateMonth
	  --,count([AttributeID]) as ObservedCounts
  FROM [WT_Processing].[dbo].[PointData]
  where activeflag = 1
  and TemporalID/1000000 between 2006 and 2015
  group by locationid, TemporalID/1000000, (TemporalID-(TemporalID/1000000)*1000000)/10000
  order by locationid, TemporalID/1000000, (TemporalID-(TemporalID/1000000)*1000000)/10000



  --Check specific locations for data 947363 count for time limit; 947363 count for time limit and flag =1; 9368 count for flag = 0
  SELECT [LocationID]
      ,TemporalID/1000000 as DateYear
	  ,(TemporalID-(TemporalID/1000000)*1000000)/10000 as DateMonth
	  ,count([AttributeID]) as ObservedCounts
  FROM [WT_Processing].[dbo].[PointData]
  where TemporalID/1000000 between 2006 and 2015
  and activeflag = 1
  group by locationid, TemporalID/1000000, (TemporalID-(TemporalID/1000000)*1000000)/10000
  order by locationid, TemporalID/1000000, (TemporalID-(TemporalID/1000000)*1000000)/10000