Select a.USAF, cast(a.TemporalDate as date) as TemporalDate, max(a.maxtemp) as MaxTemp, min(a.Mintemp) as MinTemp
	, round(avg(a.speed), 2) as MeanWind, max(a.speed) as MaxWind, max(w.MornWind) as MornWind, round(min(a.RH), 2) as MinRH, round(max(a.RH), 2) as MaxRH
into AlternateWeatherSources..ISD_Daily_1012_1025
from AlternateWeatherSources..ISD_Hourly_1012_1025 a
right outer join 
#MornWind w
on a.usaf = w.usaf
and cast(a.TemporalDate as date) = w.temporaldate
group by a.usaf, cast(a.TemporalDate as date)
order by a.usaf, cast(a.TemporalDate as date)


Select usaf, cast(TemporalDate as date) as TemporalDate
	, max(speed) as MornWind
--into #MornWind
from AlternateWeatherSOurces..ISD_Hourly_1012_1025 a
where cast(TemporalDate as time) between '06:00:00' and '12:00:00'
group by usaf, cast(temporaldate as date)
order by usaf asc, cast(temporaldate as date)

Select *
from AlternateWeatherSOurces..ISD_Hourly_1012_1025
where pcp06 is not null
order by pcp06 desc

Select *
from AlternateWeatherSOurces..ISD_Hourly_1012_1025
order by temp desc

select *
from wt_processing..master_wt_pointstations_lookup
where wmo_id2 = 828790