
--Get hourly wind and temp and min/max for wind and temp for each ISD station; include count of stations reporting each attribute
SELECT top 1000 left(YRMODAHRMN, 10)
      ,avg(Speed) as DailyWindSpeed
	  ,min(Speed) as MinWind
	  ,max(SPeed) as MaxWind
      ,avg(Temp) as DailyTemp
	  ,min(Temp) as MinTemp
	  ,max(Temp) as MaxTemp
	  ,count(Speed) as WindCountObservations
	  ,count(Temp) as TempCountObservations
	  ,count(USAF) as StationsReporting
  FROM AlternateWeatherSources..ISD_Hourly_Observations_new
  group by left(YRMODAHRMN, 10)
  order by left(YRMODAHRMN, 10) asc

--Get hourly precip and count of reporting stations

--attempt to unpivot but not working as yet
  SELECT usaf, YRMODAHRMN, PrecipHr, PrecipValue
  FROM (Select usaf, YRMODAHRMN, spd as Speed, Temp as Temperature, Dewp as Dewpoint, STP as StationPressure, pcp01 as '1', pcp06 as '6', pcpxx as '12', pcp24 as '24'
		From AlternateWeatherSources..ISD_Hourly_Observations
		) precip
  unpivot
	(PrecipValue
		For PrecipHr in ([1],[6],[12],[24])
	) AS A

  Unpivot
	(Precip
		For Value in ([pcp01],[pcp06],[pcp24],[pcpxx])
	) as p

  Select usaf, YRMODAHRMN
		,Precip case right(YRMODAHRMN, 4)
					when '0600' then pcp24
					when '1200' then pcp06
					when '1800' then pcp12

		 pcp01, pcp06, pcp24, pcpxx as pcp12
  from AlternateWeatherSources..ISD_Hourly_Observations
  where usaf like '010070'
  and YRMODAHRMN like '20150824%'