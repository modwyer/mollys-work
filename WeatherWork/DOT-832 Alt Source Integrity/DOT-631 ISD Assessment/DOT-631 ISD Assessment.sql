/** DOT-631 ISD Assessment **/

-- 1)Average number of hourly/sub-daily reports per attribute
--Get only observations btwn 10/12-10/25
Select *
into AlternateWeatherSOurces..ISD_Hourly_1012_1025
from AlternateWeatherSources..ISD_Hourly_Observations_new
where left(yrmodahrmn, 8) between 20151012 and 20151025


--Pivot table so all attributes in 1 column
SELECT   USAF, yrmodahrmn, TemporalDate, Attribute, Value 
into AlternateWeatherSOurces..ISD_Hourly_Pivot
FROM AlternateWeatherSOurces..ISD_Hourly_1012_1025
UNPIVOT
(
       Value
       FOR Attribute IN (Speed, Temp, MaxTemp, MinTemp, pcp06, pcp12, pcp24, RH)
) AS P
order by usaf, yrmodahrmn, TemporalDate


--Get count per station per attribute
SElect USAF, cast(TemporalDate as date) as TemporalDate, datepart(hour,TemporalDate) as DateHour, Attribute
	,case when Attribute = 'Temp' then count(value) else
		(case when Attribute = 'MaxTemp' then count(value) else
			(case when Attribute = 'MinTemp' then count(value) else
				(case when Attribute = 'RH' then count(value) else
					(case when Attribute = 'PCP01' then count(value) else
						(case when Attribute = 'PCP06' then count(value) else
							(case when Attribute = 'PCP12' then count(value) else
								(case when attribute = 'PCP24' then count(value) else
									(case when attribute = 'Speed' then count(value) end)
								end)
							end)
						end)
					end)
				end)
			end)
		end)
	end as HourlyCounts
--into Alternateweathersources..ISDHourlyCounts
FROM AlternateWeatherSources..ISD_Hourly_Pivot
where temporaldate is not null
group by usaf, cast(TemporalDate as date), datepart(hour,TemporalDate), Attribute
order by USAF, cast(TemporalDate as date), datepart(hour,TemporalDate) asc, Attribute

--Get daily avg for each station
Select USAF, TemporalDate, Attribute, avg(HourlyCounts) as AvgHourlyCountsPerDay
from Alternateweathersources..ISDHourlyCounts
Group by USAF, TEmporalDate, Attribute
order by USAF, TEmporalDate, ATtribute

--2)Min/Max temperature is observed, or has to be derived from hourly data
Select USAF, TemporalDate, Attribute, Value
from AlternateWeatherSOurces..ISD_Hourly_Pivot
where Attribute in ('MaxTemp', 'MinTemp')
and temporaldate is not null
Group by USAF, TEmporalDate, Attribute, Value
order by USAF, TEmporalDate, ATtribute


--3)Most common rain gauge measurement type: is it hourly, 6-hour, 12-hour, or even once a day? ie. For each station find out which type occurs the most
Select USAF, count(attribute) as OccurrenceOfAttribute, Attribute
--into #ISDAttributeCount
from Alternateweathersources..ISDHourlyCounts
where Attribute in ('pcp01', 'pcp06', 'pcp12', 'pcp24')
and temporaldate is not null
group by usaf, attribute
order by usaf, count(Attribute) desc, attribute

--Final table
Select i1.USAF
	, (case when i2.attribute = 'PCP06' then '6-Hour' else
		(case when i2.attribute = 'PCP12' then '12-Hour' else
			(case when i2.attribute = 'PCP24' then '24-Hour' end) end) end) as MeasurementType
	, max(i1.OccurrenceCount) as AttributeCount
from (select usaf, max(OccurrenceOfAttribute) as OccurrenceCount
		from #ISDAttributeCount
		group by usaf) i1
inner join #ISDAttributeCount i2
on i1.usaf = i2.usaf
and i1.OccurrenceCount = i2.OccurrenceOfAttribute
group by i1.USAf, i2.attribute
order by i1.usaf, i2.attribute

--4a)Wind observation counts per day per station
--Need to generate mean, morn and max wind
--morn wind and observation count
Select usaf, cast(TemporalDate as date) as TemporalDate
	, max(speed) as MornWind, count(speed) as MornObservations
into #MornWind
from AlternateWeatherSOurces..ISD_Hourly_1012_1025 a
where temporaldate is not null
and cast(TemporalDate as time) between '06:00:00' and '12:00:00'
group by usaf, cast(temporaldate as date)
order by usaf asc, cast(temporaldate as date)
--maxwind, meanwind and observation count
Select usaf, cast(TemporalDate as date) as TemporalDate
	, max(speed) as MaxWind, avg(speed) as MeanWind, count(speed) as MeanObservations
into #Wind
from AlternateWeatherSOurces..ISD_Hourly_1012_1025 a
where temporaldate is not null
group by usaf, cast(temporaldate as date)
order by usaf asc, cast(temporaldate as date)
--all together now
Select w.usaf, w.temporaldate, w.MaxWind, w.MeanWind, w.MeanObservations, m.MornWind, m.MornObservations
into AlternateWeatherSOurces..ISDWindAttributes
from #MornWind m 
right outer join 
#Wind w
on m.usaf = w.usaf
and m.temporaldate = w.temporaldate


select * from
AlternateWeatherSOurces..ISDWindAttributes

--5)Relative humidity counts
	--RH is calculated using Weather_Modeling.dbo.
	Select usaf, Temporaldate, Temp, Dewpoint, mintemp, maxtemp, Weather_Modeling.dbo.CalculateRelativeHumidityFromDewpoint(dewpoint, temp) as RH
	--into #ISD_RHTemp
	from AlternateWeatherSOurces..ISD_Hourly_1012_1025
	where temporaldate is not null
	order by usaf, temporaldate


	select *
	from #ISD_RHTemp
	order by usaf, temporaldate

	--day values
	Select usaf, cast(TemporalDate as date) as TemporalDate, round(max(rh), 2) as MaxRH, round(min(rh), 2) as MinRH
	from #ISD_RHTemp
	group by usaf, cast(TemporalDate as date)
	order by usaf, cast(TemporalDate as date)
	

--6)Is solar radiation observed? 
	--No