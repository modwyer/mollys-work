/** DOT-742 Define Time Zone Offset from Universal time for MADIS/ISD stations **/

--Create geometry point from lat/long ISD codes
Select USAF_Codes as USAF, WBAN_Codes as WBAN, Station_Name, Country, Latitude, Longitude, Elevation, STartDate, EndDate
	, geometry::STPointFromText('POINT(' + CAST([Longitude] AS VARCHAR(20)) + ' ' + 
                    CAST([Latitude] AS VARCHAR(20)) + ')', 4326) as SpatialMeasure
into Alternateweathersources..ISDCodesToComparev2
from Alternateweathersources..ISDCodes_All
where USAF_Codes in (
	Select distinct usaf
	from Alternateweathersources..ISD_Hourly_1012_1025
	)

--Get timezone offset for each station whose point is within the respective time zone
Select i.USAF, i.WBAN, i.Station_Name, i.COuntry, i.Latitude, i.Longitude, i.Elevation, i.StartDate, i.EndDate, i.Spatialmeasure, g.zone as UTCOffset
--into Alternateweathersources..ISDCodesWithOffset
from Alternateweathersources..ISDCodesToCompare i, Alternateweathersources..GlobalTimeZones g
where i.spatialmeasure.STWithin(g.geom) = 1

--Adjust ISD time to offset 
Alter table Alternateweathersources..ISD_Hourly_1012_1025
add TemporalDate smalldatetime

Update Alternateweathersources..ISD_Hourly_1012_1025
Set TEmporalDate = dateadd(hour, o.UTCOffset, alternateweathersources.dbo.TemporalIDToDateTime(i.yrmodahrmn, 1))
from Alternateweathersources..ISD_Hourly_1012_1025 i, Alternateweathersources..ISDCodesWithOffset o
where i.usaf = o.usaf

select temporaldate, yrmodahrmn
from Alternateweathersources..ISD_Hourly_1012_1025


Select i.USAF, i.Yrmodahrmn as OriginalDate, dateadd(hour, o.UTCOffset, alternateweathersources.dbo.TemporalIDToDateTime(i.yrmodahrmn, 1)) as TemporalId
	--,wt_processing.dbo.GetTemporalIdFromDate(cast(dateadd(hour, o.UTCOffset, alternateweathersources.dbo.TemporalIDToDateTime(i.yrmodahrmn, 1)) as datetime), 2, 0)
	, Speed, Temp, DewPoint,
--into Alternateweathersources..ISD_Hourly_
from Alternateweathersources..ISD_Hourly_1012_1025 i, Alternateweathersources..ISDCodesWithOffset o
where i.usaf = o.usaf

