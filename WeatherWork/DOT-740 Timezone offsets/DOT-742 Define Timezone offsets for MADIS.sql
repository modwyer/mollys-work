/** DOT-742 Define Time Zone Offset from Universal time for MADIS stations **/

--Create geometry point from lat/long ISD codes
Alter table Alternateweathersources..MADIS_October_12_25_new
add SpatialMeasure geometry

Update Alternateweathersources..MADIS_October_12_25_new
Set SpatialMeasure = geometry::STPointFromText('POINT(' + CAST([Longitude] AS VARCHAR(20)) + ' ' + 
                    CAST([Latitude] AS VARCHAR(20)) + ')', 4326)


--Get timezone offset for each station whose point is within the respective time zone
Alter table Alternateweathersources..MADIS_October_12_25_new
add UTCOffset int

Select station, latitude, longitude, awmdatabase.dbo.UnionAggregate(geometry::STPointFromText('POINT(' + CAST([Longitude] AS VARCHAR(20)) + ' ' + 
                    CAST([Latitude] AS VARCHAR(20)) + ')', 4326)) as SpatialMeasure
into #MADISToCompare
from Alternateweathersources..MADIS_October_12_25_new
group by station, latitude, longitude

Alter table #MADIsToCompare
add UTCOffset int

Update #MadisToCompare
Set UTCOffset = g.zone
from #MadisToCompare i, Alternateweathersources..GlobalTimeZones g
where i.spatialmeasure.STWithin(g.geom) = 1

Update Alternateweathersources..MADIS_October_12_25_new
Set UTCOffset = g.UTCOffset
from Alternateweathersources..MADIS_October_12_25_new i, #MadisToCompare g
where i.station = g.station
and i.latitude = g.latitude
and i.longitude = g.longitude

--Adjust MADIS time to offset 
Alter table Alternateweathersources..MADIS_October_12_25_new
add TemporalDate smalldatetime

select * from Alternateweathersources..MADIS_October_12_25_new

--Set temporalID to include hour
Select distinct station, theDate, [hour]
	, case when len([hour]) = 1 then cast(cast(theDate as varchar) + '0' + cast([hour] as varchar) as bigint)
		else cast(cast(theDate as varchar) + cast([hour] as varchar) as bigint) end as TemporalID
into Alternateweathersources..MADIS_TemporalIds
from Alternateweathersources..MADIS_October_12_25_new


Alter table Alternateweathersources..MADIS_TemporalIds
add TemporalIdDateTime smalldatetime

Select * from Alternateweathersources..MADIS_TemporalIds

alter table Alternateweathersources..MADIS_TemporalIds
alter column TemporalIDDateTime datetime

--convert temporalid to datetime format
Update Alternateweathersources..MADIS_TemporalIds
Set TemporalIdDateTime = alternateweathersources.dbo.TemporalIDToSmallDateTime(TemporalId, 2)

--Update MADIS temporalid with datetime format
Update Alternateweathersources..MADIS_October_12_25_new
Set TemporalDate = t.TemporalIdDateTime
from Alternateweathersources..MADIS_October_12_25_new m, Alternateweathersources..MADIS_TemporalIds t
where m.station = t.station
and m.theDate = t.theDate
and m.[hour] = t.[hour]

select * from Alternateweathersources..MADIS_October_12_25_new

--Adjust MADIS time to offset
Alter table Alternateweathersources..MADIS_October_12_25_new
add TemporalOffset smalldatetime

Update Alternateweathersources..MADIS_October_12_25_new
Set TemporalOffset = dateadd(hour, UTCOffset, TemporalDate)
from Alternateweathersources..MADIS_October_12_25_new 



