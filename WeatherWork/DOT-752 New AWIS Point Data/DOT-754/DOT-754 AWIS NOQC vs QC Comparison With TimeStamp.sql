/** Clean up the new AWIs data **/
--Unpivot the data so attributes are in single column
drop table #awisunpivot 
Select * from WT_Processing..AWISNewFormatDataWithTimeStamp

Select AWIS_timestamp, fileversion, Locationid, AWIS_ID, TemporaliD, Attribute, Value
 into #AWISUnpivot
 From WT_Processing..AWISNewFormatDataWithTimeStamp
 Unpivot
	(
		Value
		For Attribute in (MinT, MaxT, PRCP, Solar, MaxWind, MornWind, MeanWind, MinRH, MaxRH)
	) as P

Select * from #awisunpivot

--update Nulls to the correct format
Update #awisunpivot
Set Value = Null
where Value = 'Null'

--change datatype to float
alter table #awisunpivot
alter column Value float

--pivot out filtered with fileversion - NoQC_NoInt
Select awis_timestamp, fileversion, Locationid, AWIS_ID, TemporaliD, MaxT, MinT, Precip, Solar, MaxWind, MornWind, MeanWind, MaxRH, MinRH
into #awispivotNoQC_noInt
from #awisunpivot
pivot
(
	sum(value)
	for Attribute in (MaxT, MinT, Precip, Solar, MaxWind, MornWind, MeanWind, MaxRH, MinRH)
) as p
where fileversion = 'noqc_noint'

--pivot out filtered with fileversion - QC_NoInt
Select awis_timestamp, fileversion, Locationid, AWIS_ID, TemporaliD, MaxT, MinT, Precip, Solar, MaxWind, MornWind, MeanWind, MaxRH, MinRH
into #awispivotQC_NoInt
from #awisunpivot
pivot
(
	sum(value)
	for Attribute in (MaxT, MinT, Precip, Solar, MaxWind, MornWind, MeanWind, MaxRH, MinRH)
) as p
where fileversion = 'qc_noint'

--drop table #awispivot
Select * from #awispivotnoqc_noint order by locationid, temporalid


--Count of attributes per station interpolated
--drop table #InterpolatedAttributesCount
Select awis_timestamp, Locationid, AWIS_ID, TemporaliD, Attribute, noqc_noint, qc_noint, qc_int
into #FileVersionCompare
from #awisunpivot
pivot
(
	sum(value)
	for fileversion in (noqc_noint, qc_noint, qc_int)
) as p

select * from  #FileVersionCompare

/*** The following are the scripts used to compile the comparisons of various types ***/
---------------------------------------------------

--Count of attributes per station where NoQC = QC
select distinct locationid, attribute, 'NOQC = QC' as ComparisonType, count(*) as RecordCount
into #NoQC_QC_MatchAttributesCount
from #FileVersionCompare
where noqc_noint = qc_noint
group by locationid, attribute

select * from #NoQC_QC_MatchAttributesCount
order by recordcount desc, locationid, attribute

--Count of attributes per station where NoQC and QC are not null
select distinct locationid, attribute, 'NOQC<>Null; QC<>Null' as ComparisonType, count(*) as RecordCount
into #NoQC_QC_NotNullAttributesCount
from #FileVersionCompare
where noqc_noint <> null
and qc_noint <> null
group by locationid, attribute

select * from #NoQC_QC_NotNullAttributesCount
order by recordcount desc, locationid, attribute

--Count of attributes per station where NoQC = null and QC <> null
select distinct locationid, attribute, 'NOQC=Null; QC<>Null' as ComparisonType, count(*) as RecordCount
into #NoQC_EqualNullAttributesCount
from #FileVersionCompare
where noqc_noint = null
and qc_noint <> null
group by locationid, attribute

select * from #NoQC_EqualNullAttributesCount
order by recordcount desc, locationid, attribute

--Count of attributes per station where NoQC <> null and QC = null
select distinct locationid, attribute, 'NOQC<>Null; QC=Null' as ComparisonType, count(*) as RecordCount
into #QC_EqualNullAttributesCount
from #FileVersionCompare
where noqc_noint <> null
and qc_noint = null
group by locationid, attribute

select * from #QC_EqualNullAttributesCount
order by recordcount desc, locationid, attribute

--Count of attributes per station where NoQC <> QC
select distinct locationid, attribute, 'NOQC<>QC' as ComparisonType, count(*) as RecordCount
into #NoQC_NoEqualQCAttributesCount
from #FileVersionCompare
where noqc_noint <> qc_noint
group by locationid, attribute

select * from #NoQC_NoEqualQCAttributesCount
order by recordcount desc, locationid, attribute

--drop table WT_Processing..AWISNOQC_vs_QC_Comparison
--Put it all together into a table in WT_Processing
Select * into WT_Processing..AWISNOQC_vs_QC_Comparison
from (
select * from #NoQC_QC_MatchAttributesCount
union all
select * from #NoQC_QC_NotNullAttributesCount
union all
select * from #NoQC_EqualNullAttributesCount
union all
select * from #QC_EqualNullAttributesCount
union all
select * from #NoQC_NoEqualQCAttributesCount) as P
--order by locationid, attribute, recordcount desc

Select distinct comparisontype from WT_Processing..AWISNOQC_vs_QC_Comparison