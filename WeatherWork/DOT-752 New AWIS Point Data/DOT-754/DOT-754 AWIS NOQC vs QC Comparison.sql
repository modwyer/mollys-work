--Unpivot the data so attributes are in single column
 Select Locationid, AWIS_ID, TemporaliD, FileVersion, Attribute, Value
 into #AWISUnpivot
 From WT_Processing..AWISNewFormatData
 Unpivot
	(
		Value
		For Attribute in (MaxT, MinT, PRCP, Solar, MaxWind, MornWind, MeanWind, MaxRH, MinRH)
	) as P

--Pivot the data based on file version
Select Locationid, AWIS_ID, TemporaliD, Attribute, QC_Noint, NoQC_Noint, QC_int
into #AWISPivot
from #AWISUnpivot
Pivot
	(
		max(Value)
		for FileVersion in (QC_Noint, NoQC_Noint, QC_int)
	) as P
order by locationid, awis_id, temporalid, attribute

--Count of attributes per station interpolated
drop table #InterpolatedAttributesCount
select distinct locationid, attribute, count(*) as RecordCount
into #InterpolatedAttributesCountPerStation
from #AWISpivot
where qc_noint is null
and noqc_noint is null
group by locationid, attribute

select * from #InterpolatedAttributesCountPerStation
order by recordcount desc, locationid, attribute

/*** The following are the scripts used to compile the comparisons of various types ***/
---------------------------------------------------

--Count of attributes per station where NoQC = QC
select distinct locationid, attribute, 'NOQC = QC' as ComparisonType, count(*) as RecordCount
into #NoQC_QC_MatchAttributesCount
from #AWISpivot
where noqc_noint = qc_noint
group by locationid, attribute

select * from #NoQC_QC_MatchAttributesCount
order by recordcount desc, locationid, attribute

--Count of attributes per station where NoQC and QC are not null
select distinct locationid, attribute, 'NOQC<>Null; QC<>Null' as ComparisonType, count(*) as RecordCount
into #NoQC_QC_NotNullAttributesCount
from #AWISpivot
where noqc_noint <> null
and qc_noint <> null
group by locationid, attribute

select * from #NoQC_QC_NotNullAttributesCount
order by recordcount desc, locationid, attribute

--Count of attributes per station where NoQC = null and QC <> null
select distinct locationid, attribute, 'NOQC=Null; QC<>Null' as ComparisonType, count(*) as RecordCount
into #NoQC_EqualNullAttributesCount
from #AWISpivot
where noqc_noint = null
and qc_noint <> null
group by locationid, attribute

select * from #NoQC_EqualNullAttributesCount
order by recordcount desc, locationid, attribute

--Count of attributes per station where NoQC <> null and QC = null
select distinct locationid, attribute, 'NOQC<>Null; QC=Null' as ComparisonType, count(*) as RecordCount
into #QC_EqualNullAttributesCount
from #AWISpivot
where noqc_noint <> null
and qc_noint = null
group by locationid, attribute

select * from #QC_EqualNullAttributesCount
order by recordcount desc, locationid, attribute

--Count of attributes per station where NoQC <> QC
select distinct locationid, attribute, 'NOQC<>QC' as ComparisonType, count(*) as RecordCount
into #NoQC_NoEqualQCAttributesCount
from #AWISpivot
where noqc_noint <> qc_noint
group by locationid, attribute

select * from #NoQC_NoEqualQCAttributesCount
order by recordcount desc, locationid, attribute

--Put it all together into a table in WT_Processing
Select * into WT_Processing..AWISNOQC_vs_QC_Comparison
from (
select * from #NoQC_QC_MatchAttributesCount
union all
select * from #NoQC_QC_NotNullAttributesCount
union all
select * from #NoQC_EqualNullAttributesCount
union all
select * from #QC_EqualNullAttributesCount
union all
select * from #NoQC_NoEqualQCAttributesCount) as P
--order by locationid, attribute, recordcount desc

Select distinct comparisontype from WT_Processing..AWISNOQC_vs_QC_Comparison