/****** Script for SelectTopNRows command from SSMS  ******/
--Convert UTC to Central
SELECT distinct WT_Processing.dbo.GetDateFromTEmporalId(AWIS_TimeStamp, 2) as UTCTime
		, (case when right(awis_timestamp, 2) = 00
			then cast(cast(left(awis_timestamp, 8) - 1 as varchar) + '18' as int)
			else awis_timestamp - 6 end) as CentralTime
      ,FileVersion
into #TimeZones
  FROM WT_Processing.dbo.AWIS4xDataWithTimeStamp
  where fileversion <> 'qc_int'
  order by fileversion, WT_Processing.dbo.GetDateFromTEmporalId(AWIS_TimeStamp, 2)

select * from #Timezones

--Convert the interal dates to smalldatetime
Select UTCTime, WT_Processing.dbo.GetDateFromTEmporalId(CentralTime, 2) as CentralTime, FileVersion
into #TimeZonesConvertedToDate
from #Timezones

SElect * from #TimeZonesConvertedToDate
select * from [WT_Processing].[dbo].[AWIS4xFileData]

--Take list of files and get date modified in same format as timestamp
SELECT cast(cast([DateFormat] as varchar) + ' ' + cast([TimeFormat] as varchar) as smalldatetime) as DateModified
      ,WT_Processing.dbo.GetDateFromTEmporalId(timestamp, 2) as TimeStamp
      ,Fileversion
into #DateModified
  FROM [WT_Processing].[dbo].[AWIS4xFileData]

select * from #DateModified

--combine datemodified with UTC time, centraltime and fileversion
Select d.DateModified
	, WT_Processing.dbo.GetDateFromTEmporalId(t.CentralTime, 2) as CentralTime, t.fileversion
into #DateModifiedv2
from #DateModified d
inner join
#Timezones t
on d.Timestamp = t.UTCTime
and d.fileversion = t.fileversion
order by WT_Processing.dbo.GetDateFromTEmporalId(t.CentralTime, 2)

Select * from #DateModifiedv2

--Get lag time between the date the data represents and the time the file is delivered in HH:MM
Select CentralTime
	, DateModified
	, FileVersion
	, convert(varchar(5), dateadd(minute, Datediff(Minute, CentralTime, DateModified),0),114) as LagTimeHHMM
into WT_Processing..AWIS4xFileLagTime
from #Datemodifiedv2
order by CentralTime

select * from WT_Processing..AWIS4xFileLagTime

--Get lag time between noqc and qc files for same date
Select CentralTime
	, min([NoQC_NoInt]) as NoQC_NoInt, min([QC_NoInt]) as QC_NoInt
	, convert(varchar(5), dateadd(minute, datediff(minute, min([NoQC_NoInt]), min([QC_NoInt])), 0), 114) as FileVersionLagHHMM
into WT_Processing..AWIS4xFileVersionLagTime
	from WT_Processing..AWIS4xFileLagTime
pivot (
	min(Datemodified)
	for Fileversion in ([NoQC_NoInt], [QC_NoInt])
	) as  pvt
group by CentralTime

select * from WT_Processing..AWIS4xFileVersionLagTime

, Datediff(Hour, WT_Processing.dbo.GetDateFromTEmporalId(CentralTime, 2), WT_Processing.dbo.GetDateFromTEmporalId(DateModified, 2)) as LagTimeHours