Select *
from wt_processing..AWIS4xDataWithTimeStamp
where locationid like '5%'
and fileversion = 'noqc_noint'
and awis_filetime = 6
order by locationid, temporalid

--clean 4x file
Update wt_processing..AWIS4xDataWithTimeStamp
Set MaxTemp = Null
where MaxTemp = 'Null'

Update wt_processing..AWIS4xDataWithTimeStamp
Set MinTemp = Null
where MinTemp = 'Null'

Update wt_processing..AWIS4xDataWithTimeStamp
Set Precip = Null
where Precip = 'Null'

Update wt_processing..AWIS4xDataWithTimeStamp
Set Solar = Null
where Solar = 'Null'

Update wt_processing..AWIS4xDataWithTimeStamp
Set MaxWind = Null
where MaxWind = 'Null'

Update wt_processing..AWIS4xDataWithTimeStamp
Set MornWind = Null
where MornWind = 'Null'

Update wt_processing..AWIS4xDataWithTimeStamp
Set MeanWind = Null
where MeanWind = 'Null'

Update wt_processing..AWIS4xDataWithTimeStamp
Set MaxRH = Null
where MaxRH = 'Null'

Update wt_processing..AWIS4xDataWithTimeStamp
Set MinRH = Null
where MinRH = 'Null'

--change data type to real for 4x file
alter table wt_processing..AWIS4xDataWithTimeStamp
alter column MaxTemp real

alter table wt_processing..AWIS4xDataWithTimeStamp
alter column MinTemp real

alter table wt_processing..AWIS4xDataWithTimeStamp
alter column Precip real

alter table wt_processing..AWIS4xDataWithTimeStamp
alter column Solar real

alter table wt_processing..AWIS4xDataWithTimeStamp
alter column MaxWind real

alter table wt_processing..AWIS4xDataWithTimeStamp
alter column MornWind real

alter table wt_processing..AWIS4xDataWithTimeStamp
alter column MeanWind real

alter table wt_processing..AWIS4xDataWithTimeStamp
alter column MaxRH real

alter table wt_processing..AWIS4xDataWithTimeStamp
alter column MinRH real
