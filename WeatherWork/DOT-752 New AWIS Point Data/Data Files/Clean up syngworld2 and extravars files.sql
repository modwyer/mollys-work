Select s.*, e.MeanWind
from alternateweathersources..awis_noninterpolated_syngworld s
inner join 
alternateweathersources..awis_noninterpolated_extravars e
on s.locationid = e.locationid
and s.temporalid = e.temporalid

select count(*)
from alternateweathersources..awis_noninterpolated_syngworld
--200685

alter table alternateweathersources..awis_noninterpolated_syngworld
add MeanWind real

--clean extravars file
Update alternateweathersources..awis_noninterpolated_extravars
Set MeanWind = Null
where Meanwind = 'Null'

--change datatype to real
alter table alternateweathersources..awis_noninterpolated_extravars
alter column MeanWind real

--Update the meanwind
Update alternateweathersources..awis_noninterpolated_syngworld
Set MeanWind = e.MeanWind
From alternateweathersources..awis_noninterpolated_syngworld s, alternateweathersources..awis_noninterpolated_extravars e
where s.locationid = e.locationid
and s.temporalid = e.temporalid

--clean syngworld2 file
Update alternateweathersources..awis_noninterpolated_syngworld
Set MaxTemp = Null
where MaxTemp = 'Null'

Update alternateweathersources..awis_noninterpolated_syngworld
Set MinTemp = Null
where MinTemp = 'Null'

Update alternateweathersources..awis_noninterpolated_syngworld
Set Precip = Null
where Precip = 'Null'

Update alternateweathersources..awis_noninterpolated_syngworld
Set Solar = Null
where Solar = 'Null'

Update alternateweathersources..awis_noninterpolated_syngworld
Set MaxWind = Null
where MaxWind = 'Null'

Update alternateweathersources..awis_noninterpolated_syngworld
Set MornWind = Null
where MornWind = 'Null'

Update alternateweathersources..awis_noninterpolated_syngworld
Set MaxRH = Null
where MaxRH = 'Null'

Update alternateweathersources..awis_noninterpolated_syngworld
Set MinRH = Null
where MinRH = 'Null'

--change data type to real for syngworld
alter table alternateweathersources..awis_noninterpolated_syngworld
alter column MaxTemp real

alter table alternateweathersources..awis_noninterpolated_syngworld
alter column MinTemp real

alter table alternateweathersources..awis_noninterpolated_syngworld
alter column Precip real

alter table alternateweathersources..awis_noninterpolated_syngworld
alter column Solar real

alter table alternateweathersources..awis_noninterpolated_syngworld
alter column MaxWind real

alter table alternateweathersources..awis_noninterpolated_syngworld
alter column MornWind real

alter table alternateweathersources..awis_noninterpolated_syngworld
alter column MaxRH real

alter table alternateweathersources..awis_noninterpolated_syngworld
alter column MinRH real


Select * from alternateweathersources..AWis_nonInterpolated_syngworld


