drop table wt_processing..AWISNoQC_NoInt
drop table #NAMToEdit
--Create main joined table of new and old awis data (100476)
Select ts.AWIS_TimeStamp, ts.awis_filetime, ts.fileversion, ts.LocationID, ts.awis_id, ts.TemporalID, ts.temporalid + 1 as TemporalOffset
      , ts.MaxTemp as NewMaxTemp, ts.MinTemp as NewMinTemp, ts.Precip as NewPrecip, ts.Solar as NewSolar, ts.MaxWind as NewMaxWind
	  , ts.MornWind as NewMornWind, ts.MeanWind as NewMeanWind, ts.MinRH as NewMaxRH, ts.MaxRH as NewMinRH
      , a.MaxTemp, a.MinTemp, a.Precip, a.Solar, a.MaxWind, a.MornWind, a.MeanWind, a.MaxRH, a.MinRH
into wt_processing..AWISNoQC_NoInt
From wt_processing..AWIS4xDataWithTimeStamp ts
inner join
AlternateWeatherSources..AWIS_NonInterpolated_1012_1025 a
on ts.locationid = a.locationid
and ts.temporalid = a.temporalid
where ts.fileversion = 'noqc_noint'
order by ts.locationid, ts.temporalid

Select *
from wt_processing..AWISNoQC_NoInt

--Add offset flags to the Join table
alter table wt_processing..AWISNoQC_NoInt
add NewMaxTOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewMinTOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewPrecipOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewSolarOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewMaxWOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewMornWOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewMeanWOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewMaxRHOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewMinRHOffset bit

--Pull out just NAM 6am (42948) for editing
Select *
into #NAMToEdit
From wt_processing..AWISNoQC_NoInt
where locationid/10000000 = 5
and awis_filetime = 6

--Adjust NAM Newmaxtemp +1 day and set offset flag to 1 - will lose 3579 values from 10/14 (39369 values)
Update wt_processing..AWISNoQC_NoInt
Set NewMaxTOffset = 1-- NewMaxTemp = b.NewMaxTemp
--Select a.awis_timestamp, a.awis_filetime, a.locationid, a.awis_id, a.temporalid, b.temporaloffset, b.NewMaxTemp, a.MaxTemp
from wt_processing..AWISNoQC_NoInt a
inner join 
#NAMToEdit b
on a.locationid = b.locationid
and a.temporalid = b.temporaloffset
where a.awis_filetime = 6
and a.fileversion = 'noqc_noint'
and a.locationid/10000000 = 5
order by a.locationid, a.temporalid

--Check (39369 results)
Select *
from wt_processing..AWISNoQC_NoInt
where awis_filetime = 6
and fileversion = 'noqc_noint'
and locationid/10000000=5
--and temporalid <> 20151014
order by locationid, temporalid

--Use this to update the 10/14 values that were not updated offset flag will be 0 (3579 total)
Update wt_processing..AWISNoQC_NoInt
Set NewMaxTOffset = 0--NewMaxTemp = Null
--select * from wt_processing..AWISNoQC_NoInt
where awis_filetime = 6
and fileversion = 'noqc_noint'
and temporalid = 20151014
and locationid/10000000 = 5

--Adjust NAM Precip +1 day and set offset flag to 1 - will lose 3579 values from 10/14 (39369 values)
Update wt_processing..AWISNoQC_NoInt
Set NewPrecipOffset = 1, NewPrecip = b.NewPrecip
--Select a.awis_timestamp, a.awis_filetime, a.locationid, a.awis_id, a.temporalid, b.temporaloffset, b.NewPrecip, a.Precip
from wt_processing..AWISNoQC_NoInt a
inner join 
#NAMToEdit b
on a.locationid = b.locationid
and a.temporalid = b.temporaloffset
where a.awis_filetime = 6
and a.fileversion = 'noqc_noint'
and a.locationid/10000000 = 5
order by a.locationid, a.temporalid

--Check (39369 results)
Select *
from wt_processing..AWISNoQC_NoInt
where awis_filetime = 6
and fileversion = 'noqc_noint'
and locationid/10000000=5
and temporalid <> 20151014
order by locationid, temporalid

--Use this to update the 10/14 values that were not updated offset flag will be 0 (3579 total)
Update wt_processing..AWISNoQC_NoInt
Set NewPrecipOffset = 0, NewPrecip = Null
--select * from wt_processing..AWISNoQC_NoInt
where awis_filetime = 6
and fileversion = 'noqc_noint'
and temporalid = 20151014
and locationid/10000000 = 5

--Adjust NAM Solar, MinRH, and MaxWind +1 day and set offset flag to 1 - will lose 3579 values from 10/14 (39369 values)
Update wt_processing..AWISNoQC_NoInt
Set NewSolarOffset = 1, NewSolar = b.NewSolar
	, NewMaxWOffset = 1, NewMaxWind = b.NewMaxWind
	, NewMinRHOffset = 1, NewMinRH = b.NewMinRH
--Select a.awis_timestamp, a.awis_filetime, a.locationid, a.awis_id, a.temporalid, b.temporaloffset,b.NewMinRH, a.MinRH
from wt_processing..AWISNoQC_NoInt a
inner join 
#NAMToEdit b
on a.locationid = b.locationid
and a.temporalid = b.temporaloffset
where a.awis_filetime = 6
and a.fileversion = 'noqc_noint'
and a.locationid/10000000 = 5
order by a.locationid, a.temporalid

--Check (39369 results)
Select *
from wt_processing..AWISNoQC_NoInt
where awis_filetime = 6
and fileversion = 'noqc_noint'
and locationid/10000000=5
and temporalid <> 20151014
order by locationid, temporalid

--Use this to update the 10/14 values that were not updated offset flag will be 0 (3579 total)
Update wt_processing..AWISNoQC_NoInt
Set NewSolarOffset = 0, NewSolar = Null
	, NewMaxWOffset = 0, NewMaxWind = Null
	, NewMinRHOffset = 0, NewMinRH = Null
--select * from wt_processing..AWISNoQC_NoInt
where awis_filetime = 6
and fileversion = 'noqc_noint'
and temporalid = 20151014
and locationid/10000000 = 5

--Adjust NAM MornWind +1 day and set offset flag to 1 - will lose 3579 values from 10/14 (39369 values)
Update wt_processing..AWISNoQC_NoInt
Set NewMornWOffset = 1, NewMornWind = b.NewMornWind
--Select a.awis_timestamp, a.awis_filetime, a.locationid, a.awis_id, a.temporalid, b.temporaloffset,b.NewMornWind, a.MornWind
from wt_processing..AWISNoQC_NoInt a
inner join 
#NAMToEdit b
on a.locationid = b.locationid
and a.temporalid = b.temporaloffset
where a.awis_filetime = 6
and a.fileversion = 'noqc_noint'
and a.locationid/10000000 = 5
order by a.locationid, a.temporalid

--Check
Select locationid, temporalid, temporaloffset, newmornwind, mornwind
from wt_processing..AWISNoQC_NoInt
where awis_filetime = 6
and fileversion = 'noqc_noint'
and locationid/10000000=5
order by locationid, temporalid

--Use this to update the 10/14 values that were not updated offset flag will be 0 (3579 total)
Update wt_processing..AWISNoQC_NoInt
Set NewMornWOffset = 0, NewMornWind = Null
--select * from wt_processing..AWISNoQC_NoInt
where awis_filetime = 6
and fileversion = 'noqc_noint'
and temporalid = 20151014
and locationid/10000000 = 5