--Create joined table of noqc_noint 4x awis data and 1x awis data for each location and temporalid (100476 records)
SELECT n.AWIS_TimeStamp, n.LocationID, n.TemporalID, n.NewMaxTemp, n.NewMinTemp, n.NewPrecip
		,n.NewSolar, n.NewMaxWind, n.NewMornWind,n.NewMeanWind, n.NewMaxRH, n.NewMinRH
	  , a.MaxTemp, a.MinTemp, a.Precip, a.Solar, a.MaxWind, a.MornWind, a.MeanWind, a.MaxRH, a.MinRH
into wt_processing..AWIS_NOQC_NoIntJoined
  FROM AlternateWeatherSources..AWIS_NonInterpolated_1012_1025 a
  inner join 
  WT_Processing.dbo.AWISnoqc_noint n
  on a.locationid = n.locationid 
  and a.temporalid = n.temporalid
  order by n.locationid, n.temporalid
 
  
--Get data where 4x and 1x data doesn't match and put into a new table

--Pivot the awis_noqc_nointjoined table (904284 records)
  Select UniqueID, AWIS_TimeStamp, Locationid, TemporalID, Attribute, NewValue, OldValue
  into wt_processing..AWIS_NOQC_NoIntPivotedJoin
  from wt_processing..AWIS_NOQC_NoIntJoined
  cross apply
  (Select 'MaxTemp', NewMaxTemp, MaxTemp union all
	Select 'MinTemp', NewMinTemp, MinTemp union all
	Select 'Precip', Newprecip, Precip union all
	select 'Solar', Newsolar, Solar union all
	select 'MaxWind', Newmaxwind, MaxWind union all
	select 'MornWind', Newmornwind, MornWind union all
	select 'MeanWind', Newmeanwind, MeanWind union all
	select 'MinRH', Newminrh, MinRH union all
	select 'MaxRH', Newmaxrh, MaxRH
	) d (Attribute, NewValue, OldValue)
order by locationid asc, temporalid asc


 --Create and populate table with count of each attribute per station that does NOT match between the 4x data and 1x data per station.
Select AWIS_TimeStamp, LocationId, TemporalId, Attribute, OldValue, NewValue, BadMatches, GoodMatches --MatchType
into #MatchType
From (
	Select AWIS_TimeStamp, LocationId, Temporalid, Attribute, OldValue, NewValue,
		case when OldValue <> NewValue or (newvalue is null and oldvalue is not null) or (newvalue is null and oldvalue = 0)
			or (oldvalue is null and newvalue is not null) or (oldvalue is null and newvalue = 0)		
						then 'Bad' else Null end as BadMatches --MatchType
		, case when OldValue = NewValue then 'Good' else Null end as GoodMatches
	From wt_processing..AWIS_NOQC_NoIntPivotedJoin) I
order by LocationId, temporalid

  
 Select * from #Matchtype

--Get count of good and bad data matches for each location
Select Locationid, Attribute, count(BadMatches) as CountBadMatches, count(GoodMatches) as CountGoodMatches, count(BadMatches) + count(GoodMatches) as TotalMatches
from #matchtype
group by locationid, Attribute
order by locationid

--Get bad matches >0 and the actual value
Select Locationid, temporalid, Attribute, OldValue, NewValue
from #matchtype
group by locationid, temporalid, Attribute, oldvalue, newvalue
having count(BadMatches) >0
order by locationid, temporalid


