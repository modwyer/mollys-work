--Pull out just AUS 12noon (3864) for editing
Select *
into #AUSToEdit
From wt_processing..AWISNoQC_NoInt
where locationid/10000000 = 3
and awis_filetime = 12

Select *
from #AUSToEdit

--Adjust AUS MaxTemp, MinTemp, Precip, Solar, MaxWind, MornWind, MeanWind, MaxRH, MinRH +1 day and set offset flag to 1 - will lose 322 values from 10/14 (3542 values)
Update wt_processing..AWISNoQC_NoInt
Set NewMaxTOffset = 1, NewMaxTemp = b.NewMaxTemp
	, NewMinTOffset = 1, NewMinTemp = b.NewMinTemp
	, NewPrecipOffset = 1, NewPrecip = b.NewPrecip
	, NewSolarOffset = 1, NewSolar = b.NewSolar
	, NewMaxWOffset = 1, NewMaxWind = b.NewMaxWind
	, NewMornWOffset = 1, NewMornWind = b.NewMornWind
	, NewMeanWOffset = 1, NewMeanWind = b.NewMeanWind
	, NewMaxRHOffset = 1, NewMaxRH = b.NewMaxRH
	, NewMinRHOffset = 1, NewMinRH = b.NewMinRH
--Select a.awis_timestamp, a.awis_filetime, a.locationid, a.awis_id, a.temporalid, b.temporaloffset,b.NewMinRH, a.MinRH
from wt_processing..AWISNoQC_NoInt a
inner join 
#AUSToEdit b
on a.locationid = b.locationid
and a.temporalid = b.temporaloffset
where a.awis_filetime = 12
and a.fileversion = 'noqc_noint'
and a.locationid/10000000 = 3
order by a.locationid, a.temporalid

--Check (3542 results)
Select *
from wt_processing..AWISNoQC_NoInt
where awis_filetime = 12
and fileversion = 'noqc_noint'
and locationid/10000000=3
and temporalid <> 20151014
order by locationid, temporalid

--Use this to update the 10/14 values that were not updated offset flag will be 0 (322 total)
Update wt_processing..AWISNoQC_NoInt
Set NewMaxTOffset = 0, NewMaxTemp = Null
	, NewMinTOffset = 0, NewMinTemp = Null
	, NewPrecipOffset = 0, NewPrecip = Null
	, NewSolarOffset = 0, NewSolar = Null
	, NewMaxWOffset = 0, NewMaxWind = Null
	, NewMornWOffset = 0, NewMornWind = Null
	, NewMeanWOffset = 0, NewMeanWind = Null
	, NewMaxRHOffset = 0, NewMaxRH = Null
	, NewMinRHOffset = 0, NewMinRH = Null
--select * from wt_processing..AWISNoQC_NoInt
where awis_filetime = 12
and fileversion = 'noqc_noint'
and temporalid = 20151014
and locationid/10000000 = 3