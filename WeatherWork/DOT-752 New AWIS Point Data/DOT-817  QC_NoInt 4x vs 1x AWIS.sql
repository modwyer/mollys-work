--Create joined table of QC_Noint 4x awis data and 1x awis data for each location and temporalid (100476 records)
--Extract out QC_NoInt records (124140 rows)
Select * 
into WT_Processing.dbo.AWISQC_NoInt
from WT_Processing..AWis4xDataWithTimeStamp
where fileversion = 'qc_noint'

--Join 4x and 1x data (122168 rows)
SELECT n.AWIS_TimeStamp, n.LocationID, n.TemporalID, n.MaxTemp as NewMaxTemp, n.MinTemp as NewMinTemp, n.Precip as NewPrecip
		,n.Solar as NewSolar, n.MaxWind as NewMaxWind, n.MornWind as NewMornWind, n.MeanWind as NewMeanWind, n.MinRH as NewMaxRH, n.MaxRH as NewMinRH
	  , a.MaxTemp, a.MinTemp, a.Precip, a.Solar, a.MaxWind, a.MornWind, a.MeanWind, a.MaxRH, a.MinRH
into wt_processing..AWIS_QC_NoIntJoined
  FROM AlternateWeatherSources..AWIS_NonInterpolated_1012_1025 a
  inner join 
  WT_Processing.dbo.AWISqc_noint n
  on a.locationid = n.locationid 
  and a.temporalid = n.temporalid
  order by n.locationid, n.temporalid
 
  
--Get data where 4x and 1x data doesn't match and put into a new table

--Pivot the awis_QC_Nointjoined table (1099512 records)
  Select AWIS_TimeStamp, Locationid, TemporalID, Attribute, NewValue, OldValue
  into wt_processing..AWIS_QC_NointPivotedJoin
  from wt_processing..AWIS_QC_NointJoined
  cross apply
  (Select 'MaxTemp', NewMaxTemp, MaxTemp union all
	Select 'MinTemp', NewMinTemp, MinTemp union all
	Select 'Precip', Newprecip, Precip union all
	select 'Solar', Newsolar, Solar union all
	select 'MaxWind', Newmaxwind, MaxWind union all
	select 'MornWind', Newmornwind, MornWind union all
	select 'MeanWind', Newmeanwind, MeanWind union all
	select 'MinRH', Newminrh, MinRH union all
	select 'MaxRH', Newmaxrh, MaxRH
	) d (Attribute, NewValue, OldValue)
order by locationid asc, temporalid asc


 --Create and populate table with count of each attribute per station that does NOT match between the 4x data and 1x data per station.
Select AWIS_TimeStamp, LocationId, TemporalId, Attribute, OldValue, NewValue, BadMatches, GoodMatches
into #MatchType
From (
	Select AWIS_TimeStamp, LocationId, Temporalid, Attribute, OldValue, NewValue,
		case when OldValue <> NewValue or (newvalue is null and oldvalue is not null) or (newvalue is null and oldvalue = 0)
			or (oldvalue is null and newvalue is not null) or (oldvalue is null and newvalue = 0)		
						then 'Bad' else Null end as BadMatches 
		, case when OldValue = NewValue or (OldValue is Null and NewValue is null) then 'Good' else Null end as GoodMatches
	From wt_processing..AWIS_QC_NointPivotedJoin) I
order by LocationId, temporalid

  
 Select * from #Matchtype

--Get count of good and bad data matches for each location
Select Locationid, Attribute, count(BadMatches) as CountBadMatches, count(GoodMatches) as CountGoodMatches, count(BadMatches) + count(GoodMatches) as TotalMatches
from #matchtype
group by locationid, Attribute
order by locationid

--Get bad matches >0 and the actual value (401961)
Select Locationid, temporalid, Attribute, OldValue, NewValue
from #matchtype
group by locationid, temporalid, Attribute, oldvalue, newvalue
having count(BadMatches) >0
order by locationid, temporalid


