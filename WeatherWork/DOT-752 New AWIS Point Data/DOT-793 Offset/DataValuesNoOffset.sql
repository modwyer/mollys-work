SELECT ts.AWIS_TimeStamp
      ,ts.LocationID
      ,ts.TemporalId as NCDTemporalID
      ,ts.MaxT as NCDMaxTemp
	  ,a.maxtemp as SyngMaxTemp
	  ,a.temporalid as SyngTemporalID
	  ,ts.Mint as NCDMinTemp
	  ,a.mintemp as SyngMinTemp
	  ,ts.Prcp as NCDPrecip
	  ,a.Precip as SyngPrecip
	  ,ts.Solar as NCDSolar
	  ,a.Solar as SyngSolar
	  ,ts.MaxWind as NCDMaxWind
	  ,a.maxwind as SyngMaxWind
	  ,ts.MornWind as NCDMornWind
	  ,a.mornwind as SyngMornWind
	  ,ts.MeanWind as NCDMeanWind
	  ,a.meanwind as SyngMornWind
	  ,ts.MaxRH as NCDMaxRH
	  ,a.maxrh as SyngMaxRH
	  ,ts.MinRH as NCDMinRH
	  ,a.minrh as SyngMinRH
  FROM WT_Processing.dbo.AWISnoqc_noint ts
  inner join
  AlternateWeatherSources..AWISNonInterpolateddata_1012_1025 a
  on ts.locationid = a.locationid
  and ts.temporalid = a.temporalid
  where ts.locationid/10000000 = 3
 and ts.fileversion = 'noqc_noint'
 order by ts.locationid, ts.temporalid