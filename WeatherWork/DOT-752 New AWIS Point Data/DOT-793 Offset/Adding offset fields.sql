--Add offset flags to the Join table; change table name as necessary
alter table wt_processing..AWISNoQC_NoInt
add NewMaxTOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewMinTOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewPrecipOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewSolarOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewMaxWOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewMornWOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewMeanWOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewMaxRHOffset bit

alter table wt_processing..AWISNoQC_NoInt
add NewMinRHOffset bit