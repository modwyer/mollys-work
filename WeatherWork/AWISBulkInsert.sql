
declare @Cmd varchar(8000)
declare @FileDirectory varchar(500)
declare @FullFileName varchar(500)
declare @FieldTerminator varchar(10)
declare @RowTerminator varchar(10)
declare @MinRunId int
declare @MaxRunID int
declare @NewAWISFileTableFull table(AWISFileName varchar(500),RunID int identity)
declare @NewAWISFileTable table(AWISFileName varchar(500),RunID int identity)

--check if temp tables exists, delete if yes
IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#AWISImport')) BEGIN DROP TABLE #AWISImport END
IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#AWISImport2')) BEGIN DROP TABLE #AWISImport2 END

--must look like fields in raw file
--use for extravars
create table #AWISImport2
(
AWIS_ID varchar(7),TemporalID int,MeanWind real
)
--use for syngworld2
--create table #AWISImport2
--(
--AWIS_ID varchar(7),TemporalID int, MaxTemp real, MinTemp real, Precip real, Pet real, Solar real, maxwind real, MaxWind_6_noon real, max_rh real
--		, min_rh real,TEMP_AT_NOON real, DEWPOINT_AT_NOON real, HOUR_LEAF real
--)

--must look like what file table will be
create table #AWISImport
(
LocationID int,AWIS_ID varchar(7),TemporalID int, MeanWind real --MaxTemp real, MinTemp real, Precip real, Solar real, MaxWind real, MornWind real, MaxRH real, MinRH real
)

set @FileDirectory = 'F:\SQLDB\Data\Weather\WT3\RawFiles\AWIS\Daily\FTPSynch\'

--get list of file names in the above directory
set @CMD = 'dir /B '+@FileDirectory
insert @NewAWISFileTableFull(AWISFileName)
EXEC xp_cmdshell @CMD


--remove all files that are not the ones we want
delete @NewAWISFileTableFull where AWISFileName not like '%extravars.%%'

insert @NewAWISFileTable(AWISFileName)
select AWISFileName 
from @NewAWISFileTableFull

select @MinRunId = MIN(RunID),@MaxRunId = MAX(RunID)
from @NewAWISFileTable
where substring(awisFilename, 11,8) between '20151011' and '20151026' --only pull files with certain dates, maybe need to adjust substring #s)

--Select @Minrunid, @maxrunid

While @MinRunId <=@MaxRunId
begin
	Print 'test'
	Select @Fullfilename = awisfilename
	from @NewAWISFileTable
	where runid = @minrunid

	Print ('Working on file ' + @Fullfilename)

	set @FullFileName  = ''''+@FileDirectory+(select AWISFileName from @NewAWISFileTable where RunID = @MinRunId)+''''
	set @FieldTerminator = ''','''
	set @RowTerminator = '''0x0a'''
	
	--insert all fields to the awisimport2 table
	set @CMD = 
	'Bulk insert #AWISImport2
	from '+@FullFileName+ 
	' with(
	FirstRow = 1,
	fieldTErminator = '+@FieldTerminator+',
	Rowterminator = '+@RowTerminator+',
	TABLock);'
	exec (@Cmd)

	--insert to the real table
	insert #AWISImport(LocationID,AWIS_ID,TemporalID, MeanWind) --MaxTemp, MinTemp, Precip, Solar, MaxWind, MornWind, MaxRH, MinRH)
	select 
	Look.LocationID,
	Import.AWIS_ID,
	Import.TemporalID,
	case when Import.MeanWind = 999 then null else Import.MeanWind end as MeanWind
	--case when Import.MaxTemp = -99.99 then null else Import.MaxTemp end as MaxTemp,
	--case when Import.MinTemp = -99.99 then null else Import.MinTemp end as MinTemp,
	--case when Import.Precip = -99.99 then null when Import.Precip = 8888 then 0.25 else Import.Precip end as Precip,
	--case when Import.Solar in(0,-99.99) then null else Import.Solar end as Solar,
	--case when Import.MaxWind = -99.99 then null else Import.MaxWind end as MaxWind,
	--case when Import.MaxWind_6_noon = -99.99 then null else Import.MaxWind_6_noon end as MornWind,
	--case when Import.Max_RH = -99.99 then null else Import.Max_RH end as MaxRH,
	--case when Import.Min_RH = -99.99 then null else Import.Min_RH end as MinRH
	from WT_Processing..Master_WT_PointStations_Lookup Look
	inner join #AWISImport2 Import
	on Look.AWIS_ID = Import.AWIS_ID

	truncate table #AWISImport2

	set @MinRunId = @MinRunId + 1
end

--show table to copy
select * from #AWISImport
order by temporalid desc