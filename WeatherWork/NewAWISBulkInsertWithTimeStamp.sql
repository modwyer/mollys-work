
declare @Cmd varchar(8000)
declare @fileversion varchar(30)
declare @AWISTimeStamp int
Declare @AWISFileTime smallint
declare @FileDirectory varchar(500)
declare @FullFileName varchar(500)
declare @FieldTerminator varchar(10)
declare @RowTerminator varchar(10)
declare @MinRunId int
declare @MaxRunID int
declare @NewAWISFileTableFull table(AWISFileName varchar(500),RunID int identity)
declare @NewAWISFileTable table(AWISFileName varchar(500),AWIS_TimeStamp int,AWIS_FileTime smallint,RunID int identity)

IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#AWISImport')) BEGIN DROP TABLE #AWISImport END
IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#AWISImport2')) BEGIN DROP TABLE #AWISImport2 END



create table #AWISImport2
(
AWIS_ID varchar(7),TemporalID int,MinT real,MaxT real,PRCP real,Solar real, MaxWind real,Mornwind real,MeanWind real, MaxRH real,MinRH real
)
create table #AWISImport
(
AWIS_TimeStamp int,AWIS_Filetime smallint,FileVersion varchar(30),LocationID int,AWIS_ID varchar(7),TemporalID int,MinTemp real,MaxTemp real
	,Precip real,Solar real, MaxWind real,Mornwind real,MeanWind real, MaxRH real,MinRH real
)

set @FileDirectory = 'F:\SQLDB\Data\Weather\WT3\RawFiles\AWIS\Daily\FTPSynch\'

set @CMD = 'dir /B '+@FileDirectory
insert @NewAWISFileTableFull(AWISFileName)
EXEC xp_cmdshell @CMD
delete @NewAWISFileTableFull where AWISFileName not like '%%.ncd'

insert @NewAWISFileTable(AWISFileName,AWIS_TimeStamp,AWIS_Filetime)
select AWISFileName,cast(substring( AWISFileName,1,10) as int),cast(substring(AWISFileName,9,2) as smallint)
from @NewAWISFileTableFull

select @MinRunId = MIN(RunID),@MaxRunId = MAX(RunID)
from @NewAWISFileTable
where substring(awisFilename, 1,8) between '20151011' and '20151026'

While @MinRunId <=@MaxRunId
begin
	select @FullFileName =  ''''+@FileDirectory+AWISFileName+'''', @AWISTimeStamp = AWIS_TimeStamp, @AWISFileTime = AWIS_Filetime
	from @NewAWISFileTable
	where RunID = @MinRunID
	set @FieldTerminator = ''','''
	set @RowTerminator = '''0x0a'''
	set @fileversion= 
	case when @FullFileName like '%%_noqc_%%' then 'noqc_noint'
	when @FullFileName like '%%_qc_noint%%' then 'qc_noint'
	when @FullFileName like '%%_qc_int%%' then 'qc_int' end
	set @CMD = 
	'Bulk insert #AWISImport2
	from '+@FullFileName+ 
	' with(
	FirstRow = 2,
	fieldTErminator = '+@FieldTerminator+',
	Rowterminator = '+@RowTerminator+',
	TABLock);'
	exec (@Cmd)


	insert #AWISImport(AWIS_TimeStamp,AWIS_Filetime,FileVersion,LocationID,AWIS_ID,TemporalID,MinTemp,MaxTemp,Precip,Solar, MaxWind,Mornwind,MeanWind, MaxRH,MinRH)
	select 
	@AWISTimeStamp,
	@AWISFileTime,
	@fileversion,
	Look.LocationID,
	Import.AWIS_ID,
	Import.TemporalID,
	case when Import.MinT = 999 then null else Import.MinT end as MinTemp,
	case when Import.MaxT = 999 then null else Import.MaxT end as MaxTemp,
	case when Import.Prcp = 999 then null when Import.Prcp = 8888 then 0.25 else Import.Prcp end as Precip,
	case when Import.Solar in(0,999) then null else Import.Solar end as Solar,
	case when Import.MaxWind = 999 then null else Import.MaxWind end as MaxWind,
	case when Import.MornWind = 999 then null else Import.MornWind end as MornWind,
	case when Import.MeanWind = 999 then null else Import.MeanWind end as MeanWind,
	case when Import.MaxRH = 999 then null else Import.MaxRH end as MaxRH,
	case when Import.MinRH = 999 then null else Import.MinRH end as MinRH
	from WT_Processing..Master_WT_PointStations_Lookup Look
	inner join #AWISImport2 Import
	on Look.AWIS_ID = Import.AWIS_ID

	truncate table #AWISImport2

	set @MinRunId = @MinRunId + 1
end

select * from #AWISImport