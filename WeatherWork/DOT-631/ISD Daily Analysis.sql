
--Get hourly wind and temp and min/max for wind and temp for each ISD station; include count of stations reporting each attribute
SELECT top 1000 left(cast(YRMODAHRMN as bigint), 10) as YRMONDAY
      ,avg(cast(SPD as int)) as DailyWindSpeed
	  ,min(cast(SPD as int)) as MinWind
	  ,max(cast(SPD as int)) as MaxWind
      ,avg(cast(TEMP as int)) as DailyTemp
	  ,min(cast(TEMP as int)) as MinTemp
	  ,max(cast(TEMP as int)) as MaxTemp
	  ,count(cast(SPD as int)) as WindCountObservations
	  ,count(cast(TEMP as int)) as TempCountObservations
	  ,count(distinct usaf) as StationsReporting
  FROM AlternateWeatherSources..ISD_Hourly_Observations
  group by left(cast(YRMODAHRMN as bigint),10)
  order by left(cast(YRMODAHRMN as bigint),10) asc

--Get hourly precip and count of reporting stations

--attempt to unpivot but not working as yet
  SELECT usaf, YRMODAHRMN, PrecipHr, PrecipValue
  FROM (Select usaf, YRMODAHRMN, spd as Speed, Temp as Temperature, Dewp as Dewpoint, STP as StationPressure, pcp01 as '1', pcp06 as '6', pcpxx as '12', pcp24 as '24'
		From AlternateWeatherSources..ISD_Hourly_Observations
		) precip
  unpivot
	(PrecipValue
		For PrecipHr in ([1],[6],[12],[24])
	) AS A

  Unpivot
	(Precip
		For Value in ([pcp01],[pcp06],[pcp24],[pcpxx])
	) as p

  Select usaf, YRMODAHRMN
		,Precip case right(YRMODAHRMN, 4)
					when '0600' then pcp24
					when '1200' then pcp06
					when '1800' then pcp12

		 pcp01, pcp06, pcp24, pcpxx as pcp12
  from AlternateWeatherSources..ISD_Hourly_Observations
  where usaf like '010070'
  and YRMODAHRMN like '20150824%'