--For checks, skip this
SELECT a.*,--top 4700 A.[LocationID],
B.SpatialMeasure.STBuffer(.25),'A'
FROM WT_NAM_2015.dbo.NAM_2015_Point_Daily A,
WT_Processing.dbo.Master_WT_pointstations B
where a.Locationid = B.LocationID
--and A.TemporalID = 2015072500
and latitude <20
and A.AttributeId = 1 --max, 2 is min
union all
select locationid,1,1,1,1,spatialmeasure, locationname
from globalgriddatastore..countrylocations
where locationname in ('Guatemala', 'Belize', 'El Salvador', 'Honduras', 'Nicaragua', 'Costa Rica', 'Panama')

--The real stuff begins, run this to the end
Create table #CentralAmCountries (RunId int identity(0,1), LocationID int, LocationName varchar(50), SpatialMeasure geometry)
Insert into #CentralAmCountries (LocationID, LocationName, SpatialMEasure)
select locationid, locationname, spatialmeasure
from globalgriddatastore..countrylocations
where locationname in ('Guatemala', 'Belize', 'El Salvador', 'Honduras', 'Nicaragua', 'Costa Rica', 'Panama')

Create table #CentralAmStations (RunId int identity(0,1), LocationId int, TemporalID int, SpatialMeasure geometry)

--Add in a while loop for countries
Declare @Country geometry
Declare @CountryName varchar (50)
Declare @MaxRunId int
Declare @RunId int = 0

Select @MaxRunId = max(Runid)
From #CentralAmCountries

While @Runid <= @MaxRunid
	Begin
		--Select the country to do the spatial check on
		Select @Country = spatialmeasure, @CountryName = LocationName
		From #CentralAmCountries
		where Runid = @Runid

		Print ('Run id: ' + cast(@runid as varchar(10)) + ' and Country: ' + @CountryName)
	
		--Find all stations that intersect the country and insert into temp table
		Insert into #CentralAmStations (LocationId, TemporalId, SpatialMeasure)
		select locationid,1, spatialmeasure
		from #CentralAmCountries
		Where Runid = @Runid
		union all
		Select  a.Locationid, a.temporalid, b.SpatialMeasure
		FROM WT_NAM_2015.dbo.NAM_2015_Grid_Daily A,
			GlobalGridDataStore..Global5by5minGrid_full B
		Where A.Locationid = B.LocationID
		and b.latitude <20
		and A.AttributeId in (1,2)
		and b.spatialmeasure.STIntersects(@Country) = 1
	
		Set @Runid = @runid + 1
	End

Create table #temp (Locationid int, temporalid int, MaxTemp int, MinTemp int, TempDifference int)

--Pivot the data out
Insert into #temp (Locationid, temporalid, MaxTemp, MinTemp, TempDifference)
Select Locationid, temporalid, cast([1] as int) as MaxTemp, cast([2] as int) as MinTemp, cast([1] as int)-cast([2] as int) as TempDifference
From (
	Select Locationid, temporalid, attributeid, intersectionmeasure
	From WT_NAM_2015.dbo.NAM_2015_grid_Daily
	Where locationid in (
		Select distinct locationid
		from #CentralAmStations
		)
	and attributeid in (1,2)
	) As SourceTable
PIVOT
	(max(intersectionmeasure) for Attributeid in ([1],[2]))
	as PivotTable
order by temporalid asc

--To get final results:
Select temporalid, count(temporalid) as MessedUpLocations
from #temp
where TempDifference <0
group by temporalid
order by temporalid asc

