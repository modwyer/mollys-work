---- To allow advanced options to be changed.
--EXEC sp_configure 'show advanced options', 1;
--GO
---- To update the currently configured value for advanced options.
--RECONFIGURE;
--GO
---- To enable the feature.
--EXEC sp_configure 'xp_cmdshell', 1;
--GO
---- To update the currently configured value for this feature.
--RECONFIGURE;
--GO

Declare @Directory varchar(2000) = 'C:\Users\mollyodwyer\Documents\isd_files_copied\2015\text_files\'
declare @Cmd varchar(7000)
declare @fileName varchar(2000)
Declare @FileOutput varchar(2000) 
declare @TextFileIn varchar(3000) 
Declare @TextFileOut varchar(3000)
declare @RunID int 
declare @MaxRunID int

IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#Directory')) 
	BEGIN 
		DROP TABLE #Directory 
	END

Create table #Directory(FullFilePath varchar(200),ShortFilePath varchar(200), RunID int identity)


set @Cmd = 'dir /B ' + @Directory
insert #Directory(ShortFilePath)
EXEC xp_cmdshell @Cmd
delete #Directory where ShortFilePath is null or ShortFilePath not like '%%201%%'

Update #Directory
set FullFilePath = @Directory+ShortFilePath


set @RunID = 1
set @MaxRunID = (select MAX(RunID) from #Directory)
while @RunID <=@MaxRunID 
	begin
		--select @fileName = FullFilePath, @FileOutput = FullFilePath from #Directory where RunID = @RunID

		--set @Cmd = 'C:\progra~1\7-Zip\7z.exe e '+@fileName+'* -o'+@FileOutput
		--exec Master..xp_cmdshell @cmd


		set @TextFileIn = (Select FullFilePath--select reverse(substring(REVERSE(FullFilePath),4,300)) 
		from #Directory where RunID = @RunID)
		set @TextFileOut =@TextFileIn

		set @Cmd = 'C: & cd /Program Files (x86)/Java/jre1.8.0_60/bin/ & java -classpath . ishJava '+@TextFilein+' '+@TextFileOut+'.txt'
		print @Cmd
		exec Master..xp_cmdshell @cmd

		set @RunID = @RunID + 1
	end
