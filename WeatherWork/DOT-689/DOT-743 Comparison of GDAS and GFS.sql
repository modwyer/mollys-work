--Compare maxtemp between GDAS and GFS
Select g.LocationID, g.TemporalID, g.MaxTemp as GDAS,f.MaxTemp as GFS, 'MaxTemp' as Attribute 
From AlternateWeatherSources..GDASData g
inner join
AlternateWeatherSources..GFSData f
on g.locationid = f.locationid
and g.temporalid = f.temporalid
where g.maxtemp <> f.maxtemp
order by Locationid, Temporalid

--Compare precip between GDAS and GFS
Select g.LocationID, g.TemporalID, g.Precip as GDAS,f.Precip as GFS, 'Precip' as Attribute 
From AlternateWeatherSources..GDASData g
inner join
AlternateWeatherSources..GFSData f
on g.locationid = f.locationid
and g.temporalid = f.temporalid
where g.Precip <> f.Precip 
order by Locationid, Temporalid

--Compare mean wind between GDAS and GFS
Select g.LocationID, g.TemporalID, round(g.MeanWind,4) as MeanWindGDAS, round(f.MeanWind,4) as MeanWindGFS, 'MeanWind' as Attribute 
From AlternateWeatherSources..GDASData g
inner join
AlternateWeatherSources..GFSData f
on g.locationid = f.locationid
and g.temporalid = f.temporalid
where round(g.MeanWind,4) <> round(f.MeanWind,4)
order by Locationid, Temporalid