--Get AWIS data to import to data3dev
declare @MinTemporalId int = 2015101200
declare @MaxTemporalId int = 2015102500

Select LocationID, left(TemporalID, 8) as Temporal, [1] as MaxTemp, [2] as MinTemp, [3] as MeanTemp, [4] as Precip, [7] as Dewpoint, [8] as MaxWind
		, [9] as MorningWind, [88] as MeanWind, [10] as MaxRH, [11] as MinRH, [12] as MeanRH
From (
	select LocationID, TemporalID, AttributeID, AttributeValue
	from WT_Processing..PointData PD
	join WT_Processing..DataImportSource DIS
		   on DIS.SourceID = PD.SourceID
		   and DIS.SourceTypeID = 200
		   and DIS.IsInterpolated = 0
	where QualityIndex > 0 and ActiveFlag = 1
	and attributeid in (1,2,3,4,7,9,88,10,11,12)
	and TemporalID between @MinTemporalId and @MaxTemporalId 
	) as A
pivot
(avg(attributevalue) for attributeid in ([1],[2],[3],[4],[7],[8],[9],[88],[10],[11],[12])) as B
order by locationid, left(temporalid,8)