/****** Script for SelectTopNRows command from SSMS  ******/
SELECT distinct a.[StationID]
     
	  ,w.LocationID
  FROM [AlternateWeatherSources].[dbo].[AWISNonInterpolateddata_1012_1025] a,
		WT_processing..master_wt_pointstations w
where a.stationid = w.awhere_stationid



--139662 records; 9973 stations
select distinct stationid, locationid
from AlternateWeatherSources..AWISNonInterpolateddata_1012_1025
where locationid is null


--8761 awis stations
Select distinct a.stationid, c.locationid
 FROM AlternateWeatherSources..AWISNonInterpolateddata_1012_1025 a,
		AlternateWeatherSources..awis_isd_wmoCrosswalk c
where a.stationid = c.awisCode
order by a.stationid


--Add locationid to awis stations
Update AlternateWeatherSources..AWISNonInterpolateddata_1012_1025
Set Locationid = c.Locationid
From AlternateWeatherSources..AWISNonInterpolateddata_1012_1025 a, AlternateWeatherSources..awis_isd_wmoCrosswalk c
Where a.stationid = c.awiscode

--Get stations with missing loc IDs; 1216
select distinct stationid--, locationid
from AlternateWeatherSources..AWISNonInterpolateddata_1012_1025
where locationid is null

Update AlternateWeatherSources..AWISNonInterpolateddata_1012_1025
Set Locationid = w.Locationid
--Select distinct a.stationid, w.locationid
from AlternateWeatherSources..AWISNonInterpolateddata_1012_1025 a, 
	WT_processing..master_wt_pointstations_lookup w
where a.stationid = w.awis_id
and a.stationid not in (
	select distinct stationid
	from AlternateWeatherSources..AWISNonInterpolateddata_1012_1025
	where locationid is not null
	)

Select *
from WT_processing..master_wt_pointstations_lookup
where awhere_stationid


SELECT distinct a.StationID, m.locationid
  FROM AlternateWeatherSources..AWISNonInterpolateddata_1012_1025 a, WT_Processing..Master_wt_pointstations_lookup m
  where a.stationid = m.awis_id
  and m.locationid not in (
	Select distinct c.locationid
	FROM AlternateWeatherSources..AWISNonInterpolateddata_1012_1025 a,
		AlternateWeatherSources..awis_isd_wmoCrosswalk c
	where a.stationid = c.awisCode
	)