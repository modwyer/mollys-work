/****** Script for SelectTopNRows command from SSMS  ******/
Create table #MadisData (RunId int identity(0,1), Station varchar(50), ObservationDate datetime, theDate int, [hour] int, Dewpoint float, Elevation int
						, Latitude float, Longitude float, MaxTemp float, MinTemp float, Precip1 float, Precip24 float, Precip3 float, Precip6 float
						, SeaLevelPressure int, Temperature float, WindSpeed float)

Insert into #MadisData (station, ObservationDate, theDate, [hour], dewpoint, elevation, latitude,longitude, MaxTemp, MinTemp, precip1, precip24, precip3
						, precip6, SeaLevelPressure, Temperature, WindSpeed)
SELECT [station]
		, cast(ob as datetime) as ObservationDate
		, left(theDate, 4) + substring(theDate, 6,2) + right(theDate,2) as theDate
		,cast([hour] as smallint) as [Hour]
		,cast(dewpoint as float)-273.15 as Dewpoint --changing to celsius from K
		,cast(elevation as int) as Elevation
		,cast(latitude as float) as Latitude
		,cast(longitude as float) as Longitude
		,cast(maxTemp24Hour as float) as MaxTemp
		,cast(minTemp24Hour as float) as MinTemp
		,cast(precip1Hour as float) as Precip1
		,cast(precip24Hour as float) as Precip24
		,cast(precip3Hour as float) as Precip3
		,cast(precip6Hour as float) as Precip6
		,cast(seaLevelPress as int) as SeaLevelPressure
		,cast(temperature as float)-273.15 as Temperature-- changing to Celsius from K
		,cast(windSpeed as float) as Windspeed
  FROM AlternateWeatherSources..MADIS_October_12_25

 --Format #MAdisData
 --Set hour column to 2 digits with a leading 0 for all records with length 1

	Update #MadisData
	Set theDate = cast(left(cast(theDate as varchar), 8) + right('0' + cast([hour] as varchar), 2) as int)


--Calculate RH

--Find Locationid


--Aggregate to a daily table
Create Table AlternateWeatherSources..Madis_1012_1025_Daily (Locationid int, Station varchar(50), ObserveDate int, Dewpoint float, Elevation int
						, Latitude float, Longitude float, MaxTemp float, MinTemp float, Precip1 float, Precip24 float, Precip3 float, Precip6 float
						, SeaLevelPressure int, Temperature float, AltMinTemp float, AltMaxTemp float, MeanWind float, MaxWind, MinRH float, MaxRH float)

Insert into AlternateWeatherSources..Madis_1012_1025_Daily (Locationid, station, ObserveDate, [hour], elevation, latitude,longitude, MaxTemp, MinTemp, precip1, precip24, precip3
						, precip6, SeaLevelPressure, Temperature, AltMinTemp, AltMaxTemp, MeanWind, MaxWind)

Select Locationid, station, theDate, [hour], max(elevation), max(latitude), max(longitude), max(MaxTemp), max(MinTemp), max(precip1), max(precip24), max(precip3)
		, max(precip6), max(SeaLevelPressure), avg(Temperature), min(Temperature) as AltMinTemp, max(Temperature) as AltMaxTemp, avg(windspeed) as MeanWind, max(windspeed) as MaxWind
From #MadisData
group by station, theDate


--Aggregate to 3 hour intervals - needs work
--0 to 3 am
Select distinct station, left(theDate, 8) as ObserveDate, 3 as [hour], max(MaxTemp) as MaxTemp, min(MinTemp) as MinTemp, sum(precip1) as Precip1
		, sum(precip24) as Precip24, sum(precip3) as Precip3, sum(precip6) as Precip6, avg(Temperature) as Temp, min(temperature) as AltMinTemp
		, max(temperature) as AltMaxTemp, avg(windspeed) as MeanWind, max(windspeed) as MaxWind
From #MadisData
Where [hour] between 0 and 3
group by station, left(theDate, 8), [hour]
order by station, left(theDate, 8), [hour]
union all
--4 to 6 am
Select Locationid, station, ObserveDate, 6 as [hour], max(MaxTemp), min(MinTemp), sum(precip1), sum(precip24), sum(precip3)
						, sum(precip6), avg(Temperature), min(AltMinTemp), max(AltMaxTemp), avg(windspeed) as MeanWind, max(windspeed) as MaxWind)
From #MadisData
Where [hour] between 4 and 6
group by station, ObserveDate, [hour]
order by station, ObserveDate, [hour]
union all
--7 to 9 am
Select Locationid, station, ObserveDate, 9 as [hour], max(MaxTemp), min(MinTemp), sum(precip1), sum(precip24), sum(precip3)
						, sum(precip6), avg(Temperature), min(AltMinTemp), max(AltMaxTemp), avg(windspeed) as MeanWind, max(windspeed) as MaxWind)
From #MadisData
Where [hour] between 7 and 9
group by station, ObserveDate, [hour]
order by station, ObserveDate, [hour]
unionall
--10 to 12 noon
Select Locationid, station, ObserveDate, 12 as [hour], max(MaxTemp), min(MinTemp), sum(precip1), sum(precip24), sum(precip3)
						, sum(precip6), avg(Temperature), min(AltMinTemp), max(AltMaxTemp), avg(windspeed) as MeanWind, max(windspeed) as MaxWind)
From #MadisData
Where [hour] between 10 and 12
group by station, ObserveDate, [hour]
order by station, ObserveDate, [hour]
union all
--1 to 3 pm
Select Locationid, station, ObserveDate, 15 as [hour], max(MaxTemp), min(MinTemp), sum(precip1), sum(precip24), sum(precip3)
						, sum(precip6), avg(Temperature), min(AltMinTemp), max(AltMaxTemp), avg(windspeed) as MeanWind, max(windspeed) as MaxWind)
From #MadisData
Where [hour] between 13 and 15
group by station, ObserveDate, [hour]
order by station, ObserveDate, [hour]
union all
--4 to 6 pm
Select Locationid, station, ObserveDate, 18 as [hour], max(MaxTemp), min(MinTemp), sum(precip1), sum(precip24), sum(precip3)
						, sum(precip6), avg(Temperature), min(AltMinTemp), max(AltMaxTemp), avg(windspeed) as MeanWind, max(windspeed) as MaxWind)
From #MadisData
Where [hour] between 16 and 18
group by station, ObserveDate, [hour]
order by station, ObserveDate, [hour]
union all
--7 to 9 pm
Select Locationid, station, ObserveDate, 21 as [hour], max(MaxTemp), min(MinTemp), sum(precip1), sum(precip24), sum(precip3)
						, sum(precip6), avg(Temperature), min(AltMinTemp), max(AltMaxTemp), avg(windspeed) as MeanWind, max(windspeed) as MaxWind)
From #MadisData
Where [hour] between 19 and 21
group by station, ObserveDate, [hour]
order by station, ObserveDate, [hour]
union all
-- 10 to 12 pm? 12 pm = 0 am, how is this handled?
Select Locationid, station, ObserveDate, 24 as [hour], max(MaxTemp), min(MinTemp), sum(precip1), sum(precip24), sum(precip3)
						, sum(precip6), avg(Temperature), min(AltMinTemp), max(AltMaxTemp), avg(windspeed) as MeanWind, max(windspeed) as MaxWind)
From #MadisData
Where [hour] between 22 and 24
group by station, ObserveDate, [hour]
order by station, ObserveDate, [hour]
