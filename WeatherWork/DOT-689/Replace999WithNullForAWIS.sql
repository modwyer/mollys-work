
--Replace 999 with Null
Update AlternateWeatherSources..AWISNonInterpolatedData_1012_1025
Set MaxWind = Null
--Select * from AlternateWeatherSources..AWISNonInterpolatedData_1012_1025
Where MaxWind = 999

Update AlternateWeatherSources..AWISNonInterpolatedData_1012_1025
Set MeanWind = Null
--Select * from AlternateWeatherSources..AWISNonInterpolatedData_1012_1025
Where MeanWind = 999

Update AlternateWeatherSources..AWISNonInterpolatedData_1012_1025
Set MinRH = Null
--Select * from AlternateWeatherSources..AWISNonInterpolatedData_1012_1025
Where MinRH = 999

Update AlternateWeatherSources..AWISNonInterpolatedData_1012_1025
Set MaxRH = Null
--Select * from AlternateWeatherSources..AWISNonInterpolatedData_1012_1025
Where MaxRH = 999

Update AlternateWeatherSources..AWISNonInterpolatedData_1012_1025
Set MinT = Null
--Select * from AlternateWeatherSources..AWISNonInterpolatedData_1012_1025
Where MinT = 999

Update AlternateWeatherSources..AWISNonInterpolatedData_1012_1025
Set MaxT = Null
--Select * from AlternateWeatherSources..AWISNonInterpolatedData_1012_1025
Where MaxT = 999