/****** Script for SelectTopNRows command from SSMS  ******/
Create table AlternateWeatherSources..AWIS_GDASData (LocationId int, TemporalId int, AWISMinTemp real, AWISMaxTemp real, AWISPrecip real, AWISMaxWind real, AWISMeanWind real, AWISMinRH real, AWISMaxRH real
													, GDASMinTemp real, GDASMaxTemp real, GDASPrecip real, GDASMaxWind real, GDASMeanWind real, GDASMinRH real, GDASMaxRH real)
Insert into AlternateWeatherSources..AWIS_GDASData (LocationId, TemporalId, AWISMinTemp, AWISMaxTemp, AWISPrecip, AWISMaxWind, AWISMeanWind, AWISMinRH, AWISMaxRH
													, GDASMinTemp, GDASMaxTemp, GDASPrecip, GDASMaxWind, GDASMeanWind, GDASMinRH, GDASMaxRH)
SELECT A.LocationID
      ,left(A.TemporalID,8)
      , A.MinT as AWISMinTemp, A.MaxT as AWISMaxTemp, A.Precip as AWISPrecip, A.MaxWind as AWISMaxWind, A.MeanWind as AWISMeanWind, A.MinRH as AWISMinRH, A.MaxRH as AWISMaxRH
	  ,G.MinTemp as GDASMinTemp, G.MaxTemp as GDASMaxTemp, G.Precip as GDASPrecip, G.MaxWind as GDASMaxWind, G.MeanWind as GDASMeanWind, G.MinRH as GDASMinRH, G.MaxRH as GDASMaxRH
  FROM AlternateWeatherSources..[AWISNonInterpolatedData_1012_1025] A
  join
  AlternateWeatherSources..GDASData G
  on a.locationid = g.locationid
  and left(a.temporalid,8) = g.temporalid
  ORDER BY locationID, left(a.TEMPORALid , 8)