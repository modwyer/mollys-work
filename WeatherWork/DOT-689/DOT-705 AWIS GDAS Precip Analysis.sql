/****** Script for SelectTopNRows command from SSMS  ******/
SELECT a.LocationID, a.Temporal, a.Precip as AWIS
		, g.precip as GDAS
		,a.precip - g.precip as PrecipDiff
  FROM AlternateWeatherSources..[AWISNonInterpolatedData_10.12-10.25] a, AlternateWeatherSources..GDASData g
  where a.locationid = g.locationid
  and a.temporal = g.temporalid
  and a.precip - g.precip <>0
  order by a.locationid, a.temporal

--Find Absolute max difference and Absolute min difference, standard dev for each station for precip
SELECT a.LocationID
		, round(abs(avg(a.precip - g.precip)),3) as AbsAvgPrecipDiff
		, round(abs(max(a.precip - g.precip)),3) as MaxPrecipDiff
		, round(abs(min(a.precip - g.precip)),3) as MinPrecipDiff
		, round(stdevp(a.precip - g.precip),1) as StandardDeviation
  FROM AlternateWeatherSources..[AWISNonInterpolatedData_10.12-10.25] a, AlternateWeatherSources..GDASData g
  where a.locationid = g.locationid
  and a.temporal = g.temporalid
  group by a.locationid
  order by a.locationid