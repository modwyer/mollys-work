--Pivot the GDAS data
drop table AlternateWeatherSources..GDASHourly
drop table AlternateWeatherSources..GFSHourly

Create table AlternateWeatherSources..GDASHourly (LocationId int, TemporalId int, MaxTemp real, MinTemp real, Precip real
												, MeanWind real, MaxWind real, MinRH real, MaxRH real)
Insert into AlternateWeatherSources..GDASHourly (LocationId, TemporalId, MaxTemp, MinTemp, Precip, MeanWind, MaxWind, MinRH, MaxRH)
Select a.Locationid, left(g.temporalid, 10) as Temporalid
		, Max(case when g.AttributeID = 1 then g.gdas_value else 0 end) as MaxTemp
		, Min(case when g.AttributeID = 2 then g.gdas_value else 0 end) as MinTemp
		, sum(case when g.AttributeID = 4 then g.gdas_value else 0 end) as Precip
		, avg(case when g.attributeID = 88 then g.gdas_value else 0 end) as MeanWind
		, max(case when g.attributeID = 88 then g.gdas_value else 0 end) as MaxWind
		, min(case when g.AttributeID = 12 then g.gdas_value else 0 end) as MinRH
		, max(case when g.AttributeID = 12 then g.gdas_value else 0 end) as MaxRH
From AlternateWeatherSources..AWIS_GDAS_Metadata a, AlternateWeatherSources..GDAS_GFSComparison g
where a.gridx = g.x
and a.gridy = g.y
group by locationid, left(temporalid,10)
order by locationid, left(temporalid,10)

--Pivot the GFS data
Create table AlternateWeatherSources..GFSHourly (LocationId int, TemporalId int, MaxTemp real, MinTemp real, Precip real
												, MeanWind real, MaxWind real, MinRH real, MaxRH real)
Insert into AlternateWeatherSources..GFSHourly (LocationId, TemporalId, MaxTemp, MinTemp, Precip, MeanWind, MaxWind, MinRH, MaxRH)
Select a.Locationid, left(g.temporalid, 10) as Temporalid
		, Max(case when g.AttributeID = 1 then g.gfs_value else 0 end) as MaxTemp
		, Min(case when g.AttributeID = 2 then g.gfs_value else 0 end) as MinTemp
		, sum(case when g.AttributeID = 4 then g.gfs_value else 0 end) as Precip
		, avg(case when g.attributeID = 88 then g.gfs_value else 0 end) as MeanWind
		, max(case when g.attributeID = 88 then g.gfs_value else 0 end) as MaxWind
		, min(case when g.AttributeID = 12 then g.gfs_value else 0 end) as MinRH
		, max(case when g.AttributeID = 12 then g.gfs_value else 0 end) as MaxRH
From AlternateWeatherSources..AWIS_GDAS_Metadata a, AlternateWeatherSources..GDAS_GFSComparison g
where a.gridx = g.x
and a.gridy = g.y
group by locationid, left(temporalid, 10)
order by locationid, left(temporalid, 10)

/*****Get morning values and update tables NOT NEEDED NOW ***/

--Get morning wind values for GDAS, 6-noon - not needed
Create table #MorningWind (Locationid int, Temporalid int, MorningWind real)
Insert into #MorningWind (Locationid, Temporalid, MorningWind)
Select a.Locationid, left(g.temporalid, 8) as Temporalid
		, avg(case when g.attributeID = 88 then g.gdas_value else 0 end) as MorningWind
From AlternateWeatherSources..AWIS_GDAS_Metadata a, AlternateWeatherSources..GDAS_GFSComparison g
where a.gridx = g.x
and a.gridy = g.y
and right(g.temporalid, 2) between 6 and 12
group by locationid, left(temporalid,8)
order by locationid, left(temporalid,8)

Update AlternateWeatherSources..GDASHourly 
Set MorningWind = m.MorningWind
From #MorningWind m, AlternateWeatherSources..GDASHourly g
where g.locationid = m.locationid
and g.temporalid = m.temporalid

--Get morning wind values for GFS, 6-noon - not needed
truncate table #MorningWind 
Insert into #MorningWind (Locationid, Temporalid, MorningWind)
Select a.Locationid, left(g.temporalid, 8) as Temporalid
		, avg(case when g.attributeID = 88 then g.gfs_value else 0 end) as MorningWind
From AlternateWeatherSources..AWIS_GDAS_Metadata a, AlternateWeatherSources..GDAS_GFSComparison g
where a.gridx = g.x
and a.gridy = g.y
and right(g.temporalid, 2) between 6 and 12
group by locationid, left(temporalid,8)
order by locationid, left(temporalid,8)

Update AlternateWeatherSources..GFSHourly 
Set MorningWind = m.morningwind
From #MorningWind m, AlternateWeatherSources..GFSHourly g
where g.locationid = m.locationid
and g.temporalid = m.temporalid


--Get difference between GDAS and GFS and put in new table (GDAS - GFS)
Create table AlternateWeatherSources..GDAS_GFSDifferences (LocationId int, TemporalId int, MinTempDiff real, MaxTempDiff real, PrecipDiff real, MaxWindDiff real, MeanWindDiff real, MinRHDiff real, MaxRHDiff real)
Insert into AlternateWeatherSources..GDAS_GFSDifferences (LocationId, TemporalId, MinTempDiff, MaxTempDiff, PrecipDiff, MaxWindDiff, MeanWindDiff, MinRHDiff, MaxRHDiff)
Select d.LocationId, d.TemporalId, d.MinTemp-f.MinTemp as MinTempDiff, d.MaxTemp-f.maxtemp as MaxTempDiff, d.precip-f.precip as PrecipDiff
		, d.maxwind-f.maxwind as MaxWindDiff, d.meanwind-f.meanwind as MeanWindDiff, d.minrh-f.minrh as MinRHDiff, d.maxrh-f.maxrh as MaxRHDiff
From AlternateWeatherSources..GDASHourly d, AlternateWeatherSources..GFSHourly f
where d.locationid = f.locationid
and d.temporalid = f.temporalid

--Get absolute averages for each attribute for each location
truncate table AlternateWeatherSources..GDAS_GFS_FinalComparison 
Create table AlternateWeatherSources..GDAS_GFS_FinalComparison 
			(LocationId int, MinTempMin real, MinTempMax real, MinTempAvg real, MinTempSTD real
			, MaxTempMin real, MaxTempMax real, MaxTempAvg real, MaxTempSTD real
			, PrecipMin real, PrecipMax real, PrecipAvg real, PrecipSTD real
			, MaxWindMin real, MaxWindMax real, MaxWindAvg real, MaxWindSTD real
			, MeanWindMin real, MeanWindMax real, MeanWindAvg real, MeanWindSTD real
			, MinRHMin real, MinRHMax real, MinRHAvg real, MinRHSTD real
			, MaxRHMin real, MaxRHMax real, MaxRHAvg real, MaxRHSTD real)
Insert into AlternateWeatherSources..GDAS_GFS_FinalComparison (LocationId, MinTempMin, MinTempMax, MinTempAvg, MinTempSTD
			, MaxTempMin, MaxTempMax, MaxTempAvg, MaxTempSTD
			, PrecipMin, PrecipMax, PrecipAvg, PrecipSTD
			, MaxWindMin, MaxWindMax, MaxWindAvg, MaxWindSTD
			, MeanWindMin, MeanWindMax, MeanWindAvg, MeanWindSTD
			, MinRHMin, MinRHMax, MinRHAvg, MinRHSTD
			, MaxRHMin, MaxRHMax, MaxRHAvg, MaxRHSTD)
SELECT LocationId
      ,round(abs(min(MinTempDiff)), 2) as MinTempMin
	  ,round(abs(max(MinTempDiff)), 2) as MinTempMax
	  ,round(abs(avg(MinTempDiff)), 2) as MinTempAvg
	  ,round(stdevp(MinTempDiff), 2) as MinTempSTD
	  ,round(abs(min(MaxTempDiff)), 2) as MaxTempMin
	  ,round(abs(max(MaxTempDiff)), 2) as MaxTempMax
	  ,round(abs(avg(MaxTempDiff)), 2) as MaxTempAvg
	  ,round(stdevp(MaxTempDiff), 2) as MaxTempSTD
      ,round(abs(min(PrecipDiff)), 2) as PrecipMin
	  ,round(abs(max(PrecipDiff)), 2) as PrecipMax
	  ,round(abs(avg(PrecipDiff)), 2) as PrecipAvg
	  ,round(stdevp(PrecipDiff), 2) as PrecipSTD
      ,round(abs(min(MaxWindDiff)), 2) as MaxWindMin
	  ,round(abs(max(MaxWindDiff)), 2) as MaxWindMax
	  ,round(abs(avg(MaxWindDiff)), 2) as MaxWindAvg
	  ,round(stdevp(MaxWindDiff), 2) as MaxWindSTD
	  ,round(abs(min(MeanWindDiff)), 2) as MeanWindMin
	  ,round(abs(max(MeanWindDiff)), 2) as MeanWindMax
	  ,round(abs(avg(MeanWindDiff)), 2) as MeanWindAvg
	  ,round(stdevp(MeanWindDiff), 2) as MeanWindSTD
      ,round(abs(min(MinRHDiff)), 2) as MinRHMin
	  ,round(abs(max(MinRHDiff)), 2) as MinRHMax
	  ,round(abs(avg(MinRHDiff)), 2) as MinRHAvg
	  ,round(stdevp(MinRHDiff), 2) as MinRHSTD
	  ,round(abs(min(MaxRHDiff)), 2) as MaxRHMin
	  ,round(abs(max(MaxRHDiff)), 2) as MaxRHMax
	  ,round(abs(avg(MaxRHDiff)), 2) as MaxRHAvg
	  ,round(stdevp(MaxRHDiff), 2) as MaxRHSTD
  FROM AlternateWeatherSources..GDAS_GFSDifferences 
  Group by Locationid

Select * from AlternateWeatherSources..GDAS_GFS_FinalComparison
-------------------------------------------------------------------
--------------------------------------------Later analysis




--Get absolute differences between AWIS and GDAS data
Create table #AbsoluteAvgDifference (LocationID varchar(10), TemporalId int, AvgMinTempDiff real, AbsMinLowTempDiff real, AbsMinTopTempDiff real, MinTempSTD real
									, AvgMaxTempDiff real, AbsMaxLowTempDiff real, AbsMaxTopTempDiff real, MaxTempSTD real
									, AvgPrecipDiff real, AbsPrecipMinDiff real, AbsPrecipMaxDiff real, PrecipSTD real
									, AvgMeanWindDiff real, AbsMeanWindMinDiff real, AbsMeanWindMaxDiff real, MeanWindSTD real
									, AvgMaxWindDiff real, AbsMaxWindLowDiff real, AbsMaxWindTopDiff real, MaxWindSTD real
									, AvgMorningWindDiff real, AbsMorningWindLowDiff real, AbsMorningWindTopDiff real, MorningWindSTD real
									, AvgMinRHDiff real, AbsMinRHMinDiff real, AbsMinRHMaxDiff real, MinRHSTD real
									, AvgMaxRHDiff real, AbsMaxRHMinDiff real, AbsMaxRHMaxDiff real, MaxRHSTD real)
Insert into #AbsoluteAvgDifference (StationId, YYYYMMDD, AvgMinTempDiff, AbsMinLowTempDiff, AbsMinTopTempDiff, MinTempSTD
									, AvgMaxTempDiff, AbsMaxLowTempDiff, AbsMaxTopTempDiff, MaxTempSTD
									, AvgPrecipDiff, AbsPrecipMinDiff, AbsPrecipMaxDiff, PrecipSTD
									, AvgMeanWindDiff, AbsMeanWindMinDiff, AbsMeanWindMaxDiff, MeanWindSTD
									, AvgMaxWindDiff, AbsMaxWindLowDiff, AbsMaxWindTopDiff, MaxWindSTD
									, AvgMorningWindDiff, AbsMorningWindLowDiff, AbsMorningWindTopDiff, MorningWindSTD
									, AvgMinRHDiff, AbsMinRHMinDiff, AbsMinRHMaxDiff, MinRHSTD
									, AvgMaxRHDiff, AbsMaxRHMinDiff, AbsMaxRHMaxDiff, MaxRHSTD)
Select	LocationId, TemporalId, AWISMinTemp-GDASMinTemp as MinTempDiff, abs(min(MinTemp)) as AbsMinLowTempDiff, abs(max(MinTemp)) as AbsMinTopTempDiff, stdevp(MinTemp) as MinTempSTD
		,abs(avg(MaxTemp)) as AvgMaxTempDiff, abs(min(MaxTemp)) as AbsMaxLowTempDiff, abs(max(MaxTemp)) as AbsMaxTopTempDiff, stdevp(MaxTemp) as MaxTempSTD
		,abs(avg(Precip)) as AvgPrecipDiff, abs(min(Precip)) as AbsPrecipMinDiff, abs(max(Precip)) as AbsPrecipMaxDiff, stdevp(Precip) as PrecipSTD
		,abs(avg(MeanWind)) as AvgMeanWindDiff, abs(min(MeanWind)) as AbsMeanWindMinDiff, abs(max(MeanWind)) as AbsMeanWindMaxDiff, stdevp(MeanWind) as MeanWindSTD
		,abs(avg(MaxWind)) as AvgMaxWindDiff, abs(min(MaxWind)) as AbsMaxWindLowDiff, abs(max(MaxWind)) as AbsMaxWindTopDiff, stdevp(MaxWind) as MaxWindSTD
		,abs(avg(MorningWind)) as AvgMorningWindDiff, abs(min(MorningWind)) as AbsMorningWindLowDiff, abs(max(MorningWind)) as AbsMorningWindTopDiff, stdevp(MorningWind) as MorningWindSTD
		,abs(avg(MinRH)) as AvgMinRHDiff, abs(min(MinRH)) as AbsMinRHMinDiff, abs(max(MinRH)) as AbsMinRHMaxDiff, stdevp(MinRH) as MinRHSTD
		,abs(avg(MaxRH)) as AvgMaxRHDiff, abs(min(MaxRH)) as AbsMaxRHMinDiff, abs(max(MaxRH)) as AbsMaxRHMaxDiff, stdevp(MaxRH) as MaxRHSTD
From #Difference
Group by stationid, YYYYMMDD
Order by stationid, YYYYMMDD
