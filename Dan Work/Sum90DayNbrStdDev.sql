Create view Sum90DayForMinT
as

Select top 100 
	w2.locationid
	, w2.temporalid
	, w2.DateReformat
	, w2.attributeid
	, w2.StdDeviationOfValues
	, NbrOfStandardDeviation
	, Sum90DayNbrStdDeviationsForMinT = case when w2.DateReformat < convert(datetime, '2008-04-01') then null else NbrOfStandardDeviation + coalesce(
			(
				Select sum(NbrOfStandardDeviation)
				From DifferenceFromAverage as w1
				Where dateadd(d, -90, w1.DateReformat) < w2.DateReformat
				and w1.locationid = w2.locationid
				and w2.attributeid = 2
				and w2.attributeid = w1.attributeid
				),0
			)
			end
	
from DifferenceFromAverage as w2
Where Attributeid =1
order by locationid, attributeid, temporalid




Create view Sum90DayForMaxT
as

Select top 100 
	w2.locationid
	, w2.temporalid
	, w2.DateReformat
	, w2.attributeid
	, w2.StdDeviationOfValues
	, NbrOfStandardDeviation
	, Sum90DayNbrStdDeviationsForMaxT = case when w2.DateReformat < convert(datetime, '2008-04-01') then Null else NbrOfStandardDeviation + coalesce(
			(
				Select sum(NbrOfStandardDeviation)
				From DifferenceFromAverage as w1
				Where dateadd(dd, -90, w1.DateReformat) < w2.DateReformat
				and w1.locationid = w2.locationid
				and w2.attributeid = 1
				and w2.attributeid = w1.attributeid
				),0
			)
			end
	
from DifferenceFromAverage as w2
Where Attributeid =1
order by locationid, attributeid, temporalid



Create view Sum90DayForPrec
as

Select top 100 
	w2.locationid
	, w2.temporalid
	, w2.DateReformat
	, w2.attributeid
	, w2.StdDeviationOfValues
	, NbrOfStandardDeviation
	, Sum90DayNbrStdDeviationsForPrec = case when w2.DateReformat < convert(datetime, '2008-04-01') then null else NbrOfStandardDeviation + coalesce(
			(
				Select sum(NbrOfStandardDeviation)
				From DifferenceFromAverage as w1
				Where dateadd(d, -90, w1.DateReformat) < w2.DateReformat
				and w1.locationid = w2.locationid
				and w2.attributeid = 4
				and w2.attributeid = w1.attributeid
				),0
			)
			end
	
from DifferenceFromAverage as w2
Where Attributeid =1
order by locationid, attributeid, temporalid