--make views to join tables


--Gets difference between value and average
Select Top 10000 w1.Locationid
		--, w1.AttributeId
		, w1.TemporalId
		, (w1.IntersectionMeasure - w2.AverageOfValues) as AvgDiff
		, w2.StdDeviationOfValues as NbrStdDevForValue
From vWeatherOriginalAndAvg w1
inner join
WeatherMonthlyAveragesAndStdDev w2
on floor((w1.TemporalId % 1000000)/10000) = w2.WeatherMonth
and w1.LocationId = w2.LocationId
--and w1.attributeid=w2.attributeid
group by w1.locationid, w1.temporalid
order by w1.locationid, w1.temporalid


Where floor((P.TemporalId % 1000000)/10000) = W.WeatherMonth
and P.AttributeId=W.AttributeId
and P.LocationId = W.LocationId
Order by LocationId, TemporalId, AttributeId