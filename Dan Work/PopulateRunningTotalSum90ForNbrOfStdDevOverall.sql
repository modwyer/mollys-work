Declare @DistinctRegionAttributes Table (RunId int identity(1,1), RegionId int, AttributeId int, primary key (RegionId, AttributeId))
Declare @MinLocationId int, @MaxLocationId int, @ActiveRegionId int, @ActiveAttributeId int
Declare @ActiveRunId int, @MaxRunId int

IF OBJECT_ID('tempdb..#Rows') IS NOT NULL DROP TABLE #Rows
Create Table #Rows
	(AttributeId int, LocationId int, TemporalId int, NbrOfStandardDeviation float, RowNbr int, primary key (LocationId, TemporalId, AttributeId))

INSERT INTO @DistinctRegionAttributes
(regionId, AttributeId)
Select Distinct  left(LocationId, 1) as RegionId, AttributeId
From DifferenceFromAverage

Select @ActiveRunId = min(RunId), @MaxRunId = max(runId)
From @DistinctRegionAttributes

WHILE @ActiveRunId <= @MaxRunId
BEGIN
	Select @ActiveRegionId = RegionId, @ActiveAttributeId = AttributeId
	From @DistinctRegionAttributes
	Where RunId = @ActiveRunId
	
	Set @MinLocationID = @ActiveRegionId * 10000000
	Set @MaxLocationID = (@ActiveRegionId * 10000000) + 9999999

	Truncate Table #Rows
	INSERT INTO #Rows
	(AttributeId, LocationId, TemporalId, NbrOfStandardDeviation, RowNbr)
	SELECT @ActiveAttributeId as AttributeId, LocationId, TemporalId, NbrOfStandardDeviation,
	ROW_NUMBER() OVER(PARTITION BY LocationId ORDER BY TemporalId ASC) AS RowNbr
	FROM DifferenceFromAverage
	Where AttributeId = @ActiveAttributeId
	and LocationId between @MinLocationID and @MaxLocationID
	ORDER BY LocationId, TemporalId


    MERGE RunningTotalSum90ForNbrOfStdDev AS target
    USING (
	select LocationId, TemporalId, AttributeId, Max(Sum90) as Sum90
	 from #Rows a cross apply (
	  select Sum(NbrOfStandardDeviation) Sum90 
	  from #Rows
	  where RowNbr between a.RowNbr - 89 and a.RowNbr-- + 90
	  and RowNbr >= 91
	  and LocationId = a.LocationId
	) TabSum90 
	Group by LocationId, TemporalId, AttributeId --Put this in because there were some duplicates for some reason
	Having Max(Sum90) is not null
    ) AS source 
    ON (target.LocationId = source.LocationId 
    and target.TemporalId = source.TemporalId
    and target.AttributeId = source.AttributeId)
	WHEN NOT MATCHED THEN
		INSERT (LocationId, TemporalId, AttributeId, Sum90)
		VALUES (source.LocationId, source.TemporalId, source.AttributeId, source.Sum90);	
	
	set @ActiveRunId = @ActiveRunId + 1
END

truncate Table [dbo].[RunningTotalSum90ForNbrOfStdDevOverall]
INSERT INTO [dbo].[RunningTotalSum90ForNbrOfStdDevOverall]
           ([TemporalId]
           ,[AttributeId]
           ,[Sum90])
SELECT [TemporalId]
      ,[AttributeId]
      ,Sum([Sum90]) as GlobalSum90
  FROM [TestDatabase].[dbo].[RunningTotalSum90ForNbrOfStdDev]
  GROUP BY TemporalId, AttributeId



