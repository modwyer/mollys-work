USE [TestDatabase]
GO

/****** Object:  View [dbo].[DifferenceFromAverage]    Script Date: 5/7/2015 1:16:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER View [dbo].[DifferenceFromAverage]
as 
Select Locationid
		, AttributeId
		, TemporalId
		, DateReformat
		, IntersectionMeasure
		, AverageOfValues
		,StdDeviationOfValues
		, (IntersectionMeasure - AverageOfValues) as DiffFromAverage
		,case when StdDeviationOfValues = 0 then 0 else (IntersectionMeasure - AverageOfValues)/StdDeviationOfValues end as NbrOfStandardDeviation
From vWeatherOriginalAndAvg 
GO


