Select *
from universe..locations
where locationtypeid = 23
and (charindex('Mali', locationname)>0 
or charindex('Niger',locationname)>0)


Select locationid, locationname
from universe..locations
where locationtypeid = 65
and charindex(', Niger',locationname)>0
and charindex('NIgeria',locationname)=0

90004972	Diffa, Niger
90004978	Zinder, Niger


Select locationid, locationname
from universe..locations
where locationtypeid = 65
and charindex(', Mali',locationname)>0
and charindex('Somali',locationname)=0

90004590	Kayes
90004592	Koulikoro, Mali
90004594	S�gou, Mali
90004593	Mopti, Mali
90004595	Sikasso, Mali
/*
-45370	Mali
-45292	Niger

*/

Create table #CountryAdmin1 (CountryName varchar(50), CountryLocationId int, Admin1Name varchar(50), Admin1LocationId int)
--truncate table #CountryAdmin1
Insert into #CountryAdmin1 (CountryName, CountryLocationId, Admin1Name, Admin1LocationId)
Select l1.locationname as CountryName, lr.locationid as CountryLocationId
		, l2.locationname as Admin1Name, lr.relatedlocationid as Admin1LocationId
from universe..locationrelationships lr
inner join
universe..locations l1 --country
on l1.locationid = lr.locationid
inner join
universe..locations l2 --admin1
on lr.relatedlocationid = l2.locationid
where lr.locationtypeid = 23
and lr.locationid in (-45370,-45292)
and lr.relatedlocationid in (90004972, 90004978, 90004590, 90004592, 90004594, 90004593, 90004595)
and lr.relatedlocationtypeid = 65
and l1.locationtypeid = 23
and l2.locationtypeid = 65


Create table CommodityStagingData..GridsInMaliAndNiger 
	(CountryName varchar(50), CountryLocationId int, Admin1Name varchar(50), Admin1LocationId int, GridLocationId int)

Insert into CommodityStagingData..GridsInMaliAndNiger (CountryName, CountryLocationId, Admin1Name, Admin1LocationId, GridLocationId)
Select c.CountryName, c.CountryLocationId, c.Admin1Name, c.Admin1LocationId
		, lr.RelatedlocationId as GridLocationId
from #CountryAdmin1 c
inner join
universe..locationrelationships lr
on c.Admin1locationid = lr.locationid
where lr.locationtypeid = 65
and lr.locationid in (
	Select distinct Admin1Locationid
	from #CountryAdmin1
	)
and lr.relatedlocationtypeid = 8


--double check results spatially
truncate table #SpatialComparison
Create table #SpatialComparison (Locationid int)
Insert into #SpatialComparison (Locationid)
Select distinct Admin1locationid
From CommodityStagingData..GridsInMaliAndNiger
union all
Select distinct GridLocationid
From CommodityStagingData..GridsInMaliAndNiger

Select s.Locationid, l.locationname, l.spatialmeasure
from #SpatialComparison s
inner join
Universe..locations l
on s.locationid = l.locationid
order by l.locationname


Select LocationId, SpatialMeasure
From Universe.dbo.Locations
Where LocationId in (
Select distinct Admin1locationid
From CommodityStagingData..GridsInMaliAndNiger
)
union all
Select Round(G.GridLocationid,-3) as ShortId, awmDatabase.dbo.UnionAggregate(L.SpatialMeasure)
From CommodityStagingData..GridsInMaliAndNiger G, Universe.dbo.Locations L
Where G.GridLocationId = L.LocationId
Group by Round(GridLocationid,-3)

