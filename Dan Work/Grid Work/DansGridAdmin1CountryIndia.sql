Select *
from universe..locations
where locationtypeid = 23
and charindex('India', locationname)>0 
and charindex('Indian',locationname)=0


Select locationid, locationname, spatialmeasure
from universe..locations
where locationtypeid = 65
and charindex('India', locationname)>0 
and charindex('Indian',locationname)=0

90004116	Andhra Pradesh, India
90004123    Orissa, India
90004135	Bihar, India


/*
-45498	India
*/

Create table #CountryAdmin1 (CountryName varchar(50), CountryLocationId int, Admin1Name varchar(50), Admin1LocationId int)
--truncate table #CountryAdmin1
Insert into #CountryAdmin1 (CountryName, CountryLocationId, Admin1Name, Admin1LocationId)
Select l1.locationname as CountryName, lr.locationid as CountryLocationId
		, l2.locationname as Admin1Name, lr.relatedlocationid as Admin1LocationId
from universe..locationrelationships lr
inner join
universe..locations l1 --country
on l1.locationid = lr.locationid
inner join
universe..locations l2 --admin1
on lr.relatedlocationid = l2.locationid
where lr.locationtypeid = 23
and lr.locationid = -45498
and lr.relatedlocationid in (90004116, 90004123, 90004135)
and lr.relatedlocationtypeid = 65
and l1.locationtypeid = 23
and l2.locationtypeid = 65


Create table CommodityStagingData..GridsInIndia
	(CountryName varchar(50), CountryLocationId int, Admin1Name varchar(50), Admin1LocationId int, GridLocationId int)

Insert into CommodityStagingData..GridsInIndia (CountryName, CountryLocationId, Admin1Name, Admin1LocationId, GridLocationId)
Select c.CountryName, c.CountryLocationId, c.Admin1Name, c.Admin1LocationId
		, lr.RelatedlocationId as GridLocationId
from #CountryAdmin1 c
inner join
universe..locationrelationships lr
on c.Admin1locationid = lr.locationid
where lr.locationtypeid = 65
and lr.locationid in (
	Select distinct Admin1Locationid
	from #CountryAdmin1
	)
and lr.relatedlocationtypeid = 8


--double check results spatially
truncate table #SpatialComparison
Create table #SpatialComparison (Locationid int)
Insert into #SpatialComparison (Locationid)
Select distinct Admin1locationid
From CommodityStagingData..GridsInIndia
union all
Select distinct GridLocationid
From CommodityStagingData..GridsInIndia

Select s.Locationid, l.locationname, l.spatialmeasure
from #SpatialComparison s
inner join
Universe..locations l
on s.locationid = l.locationid
order by l.locationname


Select LocationId, SpatialMeasure
From Universe.dbo.Locations
Where LocationId in (
Select distinct Admin1locationid
From CommodityStagingData..GridsInIndia
)
union all
Select Round(G.GridLocationid,-3) as ShortId, awmDatabase.dbo.UnionAggregate(L.SpatialMeasure)
From CommodityStagingData..GridsInIndia G, Universe.dbo.Locations L
Where G.GridLocationId = L.LocationId
Group by Round(GridLocationid,-3)

