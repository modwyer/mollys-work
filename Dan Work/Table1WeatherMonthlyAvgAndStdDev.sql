
Use TestDatabase;

--Table 1
--LocationId, Month, AverageOfValues, StdDeviationOfValues
--Get Average Per Month over all the values within that month over the years (2008-2014) (~210 values)

--Create the table
Create Table TestDatabase.dbo.WeatherMonthlyAveragesAndStdDev
	(LocationId int, 
	AttributeId int,
	WeatherMonth int,
	AverageOfValues decimal(7,4),
	StdDeviationOfValues decimal(7,4),
	StdDeviationOfValuesP decimal (7,4)
	)

--Populate the table
Insert into WeatherMonthlyAveragesAndStdDev
Select Locationid
		, AttributeId
		, floor((TemporalId % 1000000)/10000) as WeatherMonth
		, Avg(IntersectionMeasure) as AverageOfValues
		, stdev(IntersectionMeasure) as StdDevOfValues
		, stdevP(IntersectionMeasure) as StdDevOfValuesP
From ProxyWeatherDataWithinSugarCaneAreas
Group by  LocationId, AttributeId, floor((TemporalId % 1000000)/10000)
Order by LocationId, AttributeId, WeatherMonth

--Check that table is populated
Select top 1000  
LocationId,
AttributeId,
WeatherMonth,
AverageOfValues,
StdDeviationOfValues,
StdDeviationOfValuesP
from TestDatabase..WeatherMonthlyAveragesAndStdDev

--drop table if needed
Drop table WeatherAverages



select floor((2015111300 % 1000000) / 10000)