USE [TestDatabase]
GO

/****** Object:  View [dbo].[vWeatherOriginalAndAvg]    Script Date: 5/7/2015 1:17:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER view [dbo].[vWeatherOriginalAndAvg]
as
Select --Top 10000 
		P.Locationid
		, P.AttributeId
		, P.TemporalId
		,P.DateReformat
		, P.IntersectionMeasure
		, W.AverageOfValues
		, W.StdDeviationOfValues
		,W.WeatherMonth
From ProxyWeatherDataWithinSugarCaneAreas P,
 WeatherMonthlyAveragesAndStdDev W
Where floor((P.TemporalId % 1000000)/10000) = W.WeatherMonth
and P.AttributeId=W.AttributeId
and P.LocationId = W.LocationId
--Order by LocationId, TemporalId, AttributeId





GO


