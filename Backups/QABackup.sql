/* QA Backup */
BACKUP DATABASE [ICRISAT_StarCluster_Data] TO  DISK = N'D:\Backups\icrisatMolly.bak' WITH  COPY_ONLY, NOFORMAT, INIT,  NAME = N'Universe-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO

BACKUP DATABASE [Universe] TO  DISK = N'D:\Backups\UniverseMolly.bak' WITH  COPY_ONLY, NOFORMAT, INIT,  NAME = N'Universe-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO

BACKUP DATABASE [UserData_StarCluster_Data] TO  DISK = N'D:\Backups\UserdataMolly.bak' WITH  COPY_ONLY, NOFORMAT, INIT,  NAME = N'Universe-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO

BACKUP DATABASE [WorldBank_StarCluster_Data] TO  DISK = N'F:\SQLDB\Platform\MSSQL10.PLATFORM\MSSQL\Backup\WorldbankMolly.bak' WITH  COPY_ONLY, NOFORMAT, INIT,  NAME = N'Universe-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO


