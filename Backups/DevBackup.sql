BACKUP DATABASE [Universe] TO  DISK = N'G:\Backups\universe.bak' WITH  COPY_ONLY, NOFORMAT, INIT,  NAME = N'Universe-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO

BACKUP DATABASE [WorldBank_StarCluster_Data] TO  DISK = N'G:\Backups\worldbank.bak' WITH  COPY_ONLY, NOFORMAT, INIT,  NAME = N'Universe-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO

BACKUP DATABASE [UserData_StarCluster_Data] TO  DISK = N'G:\Backups\userdata.bak' WITH  COPY_ONLY, NOFORMAT, INIT,  NAME = N'UserData_StarCluster_Data-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO

BACKUP DATABASE [ICRISAT_StarCluster_Data] TO  DISK = N'G:\Backups\icrisat.bak' WITH  COPY_ONLY, NOFORMAT, INIT,  NAME = N'ICRISAT_StarCluster_Data-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO
