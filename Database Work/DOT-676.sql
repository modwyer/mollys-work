Declare @CountryID int = -988223
Declare @StatePoly geometry	

--Get country polygon
Select @StatePoly = SpatialMeasure
from Universe..locations
where locationtypeid = 65
and charindex('Washington, United States',locationname)>0

--Select @StatePoly

--Get bounding box for Washington
Declare @Env Geometry
Declare @LocationShape geometry
Declare @LocationId int = 90006323 --washington state
Declare @MinLong as int
	Declare @MaxLong as int
	Declare @MinLat as int
	Declare @MaxLat as int

Select @Env = Spatialmeasure.STEnvelope(), @LocationShape = SpatialMeasure
From Universe..Locations
Where LocationId = @LocationId

--Get grids that intersect the state 
Select l.spatialmeasure, l.locationid, l.locationname
from universe..locations l
where l.locationtypeid = 8
and l.spatialmeasure.STIntersects(@Env) =1
union all
Select spatialmeasure, locationid, locationname
from universe..locations
where locationid = @Locationid

--Get grids within a specific bounding box
declare @MinGridX int
Declare @MaxGridX int
Declare @MinGridY int
Declare @MaxGridY int
Declare @includeLand int

set @MinGridX = -1498
set @MaxGridX  = -1465
set @MinGridY = 547
set @MaxGridY = 589
set @includeLand = 0

--create temp table to hold locationids from my box
Create table #GridsToCheckForParent (Locationid int, Latitude float, Longitude float, Elevation int, SpatialMeasure geometry)
Insert into #GridsToCheckForParent (Locationid, Latitude, Longitude, Elevation, SpatialMeasure)
select 
	LocationID,
	Latitude,
	Longitude,
	Elevation,
	SpatialMeasure
from GlobalGridDataStore..Global5by5MinGrid_full
where GridX between @MinGridX and @MaxGridX
and GridY between @MinGridY and @MaxGridY
and LocationID <(case when @IncludeLand is not null then 70000000 else 79999999 end) 

--Check if locationIds have a parent in the relationship table
Select *
from Universe..LocationRelationships
where relationshiptypeid = 1
and relatedlocationid in (
	Select distinct locationid from #GridsToCheckForParent
	)
and locationid = -988223

Select *
from #GridsToCheckForParent
Where locationid not in (
	Select relatedlocationid
	from Universe..LocationRelationships
	where relationshiptypeid = 1
	and relatedlocationtypeid = 8
	and locationtypeid = 62
	)

--union all
--Select locationid, spatialmeasure
--from universe..locations
--where locationid = 90006323



-----------------------------------------


--View all the ocean grids
Select distinct neighborlocationid
From GlobalGridDataStore..GridOceanNeighbors
where neighborlocationid in (
	select locationid 
	from universe..locations
	where locationtypeid = 8
	)

