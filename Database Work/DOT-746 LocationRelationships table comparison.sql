--Get list of grids not on Prod
Select distinct lr.locationtypeid, lr.locationid, l1.locationname as Country, lr.relatedlocationid, l2.locationname as GridName
from universe..locations l1
inner join
universe..locationrelationships lr
on l1.locationid = lr.locationid
inner join
universe..locations l2
on lr.relatedlocationid = l2.locationid
where lr.relatedlocationtypeid = 8
and lr.locationtypeid = 62
and lr.relatedlocationid not in (
	Select distinct relatedlocationid
	from GlobalGridDataStore..Data3GridsInrelationshiptable
	)


Select relatedlocationid, count(relatedlocationid)
from GlobalGridDataStore..Data3GridsInrelationshiptable
group by relatedlocationid having count(relatedlocationid)>1

--A different approach to ID the missing grids, same results
Select lr.relatedlocationid, l.spatialmeasure
from Universe..locationrelationships lr
join
Universe..locations l
on lr.relatedlocationid = l.locationid
where lr.relatedlocationtypeid = 8
and lr.locationtypeid = 62
--and lr.locationid = -988304
and lr.relatedlocationid not in (
	Select g.relatedlocationid from Universe..locationrelationships g 
	join 
	Globalgriddatastore..data3gridsinrelationshiptable r 
	on g.locationid = r.locationid and
	g.relatedlocationid = r.relatedlocationid
	)
and lr.relatedlocationid not in (
	Select relatedlocationid
	from gb_spatial..fixlocationrelationshipsfortype23grids
	)
union all
Select locationid, spatialmeasure
from universe..locations
where locationid = -988304


