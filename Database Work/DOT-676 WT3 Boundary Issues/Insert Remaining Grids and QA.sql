Insert into Universe..LocationRelationships
Select *
From GB_spatial..FixLocationRelationshipsRemainingGridsToCountry

--Check that new grids were inserted correctly
Select *
from Universe..LocationRelationships
where RelatedLocationTypeId = 8
and LocationTypeId = 62
and RelatedLocationId in (
	Select RelatedLocationId
	From GB_spatial..FixLocationRelationshipsRemainingGridsToCountry
	)


--Check if grid parents are correct
Select lr.relatedlocationid as RelatedLocationID, l1.Locationname as GridName, l2.locationname as CountryName, awmDatabase.dbo.UnionAggregate(l1.spatialmeasure)
from Universe..Locations l1
inner join
Universe..locationrelationships lr
on l1.locationid = lr.relatedlocationid
inner join
Universe..locations l2
on lr.locationid = l2.locationid
where lr.relatedlocationid in (
	Select relatedlocationid
	from GB_spatial..FixLocationRelationshipsRemainingGridsToCountry 
	)
and lr.locationtypeid = 62
group by lr.relatedlocationid, l1.locationname, l2.locationname
union all
Select lr.Locationid as CountryID, l1.locationname as CountryName, 'a', awmDatabase.dbo.UnionAggregate(l1.spatialmeasure)
from universe..locations l1, gb_spatial..FixLocationRelationshipsRemainingGridsToCountry lr
where l1.locationid = lr.locationid
and lr.locationid in (
	Select distinct locationid
	from Universe..Locationrelationships
	where locationtypeid = 62
	and relatedlocationtypeid = 8 
	)
and l1.locationtypeid = 62
group by lr.locationid, l1.locationname
