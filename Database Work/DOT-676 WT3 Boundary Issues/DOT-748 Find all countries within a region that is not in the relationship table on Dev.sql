--Get list of countries not in relationship table
Create table #ListOfCountries (Runid int identity(0,1), LocationId int, LocationName varchar(50), SpatialMeasure geometry)
Insert into #ListOfCountries (LocationId, LocationName, SpatialMeasure)
Select locationid, Locationname, spatialmeasure
from Universe..locations
where locationtypeid = 62
and locationid not in (
	Select relatedlocationid
	from Universe..LocationRelationships
	where relatedLocationtypeid = 62
	and locationtypeid = 63
	)

Create table #CountryToRegion (locationtypeid smallint, locationid int, relatedlocationtypeid smallint, relatedlocationid int, relationshiptypeid tinyint, Distance float, isaggregatepath bit)

--Loop through list of countries and see which region they intersect
Declare @CountryShape geometry, @RegionShape geometry
Declare @RunId int = 0, @MaxRunId int
Declare @Locationid int, @Locationname varchar(50)

Select @MaxRunid = max(Runid)
from #ListOfCountries

While @Runid <= @Maxrunid
	Begin
		--Get country name, id and shape
		Select @CountryShape = Spatialmeasure, @Locationid = locationid, @Locationname = locationname
		from #ListOfCountries
		where runid = @Runid

		Print (char(13) + 'Working on ' + @LocationName)

		Insert into #CountryToRegion (locationtypeid, locationid, relatedlocationtypeid, relatedlocationid, relationshiptypeid, isaggregatepath)
		Select 63, Locationid, 62, @Locationid, 1,1
		from Universe..locations
		where locationtypeid = 63
		and @CountryShape.STIntersects(spatialmeasure) =1
		and locationname like '%WT3'
		and @CountryShape.STIntersection(spatialmeasure).STArea()/@CountryShape.STArea() >.9

		Set @Runid = @runid + 1

	End


--Check results
Select * 
from #CountryToRegion c, universe..locations l
where c.relatedlocationid = l.locationid

--Insert into relationship table	
Insert into Universe..locationrelationships
Select *
from #CountryToRegion


--Insert all 62 to 63 relationships into the relationship table
Insert into gb_spatial..fixlocationrelationshipscountrytoregion
Select *
from Universe..locationrelationships
where locationtypeid = 63
and relatedlocationtypeid = 62

