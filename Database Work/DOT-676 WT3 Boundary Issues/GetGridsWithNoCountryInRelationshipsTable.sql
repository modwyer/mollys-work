declare @MinGridX int
Declare @MaxGridX int
Declare @MinGridY int
Declare @MaxGridY int
Declare @includeLand int

set @MinGridX = -1498
set @MaxGridX  = -1465
set @MinGridY = 547
set @MaxGridY = 589
set @includeLand = 0

--create temp table to hold locationids from my box
Create table #GridsToCheckForParent (Locationid int, Latitude float, Longitude float, Elevation int, SpatialMeasure geometry)
Insert into #GridsToCheckForParent (Locationid, Latitude, Longitude, Elevation, SpatialMeasure)
select 
	LocationID,
	Latitude,
	Longitude,
	Elevation,
	SpatialMeasure
from GlobalGridDataStore..Global5by5MinGrid_full
where GridX between @MinGridX and @MaxGridX
and GridY between @MinGridY and @MaxGridY
and LocationID <(case when @IncludeLand is not null then 70000000 else 79999999 end) 

Select * from #GridsToCheckForParent

--Check which locationIds don't have a parent in the relationship table
Select *
from #GridsToCheckForParent
Where locationid not in (
	Select relatedlocationid
	from Universe..LocationRelationships
	where relationshiptypeid = 1
	and relatedlocationtypeid = 8
	and locationtypeid = 62
	)
	
--Of these location IDs without parents, do any intersect the country polygon?
Declare @Country geometry

Select @Country = Spatialmeasure
from Universe..Locations
where LocationId = -988223


Select Spatialmeasure
from #GridsToCheckForParent
Where locationid not in (
	Select relatedlocationid
	from Universe..LocationRelationships
	where relationshiptypeid = 1
	and relatedlocationtypeid = 8
	and locationtypeid = 62
	)
and spatialmeasure.STIntersects(@Country) = 1
union all
Select @Country		

--should be 134 locations; none of these locations show up in the relationship table with loctypeid = 62; 47 show up for loctypeid = 23; none show up for locationid = -988223
Select Locationid,LocationTypeId, relatedlocationid, RelatedLocationTypeId
from Universe..Locationrelationships
where --RelationshipTypeId = 1
RelatedLocationTypeId = 8
--and LocationTypeId = 23 --62
and LocationId = -988223
and RelatedLocationId in (
	Select Locationid
	from #GridsToCheckForParent
	Where locationid not in (
		Select relatedlocationid
		from Universe..LocationRelationships
		where relationshiptypeid = 1
		and relatedlocationtypeid = 8
		and locationtypeid = 62
		)
	and spatialmeasure.STIntersects(@Country) = 1
)