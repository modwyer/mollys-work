--Get coastal grids without a parent country
Select locationid
from [GB_SPATIAL].[dbo].[GridsOnBoundaryNAM]
Where locationid not in (
	Select relatedlocationid
	from Universe..LocationRelationships
	where relationshiptypeid = 1
	and relatedlocationtypeid = 8
	and locationtypeid = 62

	)

