--Put it all together: Get all grids that intersect the boundary of the specific country by looping through a list of selected countries, getting the 
--grids on the border of the country that don't have a parent ID in the relationships table then insert those grids and the corresponding parent ID
--into a table to be used during an update of the locationrelationships table.


--IF OBJECT_ID('tempdb..#GridsMissingParents') IS NOT NULL Drop table #GridsMissingParents 
--Create table #GridsMissingParents (LocationId int, LocationName varchar(2000),SpatialMeasure geometry, ParentID int, ParentName varchar(2000))
--Go

--IF OBJECT_ID('tempdb..#GridsMissingParents') IS NOT NULL Drop table #CountryListForGridsTest 
--Create table #CountryListForGridsTest (RunID int identity(0,1), RegionName varchar(100), LocationID int, CountryName varchar(100))
--Go
--3:03:54 to Isle of Man
--Add list of countries to the temp table to loop through


truncate table #CountryListForGridsTest
Insert into #CountryListForGridsTest (RegionName, LocationID, CountryName)
Select RegionName, Locationidtype23, Countryname from GB_Spatial..CountryListForGridsTest



Declare @CountryID int
Declare @RunID int = 0
Declare @MaxRunID int
Declare @CountryName varchar(100)
Declare @CountryGeom geometry

Select @MaxRunID = max(Runid)
From #CountryListForGridsTest

While @RunID <= @MaxRunID
	Begin
		--Find grids with missing parents in the relationship table and put them and the associated parent into a perm table
		Truncate table #GridsMissingParents

		Print (char(13) +'Run Id: ' + cast(@RunID as varchar(10)))

		Select @CountryID = c.Locationid, @CountryName = c.CountryName, @CountryGeom = l.Spatialmeasure.STPointN(1)
		From #CountryListForGridsTest c, Universe..locations l
		Where c.Runid = @RunID
		and c.locationid = l.LocationId
		

		Print ('Working on ' + @CountryName)

		Select distinct ParentID, ParentName 
		from GB_Spatial..GridsWithMissingParentsForType23 
		Where ParentID = @CountryID and ParentName = @CountryName

		--If the country being worked on is not in the Grids table yet, continue with insert of grids to the table
		If @@rowcount = 0
			Begin
				Insert into #GridsMissingParents (Locationid, LocationName,SpatialMeasure, ParentID, ParentName)
				Select l2.locationid, l2.locationname,l2.spatialmeasure, l1.locationid as ParentId, l1.locationname as ParentName
				from Universe..locations l1, Universe..locations l2
				Where l1.locationtypeid = 23  --country
				and l1.locationid = @CountryID
				and l2.locationtypeid = 8 --grid
				--and l2.spatialmeasure.STIntersects(l1.Spatialmeasure.STBoundary().STBuffer(.04))=1
				and l2.spatialmeasure.STContains(@CountryGeom) = 1
				and l2.locationid not in (
					Select relatedlocationid
					from Universe..LocationRelationships
					where relationshiptypeid = 1
					and relatedlocationtypeid = 8
					and locationtypeid = 23
					)
				and l2.locationid not in (
					Select relatedlocationid
					from Universe..LocationRelationships
					where relationshiptypeid = 1
					and relatedlocationtypeid = 8
					and locationtypeid = 62
					)
				and l2.locationid not in (
					Select locationid
					from GB_SPATIAL..GridsWithMissingParentsForType23
					)
				and l2.locationid not in (
					Select Locationid
					From GB_spatial..GridsWithMissingParents)

				Print (char(13) + 'Rows inserted to temp table: ' + cast(@@Rowcount as varchar(10)))
				
				--Add grids to permanent table
				Insert into GB_SPATIAL..GridsWithMissingParentsForType23 (ParentId, LocationId, ParentName)
				Select ParentID, LocationID, ParentName
				from #GridsMissingParents
				where locationID not in (
					Select Locationid
					from GB_Spatial..GridsWithMissingParentsforType23)
				

				Print (char(13) + 'Rows inserted to final table for ' + @CountryName + ': ' + cast(@@Rowcount as varchar(10)))

				Declare @GridId int
				Declare @GridName varchar(100)
				Declare @GridSpatial geometry
				

				--If there is nothing to insert, try this method
				If @@ROWCOUNT = 0
					Begin
						SELECT @CountryGeom = l.spatialmeasure
						FROM GB_SPATIAL.dbo.CountryListForGridsTest c, Universe..locations l
						where c.locationidtype23 = l.locationid
						and c.locationidtype23 not in (
							SELECT distinct parentid
							FROM GB_SPATIAL.dbo.GridsWithMissingParentsforType23
							)
					
						Select @GridID = Locationid, @GridName = Locationname, @GridSpatial = Spatialmeasure
						from Universe..locations
						where locationtypeid = 8
						and Spatialmeasure.STContains(@CountryGeom) = 1
						and locationid not in (
							Select relatedlocationid
							from Universe..LocationRelationships
							where relationshiptypeid = 1
							and relatedlocationtypeid = 8
							and locationtypeid = 23
							)
						and locationid not in (
							Select locationid
							from GB_SPATIAL..GridsWithMissingParentsForType23
							)
						
					End		
				
				
				Set @RunID = @RunId +1
			End
		Else
			Begin
				Print (char(13) + 'Grids already exist in the permanent table for ' + @CountryName + ', ID ' + cast(@CountryID as varchar(10)))
				Set @RunID = @RunId +1
			End
	End

----------------------------------------------------------- Miscellaneous Queries
Select * from GB_SPATIAL..GridsWithMissingParents where parentname like 'Mauritius' order by ParentName

--Alter the GridsWithMissingParents table to include locationtypeid, relatedlocationtypeid, relationshiptypeid, isaggregatepath and distance fields



Select l.locationid, l.locationname, l.spatialmeasure from GB_SPATIAL..GridsWithMissingParents g, Universe..locations l
where l.locationid = g.locationid
and g.locationid in (50223413,50223443,50224664,50228368,50228632,50231109,50231123,50231341,50233421,50235445,50238181,50240525,50240748,50240942
					,50249165,50249219,50251179,50251948,50252614,50253336,50255747,50256143,50257321,50257669,50259423,50259861,50260135,50260681
					,50260895,50261102,50261753,50261938)
union all
Select locationid, locationname, spatialmeasure
from universe..locations
where locationtypeid = 23
and locationid in (-988223, -988252)

--Remove erroneous duplicates from GB_SPATIAL..GridsWithMissingParents
Delete from GB_SPATIAL..GridsWithMissingParents
Where Locationid in (50228632,50238181,50259423,50261938)

--Remove US duplicates
Delete from GB_SPATIAL..GridsWithMissingParents
Where Locationid in (50228368,50223443,50240525,50233421,50231109,50253336,50249219,50261753,50251179,50260135,50259861,50260681,50252614)
and parentID = -988223

--Remove Mexico duplicates
Delete from GB_SPATIAL..GridsWithMissingParents
Where Locationid in (50240748,50231123,50235445,50224664,50231341,50240942,50223413,50249165,50255747,50257321,50251948,50257669,50260895,50256143,50261102)
and parentID = -988252


--Find countries within regions
Declare @Region geometry
Select @Region = Spatialmeasure
	from Universe..locations
	where locationtypeid = 25
	and locationid = -275116							

Select locationid, Locationname, spatialmeasure 
from Universe..locations
where locationtypeid = 23
and spatialmeasure.STIntersects(@Region) =1
union all
Select locationid, Locationname, spatialmeasure
from Universe..locations
where locationtypeid = 25
and locationid = -275116							

--Troubleshoot orphan countries
Declare @Region geometry
Select @Region = Spatialmeasure.STBuffer(5)
	from Universe..locations
	where locationtypeid = 23
	and locationid = -988160

Select locationid, Locationname, spatialmeasure 
from Universe..locations
where locationtypeid = 23
and locationid = -988160
union all
Select locationid, Locationname, spatialmeasure
from Universe..locations
where locationtypeid = 25
and spatialmeasure.STIntersects(@Region)=1

--Count of countries in a continent
Declare @Region geometry
Select @Region = Spatialmeasure
	from Universe..locations
	where locationtypeid = 26
	and locationid = -45384				

Select locationid, Locationname, spatialmeasure 
from Universe..locations
where locationtypeid = 23
and spatialmeasure.STIntersects(@Region) =1
union all
Select locationid, Locationname, spatialmeasure
from Universe..locations
where locationtypeid = 26
and locationid = -45384
				


Select *
	from Universe..locations
	where locationtypeid = 62
	and charindex('United states',locationname)>0

--Update CountryListForGridsTest table to include LocationId for type 23
Alter table GB_spatial..Countrylistforgridstest
Add LocationIDType23 int

Create table #CountryList (Runid int identity(0,1), CountryName varchar(50), LocationId int)
Insert into #CountryList (CountryName)
Select CountryName
From GB_spatial..Countrylistforgridstest

Declare @RunID int = 0
Declare @MaxRunID int
Declare @LocationId int
Declare @CountryName varchar(50)

Select @MaxRunId = max(runid)
From #CountryList


While @Runid <= @MaxRunid
	Begin
		Print @RunId

		Select @CountryName = CountryName
		From #CountryList
		Where Runid = @Runid

		Print @CountryName

		Select @Locationid = Locationid
		from Universe..locations
		Where Locationname = @CountryName
		and Locationtypeid = 23

		Update GB_spatial..Countrylistforgridstest
		Set LocationIDType23 = @LocationID
		Where CountryName = @CountryName
		
		Set @Runid = @Runid + 1
	End

Select c.*, l.locationname from GB_spatial..CountryListForGridsTest c
inner join
Universe..Locations l
on c.LocationIdType23 = l.locationid

Update GB_spatial..CountryListForGridsTest
Set LocationIDType23 = -45330
Where LocationIDType23 = -45271
and Locationid = -988270

Select *
from GB_spatial..CountryListForGridsTest

Select *
from Universe..locations
where charindex('Congo, DRC',locationname)>0
-----------------------------------------------




Select @Region = Spatialmeasure
from Universe..Locations
where Locationtypeid = 26


Select Locationid, LocationName
From Universe..Locations
Where Locationtypeid = 62
and locationname in (
	Select CountryName
	From GB_Spatial..CountryListForGridsTest
	)
