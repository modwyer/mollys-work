--Get list of countries - 246
--drop table #Countries
Create table #Countries (RunId int identity(0,1), Locationid int, LocationName varchar(50), CountryShape geometry)
Insert into #Countries (Locationid, Locationname, CountryShape)
Select locationid, locationname, spatialmeasure
from Universe..Locations
where locationtypeid = 62


--Get list of grids not in the relationship table and centroid, type 8 and relatedlocationid - 1,457,863 
--drop table #Centroids
Create table #Centroids (RunId int identity(0,1), RelatedLocationId int, LocationTypeid int, CentroidSpatial geometry)
Insert into #Centroids (RelatedLocationid, Locationtypeid, centroidspatial)
Select locationid, 8, centroidspatialmeasure
from Universe..Locations l
where locationtypeid = 8
and locationid not in (
	Select distinct relatedlocationid
	from Universe..locationrelationships
	where relatedlocationtypeid = 8
	and locationtypeid = 62
	and relatedlocationid not like '7%'
	)
and locationid not like '7%'

Declare @CountryShape geometry
Select @CountryShape = spatialmeasure
from Universe..locations
where locationtypeid = 62

Select l1.locationid, l1.spatialmeasure as GridShape, l2.locationid as CountryID, l2.locationname, l2.spatialmeasure as CountryShape
from universe..locations l1, universe..locations l2
where l1.locationid = 40475461
and l2.locationid = @CountryShape
and l1.spatialmeasure.STIntersects(l2.spatialmeasure)=1


--Find out which country that centroid falls within, type 62 and locationid ~1.5 hrs
Declare @RunId int = 2, @MaxRunId int
Declare @CountryID int, @CountryName varchar(50), @CountryShape geometry
Declare @RelatedLocationId int

Create table #CountryGridList (CountryId int, CountryName varchar(50), RelatedLocationId int)
Select @MaxRunID = max(runid)
from #Countries

While @RunId <= @MaxRunId
	Begin
		--Select variables for acting country
		Select @CountryId = Locationid, @CountryName = LocationName, @CountryShape = CountryShape
		From #Countries
		where runid = @RunId

		Print ('Working on ' + @CountryName)
		
		--Select parentid, name and relatedlocationid and insert into table
		Insert into #CountryGridList (CountryId, CountryName, RelatedLocationId)
		Select @CountryID, @CountryName, c2.relatedlocationid
		from #Countries, #Centroids c2
		where c2.centroidspatial.STWithin(@CountryShape) = 1
		

		Set @Runid = @runid + 1

	End

Select l.Locationname as Country, l.locationid as Locationid, l2.locationid as Relatedlocationid
from universe..locations l, universe..locations l2
where l.locationtypeid = 62
and l2.locationid in (
	Select distinct locationid
	from Universe..Locations l
	where locationtypeid = 8
	and locationid not in (
		Select distinct relatedlocationid
		from Universe..locationrelationships
		where relatedlocationtypeid = 8
		and locationtypeid = 62
		and relatedlocationid not like '7%'
		)
	and locationid not like '7%'
	)
and l2.centroidspatialmeasure.STWithin(l.spatialmeasure) = 1

--Find out if any grids in the list don't fall within a country. Check if the grid poly intersects any countries

--Populate a table with country locationid and grid locationid

--Double check scripts on prod before running insert on dev