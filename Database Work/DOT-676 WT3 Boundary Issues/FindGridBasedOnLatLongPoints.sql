Declare @Lat float = 36.5928915107767
Declare @Long float = -121.929531291667
Declare @Point geometry

Set @Point = geometry::STGeomFromText('POINT (' + cast(@Long as varchar(20)) + ' ' + cast(@Lat as varchar(20)) + ')', 4326)

Select l.SpatialMeasure, gridx, gridy, locationid
From GlobalGridDataStore..Global5by5minGrid_Full l
where @Point.STIntersects(l.spatialmeasure) = 1
union all
Select @Point
