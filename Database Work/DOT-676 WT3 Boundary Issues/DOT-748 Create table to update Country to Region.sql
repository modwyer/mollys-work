Select *
from Universe..locations
where locationtypeid = 62
and locationid not in (
	Select relatedlocationid
	from Universe..LocationRelationships
	where relatedLocationtypeid = 62
	and locationtypeid = 63
	)

--Get list of regions
Select locationid, locationname, spatialmeasure
from Universe..locations
where locationtypeid = 63

--Get list of countries
Create table #ListOfCountries (Runid int identity(0,1), LocationId int, LocationName varchar(50), SpatialMeasure geometry)
Insert into #ListOfCountries (LocationId, LocationName, SpatialMeasure)
Select locationid, Locationname, spatialmeasure
from Universe..locations
where locationtypeid = 62
and locationid not in (
	Select relatedlocationid
	from Universe..LocationRelationships
	where relatedLocationtypeid = 62
	and locationtypeid = 63
	)


--Loop through list of countries and see which region they intersect
Declare @CountryShape geometry, @RegionShape geometry
Declare @RunId int = 0, @MaxRunId int
Declare @Locationid int, @Locationname varchar(50)

Select @MaxRunid = max(Runid)
from #ListOfCountries

--Create table #CountryToRegion (locationtypeid smallint, locationid int, relatedlocationtypeid smallint, relatedlocationid int, relationshiptypeid tinyint, Distance float, isaggregatepath bit)
While @Runid <= @Maxrunid
	Begin
		--Get country name, id and shape
		Select @CountryShape = Spatialmeasure, @Locationid = locationid, @Locationname = locationname
		from #ListOfCountries
		where runid = @Runid

		Print (char(13) + 'Working on ' + @LocationName)

		Insert into #CountryToRegion (locationtypeid, locationid, relatedlocationtypeid, relatedlocationid, relationshiptypeid, isaggregatepath)
		Select 63, Locationid, 62, @Locationid, 1,1
		from Universe..locations
		where locationtypeid = 63
		and @CountryShape.STIntersects(spatialmeasure) =1
		and locationname like '%WT3'
		and @CountryShape.STIntersection(spatialmeasure).STArea()/@CountryShape.STArea() >.9

		Set @Runid = @runid + 1

	End
truncate table #CountryToRegion
Select * 
from #CountryToRegion c, universe..locations l
where c.relatedlocationid = l.locationid

Select c.locationid, l1.locationname, c.relatedlocationid, l2.locationname
from Universe..locations l1
inner join
#CountryToRegion c
on l1.locationid = c.locationid
inner join
universe..locations l2
on c.relatedlocationid = l2.locationid
where l1.locationtypeid = 63
and l2.locationtypeid = 62
and c.relatedlocationid in (-988224, -988170, -988071, -988218, -988186, -988237, -988231)
order by l2.locationname


Delete
From #CountryToRegion
Where (locationid = -2620112 and relatedlocationid = -988224) --Colombia
or (locationid = -2620117 and relatedlocationid = -988218) --Panama
or (locationid =  and relatedlocationid = -988170) --Iran
or (locationid =  and relatedlocationid = -988071) --Kasakhstan
or (locationid =  and relatedlocationid = -988186) --Russia
or (locationid = -2620117 and relatedlocationid = -988237) --Trinidad
or (locationid = -2620112 and relatedlocationid = -988231) --Venezuela
