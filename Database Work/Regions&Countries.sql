
--See both region and countries in region together
Declare @Region geometry

select @Region = spatialmeasure
from universe..locations
where locationid = -275120

select *
from universe..locations
where locationtypeid = 62
and spatialmeasure.STIntersects(@Region)>0
union all
select *
from universe..locations
where locationid = -275120

--View border of all countries within a certain region
Select locationid, locationname, locationtypeid, spatialmeasure.STBoundary() as SpatialBorder
From Universe..locations
where locationid in (-988216,-988221,-988133,-988241,-988240,-988223,-988252,-988230) --North America
