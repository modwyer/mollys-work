/****** List all RelateLocationIds that are on prod but where the parent ID doesn't match what's currently there  ******/
--Lists out current parent and new parent IDs

SELECT d.LocationId as CurrentLocationid
	  ,l1.locationname as CurrentParent
      ,d.RelatedLocationId as CurrentRelatedlocationid
	  , f.Locationid as NewLocationId
	  ,l2.locationname as NewParent
	  ,f.relatedlocationid as NewRelatedLocationid
FROM GB_SPATIAL..FixLocationRelationshipsforType23Grids f
join
globalgriddatastore..data3gridsinrelationshiptable d
on f.relatedlocationid = d.relatedlocationid
inner join
Universe..locations l1
on  d.locationid = l1.locationid
inner join
Universe..locations l2
on f.locationid = l2.locationid