/********************** DRC UPDATES *********************/
--See next '/*' section for process of identifying and fixing issue of missing base polygons, for reference.

--Run: Check if ghost Admin 0 ID exists, this will catch any that may not be the exact same ID
	Select *
	From Universe..BaseAdminFeatures
	Where Admin0LocationID = 90647193
	--137 rows on QA

--Run: to update the ghost Admin0 to the correct DRC ID
	Update Universe..BaseAdminFeatures
	Set Admin0LocationId = -45330
	Where Admin0LocationID = 90647193

--Run: Delete excess BaseAdmin features for DRC, proc located in testdatabase
	EXEC dbo.[UpdateBaseFeatures_DeleteAndInsertBaseAdmin] 70, 'Democratic Republic of the Congo', -45330, -4841, -46118

--Run: Update geohashes, takes 18:14 to run
	Exec [4UpdateGeohashes_LoopBaseFeaturesFaster] -45330

--BaseAdmin only has 46 base locations but locations has 150. Looking at the spatial results tab shows there are clearly polygons missing in the base admin table
/* This commented out section is just for viewing purposes if you need, no action needs to be taken
	--View missing polygons
		Select *
		FROM [Universe].[dbo].[BaseAdminFeatures]
		Where Admin0LocationId = -45330
		and BaseAdminLevelId = 3

	--View Locations table that has admin 3 locations
		Select *
		From Universe..Locations
		Where LocationTypeId = 70
		and charindex('Democratic Republic of the Congo',Locationname)>0

	--View Duplicate admin 3 ID
		Select L.LocationName, B.*
		FROM [Universe].[dbo].[BaseAdminFeatures] B
		inner join
		Universe..Locations L
		on B.Admin3LocationId = L.LocationId
		Where Admin3LocationId = 90337387

	--View Admin 0 for that duplicate in Locations
		Select *
		From Universe..Locations
		Where LocationId = 90647193

	--View ghost admin 0 in Base Admin table
		SElect *
		From Universe..BaseAdminFeatures
		Where Admin0LocationID = 90647193

	--View double check that all ghost ID's lower admin levels are DRC
		Select L.LocationName, B.*
		FROM [Universe].[dbo].[BaseAdminFeatures] B
		inner join
		Universe..Locations L
		on B.Admin1LocationId = L.LocationId
		Where Admin0LocationId = 90647193

		Select L.LocationName, B.*
		FROM [Universe].[dbo].[BaseAdminFeatures] B
		inner join
		Universe..Locations L
		on B.Admin1LocationId = L.LocationId
		Where Admin0LocationId = 90647193
*/









