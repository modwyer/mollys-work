/****** Metadata checks ******/

--Check Locations table
SELEct *
From Universe..Locations
Where LocationTypeId = 68
and CHARINDEX(', Democratic Republic of the Congo',locationname)>0
and CHARINDEX(', Democratic Republic of the Congo (',locationname)=0

--Check Base Admin table
SELEct *
From Universe..BaseAdminFeatures 
Where Admin0LocationId = -45330

SELEct *
From Universe..BaseAdminFeatures B
inner join
Universe..Locations L
on B.Admin1LocationId = L.LocationId
--on B.Admin2LocationId = L.LocationId
Where Admin0LocationId = -45330

--Check Geohash table
SELEct *
From Universe..Geohash6LocationsLookup 
Where Admin0LocationId = -45330