Select *
From Universe..Locations
Where LocationTypeId = 65
and charindex('Democratic Republic of the Congo',LocationName)>0

Select *
From Universe..Locations
Where LocationId = 

Select *
From Universe..Locations
Where LocationTypeId = 23
and charindex('Congo',LocationName)>0
order by LocationName asc

/*
-45330	Democratic Republic of the Congo
-45271	Congo
*/

Select L.LocationName as Admin1Name, G.Admin0LocationId
	, G.Admin1LocationId, G.Admin2LocationId, G.Geohash6
	, cast([Latitude1km] as varchar(250)) + ', ' + cast([Longitude1km] as varchar(250)) as LatLong
    , Geometry::STGeomFromText([ShapeWKT],4326), G.ShapeWKT as WKT
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin1LocationId = L.LocationId
Where G.Admin0LocationId = -45330
Order by Admin1Name

Select L.LocationName as Admin1Name, G.Admin0LocationId
	, G.Admin1LocationId, G.Admin2LocationId, G.BaseAdminLevelId
From Universe.dbo.BaseAdminFeatures G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin1LocationId = L.LocationId
Where G.Admin0LocationId = -45271
Order by Admin1Name


Select *
From Universe..GeoHash6LocationsLookup
Where Admin0LocationId = -45271

Select *
From Universe..BaseAdminFeatures
Where Admin0LocationId = -45330