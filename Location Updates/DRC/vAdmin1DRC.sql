USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAdmin1DRC]    Script Date: 4/7/2015 12:56:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create view [dbo].[vAdmin1DRC]
as 
SELECT 
	NAME + ', Democratic Republic of the Congo' as LocationName
	,65 as LocationTypeId
	,[Shape] as LocationFeature
	,[OBJECTID] as Id
	,case when [Shape].MakeValid().STCentroid() is null then [Shape].MakeValid().STEnvelope().STCentroid() else [Shape].MakeValid().STCentroid() end as Centroid
	,NAME + ', Democratic Republic of the Congo' as PresentationLocationName
	,'DRC-' + CAST(Id as varchar(8)) as Hashkey
  FROM [TestDatabase].[dbo].[DRCAdmin1_4.7.15]

GO


