kws8pb

kxhftw

Select L.LocationName, G.* 
FROM [Universe].[dbo].[GeoHash6LocationsLookup] G
inner join
Universe..Locations L
on G.Admin2LocationId = L.LocationId
Where geohash6 = 'kws8pb' --Haut-Shaba, Katanga, Democratic Republic of the Congo

Select L.LocationName, G.* 
FROM [Universe].[dbo].[GeoHash6LocationsLookup] G
inner join
Universe..Locations L
on G.Admin2LocationId = L.LocationId
Where geohash6 = 'kxhftw' --n.a. ( 1030), Lake Tanganyika, Kigoma, Tanzania
--admin 2 is Uvinza, Kigoma, Tanzania
--admin 1 is Kigoma, Tanzania

Select *
FROM [Universe].[dbo].[GeoHash6LocationsLookup]
WHERE Geohash6 
IN (
SELECT Geohash6 FROM [UserData_StarCluster_Data].[dbo].[GetGeohash6Neighbors] (
  'sc5d19'  		
  , 5.984492	
  , 38.74756  
  )
  )

--part 1

Select *
FROM [Universe].[dbo].[GeoHash6LocationsLookup]
Where Admin0LocationId = -45330
and Admin3LocationId is not null

--check geohashes for DRC where A3 is not null and see what related locations are
SElect L.LocationName as LocationName, LR.*
From UserData_StarCluster_Data..LocationRelationships LR
inner join
Universe..Locations L
on LR.LocationId = L.LocationId
Where LR.RelatedLocationId in (
	Select Admin2LocationId 
	From Universe..GeoHash6LocationsLookup
	Where Admin0LocationId = -45330
	and Admin3LocationId is not null
	)
	--and LR.LocationTypeId = 65
order by LR.RelatedLocationID

--same as above but comparing names for location and related location. run this with above script
SElect L.LocationName as RelatedLocationName, LR.*
From UserData_StarCluster_Data..LocationRelationships LR
inner join
Universe..Locations L
on LR.RelatedLocationId = L.LocationId
Where LR.RelatedLocationId in (
	Select Admin2LocationId 
	From Universe..GeoHash6LocationsLookup
	Where Admin0LocationId = -45330
	and Admin3LocationId is not null
	)
	and LR.LocationTypeId = 65
order by LR.RelatedLocationID

Select *
From Universe..Locations
Where LocationId = 90309297




--part 2

Select *
FROM [Universe].[dbo].[GeoHash6LocationsLookup]
Where Admin0LocationId = -45330
and Admin3LocationId is not null
and Admin2LocationId is not null

--check geohashes for DRC where A2 is not null and A3 is null and see what related locations are
SElect L.LocationName as LocationName, LR.*
From UserData_StarCluster_Data..LocationRelationships LR
inner join
Universe..Locations L
on LR.LocationId = L.LocationId
Where LR.RelatedLocationId in (
	Select Admin1LocationId 
	From Universe..GeoHash6LocationsLookup
	Where Admin0LocationId = -45330
	and Admin3LocationId is null
	and Admin2LocationId is not null
	)
	--and LR.LocationTypeId = 65
order by LR.RelatedLocationID

SElect L.LocationName as RelatedLocationName, LR.*
From UserData_StarCluster_Data..LocationRelationships LR
inner join
Universe..Locations L
on LR.RelatedLocationId = L.LocationId
Where LR.RelatedLocationId in (
	Select Admin1LocationId 
	From Universe..GeoHash6LocationsLookup
	Where Admin0LocationId = -45330
	and Admin3LocationId is null
	and Admin2LocationId is not null
	)
	--and LR.LocationTypeId = 65
order by LR.RelatedLocationID

--part 3
Select *
FROM [Universe].[dbo].[GeoHash6LocationsLookup]
Where Admin0LocationId = -45330
and Admin1LocationId is not null
and ContinentLocationId is not null
and Admin3LocationId is null
and Admin2LocationId is null


--check geohashes for DRC where A1 is not null and A2/A3 is null and see what related locations are
SElect L.LocationName as LocationName, LR.*
From UserData_StarCluster_Data..LocationRelationships LR
inner join
Universe..Locations L
on LR.LocationId = L.LocationId
Where LR.RelatedLocationId in (
	Select Admin1LocationId 
	From Universe..GeoHash6LocationsLookup
	Where Admin0LocationId = -45330
	and Admin3LocationId is null
	and Admin2LocationId is null
	and Admin1LocationId is not null
	)
	--and LR.LocationTypeId = 65
order by LR.RelatedLocationID

SElect L.LocationName as RelatedLocationName, LR.*
From UserData_StarCluster_Data..LocationRelationships LR
inner join
Universe..Locations L
on LR.RelatedLocationId = L.LocationId
Where LR.RelatedLocationId in (
	Select Admin1LocationId 
	From Universe..GeoHash6LocationsLookup
	Where Admin0LocationId = -45330
	and Admin3LocationId is null
	and Admin2LocationId is null
	and Admin1LocationId is not null
	)
	--and LR.LocationTypeId = 65
order by LR.RelatedLocationID

--part 4
Select * 
FROM [Universe].[dbo].[GeoHash6LocationsLookup] 
Where Admin0LocationId = -45330
and Admin3LocationId is not null
and admin2Locationid is not null
and admin1Locationid is not null

Select L.LocationName, G.Geohash6, G.Admin0LocationId, G.Admin1LocationId, G.Admin2Locationid, G.Admin3locationid
FROM [Universe].[dbo].[GeoHash6LocationsLookup] G
inner join
Universe..Locations L
on G.Admin3LocationId = L.LocationId
Where Admin3LocationId in (90337418, 90337428, 90337432, 90337441)

Select *
From Universe..Locations
Where LocationId in (90337418, 90337428, 90337432, 90337441)



--Locations with geohashes that only go to admin 2
Select distinct(Admin2LocationId), L.LocationName--, L.LocationFeature
FROM [Universe].[dbo].[GeoHash6LocationsLookup] G
inner join
Universe..Locations L
on G.Admin2LocationId = L.LocationId
Where Admin0LocationId = -45330
and Admin2LocationId is not null
and Admin3LocationId is null
and Admin2LocationId not in (
	Select distinct(Admin2LocationId )
	FROM [Universe].[dbo].[GeoHash6LocationsLookup] 
	Where Admin0LocationId = -45330
	and Admin3LocationId is not null)

Select * 
FROM [Universe].[dbo].[GeoHash6LocationsLookup] 
Where Admin0LocationId = -45330
and Admin2LocationId is null
and Admin3LocationId is null
and Admin1LocationId is not null in (
	Select distinct(Admin1LocationId )
	FROM [Universe].[dbo].[GeoHash6LocationsLookup] 
	Where Admin0LocationId = -45330
	and Admin3LocationId is not null)