
--Update bad geometry prior to updating centroids
Update [Universe].dbo.Locations
SET LocationFeature = Geometry::STGeomFromText(awmDatabase.dbo.MakeValidGeographyFromGeometry(LocationFeature).ToString(),4326)
--Select * From [Universe].dbo.Locations
Where LocationTypeId = 68
and charindex(' Democratic Republic of the Congo',Locationname,0)>0
and charindex('Democratic Republic of the Congo (',Locationname,0)=0
and charindex('Democratic Republic of the Congo(',Locationname,0)=0
and awmDatabase.dbo.IsValidGeographyFromGeometry(LocationFeature) = 0

--Update centroids
Update [Universe].[dbo].[Locations]  
Set CentroidFeature = [LocationFeature].STCentroid()
--Select * from [Universe].[dbo].[Locations]
Where LocationTypeId = 68
and CHARINDEX(', Democratic Republic of the Congo',LocationName)>0
and CHARINDEX(', Democratic Republic of the Congo (',LocationName)=0
and CHARINDEX(', Democratic Republic of the Congo(',LocationName)=0