
Create view UgandaCountryShape
as
	(Select LocationId,LocationName,LocationTypeId, LocationFeature, Hashkey
	from Universe.dbo.Locations
	Where LocationTypeId = 23
	and CHARINDEX('Uganda', LocationName)>0)
	
	
Select LocationName, LocationFeature, LocationId
From Universe.dbo.Locations
Where LocationTypeId = 65
and CHARINDEX(', Ethiopia', LocationName)>0
and CHARINDEX(', Ethiopia (', LocationName)=0


Select *
From Universe.dbo.GeoHash6LocationsLookup
Where Geohash6 = 'sbccv3'

Select *
From Universe.dbo.GeoHash6LocationsLookup
Where Admin1LocationId in (

Select  LocationId
From Universe.dbo.Locations
Where LocationTypeId = 68
and CHARINDEX(', Ethiopia', LocationName)>0
and CHARINDEX(', Ethiopia (', LocationName)=0
)


Select *
From Universe.dbo.Locations
Where LocationTypeId = 65
and CHARINDEX(', Ethiopia',LocationName)>0

Select *
From Universe.dbo.GeoHash6LocationsLookup
Where Admin2LocationId = 91565303  --Amaro Special Woreda, SNNPR    Admin 1: 90007146 (SNNPR)*, 90007154 (Oromia)

Select *
From Universe.dbo.GeoHash6LocationsLookup
Where Admin2LocationId = 91565308  --Basketo, SNNPR   Admin 1: 90007146 (SNNPR)*

Select *
From Universe.dbo.GeoHash6LocationsLookup
Where Admin2LocationId = 91565314  --Derashe Special Woreda, SNNPR    Admin 1:90007146 (SNNPR)*

Select *
From Universe.dbo.GeoHash6LocationsLookup
Where Admin1LocationId = 90007154
