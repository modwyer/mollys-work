/* This script will delete recently loaded locations. Change the country name to reflect whatever country you are working on.
Make sure to delete the current views and rerun those with the new file and delete any old shapefiles.
*/

/*Find the rows you want to delete
Select LocationName, LocationFeature, DateLastChanged
From Universe.dbo.Locations
Where LocationTypeId = 68
and CHARINDEX(', Ethiopia',LocationName)>0
and CHARINDEX(', Ethiopia (',LocationName)=0
*/

/*Delete from Universe*/
Delete from Universe.dbo.Locations
Where LocationTypeId = 68
and CHARINDEX(', Ethiopia',LocationName)>0
and CHARINDEX(', Ethiopia (',LocationName)=0

/*Delete from World Bank*/
Delete from WorldBank_StarCluster_Data.dbo.Locations
Where LocationTypeId = 68
and CHARINDEX(', Ethiopia',LocationName)>0
and CHARINDEX(', Ethiopia (',LocationName)=0

/*Delete from UserData*/
Delete from UserData_StarCluster_Data.dbo.Locations
Where LocationTypeId = 68
and CHARINDEX(', Ethiopia',LocationName)>0
and CHARINDEX(', Ethiopia (',LocationName)=0

/*Delete from ICRISAT*/
Delete from ICRISAT_StarCluster_Data.dbo.Locations
Where LocationTypeId = 68
and CHARINDEX(', Ethiopia',LocationName)>0
and CHARINDEX(', Ethiopia (',LocationName)=0

/*Update the old location names to remove the suffix. Prio to this you should have copied the names with the suffix to Excel
and and removed the suffix. Then import that txt file to the TestDatabase. See UpdateLocationNames.sql for initial cleanup of 
the txt name file, then proceed with the following*/

/*Update Universe names*/
Update Universe.dbo.Locations
  set LocationName = C.New_LocationName, PresentationLocationName = C.New_LocationName
  From Universe.dbo.Locations L, 
  [TestDatabase].[dbo].[Ethiopia_Admin2_LocationNames] C --this is the name of the txt file
  Where L.LocationId = C.LocationId 

/*Update World Bank names*/
Update WorldBank_StarCluster_Data.dbo.Locations
  set LocationName = C.New_LocationName, PresentationLocationName = C.New_LocationName
  From WorldBank_StarCluster_Data.dbo.Locations L, 
  [TestDatabase].[dbo].[Ethiopia_Admin2_LocationNames] C
  Where L.LocationId = C.LocationId 
  
/*Update UserData names*/
 Update UserData_StarCluster_Data.dbo.Locations
  set LocationName = C.New_LocationName, PresentationLocationName = C.New_LocationName
  From UserData_StarCluster_Data.dbo.Locations L, 
  [TestDatabase].[dbo].[Ethiopia_Admin2_LocationNames] C
  Where L.LocationId = C.LocationId 
  
 /*Update ICRISAT names*/
 Update ICRISAT_StarCluster_Data.dbo.Locations
  set LocationName = C.New_LocationName, PresentationLocationName = C.New_LocationName
  From ICRISAT_StarCluster_Data.dbo.Locations L, 
  [TestDatabase].[dbo].[Ethiopia_Admin2_LocationNames] C
  Where L.LocationId = C.LocationId 

/*Double check that none of the location names for Universe, WorldBank, UserData, or ICRISAT have a suffix*/
Select LocationName, PresentationLocationName
From Universe.dbo.Locations
Where LocationTypeId = 68
and CHARINDEX(', Ethiopia',LocationName)>0






