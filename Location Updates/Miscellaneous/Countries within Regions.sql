/*** Script to get a list of countries within a selected region ***/

use Universe;
declare @country int = 23;
declare @region int = 25;
declare @admin1 int = 65;
declare @admin2 int = 68;

select
--region
	loc1.LocationId as RegionId,
	loc1.LocationTypeId as RegionType,
	loc1.LocationName as RegionName,
--country
	loc2.LocationId as CountryId,
	loc2.LocationTypeId as CountryTypeId,
	loc2.LocationName as CountryName,
	loc2.DateLastChanged as DateCountryChanged,
--Admin 1
	loc3.LocationId as Admin1Id,
	loc3.LocationTypeId as Admin1TypeId,
	loc3.LocationName as Admin1Name,
	loc3.DateLastChanged as DateAdmin1Changed,
--Admin 2
	loc4.LocationId as Admin2Id,
	loc4.LocationTypeId as Admin2TypeId,
	loc4.LocationName as Admin2Name,
	loc4.DateLastChanged as DateAdmin2Changed
from 
	Locations loc1
inner join
	Locations loc2 on loc1.LocationFeature.STContains(loc2.CentroidFeature) = 1
inner join
	Locations loc3 on loc2.LocationFeature.STContains(loc3.CentroidFeature) = 1
inner join
	Locations loc4 on loc3.LocationFeature.STContains(loc4.CentroidFeature) = 1
where 
	loc1.LocationTypeId = @region
	and loc2.LocationTypeId = @country
	and loc3.LocationTypeId = @admin1
	and loc4.LocationTypeId = @admin2
	and CHARINDEX('Africa', loc1.LocationName)>0
order by
	loc1.LocationName,loc2.LocationName,loc3.LocationName,loc4.LocationName
	--loc2.DateLastChanged
;

--Swaziland (-45333) in Southeastern Africa -44039 is not in the full list