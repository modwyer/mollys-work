-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dan Allen
-- Create date: 12/11/2014
-- Description:	Split a shape into four quadrant intersections
-- =============================================
Alter Function SplitShapeIntoQuadrants (@Shape geometry)
Returns  @OutputShapes Table (RunId int, Shape geometry)
as
BEGIN	
	
	Declare @Env geometry
	Declare @Point1 geometry, @Point2 Geometry
	Declare @Output geometry

	Select @Env = @Shape.STEnvelope()

	Select @Point1 = @Env.STPointN(1)
	Select @Point2 = @Env.STCentroid()
	Select @Output = @Point1.STUnion(@Point2).STEnvelope().STIntersection(@Shape)

	INSERT INTO @OutputShapes (RunId, Shape) Select 1, @Output

	Select @Point1 = @Env.STPointN(2)
	Select @Point2 = @Env.STCentroid()
	Select @Output = @Point1.STUnion(@Point2).STEnvelope().STIntersection(@Shape)

	INSERT INTO @OutputShapes (RunId, Shape) Select 2, @Output

	Select @Point1 = @Env.STPointN(3)
	Select @Point2 = @Env.STCentroid()
	Select @Output = @Point1.STUnion(@Point2).STEnvelope().STIntersection(@Shape)

	INSERT INTO @OutputShapes (RunId, Shape) Select 3, @Output

	Select @Point1 = @Env.STPointN(4)
	Select @Point2 = @Env.STCentroid()
	Select @Output = @Point1.STUnion(@Point2).STEnvelope().STIntersection(@Shape)

	INSERT INTO @OutputShapes (RunId, Shape) Select 4, @Output

	RETURN
END