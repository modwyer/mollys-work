/****** Script for SelectTopNRows command from SSMS  ******/
/** Script to update location names only, not boundaries!**/ 

/*Script 1: Find the current names*/

Declare @LocationTypeId int = 68 --change this number as needed
Declare @LocationName varchar(1000) = 'Uganda' --change this as needed but ALWAYS use quotes
Declare @LocationNameToExclude varchar(1000) = 'Uganda (' --ditto to the above
	Select LocationId, LocationName, PresentationLocationName, LocationFeature
	From Universe.dbo.Locations
	Where LocationTypeId = @LocationTypeId
	and CHARINDEX(@LocationName,LocationName)>0
	and CHARINDEX(@LocationNameToExclude,LocationName)=0

/*Script 2: Remove quotes around presentation name*/

  update TestDatabase.[dbo].Uganda_Admin2_LocationNames --change this to the file that was imported
  set [New_LocationName] =LEFT([New_LocationName],LEN([New_LocationName])-1)
  
  update [TestDatabase].[dbo].[Uganda_Admin2_LocationNames]
  set [New_LocationName] =Right([New_LocationName],LEN([New_LocationName])-1)    
  
  update TestDatabase.[dbo].[Uganda_Admin2_LocationNames]
  set [New_PresentationLocationName] =LEFT([New_PresentationLocationName],LEN([New_PresentationLocationName])-1)
  
  update [TestDatabase].[dbo].[Uganda_Admin2_LocationNames]
  set [New_PresentationLocationName] =Right([New_PresentationLocationName],LEN([New_PresentationLocationName])-1)
  
/*Script 3: Adjust specific location names that are longer than 50 characters; may not need to do this*/  
  Update Universe.[dbo].Locations
  set PresentationLocationName = 'Konso Special Woreda, SNNPR, Ethiopia'
  Where PresentationLocationName = 'Konso Sp Woreda, SNNPR, Ethiopia'
 
  Update Universe.[dbo].Locations
  set LocationName = 'Konso Special Woreda, SNNPR, Ethiopia'
  Where LocationName = 'Konso Sp Woreda, SNNPR, Ethiopia'
  
/* Set the location name to the presentation name; if this is done pre-import, you can skip this step*/
  Update [TestDatabase].[dbo].[Uganda_Admin2_LocationNames]
  set [New_LocationName] = New_PresentationLocationName
  
/*Script 4: Update the Universe, WorldBank_Star_Cluster, User_Star_Cluster and ICRISAT cluster databases with the new name*/
  Update Universe.dbo.Locations
  set LocationName = C.New_LocationName, PresentationLocationName = C.New_LocationName
  From Universe.dbo.Locations L, 
  [TestDatabase].[dbo].[Uganda_Admin2_LocationNames] C --change this name to the current file you imported
  Where L.LocationId = C.LocationId 
  
  
  Update WorldBank_StarCluster_Data.dbo.Locations
  set LocationName = C.New_LocationName, PresentationLocationName = C.New_LocationName
  From WorldBank_StarCluster_Data.dbo.Locations L, 
  [TestDatabase].[dbo].[Uganda_Admin2_LocationNames] C  --change this name to the current file you imported
  Where L.LocationId = C.LocationId 
  
  Update UserData_StarCluster_Data.dbo.Locations
  set LocationName = C.New_LocationName, PresentationLocationName = C.New_LocationName
  From UserData_StarCluster_Data.dbo.Locations L, 
  [TestDatabase].[dbo].[Uganda_Admin2_LocationNames] C  --change this name to the current file you imported
  Where L.LocationId = C.LocationId 
  
  Update ICRISAT_StarCluster_Data.dbo.Locations
  set LocationName = C.New_LocationName, PresentationLocationName = C.New_LocationName
  From ICRISAT_StarCluster_Data.dbo.Locations L, 
  [TestDatabase].[dbo].[Uganda_Admin2_LocationNames] C  --change this name to the current file you imported
  Where L.LocationId = C.LocationId 
  
 /*End of Script 4*/ 
  
  --Truncate Table [TestDatabase].[dbo].[Ethiopia_Admin2_LocationNames
  
/*Double Checks: Are the changes in all databases correct?*/
Select *
From Universe.dbo.Locations 
Where LocationId in (
	Select LocationId 
	From [TestDatabase].[dbo].[Uganda_Admin2_LocationNames] ----change this name to the current file you imported
	)

Select LocationName, LocationId, DateLastChanged, LocationFeature
From Universe.dbo.Locations 
Where LocationTypeId = 68
and CHARINDEX(', Uganda',LocationName)>0
and CHARINDEX(', Uganda (',LocationName)=0