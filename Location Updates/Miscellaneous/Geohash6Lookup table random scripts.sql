/* Various scripts for viewing data in the GeoHash6LocationsLookup table*/

--See the spatial results of geohashes
SELECT [Geohash6]
      ,cast([Latitude1km] as varchar(250)) + ', ' + cast([Longitude1km] as varchar(250)) as LatLong
      ,Geometry::STGeomFromText([ShapeWKT],4326), ShapeWKT as WKT
  FROM [Universe].[dbo].[GeoHash6LocationsLookup]
  Where Admin2LocationId = 90310741
  --or Admin2LocationId = 91934684
  
/*not sure what this part did for the above script
UNION ALL  
Select LocationName, cast(LocationId as varchar(250)), LocationFeature, ''
From Universe.dbo.Locations Where LocationId = 91934684
UNION ALL
Select LocationName, cast(LocationId as varchar(250)), LocationFeature, ''
From Universe.dbo.Locations Where LocationId = 91423125  
*/

Select L.LocationName as Admin1Name, G.Admin0LocationId
	, G.Admin1LocationId, G.Admin2LocationId, G.Geohash6
	, cast([Latitude1km] as varchar(250)) + ', ' + cast([Longitude1km] as varchar(250)) as LatLong
    , Geometry::STGeomFromText([ShapeWKT],4326), G.ShapeWKT as WKT
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin2LocationId = L.LocationId
Where G.Admin2LocationId = 90310741
Order by Admin1Name

Select L.LocationName as Admin1Name, G.Admin0LocationId
	, G.Admin1LocationId, G.Admin2LocationId, G.Geohash6
	, cast([Latitude1km] as varchar(250)) + ', ' + cast([Longitude1km] as varchar(250)) as LatLong
    , Geometry::STGeomFromText([ShapeWKT],4326), G.ShapeWKT as WKT
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin1LocationId = L.LocationId
Where G.Admin0LocationId = (Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Somalia' 
							and LocationTypeId = 23)
and CHARINDEX(', Ethiopia (',L.LocationName)>0

--[1/29/2015 12:20 PM] Dan Allen: 
Select *
FROM [Universe].[dbo].[GeoHash6LocationsLookup]
WHERE Geohash6 
IN (
SELECT Geohash6 FROM [UserData_StarCluster_Data].[dbo].[GetGeohash6Neighbors] (
  'sfkum4'
  , 13.4193
  ,40.6573  
  )
  )



--get name of admin 1 that admin 2 points to 
Select L.LocationName as Admin1Name, G.*
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin1LocationId = L.LocationId
Where G.Admin2LocationId = 91565328

--get name of admin 2
Select L.LocationName as Admin2Name, G.*
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin2LocationId = L.LocationId
Where G.Admin2LocationId = 91565328

--get name of admin0 that admin 2 points to
Select L.LocationName as Admin0Name, G.*
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin0LocationId = L.LocationId
Where G.Admin2LocationId = 90310722

--get name of admin 1 that points to Admin0 
Select L.LocationName as Admin1Name, G.*
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin1LocationId = L.LocationId
Where G.Admin0LocationId = -45287 --Somalia

--get name of admin 1 that points to Admin0 
Select L.LocationName as Admin2Name, G.*
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin2LocationId = L.LocationId
Where G.Admin0LocationId = -45287