/*** Simplified version of Countries within Regions query ***/
--select * from locationtypes
--countries=23 regions=25
Use WorldBank_StarCluster_Data;

SELECT DISTINCT lr.locationtypeid, lr.relatedlocationtypeid, lr.LocationId, l1.locationname, lr.RelatedLocationId, l2.locationname 
	FROM WorldBank_StarCluster_Data.dbo.LocationRelationships lr
		inner join WorldBank_StarCluster_Data..locations l1 ON l1.locationid=lr.locationid --regions
		inner join WorldBank_StarCluster_Data..locations l2 ON l2.locationid=lr.relatedlocationid --countries
	WHERE lr.relatedlocationtypeid=23 and lr.locationtypeid=25
		and CHARINDEX('Zambia',l2.LocationName)>0
		--or CHARINDEX('Southeastern Asia',l1.LocationName)>0
	ORDER BY l1.locationname, l2.locationname