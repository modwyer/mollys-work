--India -45498



Select G.Geohash6
	From Universe.dbo.GeoHash6LocationsLookup G
	INNER JOIN
	Universe.dbo.Locations L
	ON G.Admin2LocationId = L.LocationId
	Where G.Admin0LocationId = -45498
	and CHARINDEX(', India',L.LocationName)>0
	
	
Select L.LocationName as Admin2Name, G.*   --rename field name
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin2LocationId = L.LocationId   ---Admin level you want to see it mapped to and the name
Where G.Admin0LocationId = -45498


  --Location you want to look up, change to relevant admin level
--Where CHARINDEX('SNNPR, Ethiopia',L.LocationName)>0
--Where Geohash6 = 'sc48s3'
--order by L.LocationName
Where G. Admin1LocationId in
(Select LocationId
 From Universe.dbo.Locations
 Where LocationTypeId = 65
 and CHARINDEX(', India', LocationName)>0
 and CHARINDEX('United State',LocationName)=0)
 
 
Select LocationName
From Universe.dbo.Locations
Where LocationId = -45503
