-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
	Author:		Molly O'Dwyer
	Create date: 12/22/2014
	Description:	Get Admin 1 location IDs from the DB and match to Admin 2 where Admin 2 centroid is within the 
					Admin 1 poly. Use this only when loading Admin 2 boundaries.
 =============================================*/
CREATE PROCEDURE FindMatchingAdmin1ForAdmin2
	-- Add the parameters for the stored procedure here
	@LocationTypeId int,
	@CountryName varchar(250),
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--we need to get the Admin1LocationIds above our newly added Admin2Locations so we need to do some spatial processing using point and polygon analysis

	Declare @Relationship2To1 Table (Admin2LocationId int, Admin1LocationId int)

	Insert into @Relationship2To1
	(Admin2LocationId,Admin1LocationId)
	Select NewAdmin2.LocationId as Admin2LocationId, Admin1.LocationId as Admin1LocationId
	From  
	(
		Select LocationId, CentroidFeature
		From Universe.dbo.Locations
		Where LocationTypeId = 65 --admin1
		and Charindex(', '+@CountryName+'',LocationName,0) > 0
		and HashKey like '@CountryName-%'  
	) NewAdmin2,
	(  
	Select LocationId, LocationFeature
	From Universe.dbo.Locations
	Where LocationTypeId = 65 --admin1
	and Charindex(', '+@CountryName+'',LocationName,0) > 0 
	) Admin1
	Where NewAdmin2.CentroidFeature.STWithin(Admin1.LocationFeature) = 1


	Insert into @Relationship2To1
	(Admin2LocationId,Admin1LocationId)
	Select NewAdmin2.LocationId as Admin2LocationId, Admin1.LocationId as Admin1LocationId
	From  
	(
		Select LocationId, LocationFeature
		From Universe.dbo.Locations
		Where LocationTypeId = 65 --admin 1
		and Charindex(', '+@CountryName+'',LocationName,0) > 0
		and HashKey like '@CountryName-%'  
	) NewAdmin2,
	(  
		Select LocationId, LocationFeature
		From Universe.dbo.Locations
		Where LocationTypeId = 65 --admin 1
		and Charindex(', '+@CountryName+'',LocationName,0) > 0 
	) Admin1
	Where NewAdmin2.LocationFeature.STIntersects(Admin1.LocationFeature) = 1
	and NewAdmin2.LocationId not in (Select Admin2LocationId from @Relationship2To1)


	INSERT INTO [Universe].[dbo].[BaseAdminFeatures]
			   ([BaseAdminLocationId]
			   ,[BaseFeature]
			   ,[BaseAdminLevelId]
			   ,[Admin0LocationId]
			   ,[Admin1LocationId]
			   ,[Admin2LocationId]
			   ,[ContinentLocationId]
			   ,[WorldRegionLocationId])
	SELECT L.[LocationId]
		  ,L.[LocationFeature]
		  ,1 as BaseAdminLevelId
		  ,-45282 as Admin0LocationId
		  ,R.Admin1LocationId
		  ,R.Admin2LocationId
		  ,-46118 as [ContinentLocationId]
		  ,-4842 as [WorldRegionLocationId]
	  FROM [Universe].[dbo].[Locations] L, 
	  @Relationship2To1 R
	Where LocationTypeId = 65 --admin 1
	and Left(HashKey, LEN('@CountryName-')) = '@CountryName-'
	and R.Admin2LocationId = L.LocationId
END
GO
