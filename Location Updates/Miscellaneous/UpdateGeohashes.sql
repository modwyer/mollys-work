-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE UpdateGeohashes
	-- Add the parameters for the stored procedure here
	@CountryLocationId,
	@LocationTypeId,
	@CountryName
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
--if @LocationTypeId = 68
Create Table #Features (RunId int identity(1,1), Admin2LocationId int, Shape geometry, primary key (RunId), unique (Admin2LocationId));

Create Table #Geohashes (Geohash6 char(6), Shape geometry, primary key (Geohash6));

--STOP   If no error, proceed to next statement

Declare @Env Geometry

Select @Env = LocationFeature.STEnvelope()
From Universe.dbo.Locations
Where LocationId = -45282 --@CountryLocationId

--This is to get the bounding box to put into the spatial indexes below
--Declare @MinLong as int
--Declate @MaxLong as int
--Declare @MinLat as int
--Declare @MaxLat as int
Select floor(Min(Longitude)) as MinLong, ceiling(Max(longitude)) as MaxLong, 
floor(Min(Latitude)) as MinLat, ceiling(Max(Latitude)) as MaxLat
From 
	(
	Select @Env.STPointN(1).STX as Longitude, @Env.STPointN(1).STY as Latitude
	UNION ALL
	Select @Env.STPointN(2).STX as Longitude, @Env.STPointN(2).STY as Latitude
	UNION ALL
	Select @Env.STPointN(3).STX as Longitude, @Env.STPointN(3).STY as Latitude
	UNION ALL
	Select @Env.STPointN(4).STX as Longitude, @Env.STPointN(4).STY as Latitude
	) i
--record the results in the bounding boxes below
--STOP   If no error, proceed to next statement

--Build Spatial indexes to help speed up the updates on Geohash lookup table

--If error creating spatial index, simply drop them and redo:
--drop index #SPX_Geohashes on #Geohashes
--drop index #SPX_Features on #Features

CREATE SPATIAL INDEX #SPX_Geohashes ON #Geohashes
(
	[shape]
)USING  GEOMETRY_GRID 
WITH (
--bounding_box = (@MinLong, @MinLat, @MaxLong, @MaxLat)
BOUNDING_BOX =(29, -12, 41, 0), 
GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
CELLS_PER_OBJECT = 16);

CREATE SPATIAL INDEX #SPX_Features ON #Features
(
	[shape]
)USING  GEOMETRY_GRID 
WITH (
--bounding_box = (@MinLong, @MinLat, @MaxLong, @MaxLat)
BOUNDING_BOX =(29, -12, 41, 0), GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
CELLS_PER_OBJECT = 16);

--STOP   If no error, proceed to next statement

--Convert the wkt from the geohash lookup table into a geometry data type in a temp table
--this may take a minute or two
Truncate Table #Geohashes
INSERT INTO #Geohashes
(Geohash6, Shape)
Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326)
From [Universe].[dbo].[GeoHash6LocationsLookup]
Where Admin0LocationId = -45282 --@CountryLocationId
Order by Geohash6


--STOP   If no error, proceed to next statement

--Insert all the features that you added so that you can loop through them and grab the geohash coordinates that are within each feature
Truncate Table #Features
Insert into #Features 
(Admin2LocationId, Shape)
Select LocationId, LocationFeature 
FROM [Universe].[dbo].[Locations] L
Where L.LocationTypeId = 68 --admin2;   @LocationTypeId
and Left(Hashkey, LEN('Tanzania-')) = 'Tanzania-'   --@CountryName

select COUNT(*) from #Features  --this is a doublecheck
--STOP   If no error, proceed to next statement

--This will loop through and update all the geohash lookups to have the new admin2 locationId
--This took about 3 minutes for admin2 for Kenya
Declare @Geohash6 char(7), @Shape geometry, @ActiveAdmin2LocationId int --> again change the (#) to match the length of text in red above
Declare @ActiveRunId int, @MaxRunId int

Select @ActiveRunId = Min(RunId), @MaxRunId = Max(RunId)
From #Features

While @ActiveRunId <= @MaxRunId
BEGIN
	Select @ActiveAdmin2LocationId = Admin2LocationId , @Shape = Shape 
	From #Features 
	Where RunId = @ActiveRunId  
	
	Update [Universe].[dbo].[GeoHash6LocationsLookup]
	Set Admin2LocationId = @ActiveAdmin2LocationId,
		Admin3LocationId = Null, 
		Admin4LocationId = Null, 
		Admin5LocationId = Null
	Where Geohash6 in ( 
		Select Geohash6
		From #Geohashes
		Where Shape.STWithin(@Shape) = 1
		)
		and Admin0LocationId = -45282     --@CountryLocationId
		

	set @ActiveRunId = @ActiveRunId + 1
END

--STOP   If no errors, proceed to next statement

--Find all the geohashes that are still pointing to old admin2 locations in Tanzania
Select LocationName, LocationFeature
From Universe.dbo.Locations
Where LocationTypeId = 68 and CHARINDEX(', Ethiopia', LocationName)>0--Left(Hashkey, LEN('Ethiopia-')) = 'Ethiopia-'
or CHARINDEX(', Eritrea',LocationName)>0
UNION ALL
Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326)
From [Universe].[dbo].[GeoHash6LocationsLookup]
Where Admin0LocationId = -45303
and Admin2LocationId not in 
	(Select LocationId 
	from Universe.dbo.Locations 
	Where LocationTypeId = 68 and Left(Hashkey, LEN('Ethiopia-')) = 'Ethiopia-')
--16412
--16243
--STOP   If no error, proceed to next statement
--now run just the second Select above to make sure there are no extra geohashes pointing to old admin2 locations

--run the following Select to check count	
Select count(*) 
From [Universe].[dbo].[GeoHash6LocationsLookup]
Where Admin0LocationId = -45282 

-------****if no extra geohashes, do not run any script below this line. You are done and can now test! **** -----------	
Create Table #NeedToBufferPoints (RunId int identity(1,1), Geohash6 char(6), Shape Geometry )

--Buffer the geohash point so that we can intersect it with features (since
--the point was not within a feature
Insert into #NeedToBufferPoints
(Geohash6, Shape)
Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326).STBuffer(0.05)
From [Universe].[dbo].[GeoHash6LocationsLookup]
Where Admin0LocationId = -45282 
and Admin2LocationId not in 
	(Select LocationId 
	from Universe.dbo.Locations 
	Where LocationTypeId = 68 and Left(Hashkey, LEN('Tanzania-')) = 'Tanzania-')

select * from #NeedToBufferPoints
--STOP, If no error, proceed to next statement

Create Table #AreaThatIntersectsBuffer (Geohash6 char(6), Admin2LocationId int, Area float)

--Get the intersection of the buffered geohash point with the admin2 polygon feature
Truncate Table 	#AreaThatIntersectsBuffer
INSERT INTO #AreaThatIntersectsBuffer
(Geohash6, Admin2LocationId, Area)
Select Geohash6, Admin2LocationId, F.Shape.STIntersection(B.Shape).STArea()
From #Features F, #NeedToBufferPoints B
Where F.Shape.STIntersects(B.Shape) = 1


--STOP, If no error, proceed to next statement

Create Table #UpdateForGeohashAdmin2 (Geohash6 char(6), Admin2LocationId int)

--Get the max area for each of the intersections and then associate
--the geohash with the locationId that has that max
INSERT INTO #UpdateForGeohashAdmin2
(Geohash6, Admin2LocationId)
Select B.Geohash6, B.Admin2LocationId
From #AreaThatIntersectsBuffer B,	
	(
	Select Geohash6, Max(Area) as MaxArea
	From #AreaThatIntersectsBuffer
	Group by Geohash6
	) I
Where B.Geohash6 = I.Geohash6
and B.Area = I.MaxArea
order by B.Geohash6, B.area desc

--Just take a quick look at the returned records to make sure it looks good before the update
Select *
From 
	[Universe].[dbo].[GeoHash6LocationsLookup] G,
	#UpdateForGeohashAdmin2 U
Where G.Geohash6 = U.Geohash6
	and G.Admin0LocationId = -45282

--Update those pesky geohashs that were not contained by the new features
Update [Universe].[dbo].[GeoHash6LocationsLookup]
Set Admin2LocationId = U.Admin2LocationId
From 
	[Universe].[dbo].[GeoHash6LocationsLookup] G,
	#UpdateForGeohashAdmin2 U
Where G.Geohash6 = U.Geohash6
	and G.Admin0LocationId = -45282
END
GO
