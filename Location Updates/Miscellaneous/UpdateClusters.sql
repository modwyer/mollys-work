/*
get all databases on the current SQL instance
*/
Select * from sys.databases


System objects

Use Universe; 
Select * from INFORMATION_SCHEMA.TABLES Where TABLE_NAME = 'Locations'

select * from INFORMATION_SCHEMA.

--Loop through all databases (excluding Universe)  - While
--For active database see if Locations table is present
--if yes, then update using dynamic SQL like below
*/

select * from ICRISAT_StarCluster_Data.[dbo].[Locations]
Where LocationTypeId = 68
and Charindex( ', Ethiopia',LocationName,0) > 0
and Charindex( ', Ethiopia (',LocationName,0) = 0

--take results of the following select and make that the table to look through
Declare @strDatabase varchar(100)
Declare @CountryName varchar(250)
Declare @databaseName varchar(100)
Declare @U varchar(250)
Declare name_cursor cursor for
	Select name 
	from sys.databases 
	where charindex('StarCluster',name)>0
	and charindex('Base',name) = 0;

Set @CountryName = 'Ethiopia'
--Set @databaseName = @strDatabase + '.dbo.Locations'
--Set @U = 'Update ' + @databaseName + CHAR(13)
--	+ 'Set LocationName = U.LocationName' + CHAR(13)
--	+ 'From ' + @databaseName + ' L,' + CHAR(13)
--	+ 'Universe.dbo.Locations U' + CHAR(13)
--	+ 'Where U.LocationTypeId = 68' + CHAR(13)
--	+ 'and U.LocationId = L.LocationId' + CHAR(13)
--	+ ' and Charindex(' + char(39) + ', '+@CountryName + char(39) + ',LocationName,0) > 0'  + char(13) 
--	+ ' and Charindex(' + char(39) + ', '+@CountryName + ' (' + char(39) + ',LocationName,0) = 0'  + char(13) 

Open name_cursor;
While @@FETCH_STATUS = 0 --0=successful
	Begin
		Fetch next from name_cursor Into @strDatabase; 
		Set @databaseName = @strDatabase + '.dbo.Locations'
		Set @U = 'Update ' + @databaseName + CHAR(13)
			+ 'Set LocationName = U.LocationName' + CHAR(13)
			+ 'From ' + @databaseName + ' L,' + CHAR(13)
			+ 'Universe.dbo.Locations U' + CHAR(13)
			+ 'Where U.LocationTypeId = 68' + CHAR(13)
			+ 'and U.LocationId = L.LocationId' + CHAR(13)
			+ ' and Charindex(' + char(39) + ', '+@CountryName + char(39) + ',LocationName,0) > 0'  + char(13) 
			+ ' and Charindex(' + char(39) + ', '+@CountryName + ' (' + char(39) + ',LocationName,0) = 0'  + char(13) 
		Exec (@U)
		--Update @databaseName  --WorldBank_StarCluster_Data.[dbo].[Locations]
		--Set LocationName = U.LocationName
		--From @databaseName L, --WorldBank_StarCluster_Data.[dbo].[Locations] L, 
		--Universe.[dbo].[Locations] U
		--Where U.LocationTypeId = 68
		--and U.LocationId = L.LocationId
		--and Charindex(', '+@CountryName+'',L.LocationName,0) > 0
		--and Charindex(', '+@CountryName+ ' (',L.LocationName,0) = 0 
	End
CLOSE name_cursor;
DEALLOCATE name_cursor;
Go
--The above just updates the old files with 2014. Where does the insert of the new files come in? I thought that was done in the procedure already.



--Set @strSQL = 
--		'Update ' + @strDatabase + CHAR(13) --WorldBank_StarCluster_Data.[dbo].[Locations]
--		+'Set LocationName = U.LocationName ' + CHAR(13)
--		+ 'From ' + @strDatabase + 'L,' + CHAR(13) --WorldBank_StarCluster_Data.[dbo].[Locations] L, 
--		+ 'Universe.[dbo].[Locations] U' + CHAR(13)
--		+ 'Where U.LocationTypeId = 68' + CHAR(13)
--		+ 'and U.LocationId = L.LocationId' + CHAR(13)
--		+ ' and Charindex(' + char(39) + ', '+@CountryName + char(39) + ' ,LocationName,0) > 0'  + char(13)
--		+ ' and Charindex(' + char(39) + ', '+@CountryName + ' (' + char(39) + ' ,LocationName,0) = 0'  + char(13) 
		
		
Select * From WorldBank_StarCluster_Data.[dbo].[Locations]
Where LocationTypeId = 68
and Charindex( ', Ethiopia',LocationName,0) > 0 
and Charindex( ', Ethiopia (2014)',LocationName,0) = 0 


Update ICRISAT_StarCluster_Data.[dbo].[Locations]
Set LocationName = U.LocationName 
From ICRISAT_StarCluster_Data.[dbo].[Locations] L, Universe.[dbo].[Locations] U
Where U.LocationTypeId = 68
and U.LocationId = L.LocationId
and Charindex( ', Ethiopia',L.LocationName,0) > 0 

Update UserData_StarCluster_Data.[dbo].[Locations]
Set LocationName = U.LocationName 
From UserData_StarCluster_Data.[dbo].[Locations] L, 
Universe.[dbo].[Locations] U
Where U.LocationTypeId = 68
and U.LocationId = L.LocationId
and Charindex( ', Ethiopia',L.LocationName,0) > 0 