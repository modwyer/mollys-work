USE [awmDatabase]
Use WT_Processing
GO
/****** Object:  StoredProcedure [dbo].[Generate_Shape_File_From_View]    Script Date: 01/21/2015 11:42:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Jim Hancock
-- Create date: 09/28/2011
-- Description:	Creates a shape file from a view
--
--UPdates: DJA 12/18/2014 --Removed the things and that were specific to weather on Data3Dev and repurposed to work with Platform
/*
Exec Generate_Shape_File_From_View 'G:\SQLMounts\Operations\Transfer\ExportedShapes', 'vGlobalCountryBorders', 'GB_Spatial', '[where clause if desired]', '[text to append to file name]'
Exec Generate_Shape_File_From_View_data3dev 'G:\SQLMounts\Operations\Transfer\ExportedShapes', 'vGlobalCountryBorders', 'GB_Spatial','',''
type the following into Windows explorer to find the files. Copy those to whatever drive you want to store them in.
\\data1dev\transfer\ExportedShapes
*/ 
-- =============================================
alter PROCEDURE [dbo].[Generate_Shape_File_From_View_data3dev]
	@FilePath varchar(200) 
	,@Viewname varchar(100)
	,@Database varchar(100) 
	,@WhereClause varchar(400) = null
	,@filenamesuffix varchar(20) = null
AS
BEGIN

	SET NOCOUNT ON;

Declare @Filename varchar(50)
Declare @cmdstr varchar(2000)
Declare @SQLstr varchar(2000)
Declare @DataSource varchar(50)
Declare @DataSourceType varchar(50)

Select @Filename = @Viewname + (case when @filenamesuffix is null then '' else '_' + @filenamesuffix end)

print 'FileName'
print @Filename

Set @SQLstr = 'Select * from ' 

+ @Database + '..' + @Viewname  
			+ ' ' + ISNULL(@WhereClause, '')
			+ '" "MSSQL:server=DATA3DEV\;database=' + @Database + ';Tables=' + @Viewname + '"'
			--+ '" "MSSQL:server=DATA1DEV\SS08STD;database=' + @Database + ';UID=testuser;pwd=testuser;Tables=' + @DataSource + '"'

Set @cmdstr = 'exec xp_cmdshell ' + CHAR(39) + 'C:\OSGeo4W\bin\ogr2ogr -f "ESRI Shapefile" -overwrite ' 
			+ @FilePath + ' -nln ' 
			+ @Filename + ' -sql "'
			+ @SQLstr 
			+ CHAR(39)
			--+ ', NO_OUTPUT'

Print ISNULL(Cast(@cmdstr as varchar(2000)), '@cmdstr is null')
exec (@cmdstr)
END





