Declare @tempcentroid geometry

Select @tempcentroid = CentroidFeature
From Locations 
Where LocationId = 91712395

Select @tempcentroid.STWithin(LocationFeature) as CentroidWithin, LocationName, LocationId
	From Locations
	Where LocationTypeId = 1 --in (68,65, 23, 25, 26)
	and @tempcentroid.STWithin(LocationFeature) <>0

Select * from Universe..BaseAdminFeatures
Where Admin0LocationId = -45301 
and [BaseFeature].STIsValid() = 0

Select * from Universe..Locations
Where LocationTypeId = 68
and charindex(', Uganda',locationname)>0
and charindex(', Uganda(',locationname)=0
and charindex(', Uganda (',locationname)=0
and [LocationFeature].STIsValid() = 0

Select * from Universe..Locations
Where LocationTypeId = 1
and CentroidFeature.STWithin(SElect LocationFeature from Locations Where LocationId = 91712395)