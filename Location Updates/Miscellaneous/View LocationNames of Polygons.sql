Select l1.locationid, l2.LocationName, l1.LocationFeature
From Universe..Locations l1
inner join
Universe..Locations l2
on l1.Locationid = l2.LocationId
Where l1.LocationTypeId = 68
and charindex(', Pakistan', l1.locationname)>0
and charindex(', India (', l1.locationname)=0
and charindex(', Indian', l1.locationname)=0