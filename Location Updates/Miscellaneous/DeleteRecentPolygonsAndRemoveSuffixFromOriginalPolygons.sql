USE [TestDatabase]
GO
/****** Object:  StoredProcedure [dbo].[DeleteRecentPolygonsAndRemoveSuffixFromOriginalPolygons]    Script Date: 2/9/2015******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Molly O'Dwyer
-- Create date: 2/9/2015
-- Description:	Delete newly added polygons and remove suffix from original locations in order to redo the update procedure
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Script to remove old polygons and remove suffix from original polygons. Do this before reloading polygons.

Run:

Exec DeleteRecentPolygonsAndRemoveSuffixFromOriginalPolygons 'Uganda', 68, '2014'

*/
Alter PROCEDURE [dbo].[DeleteRecentPolygonsAndRemoveSuffixFromOriginalPolygons] 
	-- Add the parameters for the stored procedure here
	@CountryName varchar(250),
	@LocationTypeId smallint,
	@PriorYear varchar(20)
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--Uncomment the 3 following declares for testing:
	--Declare @CountryName varchar(250) = 'Uganda'
	--Declare @LocationTypeId smallint = 68
	--Declare @PriorYear varchar(20)= '2014'

	Declare @tableName varchar(100)
	Declare @U varchar(2000)
	Declare @Delete varchar(2000)
	Declare @strDatabase varchar(100)
	Declare @DatabaseNames Table (RunId int identity(1,1), DatabaseName varchar(250))
	Declare @SubSelectWhereForDelete varchar(2000)
	Declare @SubSelectWhereForUpdate varchar(2000)
	Declare @LocationsWhereForDelete varchar(2000)
	
	Set @SubSelectWhereForUpdate= 'Where LocationTypeId = ' + cast(@LocationTypeId as varchar(10)) + CHAR(13)
					+ 'and Charindex(' + CHAR(39) + ', ' + @CountryName+ ' (' + @PriorYear + ')' + CHAR(39) + ',LocationName,0) > 0'
	
	Set @SubSelectWhereForDelete = 'Where LocationTypeId = ' + cast(@LocationTypeId as varchar(10)) + CHAR(13)	
					+ ' and Charindex(' + char(39) + ', '+@CountryName + char(39) + ',L.LocationName,0) > 0'  + char(13) 
					+ ' and Charindex(' + char(39) + ', '+@CountryName + ' (' + CHAR(39) + ',L.LocationName,0) = 0'  + char(13)	
	
	Set @LocationsWhereForDelete = 'Where LocationTypeId = ' + cast(@LocationTypeId as varchar(10)) + CHAR(13)	
					+ ' and Charindex(' + char(39) + ', '+@CountryName + char(39) + ',LocationName,0) > 0'  + char(13) 
					+ ' and Charindex(' + char(39) + ', '+@CountryName + ' (' + CHAR(39) + ',LocationName,0) = 0'  + char(13)	
	
	/*Delete recent polygons from Universe (ones without suffix)*/
	Print (upper('*** Ready to delete last loaded locations from databases for ' + @CountryName + ' ***') + char(13))
	
	--Get list of databases
	INSERT INTO @DatabaseNames 
	(DatabaseName)
	Select name 
	from sys.databases 
	where charindex('StarCluster',name)>0 --would like to simplify this
	and charindex('Base',name) = 0;

	Declare @DeleteActiveRunId int, @DeleteMaxRunId int
	
		Select @DeleteActiveRunId = MIN(RunId), @DeleteMaxRunId = MAX(RunId)
		From @DatabaseNames 
	
	Print (char(13) + upper('::: Deleting last loaded locations from child databases :::') + char(13))
	
	/*Loop through database list and delete recent locations without a 
	suffix for specified country*/
	
	While @DeleteActiveRunId <= @DeleteMaxRunId 
	BEGIN
			Select @strDatabase = [DatabaseName]
			From @DatabaseNames 
			Where RunId = @DeleteActiveRunId 

			--Delete related location IDs and location IDs in LocationRelationships table that do not have a suffix for specified country
			Set @tableName = @strDatabase + '.dbo.LocationRelationships'
	
			Print (upper('Operating on ' + char(13)	+ 'database: ') + @strDatabase)
			Print (upper('table: ') +'LocationRelationsips')
			Set @Delete = 
				'Delete	From ' + @tableName + char(13)
				+ 'Where LocationId in' + CHAR(13)
					+ '(Select LocationId From ' + @strDatabase + '.dbo.Locations L' + char(13)
					+ @SubSelectWhereForDelete				
					+ ')' + char(13)
					+ 'or RelatedLocationId in' + char(13)
						+ '(Select LocationId From ' + @strDatabase + '.dbo.Locations L' + char(13)
						+ @SubSelectWhereForDelete				
						+ ')'
			Print (char(13) + upper('script: ') + char(13) + @Delete)
			Exec    (@Delete)
			
			Print ('<<< LocationRelationships delete COMPLETE >>' + char(13))
			
			--Delete related location IDs from StarData_SQLVariant
			Set @tableName = @strDatabase + '.dbo.StarData_SQLVariant'
	
			Print (upper('Operating on ' + char(13)	
						+ 'database: ') + @strDatabase)
			Print (upper('table: ') +'StarData_SQLVariant')
			Set @Delete = 
				'Delete	From ' + @tableName + char(13)
				+ 'Where LocationId in' + CHAR(13)
					+ '(Select LocationId From ' + @strDatabase + '.dbo.Locations L' + char(13)
					+ @SubSelectWhereForDelete			
					+ ')'
			Print (char(13) + upper('script: ') + char(13) + @Delete)
			Exec    (@Delete)
			
			Print ('<<< StarData_SQLVariant delete COMPLETE >>' + char(13))

			--Delete related location IDs from StarData_ExtendedString
			Set @tableName = @strDatabase + '.dbo.StarData_ExtendedString'
	
			Print (upper('Operating on ' + char(13)	
						+ 'database: ') + @strDatabase)
			Print (upper('table: ') + 'StarData_ExtendedString')
			Set @Delete = 
				'Delete	From ' + @tableName + char(13)
				+ 'Where LocationId in' + CHAR(13)
					+ '(Select LocationId From ' + @strDatabase + '.dbo.Locations L' + char(13)
					+ @SubSelectWhereForDelete			
					+ ')'
			Print (char(13) + upper('script: ') + char(13) + @Delete)
			Exec    (@Delete)
			
			Print ('<<< StarData_ExtendedString delete COMPLETE >>>' + char(13))
					
			--Delete related location IDs from StarData_Float
			Set @tableName = @strDatabase + '.dbo.StarData_Float'
	
			Print (upper('Operating on ' + char(13)	
						+ 'database: ') + @strDatabase)
			Print (upper('table: ') + 'StarData_Float')
			Set @Delete = 
				'Delete	From ' + @tableName + char(13)
				+ 'Where LocationId in' + CHAR(13)
					+ '(Select LocationId From ' + @strDatabase + '.dbo.Locations L' + char(13)
					+ @SubSelectWhereForDelete			
					+ ')'
			Print (char(13) + upper('script: ') + char(13) + @Delete)
			Exec    (@Delete)
			
			Print ('<<< StarData_Float delete COMPLETE >>>' + char(13))
			
				
			--Delete location IDs from Locations tables
			Set @tableName = @strDatabase + '.dbo.Locations'
	
			Print (upper('Operating on ' + char(13)	
						+ 'database: ') + @strDatabase)
			Print (upper('table: ') + 'Locations')
			Set @Delete = 
				'Delete	From ' + @tableName + char(13)
				+ @LocationsWhereForDelete
			Print (char(13) + upper('script: ') + char(13) + @Delete)
			Exec    (@Delete)
			
			Print ('<<< Locations delete COMPLETE >>>' + char(13))
			set @DeleteActiveRunId= @DeleteActiveRunId + 1
	End
			
	Print (upper('<<< Fininshed deleting last loaded locations from child databases >>>') + char(13) 
			+ char(13) + upper('::: Deleting last loaded locations from Universe :::') + char(13))
	
	Set @Delete = 'Delete From Universe.dbo.Locations ' +CHAR(13) 
		+ @LocationsWhereForDelete
	Print (char(13) + upper('script: ') + char(13) + @Delete)
	Exec    (@Delete)
	
	Print ('<<< Locations delete COMPLETE >>>' + char(13))
		
	/*Remove suffixes from Universe locations table*/
	Print (upper('*** Ready to remove suffix from now current locations ***') + char(13))
	
	Print (upper('Operating on ' + char(13)	
			+ 'database: ') + 'Universe')
	Print (upper('table: ') + 'Locations')
	Set @U = 'Update Universe.dbo.Locations ' + CHAR(13) 
			+ 'Set LocationName = PresentationLocationName ' + CHAR(13)
			+ @SubSelectWhereForUpdate
	
	Print (char(13) + upper('script: ') + char(13) + @U)
	Exec    (@U)
	
	Print (char(13) + upper('<<< Suffixes removed from Universe..Locations table >>>'))
	
	Print (char(13) + upper('::: Removing suffixes from child databases :::') + char(13))
	
	Declare @ActiveRunId int, @MaxRunId int
	
	Select @ActiveRunId = MIN(RunId), @MaxRunId = MAX(RunId)
	From @DatabaseNames
		
	While @ActiveRunId <= @MaxRunId 
	BEGIN
			Select @strDatabase = [DatabaseName]
			From @DatabaseNames 
			Where RunId = @ActiveRunId 
			
			Set @tableName = @strDatabase + '.dbo.Locations'
			
			Print (upper('Updating ' + char(13)	
					+ 'database: ') + @strDatabase)
			Print (upper('table: ') + 'Locations')
			
			--this updates from PresentationLocationName of same table
			Set @U = 'Update ' + @tableName + char(13)
			+ 'Set LocationName = PresentationLocationName' + char(13)
			+ @SubSelectWhereForUpdate
			
			Print (char(13) + upper('script: ') + char(13) + @U)
			Exec    (@U)
			Print (char(13) + upper('<<< Suffix (' + @PriorYear + ') successfully removed from ') 
					+ @tableName + ' >>>' + char(13))
					
			set @ActiveRunId= @ActiveRunId + 1
		End
		
	Print (upper('*** ' + @CountryName + ' locations are ready to be reloaded!! ***'))
		
End


/* Check databases here 
*/
--Select *
--From UserData_StarCluster_Data..Locations
--Where LocationTypeId = 68
--and CHARINDEX(', Ethiopia', LocationName)>0