/*** Script to find centroids for Admin 2 locations using a character search ***/

Use Universe;
SELECT [LocationId]
      ,[LocationName]
      ,[CentroidFeature].STY as Latitude --STY must be capitalized, parses out lat
      ,[CentroidFeature].STX as Longitude --STX must be capitalized, parses out long
      ,[LocationFeature]
      ,[DateLastChanged]
      ,[PresentationLocationName]
  FROM [Universe].[dbo].[Locations]  
  WHERE LocationTypeID = 65 --found in LocationTypes table; 25 = World Regions, 23 = Countries, 65 = admin1, 68 = admin2, 70 = admin3, 71 = admin4, 72 = admin5   
  and CHARINDEX(', Ghana',LocationName) > 0 
  and CHARINDEX(', Ghana (',LocationName) = 0 
  --and CHARINDEX(', Indian',LocationName) = 0 
  order by LocationName
  
  --count number of admin 1 locations, group by country where LocationTypeID