/*Updating location names and polygons peicemeal*/


--Find bad polygon ID
use Universe;
select * from Locations 
where LocationTypeId = 68 
	and CHARINDEX(', Ethiopia', LocationName)>0
	and CHARINDEX(', Ethiopia (', LocationName)=0
	

--Remove bad polygons
Delete
From Universe.dbo.Locations
Where LocationId = 91525936

--Name changes
Update Universe.dbo.Locations
  set LocationName = 'Konso Special Woreda, SNNPR, Ethiopia'
  Where LocationName = 'Konso Sp Woreda, SNNPR, Ethiopia'
  
--Insert good polygon into the space where bad polygon once resided  
INSERT INTO [Universe].[dbo].[Locations]
			   ([LocationName]
			   ,[LocationTypeId]
			   ,[LocationFeature]
			   ,[CentroidFeature]
			   ,[PresentationLocationName]
			   ,HashKey)
	Select [LocationName]
			   ,[LocationTypeId]
			   ,[LocationFeature]
			   ,[Centroid]
			   ,[PresentationLocationName]
			   ,[HashKey]
From Testdatabase.dbo.vAdmin2Ethiopia v
WHERE v.LocationName not in 
		(Select LocationName 
		From Universe.dbo.Locations 
		Where LocationTypeId = 68
		and CHARINDEX('Ethiopia', LocationName)>0
		and CHARINDEX('Ethiopia (',LocationName)=0)
	
Select * from Universe.dbo.Locations
where LocationTypeId = 68
and CHARINDEX('Ethiopia',LocationName)>0
and CHARINDEX('Ethiopia (',LocationName)=0
		
