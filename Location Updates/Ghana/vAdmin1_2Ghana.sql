USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAdmin1_2Ghana]    Script Date: 8/18/2015 10:43:21 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE view [dbo].[vAdmin1_2Ghana]
as 
SELECT 
	District_1 as Admin2
	, Region_1 as Admin1
	,68 as LocationTypeId
	,[Shape] as Shape
	,[ID] as Id
	, [awmDatabase].[dbo].[GetGeometryForCoordinate] (INSIDE_Y, INSIDE_X) as Centroid --use this when you import the centroid x and y columns with the table
	--,case when (case when [Shape].MakeValid().STCentroid() is null 
	--then [Shape].MakeValid().STEnvelope().STCentroid() 
	--else [Shape].MakeValid().STCentroid() end).STIntersects(Shape) = 1 then 
	--(case when [Shape].MakeValid().STCentroid() is null 
	--then [Shape].MakeValid().STEnvelope().STCentroid() 
	--else [Shape].MakeValid().STCentroid() end)
	--else Shape.STPointN(1) end as Centroid
  FROM [TestDatabase].[dbo].[GhanaAdmin2] 

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'View of original admin 2 boundary data for Ghana formatted for our database.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vAdmin1_2Ghana'
GO

