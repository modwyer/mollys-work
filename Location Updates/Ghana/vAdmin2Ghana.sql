USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAdmin2Ghana]    Script Date: 8/18/2015 10:46:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[vAdmin2Ghana]
as 
SELECT 
	v.Admin2 + ', ' + l.locationname as LocationName
	, v.Centroid
	,[Shape] as LocationFeature
	,v.Id
	,v.LocationTypeId
	,v.Admin2 + ', ' + v.Admin1 + ', Ghana' as PresentationLocationName
	,'Ghana-' + CAST(v.Id as varchar(13)) as Hashkey
	FROM vAdmin1_2Ghana v, Universe..Locations l
	where v.Centroid.STWithin(l.LocationFeature) = 1
	and l.LocationTypeId = 65 --admin1
	and Charindex(', Ghana',L.LocationName,0) > 0 
	and Charindex(', Ghana (',L.LocationName,0) = 0 
	and l.HashKey is not null

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'View of final admin 2 boundary data for Ghana formatted for our database.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vAdmin2Ghana'
GO
