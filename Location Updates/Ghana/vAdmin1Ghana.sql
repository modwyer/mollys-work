USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAdmin1Ghana]    Script Date: 8/18/2015 10:39:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE view [dbo].[vAdmin1Ghana]
as 
SELECT 
	Admin1 + ', Ghana' as LocationName
	,65 as LocationTypeId
	,[Shape] as LocationFeature
	,[OBJECTID] as Id
	,case when [Shape].MakeValid().STCentroid() is null then [Shape].MakeValid().STEnvelope().STCentroid() else [Shape].MakeValid().STCentroid() end as Centroid
	,Admin1 + ', Ghana' as PresentationLocationName
	,'Ghana-' + CAST(Id as varchar(8)) as Hashkey
  FROM [TestDatabase].[dbo].[GhanaAdmin1]




GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'View of original admin 1 boundary data for Ghana formatted for our database.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vAdmin1Ghana'
GO


