

/* The centroid for admin 2 is not within the polygon and this is causing it to map to other admin 1 where the centroid falls within 
92095257	Bulambuli, Kapchorwa, Uganda should map to:
91191623	Bulambuli, Uganda not to:
91191638	Kapchorwa, Uganda
*/

Select *
From Universe.dbo.Locations 
Where LocationTypeId = 65
and charindex(', Uganda',locationname)>0
and charindex(', Uganda(',locationname)=0
and charindex(', Uganda (',locationname)=0


--Step 1. Update the base admin 1 for Bulambuli a2 so it points to Bulambuli, not Kapchorwa
Update Universe..BaseAdminFeatures
Set Admin1LocationId = 91191623 --Bulambuli, Uganda
Where Admin0LocationId = -45301
and Admin1LocationId = 91191638 --Kapchorwa
and Admin2LocationId = 92095257 --Bulambuli

--Step 2. Update Name for Bulambuli
Update Universe..Locations
Set LocationName = 'Bulambuli, Bulambuli, Uganda'
Where LocationId = 92095257

--Step 3. Update Presentation name
Update Universe..Locations
Set PresentationLocationName = 'Bulambuli, Bulambuli, Uganda'
Where LocationId = 92095257

--Step 4. Check other databases and run steps 2-3
Update WorldBank_StarCluster_Data..Locations
Set LocationName = 'Bulambuli, Bulambuli, Uganda'
Where LocationId = 92095257

Update WorldBank_StarCluster_Data..Locations
Set PresentationLocationName = 'Bulambuli, Bulambuli, Uganda'
Where LocationId = 92095257

Update UserData_StarCluster_Data..Locations
Set LocationName = 'Bulambuli, Bulambuli, Uganda'
Where LocationId = 92095257

Update UserData_StarCluster_Data..Locations
Set PresentationLocationName = 'Bulambuli, Bulambuli, Uganda'
Where LocationId = 92095257

Update ICRISAT_StarCluster_Data..Locations
Set LocationName = 'Bulambuli, Bulambuli, Uganda'
Where LocationId = 92095257

Update ICRISAT_StarCluster_Data..Locations
Set PresentationLocationName = 'Bulambuli, Bulambuli, Uganda'
Where LocationId = 92095257

--Step 5. Fix Kaberamaido admin 1 polygon not showing up by making the geography valid
Update Universe..Locations
Set LocationFeature = awmDatabase.dbo.GetValidShapeForGeometryAndGeography(LocationFeature) 
Where LocationId = 91191552 --Kaberamaido, Uganda

--Step 6. Rerun geohash update
Exec [4UpdateGeohashes_LoopBaseFeaturesFaster] -45301 --completes in 2:28 minutes

/* Random select statements to test fixes. You can ignore these or use them if you need.
Select L.LocationName, G.*
From Geohash6LocationsLookup G
inner join
Locations L
on G.Admin1LocationId = L.LocationId
Where Admin0LocationId = -45301
and Admin2LocationId = 91725575

Select L.LocationName, G.*
From BaseAdminFeatures G
inner join
Locations L
on G.Admin2LocationId = L.LocationId
Where Admin0LocationId = -45301
and Admin2LocationId = 91725575


Select L.LocationName, G.*
From WorldBank_STarCluster_Data..LocationRelationships G
inner join
Locations L
on G.LocationId = L.LocationId
Where RelatedLocationId = 91725575

Select *
From Universe.dbo.Locations 
Where LocationTypeId = 65
and charindex(', Uganda',locationname)>0
and charindex(', Uganda(',locationname)=0
and charindex(', Uganda (',locationname)=0
Where LocationId = 91725575

Select *
From Universe..Geohash6LocationsLookup 
Where Admin0LocationId = -45301

*/


