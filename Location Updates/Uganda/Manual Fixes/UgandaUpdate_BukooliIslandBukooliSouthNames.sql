/*** Update Bukooli Island, Bukooli South Location Name - QA LocationIDs ***/

  Update TestDatabase..Uganda_Update_Admin2_32015
  Set County_2 = 'Bukooli Island & Bukooli South'
  Where County_2 = 'Bukooli Island, Bukooli South'

  Update Universe..Locations
  Set LocationName = 'Bukooli Island & Bukooli South, Namiyango, Uganda',
		PresentationLocationName = 'Bukooli Island & Bukooli South, Namiyango, Uganda'
  Where locationid = 92161446 

  Update WorldBank_StarCluster_Data..Locations
  Set LocationName = 'Bukooli Island & Bukooli South, Namiyango, Uganda',
		PresentationLocationName = 'Bukooli Island & Bukooli South, Namiyango, Uganda'
  Where locationid = 92161446
