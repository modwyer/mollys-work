Select *
From Universe..Locations
Where LocationTypeId = 68
and CHARINDEX('Uganda (',LocationName)>0
and (CHARINDEX('Uganda', LocationName)>0
or CHARINDEX('Rwanda',LocationName)>0
or CHARINDEX('South Sudan',LocationName)>0
or CHARINDEX('Congo',LocationName)>0
or (CHARINDEX('Kenya',LocationName)>0 and CHARINDEX('Kenya (',LocationName)=0)
or CHARINDEX('Tanzania',LocationName)>0) --and CHARINDEX('Tanzania (',LocationName)=0)


Select *
From Universe..Locations
Where LocationTypeId = 65
and CHARINDEX(', Uganda',LocationName)>0
and CHARINDEX(', Uganda(',LocationName)=0


/*
Admin1 issue: not showing
90980545	Kaberamaido, Uganda
Admin2s for the above
91712395	Kalaki, Kaberamaido, Uganda
91712390	Kaberamaido, Kaberamaido, Uganda
*/

Select L.LocationName, B.*
From Universe..BaseAdminFeatures B
inner join
Universe..Locations L
on B.Admin2LocationId = L.LocationId
Where B.Admin1LocationID = 90980545
Where CHARINDEX('Kaberamaido, Uganda',L.LocationName)>0
and CHARINDEX('Kaberamaido, Uganda(',LocationName)=0

Select *
From Universe..Locations
Where LocationId = 90980545


Select L.LocationName, B.*
From Universe..GeoHash6LocationsLookup B
inner join
Universe..Locations L
on B.Admin1LocationId = L.LocationId
Where CHARINDEX('Kaberamaido, Uganda',L.LocationName)>0
and CHARINDEX('Kaberamaido, Uganda(',LocationName)=0

Select L.LocationName, B.*
From Universe..BaseAdminFeatures B
inner join
Universe..Locations L
on B.Admin2LocationId = L.LocationId
Where CHARINDEX(', Uganda',L.LocationName)>0
and CHARINDEX(', Uganda(',LocationName)=0

/*
Admin 1 with centroid issue
90980631   Kapchorwa, Uganda K & B points to this one
90980616	Bulambuli, Uganda --nothing points to this but should, not in Base Admin
Admin 2
91712349	Bulambuli, Kapchorwa, Uganda - should be Bulambuli admin 1
91712474	Tingey, Kapchorwa, Uganda
*/
Select L.LocationName, B.*
From Universe..BaseAdminFeatures B
inner join
Universe..Locations L
on B.Admin2LocationId = L.LocationId
Where B.Admin1LocationId = 90980616

Select *
From Universe..Locations
Where LocationTypeId = 68
and CHARINDEX(', Uganda',LocationName)>0
and CHARINDEX(', Uganda (',LocationName)=0
Order by LocationName

/* Changes to make for centroid issue in base admin table
--Update Bulambuli ad min 2 to map to Bulambuli admin 1
 Update Universe..BaseAdminFeatures
 Set Admin1LocationId = 90980616
 Where Admin2LocationId = 91712349

--Update admin 2 location name for Bulambuli
Update Universe..Locations
Set LocationName = 'Bulambuli, Bulambuli, Uganda'
Where LocationName = 'Bulambuli, Kapchorwa, Uganda'
and LocationId = 91712349
*/

Select L.LocationName, B.*
From Universe..GeoHash6LocationsLookup B
inner join
Universe..Locations L
on B.Admin2LocationId = L.LocationId
Where B.Admin1LocationId = 90980616

--Bulambuli admin 1 is not in geohashes at all. Rerun geohashes now that base admin is fixed
/* 