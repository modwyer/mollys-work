Select *
From Universe..Locations
Where CHARINDEX(' Uganda',LocationName)>0
and LocationTypeId = 70



Select *
From Universe..GeoHash6LocationsLookup
Where Admin3LocationId = 90423512

Select *
From Universe..BaseAdminFeatures
Where Admin3LocationId = 90423512

--Set hashkey to null
	Update UserData_StarCluster_Data.[dbo].[Locations]
	Set HashKey = Null
	Where LocationTypeId = 68
	and Charindex(', Uganda (2014)',LocationName,0) > 0

Select LocationId, LocationName, LocationTypeId, HashKey,DateLastChanged
From Universe..Locations
Where LocationTypeId = 68
and CHARINDEX(', Uganda',LocationName)>0
order by DateLastChanged

Select LocationId, CentroidFeature, LocationName
		From Universe.dbo.Locations
		Where LocationTypeId = 68 --admin2
		and Charindex(', Uganda',LocationName,0) > 0				
		and Charindex(', Uganda (',LocationName,0) = 0
		and HashKey like ('Uganda-%')

Select LocationId, CentroidFeature, LocationName
		From Universe.dbo.Locations
		Where LocationTypeId = 65 --admin1
		and Charindex(', Uganda',LocationName,0) > 0				
		and Charindex(', Uganda (',LocationName,0) = 0
		and HashKey like ('Uganda-%')
		
Select LocationId, LocationName, HashKey, DateLastChanged
From WorldBank_StarCluster_Data..Locations
Where LocationTypeId = 68
and Charindex(', Uganda',LocationName,0) > 0				
and Charindex(', Uganda (',LocationName,0) = 0