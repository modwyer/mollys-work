--create the view first
Create View vAdmin1_2Uganda
as 
SELECT 
	--Left([CNAME_2006],1) + right(lower([CNAME_2006]),len([CNAME_2006])-1) as Admin2
	[CNAME_2006] as Admin2
	,68 as LocationTypeId
	,[Shape] as Shape
	,[ID] as Id
	,case when [Shape].MakeValid().STCentroid() is null then [Shape].MakeValid().STEnvelope().STCentroid() else [Shape].MakeValid().STCentroid() end as Centroid
  FROM [TestDatabase].[dbo].[Uganda_Update_21715] 
 
 --find all admin 1 names where the admin 2 centroid is within the admin1 locationfeature 
 Create view vAdmin2Uganda
 as
 Select v.Admin2 + ', ' + l.LocationName as LocationName
	, v.Centroid
	, v.Shape as LocationFeature
	, v.Id
	, v.LocationTypeId
	, v.Admin2 + ', ' + l.LocationName as PresentationLocationName
 from vAdmin1_2Uganda v, Universe.dbo.Locations l
 where v.Centroid.STWithin(l.LocationFeature) = 1
 and l.LocationTypeId = 65 --admin1
 and Charindex(', Uganda',L.LocationName,0) > 0 
 and Charindex(', Uganda(2006)', L.LocationName,0)=0
 and Charindex(', Uganda (2013)', L.LocationName,0)=0
 

 



         
Create view vUgandaCentroids
as
Select
	case when [Uganda_Counties].MakeValid().STCentroid() is null then [Uganda_Counties].MakeValid().STEnvelope().STCentroid() else [Uganda_Counties].MakeValid().STCentroid() end as Centroid
	from TestDatabase.dbo.UgandaAdmin2
	

 --this checks to see if the two admin levels match up
 Select v.Admin2, v.shape
 from vAdmin1Admin2Uganda v
 UNION ALL 
 Select l.LocationName, l.LocationFeature 
 From Universe.dbo.Locations l
 where l.LocationTypeId = 65 --admin1
 and Charindex( ', Uganda',L.LocationName,0) > 0 
 and CHARINDEX(', Uganda(2006)',L.LocationName,0)=0
 
 
 
 
