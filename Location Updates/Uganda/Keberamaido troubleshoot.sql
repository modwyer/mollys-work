

Select 
From Universe..Locations
Where LocationTypeId = 68
and charindex(', Uganda',locationname)>0
and charindex(', Uganda(',locationname)=0
and charindex(', Uganda (',locationname)=0

/*
90980545	Kaberamaido, Uganda
91712395	Kalaki, Kaberamaido, Uganda
91712390	Kaberamaido, Kaberamaido, Uganda
*/

Select *
From WorldBank_StarCluster_Date..Stars


Select *
From Universe..GeoHash6LocationsLookup
Where Admin1LocationId = 90980545

Select *
From Universe..GeoHash6LocationsLookup
Where Admin2LocationId in (91712395, 91712390)


Declare @Env Geometry
Declare @Admin1Shape geometry
--This is to get the bounding box to put into the spatial indexes below
Declare @MinLong as int
Declare @MaxLong as int
Declare @MinLat as int
Declare @MaxLat as int
Declare @strSQL varchar(2000)

Select @Env = LocationFeature.STEnvelope(), @Admin1Shape = LocationFeature
From Universe.dbo.Locations
Where LocationId = 90980545 --Kaberamaido, Uganda

Select @MinLong = floor(Min(Longitude)), @MaxLong = ceiling(Max(longitude)), 
	@MinLat = floor(Min(Latitude)), @MaxLat = ceiling(Max(Latitude))
	From 
		(
		Select @Env.STPointN(1).STX as Longitude, @Env.STPointN(1).STY as Latitude
		UNION ALL
		Select @Env.STPointN(2).STX as Longitude, @Env.STPointN(2).STY as Latitude
		UNION ALL
		Select @Env.STPointN(3).STX as Longitude, @Env.STPointN(3).STY as Latitude
		UNION ALL
		Select @Env.STPointN(4).STX as Longitude, @Env.STPointN(4).STY as Latitude
		) i

set @strSQL = 
	'CREATE SPATIAL INDEX SPX_TempUgandaFeature 
	ON Universe..Locations
	([LocationFeature])
	USING  GEOMETRY_GRID 
	WITH (
	bounding_box = ('+ cast(@MinLong as varchar(4)) + ', ' +  cast(@MinLat as varchar(4)) + ', ' +  cast(@MaxLong as varchar(4)) + ', ' + cast(@MaxLat as varchar(4)) + '), '
	+ ' GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
	CELLS_PER_OBJECT = 16);'
	
	
	exec(@strSQL)


Declare @tempcentroid geometry

Select @tempcentroid = CentroidFeature
From Locations 
Where LocationId = 91712395

Select distinct Admin2LocationId
From Geohash6LocationsLookup
Where Admin2LocationId in (Select LocationId from
	(Select @tempcentroid.STWithin(LocationFeature) as CentroidWithin, LocationName, LocationId
	From Locations
	Where LocationTypeId in (68,65, 23, 25, 26)
	and @tempcentroid.STWithin(LocationFeature) <>0) C 
)



Select ST_GeoHash(1.694349514, 33.12739629, 6)




