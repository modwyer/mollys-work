USE [TestDatabase]
GO

/****** Object:  View [dbo].[vUgandaAdmin1Shapes]    Script Date: 02/20/2015 10:25:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




Create view [dbo].[vUgandaAdmin1Shapes]
as
	(Select LocationId,LocationName,LocationTypeId, LocationFeature, Hashkey
	from Universe.dbo.Locations
	Where LocationTypeId = 65
	and CHARINDEX(', Uganda', LocationName)>0
	and CHARINDEX(', Uganda(',LocationName)=0
	)


GO


