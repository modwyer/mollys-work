Use Universe

Select *
From Universe..BaseAdminFeatures
Where Admin0LocationId = -45301  --91712317

Select *
From Universe..GeoHash6LocationsLookup
Where Admin0LocationId = -45301
order by Admin2LocationId asc


Select *
From Universe..BaseAdminFeatures
Where Admin1LocationId = 90980616

Select *
From Universe..Locations
Where LocationTypeId = 65
and CHARINDEX(' Uganda',LocationName)>0
and CHARINDEX(' Uganda(',LocationName)=0
order by LocationName

/*
90980621	Lwengo, Uganda not in baseadminfeatures or geohash6lookup
90980535	Bugiri, Uganda not in baseadminfeatures or geohash6lookup
90980616	Bulambuli, Uganda 
90980545	Kaberamaido, Uganda both admin 2 geohashes are correct, polygon boundaries are consistent between 1 and 2;
			 not sure why this isn't showing up
90980522	Kiboga, Uganda

admin2 - 
Kaberamaido, Kaberamaido, Uganda	91712390
Kalaki, Kaberamaido, Uganda	91712395
*/

Select *
From universe..Locations
Where LocationId = 90980535

Select *
From Universe..BaseAdminFeatures
Where Admin1LocationId = 90980535


Select *
from universe..Locations
where LocationId = 90980535

Select *
from universe..BaseAdminFeatures
where Admin1LocationId = 90980535

Select *
from universe..GeoHash6LocationsLookup
where Admin1LocationId = 90980535

Select L.LocationName as Admin1Name, G.*
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin1LocationId = L.LocationId
Where G.Admin2LocationId = 91712395


Select L.LocationName as AdminName, G.*
From Universe.dbo.BaseAdminFeatures G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin2LocationId = L.LocationId
Where G.Admin1LocationId = 90980545

--Admin2
Select *
From Universe..GeoHash6LocationsLookup
Where Admin2LocationId in
	(Select LocationId
	From Universe..Locations
	Where LocationTypeId = 68
	and CHARINDEX(' Uganda',LocationName)>0
	and CHARINDEX(' Uganda (',LocationName)=0
	)

--result:none of the new locations are attached to geohashes

Select *
From Universe..BaseAdminFeatures
Where BaseAdminLocationId in
	(Select LocationId
	From Universe..Locations
	Where LocationTypeId = 68
	and CHARINDEX(' Uganda',LocationName)>0
	and CHARINDEX(' Uganda (',LocationName)=0
	)
--result: new locations are in the base admin table

Select *
From Universe..BaseAdminFeatures
Where Admin0LocationId = -45301
and BaseAdminLocationId not in
	(Select LocationId
	From Universe..Locations
	Where LocationTypeId = 68
	and CHARINDEX(' Uganda',LocationName)>0
	and CHARINDEX(' Uganda (',LocationName)=0
	)
--result: there are no other base admins for Ethiopia that are not the new locations

--Admin 1
Select *
From Universe..GeoHash6LocationsLookup
Where Admin1LocationId in
	(Select LocationId, LocationName
	From Universe..Locations
	Where LocationTypeId = 65
	and CHARINDEX(' Uganda',LocationName)>0
	and CHARINDEX(' Uganda(',LocationName)=0
	)

--result: 126,751 geohashes are attached to the new admin1
/* Admin 1 locations not showing in platform map or table (53)
LocationId	LocationName
90980556	Adjumani, Uganda
90980564	Agago, Uganda
90980547	Amuria, Uganda
90980567	Amuru, Uganda
90980557	Arua, Uganda
90980548	Budaka, Uganda
90980549	Bududa, Uganda
90980535	Bugiri, Uganda
90980601	Bukomansimbi, Uganda
90980616	Bulambuli, Uganda
90980627	Buliisa, Uganda
90980573	Bundibugyo, Uganda
90980536	Busia, Uganda
90980611	Butambala, Uganda
90980620	Buvuma, Uganda
90980615	Buyende, Uganda
90980568	Dokolo, Uganda
90980623	Gomba, Uganda
90980586	Ibanda, Uganda
90980537	Iganga, Uganda
90980587	Isingiro, Uganda
90980545	Kaberamaido, Uganda
90980520	Kalangala, Uganda
90980599	Kalungu, Uganda
90980614	Kamuli, Uganda
90980631	Kapchorwa, Uganda
90980577	Kasese, Uganda
90980539	Katakwi, Uganda
90980596	Katerere, Uganda
90980522	Kiboga, Uganda
90980609	Kibuku, Uganda
90980629	Kiryandongo, Uganda
90980570	Koboko, Uganda
90980607	Kole, Uganda
90980560	Kotido, Uganda
90980540	Kumi, Uganda
90980585	Kyegegwa, Uganda
90980590	Lamwo, Uganda
90980600	Luuka, Uganda
90980621	Lwengo, Uganda
90980628	Masindi, Uganda
90980541	Mbale, Uganda
90980534	Nakaseke, Uganda
90980604	Napak, Uganda
90980591	Nwoya, Uganda
90980624	Otuke, Uganda
90980572	Oyam, Uganda
90980582	Rukungiri, Uganda
90980612	Serere, Uganda
90980630	Sironko, Uganda
90980543	Soroti, Uganda
90980529	Ssembabule, Uganda
90980565	Yumbe, Uganda
90980608	Zombo, Uganda   geohash with no admin 2: s8qj2n
*/

Select L.LocationName as AdminName, G.*
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin2LocationId = L.LocationId
Where G.Admin2LocationId = 91571735
--1214 rows, all Uganda

Select *
From Universe..GeoHash6LocationsLookup
Where Admin2LocationId = 91571735
and Admin2LocationId is Null

Select LocationId, LocationName
From Universe..Locations
Where LocationTypeId = 65
and CHARINDEX(' Uganda',LocationName)>0
and CHARINDEX(' Uganda(',LocationName)=0
and LocationId not in
	(Select Admin1LocationId
	From Universe..GeoHash6LocationsLookup
	Where Admin0LocationId = -45301
	)
--result:


	
	
Select *
From Universe..BaseAdminFeatures
Where Admin1LocationId in
	(Select LocationId
	From Universe..Locations
	Where LocationTypeId = 65
	and CHARINDEX(' Uganda',LocationName)>0
	and CHARINDEX(' Uganda(',LocationName)=0
	)
--result: 

Select *
From Universe..BaseAdminFeatures
Where Admin0LocationId = -45301
and BaseAdminLocationId not in
	(Select LocationId
	From Universe..Locations
	Where LocationTypeId = 68
	and CHARINDEX(' Uganda',LocationName)>0
	and CHARINDEX(' Uganda (',LocationName)=0
	)
--result: there are no other base admins for Ethiopia that are not the new locations