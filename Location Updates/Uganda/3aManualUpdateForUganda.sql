

	Declare @CountryLocationId int = -45301
	Declare @RegionLocationId int = -4842
	Declare @ContinentLocationId int = -46118
	Declare @CountryName varchar(250) = 'Uganda'
	Declare @LocationTypeId smallint = 68
	
	--Print 'Deleting base features for ' + @CountryName

	--DELETE      
	--FROM [Universe].[dbo].[BaseAdminFeatures]
	--Where Admin0LocationId = @CountryLocationId	

	--Print 'Base features deleted'

	--If processing Admin 2
	if @LocationTypeId = 68
	BEGIN

		Print 'Processing Admin 2 locations'

		--Only do this if it is admin level 2
		--We need to get the Admin1LocationIds above our newly added Admin2Locations so we need to do some spatial processing using point and polygon analysis
		Declare @Relationship2To1 Table (Admin2LocationId int, Admin1LocationId int)

		--Insert admin 2 and 1 IDs where Admin2 Centroid is within the Admin 1 polygon
		Insert into @Relationship2To1
		(Admin2LocationId,Admin1LocationId)
		Select NewAdmin2.LocationId as Admin2LocationId, Admin1.LocationId as Admin1LocationId
		From  
			(
			Select LocationId, CentroidFeature
			From Universe.dbo.Locations
			Where LocationTypeId = 68 --admin2
			and Charindex(', Uganda',LocationName,0) > 0				
			and Charindex(', Uganda (',LocationName,0) = 0
			and HashKey like ('Uganda-%')
			) NewAdmin2,
			(  
			Select LocationId, LocationFeature
			From Universe.dbo.Locations
			Where LocationTypeId = 65 --admin1
			and Charindex(', Uganda',LocationName,0) > 0				
			and Charindex(', Uganda (',LocationName,0) = 0
			and Charindex(', Uganda(',LocationName,0) = 0
			) Admin1
		Where NewAdmin2.CentroidFeature.STWithin(Admin1.LocationFeature) = 1 

		Print 'Point locations added to relationship table'
		
			
		--Insert admin2 and 1 IDs where Admin 2 polygon intersects the Admin 1 polygon and Admin2 ID is not already in the table
		Insert into @Relationship2To1
		(Admin2LocationId,Admin1LocationId)
		Select NewAdmin2.LocationId as Admin2LocationId, Admin1.LocationId as Admin1LocationId
		From  
			(
			Select LocationId, LocationFeature
			From Universe.dbo.Locations
			Where LocationTypeId = 68 --admin2
			and Charindex(', Uganda',LocationName,0) > 0				
			and Charindex(', Uganda (',LocationName,0) = 0
			and HashKey like ('Uganda-%') 
			) NewAdmin2,
			(  
			Select LocationId, LocationFeature, LocationName 
			From Universe.dbo.Locations
			Where LocationTypeId = 65 --admin1
			and Charindex(', Uganda',LocationName,0) > 0				
			and Charindex(', Uganda (',LocationName,0) = 0
			and Charindex(', Uganda(',LocationName,0) = 0
			) Admin1
		Where NewAdmin2.LocationFeature.STIntersects(Admin1.LocationFeature) = 1
		and NewAdmin2.LocationId not in (Select Admin2LocationId from @Relationship2To1)

		Print 'Polygon locations added to relationship table'
		Print 'Inserting Admin 2 IDs into base features'
		
		--Insert Admin 2 IDs from above table and Locations table into the BaseAdminFeatures table as the base admin level
		INSERT INTO [Universe].[dbo].[BaseAdminFeatures]
				   ([BaseAdminLocationId]
				   ,[BaseFeature]
				   ,[BaseAdminLevelId]
				   ,[Admin0LocationId]
				   ,[Admin1LocationId]
				   ,[Admin2LocationId]
				   ,[ContinentLocationId]
				   ,[WorldRegionLocationId])
		SELECT L.[LocationId]
			  ,L.[LocationFeature]
			  ,2 as BaseAdminLevelId
			  ,-45301 as Admin0LocationId
			  ,R.Admin1LocationId
			  ,R.Admin2LocationId
			  ,-46118 as [ContinentLocationId]
			  ,-4842 as [WorldRegionLocationId]
		  FROM [Universe].[dbo].[Locations] L, 
		  @Relationship2To1 R
		Where LocationTypeId = 68 --Admin2
		and Left(Hashkey, LEN('Uganda-')) = 'Uganda-' 																				
		and R.Admin2LocationId = L.LocationId

	END
		
	--If processing Admin 1	
	--If @LocationTypeId = 65
	--BEGIN

	--	Print 'Processing Admin 1 locations'

	--	INSERT INTO [Universe].[dbo].[BaseAdminFeatures]
	--			   ([BaseAdminLocationId]
	--			   ,[BaseFeature]
	--			   ,[BaseAdminLevelId]
	--			   ,[Admin0LocationId]
	--			   ,[Admin1LocationId]
	--			   ,[ContinentLocationId]
	--			   ,[WorldRegionLocationId])
	--	Select 
	--		LocationId as [BaseAdminLocationId]
	--		,LocationFeature as [BaseFeature]
	--		,1 as BaseAdminLevelId
	--		,@CountryLocationId as Admin0LocationId
	--		,LocationId
	--		,@ContinentLocationId as [ContinentLocationId]
	--		,@RegionLocationId as [WorldRegionLocationId]				
	--	From Universe.dbo.Locations
	--	Where LocationTypeId = 65 --admin1
	--	and Charindex(', '+@CountryName+'',LocationName,0) > 0				
	--	and Charindex(', '+@CountryName+ ' (',LocationName,0) = 0					   
	--END