/*** Universe ***/
--delete, yes
Select *
From Universe..Locations
Where LocationTypeId = 68
and CHARINDEX(', Uganda',LocationName)>0
order by LocationName

--delete current location without suffix
Delete From Universe.dbo.Locations Where LocationTypeId = 68 and Charindex(', Uganda',LocationName,0) > 0 and Charindex(', Uganda (',LocationName,0) = 0

--make 2014 the 'current' location without suffix
Update Universe.dbo.Locations Set LocationName = PresentationLocationName Where LocationTypeId = 68and Charindex(', Uganda (2014)',LocationName,0) > 0

/****UserData_StarCluster_Data **/
--delete, yes; update, no; need to change 2014 to current
Select *
From UserData_StarCluster_Data..Locations
Where LocationTypeId = 68
and CHARINDEX(', Uganda',LocationName)>0
order by LocationName

--change 2014 to current location without suffix
Update UserData_StarCluster_Data.dbo.LocationsSet LocationName = PresentationLocationNameWhere LocationTypeId = 68and Charindex(', Uganda (2014)',LocationName,0) > 0
--keeping 2013 and locations with no suffix to be the ones updated with a 2014 suffix

/*** WorldBank_StarCluster_Data ***/
--delete, yes; need to delete 2014 and rename 2013
Select *
From WorldBank_StarCluster_Data..Locations
Where LocationTypeId = 68
and CHARINDEX(', Uganda',LocationName)>0
order by LocationName

Delete From WorldBank_StarCluster_Data.dbo.LocationRelationshipsWhere LocationId in(Select LocationId From WorldBank_StarCluster_Data.dbo.Locations LWhere LocationTypeId = 68 and Charindex(', Uganda',L.LocationName,0) > 0 and Charindex(', Uganda (',L.LocationName,0) = 0)or RelatedLocationId in(Select LocationId From WorldBank_StarCluster_Data.dbo.Locations LWhere LocationTypeId = 68 and Charindex(', Uganda',L.LocationName,0) > 0 and Charindex(', Uganda (',L.LocationName,0) = 0)

Delete From WorldBank_StarCluster_Data.dbo.StarData_SQLVariantWhere LocationId in(Select LocationId From WorldBank_StarCluster_Data.dbo.Locations LWhere LocationTypeId = 68 and Charindex(', Uganda',L.LocationName,0) > 0 and Charindex(', Uganda (',L.LocationName,0) = 0)

Delete From WorldBank_StarCluster_Data.dbo.StarData_ExtendedStringWhere LocationId in(Select LocationId From WorldBank_StarCluster_Data.dbo.Locations LWhere LocationTypeId = 68 and Charindex(', Uganda',L.LocationName,0) > 0 and Charindex(', Uganda (',L.LocationName,0) = 0)

Delete From WorldBank_StarCluster_Data.dbo.StarData_FloatWhere LocationId in(Select LocationId From WorldBank_StarCluster_Data.dbo.Locations LWhere LocationTypeId = 68 and Charindex(', Uganda',L.LocationName,0) > 0 and Charindex(', Uganda (',L.LocationName,0) = 0)

Delete From WorldBank_StarCluster_Data.dbo.LocationsWhere LocationTypeId = 68 and Charindex(', Uganda',LocationName,0) > 0 and Charindex(', Uganda (',LocationName,0) = 0

--remove 2014 suffix
Update WorldBank_StarCluster_Data.dbo.LocationsSet LocationName = PresentationLocationNameWhere LocationTypeId = 68and Charindex(', Uganda (2014)',LocationName,0) > 0
--keeping 2013 and locations with no suffix to be the ones updated with a 2014 suffix

/*** ICRISAT ***/
--delete, yes; update, no; no need to update as there are no 2014 locations
Select *
From ICRISAT_StarCluster_Data..Locations
Where LocationTypeId = 68
and CHARINDEX(', Uganda',LocationName)>0
order by LocationName

