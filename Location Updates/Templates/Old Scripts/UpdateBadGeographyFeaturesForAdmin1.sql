Update [Universe].dbo.Locations
SET LocationFeature = Geometry::STGeomFromText(awmDatabase.dbo.MakeValidGeographyFromGeometry(LocationFeature).ToString(),4326)
Where LocationTypeId = 65
and charindex('Tanzania',Locationname,0)>0
and charindex('Tanzania (',Locationname,0)=0
and awmDatabase.dbo.IsValidGeographyFromGeometry(LocationFeature) = 0


Update [WorldBank_StarCluster_Data].dbo.Locations
SET LocationFeature = Geometry::STGeomFromText(awmDatabase.dbo.MakeValidGeographyFromGeometry(LocationFeature).ToString(),4326)
Where LocationTypeId = 65
and charindex('Tanzania',Locationname,0)>0
and charindex('Tanzania (',Locationname,0)=0
and awmDatabase.dbo.IsValidGeographyFromGeometry(LocationFeature) = 0

Update [UserData_StarCluster_Data].dbo.Locations
SET LocationFeature = Geometry::STGeomFromText(awmDatabase.dbo.MakeValidGeographyFromGeometry(LocationFeature).ToString(),4326)
Where LocationTypeId = 65
and charindex('Tanzania',Locationname,0)>0
and charindex('Tanzania (',Locationname,0)=0
and awmDatabase.dbo.IsValidGeographyFromGeometry(LocationFeature) = 0

Update [Icrisat_StarCluster_Data].dbo.Locations
SET LocationFeature = Geometry::STGeomFromText(awmDatabase.dbo.MakeValidGeographyFromGeometry(LocationFeature).ToString(),4326)
Where LocationTypeId = 65
and charindex('Tanzania',Locationname,0)>0
and charindex('Tanzania (',Locationname,0)=0
and awmDatabase.dbo.IsValidGeographyFromGeometry(LocationFeature) = 0



