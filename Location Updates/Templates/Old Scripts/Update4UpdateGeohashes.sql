USE [Universe]
GO
/****** Object:  StoredProcedure [dbo].[UpdateGeohashes]    Script Date: 12/23/2014 11:44:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Molly O'Dwyer/Dan Allen
-- Create date: 12/23/2014
-- Description: 

/*


Exec UpdateGeohashes -45303, 68, 'Ethiopia'
*/
-- =============================================
Create PROCEDURE [dbo].[UpdateGeohashes_Old]
	-- Add the parameters for the stored procedure here
	@CountryLocationId int,
	@LocationTypeId smallint,
	@CountryName varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRANSACTION;
	
	Create Table #Features (RunId int identity(1,1), AdminLocationId int, Shape geometry, primary key (RunId), unique (AdminLocationId));
	Create Table #Geohashes (Geohash6 char(6), Shape geometry, primary key (Geohash6));
	

	Declare @strSQL varchar(2000)
	Declare @Env Geometry
	--This is to get the bounding box to put into the spatial indexes below
	Declare @MinLong as int
	Declare @MaxLong as int
	Declare @MinLat as int
	Declare @MaxLat as int
	
	Declare @Geohash6 char(6), @Shape geometry, @ActiveLocationId int 
	Declare @ActiveRunId int, @MaxRunId int	

	Declare @RC int
	Declare @PrintStr varchar (250)
		
	--Get the bounding box around the country LocationId that was provided as parameter
	Select @Env = LocationFeature.STEnvelope()
	From Universe.dbo.Locations
	Where LocationId = @CountryLocationId

	Select @MinLong = floor(Min(Longitude)), @MaxLong = ceiling(Max(longitude)), 
	@MinLat = floor(Min(Latitude)), @MaxLat = ceiling(Max(Latitude))
	From 
		(
		Select @Env.STPointN(1).STX as Longitude, @Env.STPointN(1).STY as Latitude
		UNION ALL
		Select @Env.STPointN(2).STX as Longitude, @Env.STPointN(2).STY as Latitude
		UNION ALL
		Select @Env.STPointN(3).STX as Longitude, @Env.STPointN(3).STY as Latitude
		UNION ALL
		Select @Env.STPointN(4).STX as Longitude, @Env.STPointN(4).STY as Latitude
		) i
	--record the results in the bounding boxes below

	--Build Spatial indexes to help speed up the updates on Geohash lookup table

	--If error creating spatial index, simply drop them and redo:
	--drop index #SPX_Geohashes on #Geohashes
	--drop index #SPX_Features on #Features
	
	set @strSQL = 
	'CREATE SPATIAL INDEX #SPX_Geohashes ON #Geohashes
	([shape])
	USING  GEOMETRY_GRID 
	WITH (
	bounding_box = ('+ cast(@MinLong as varchar(4)) + ', ' +  cast(@MinLat as varchar(4)) + ', ' +  cast(@MaxLong as varchar(4)) + ', ' + cast(@MaxLat as varchar(4)) + '), '
	+ ' GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
	CELLS_PER_OBJECT = 16);'
	
	
	set @strSQL = 
	'CREATE SPATIAL INDEX #SPX_Features ON #Features
	([shape])
	USING  GEOMETRY_GRID 
	WITH (
	bounding_box = ('+ cast(@MinLong as varchar(4)) + ', ' +  cast(@MinLat as varchar(4)) + ', ' +  cast(@MaxLong as varchar(4)) + ', ' + cast(@MaxLat as varchar(4)) + '), '
	+ ' GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
	CELLS_PER_OBJECT = 16);'
	
		
	--Convert the wkt from the geohash lookup table into a geometry data type in a temp table
	--this may take a minute or two
	INSERT INTO #Geohashes
	(Geohash6, Shape)
	Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326)
	From [Universe].[dbo].[GeoHash6LocationsLookup]
	Where Admin0LocationId = @CountryLocationId
	Order by Geohash6

		
	--Insert all the features that you added so that you can loop through them and grab the geohash coordinates that are within each feature

	Truncate Table #Features
	Insert into #Features 
	(AdminLocationId, Shape)
	Select LocationId, LocationFeature 
	FROM [Universe].[dbo].[Locations] L
	Where L.LocationTypeId = @LocationTypeId
	and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-'  
	
	Select @RC = Count(*)
      From #Features
     
      if @RC = 0
      BEGIN
     
            Insert into #Features
            (AdminLocationId, Shape)
            Select LocationId, LocationFeature
            FROM [Universe].[dbo].[Locations] L
            Where L.LocationTypeId = @LocationTypeId
            and charindex(@CountryName, LocationName) > 0
            and charindex(@CountryName + ' (', LocationName) = 0
     
      END
	
	
	--This will loop through and update all the geohash lookups to have the new admin locationId
	--This took about 3 minutes for admin2 for Kenya
	Select @ActiveRunId = Min(RunId), @MaxRunId = Max(RunId)
	From #Features
	
	print ('Max Run Id: ' + cast(@MaxRunId as varchar(1000)))	
		
	While @ActiveRunId <= @MaxRunId
	BEGIN
		Select @ActiveLocationId = AdminLocationId , @Shape = Shape 
		From #Features 
		Where RunId = @ActiveRunId  
		
		if @LocationTypeId = 65
		BEGIN

			Update [Universe].[dbo].[GeoHash6LocationsLookup]
			Set 
				Admin1LocationId = @ActiveLocationId
			Where Geohash6 in ( 
				Select Geohash6
				From #Geohashes
				Where Shape.STWithin(@Shape) = 1
				)
				and Admin0LocationId = @CountryLocationId  		
		end
		
		if @LocationTypeId = 68
		BEGIN

			Update [Universe].[dbo].[GeoHash6LocationsLookup]
			Set 
				Admin2LocationId = @ActiveLocationId
			Where Geohash6 in ( 
				Select Geohash6
				From #Geohashes
				Where Shape.STWithin(@Shape) = 1
				)
				and Admin0LocationId = @CountryLocationId  
		
		END
		 

			
		print ('Current Active Run ID: ' + cast(@ActiveRunId as varchar(1000)))
		
		set @ActiveRunId = @ActiveRunId + 1
	END

	--Get the count of all the geohashes that are still pointing to old admin locations in country
	Select @RC = count(*)
	From [Universe].[dbo].[GeoHash6LocationsLookup]
	Where Admin0LocationId = @CountryLocationId 
	and (case when @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in 		
		(Select LocationId 
		from Universe.dbo.Locations 
		Where LocationTypeId = @LocationTypeId 
		and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-') 			
	
	print ('Count of geohashes pointing to old locations: ' + cast(@RC as varchar(100)))
	
	/* This is a manual check of geohashes after things have run
	
	Select LocationName, LocationFeature
	From Universe.dbo.Locations
	Where LocationTypeId = 65 and Left(Hashkey, LEN('Ethiopia-')) = 'Ethiopia-'
	--or CHARINDEX(', Eritrea',LocationName)>0
	UNION ALL
	Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326)
	From [Universe].[dbo].[GeoHash6LocationsLookup]
	Where Admin0LocationId = -45303
	and Admin2LocationId not in 
		(Select LocationId 
		from Universe.dbo.Locations 
		Where LocationTypeId = 65 and Left(Hashkey, LEN('Ethiopia-')) = 'Ethiopia-')
		 
	Select count(*)
	From Universe.dbo.GeoHash6LocationsLookup
	Where Admin0LocationId = -45303
	and Admin2LocationId not in 
		(Select LocationId 
		from Universe.dbo.Locations 
		Where LocationTypeId = 65 and Left(Hashkey, LEN('Ethiopia-')) = 'Ethiopia-') 
	*/
	-------****if no extra geohashes, do not run any script below this line. You are done and can now test! **** -----------	

	
		Create Table #NeedToBufferPoints (RunId int identity(1,1), Geohash6 char(6), Shape Geometry, primary key (RunId));
		Create Table #AreaThatIntersectsBuffer (Geohash6 char(6), AdminLocationId int, Area float);
		Create Table #UpdateForGeohashAdmin (Geohash6 char(6), AdminLocationId int);
		
		--Buffer the geohash point so that we can intersect it with features (since
		--the point was not within a feature
		Insert into #NeedToBufferPoints
		(Geohash6, Shape)
		Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326).STBuffer(0.5)
		From [Universe].[dbo].[GeoHash6LocationsLookup]
		Where Admin0LocationId = @CountryLocationId 
		and (case when @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in 
			(Select LocationId 
			from Universe.dbo.Locations 
			Where LocationTypeId = @LocationTypeId
			and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-') 
		
		Select @PrintStr = (Select count(*) from #NeedToBufferPoints)
		print ('Count of geohashes not in a feature that will be buffered: ' + @PrintStr)
		
		set @strSQL = 
		'CREATE SPATIAL INDEX #SPX_Buffer ON #NeedToBufferPoints
		([Shape])
		USING GEOMETRY_GRID 
		WITH (
		bounding_box = ('+ cast(@MinLong as varchar(4)) + ', ' +  cast(@MinLat as varchar(4)) + ', ' +  cast(@MaxLong as varchar(4)) + ', ' + cast(@MaxLat as varchar(4)) + '), '
		+ ' GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
		CELLS_PER_OBJECT = 16);'
		

		--Get the intersection of the buffered geohash point with the admin2 polygon feature
		Truncate Table 	#AreaThatIntersectsBuffer
		INSERT INTO #AreaThatIntersectsBuffer
		(Geohash6, AdminLocationId, Area)
		Select Geohash6, F.AdminLocationId, F.Shape.STIntersection(B.Shape).STArea()
		From #Features F, #NeedToBufferPoints B
		Where F.Shape.STIntersects(B.Shape) = 1

		--Select @PrintStr = (Select * from #AreaThatIntersectsBuffer)
		--print ('Intersection of the buffered geohash point with polygon feature: ' + @PrintStr)
		
		 --Extend it one more time to see if we can capture them
        Declare @CountOfRemaining int = 1
        Declare @ActiveIteration int = 1, @MaxIterations int = 5
        Declare @BufferSize float = 0.1
       
        Select @CountOfRemaining = count(*)
        From [Universe].[dbo].[GeoHash6LocationsLookup]
        Where Admin0LocationId = @CountryLocationId
        and (case when @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in        
              (Select LocationId
              from Universe.dbo.Locations
              Where LocationTypeId = @LocationTypeId
              and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-')                      
       
       print ('Count of Remaining geohashes: ' + cast(@CountOfRemaining as varchar(100)))
       
        While @CountOfRemaining > 0 AND @ActiveIteration < @MaxIterations
        BEGIN
			
              Select @CountOfRemaining = count(*)
              From [Universe].[dbo].[GeoHash6LocationsLookup]
              Where Admin0LocationId = @CountryLocationId
              and (case when @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in         
                    (Select LocationId
                    from Universe.dbo.Locations
                    Where LocationTypeId = @LocationTypeId
                    and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-')          
               
               print ('Geohashes now remaining: ' + cast(@CountOfRemaining as varchar(100)))
                   
              INSERT INTO #AreaThatIntersectsBuffer
              Select Geohash6, F.AdminLocationId, F.Shape.STIntersection(B.Shape.STBuffer(@BufferSize)).STArea()
              From #Features F, #NeedToBufferPoints B
              Where F.Shape.STIntersects(B.Shape) = 0
              and F.Shape.STIntersects(B.Shape.STBuffer(@BufferSize)) = 1
              and Geohash6 not in (Select Distinct Geohash6 From #AreaThatIntersectsBuffer Where Area > 0)
             
              print ('Current Buffer size: ' + cast(@BufferSize as varchar(100)))
              print ('Active Iteration: ' + cast(@ActiveIteration as varchar(100)))
             
        --Get the max area for each of the intersections and then associate
		--the geohash with the locationId that has that max
		INSERT INTO #UpdateForGeohashAdmin
		(Geohash6, AdminLocationId)
		Select B.Geohash6, B.AdminLocationId
		From #AreaThatIntersectsBuffer B,	
			(
			Select Geohash6, Max(Area) as MaxArea
			From #AreaThatIntersectsBuffer
			Group by Geohash6
			) I
		Where B.Geohash6 = I.Geohash6
		and B.Area = I.MaxArea
		order by B.Geohash6, B.area desc

		--Select @PrintStr = (Select * from #UpdateForGeohashAdmin)
		--print ('Geohashes in the intersection: ' + @PrintStr)

		--Update those pesky geohashs that were not contained by the new features
		if @LocationTypeId = 65
		BEGIN
			Update [Universe].[dbo].[GeoHash6LocationsLookup]
			Set Admin1LocationId = U.AdminLocationId
			From 
				[Universe].[dbo].[GeoHash6LocationsLookup] G,
				#UpdateForGeohashAdmin U
			Where G.Geohash6 = U.Geohash6
				and G.Admin0LocationId = @CountryLocationId
		END	
			
		if @LocationTypeId = 68
		BEGIN
			Update [Universe].[dbo].[GeoHash6LocationsLookup]
			Set Admin2LocationId = U.AdminLocationId
			From 
				[Universe].[dbo].[GeoHash6LocationsLookup] G,
				#UpdateForGeohashAdmin U
			Where G.Geohash6 = U.Geohash6
				and G.Admin0LocationId = @CountryLocationId
		END
		
		Select @PrintStr = (Select count(*)
				From Universe.dbo.GeoHash6LocationsLookup
				Where Admin0LocationId = @CountryLocationId 
				and (case when + @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in 
					(Select LocationId 
					from Universe.dbo.Locations 
					Where LocationTypeId = 68 and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-')) 
		print ('Remaining geohashes: ' + @PrintStr)
             
              set @BufferSize = @BufferSize + .5
              set @ActiveIteration = @ActiveIteration + 1
        END  


/*Update Centroids for new boundaries*/
		Update [Universe].[dbo].[Locations]  
		Set CentroidFeature = [LocationFeature].STCentroid()
		Where LocationTypeId = @LocationTypeId
		and charindex(@CountryName, LocationName) > 0
        and charindex(@CountryName + ' (', LocationName) = 0 
        
 /* Update null centroids */
        Update [Universe].[dbo].[Locations]    
		set [CentroidFeature] = LocationFeature.STEnvelope().STCentroid()
		WHERE [LocationFeature] is not null
		and [CentroidFeature] is null  
		and LocationTypeId = @LocationTypeId
		and charindex(@CountryName, LocationName) > 0
        and charindex(@CountryName + ' (', LocationName) = 0





	End	
	COMMIT TRANSACTION;

