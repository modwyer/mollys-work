USE [TestDatabase]
GO
/****** Object:  StoredProcedure [dbo].[UpdateGeohashes_New]    Script Date: 2/4/2015  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Molly O'Dwyer/Dan Allen
-- Create date: 2/4/2015
-- Description: 

/*


Exec UpdateGeohashes_New -45303, 68
*/
-- =============================================
alter PROCEDURE [dbo].[UpdateGeohashes_New]
	-- Add the parameters for the stored procedure here
	
	@CountryLocationId int,
	@LocationTypeId smallint
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRANSACTION;

Declare @Env Geometry

--This is to get the bounding box to put into the spatial indexes below
Declare @MinLong as int
Declare @MaxLong as int
Declare @MinLat as int
Declare @MaxLat as int

Declare @Geohash6 char(6), @Shape geometry, @ActiveGeohash char(6) 
Declare @ActiveRunId int, @MaxRunId int	

Declare @Admin0 int
Declare @Admin1 int
Declare @Admin2 int

Declare @StartTime as datetime2(7)
Declare @EndTime as datetime2(7)


Set @StartTime = SYSDATETIME()
print ('Start time is: ' + cast(@StartTime as varchar(19)))
--create table to hold geohashes within the bounding box
print 'Creating table #CountrySpecificGeohashes'

Create Table #CountrySpecificGeohashes 
	(RunId int identity(1,1), Geohash6 char(6), GeoShape geometry, Location0 int, Location1 int, Location2 int, primary key (Geohash6));

--Get country parameter
--Get country shape
--Get country envelope
Select @Env = LocationFeature.STEnvelope()
From Universe.dbo.Locations
Where LocationId = @CountryLocationId

print 'Getting bounding box for country'

--Get the bounding box around the country LocationId that was provided as parameter
Select @MinLong = floor(Min(Longitude)), @MaxLong = ceiling(Max(longitude)), 
	@MinLat = floor(Min(Latitude)), @MaxLat = ceiling(Max(Latitude))
	From 
		(
		Select @Env.STPointN(1).STX as Longitude, @Env.STPointN(1).STY as Latitude
		UNION ALL
		Select @Env.STPointN(2).STX as Longitude, @Env.STPointN(2).STY as Latitude
		UNION ALL
		Select @Env.STPointN(3).STX as Longitude, @Env.STPointN(3).STY as Latitude
		UNION ALL
		Select @Env.STPointN(4).STX as Longitude, @Env.STPointN(4).STY as Latitude
		) i

print ('Max Lat: ' + cast(@MaxLat as varchar(50)))
print ('Min Lat: ' + cast(@MinLat as varchar(50)))
print ('Max Long: ' + cast(@MaxLong as varchar(50)))
print ('Min Long: ' + cast(@MinLong as varchar(50)))
print 'Inserting geohashes within the bounds into #CountrySpecificGeohashes'
--Get all geohashes within that envelope (or intersect)
--insert those selected geohashes into the temp table	
Insert into #CountrySpecificGeohashes
	(Geohash6, GeoShape)
	Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326)
	From [Universe].[dbo].[GeoHash6LocationsLookup]
	Where Latitude1km between @MinLat and @MaxLat
	and Longitude1km between @MinLong and @MaxLong

--Loop through all geohashes
--loop through the geohashes in the table and set Admin0 according to intersection with baseadmin table	
Select @ActiveRunId = Min(RunId), @MaxRunId = Max(RunId)
From #CountrySpecificGeohashes G

print ('Max Run Id: ' + cast(@MaxRunId as varchar(1000)))

Set @StartTime = SYSDATETIME()
print ('Start time for the loop through is: ' + cast(@StartTime as varchar(19)))

While @ActiveRunId <= @MaxRunId
		
	BEGIN
	
		Select @Shape = G.GeoShape, @ActiveGeohash = G.Geohash6
		From #CountrySpecificGeohashes G
		Where G.RunId = @ActiveRunId
		
		--Get the admin0 that intersects
		--Get the admin1 that intersects
		--get the admin2 that intersects
		Select @Admin0 = Admin0LocationId, @Admin1 = Admin1LocationId, @Admin2 = Admin2LocationId
		From Universe.dbo.BaseAdminFeatures B
		Where BaseFeature.STIntersects(@Shape)=1
		
		--Then update the geohashlookup for that active geohash6

		Update Universe.dbo.GeoHash6LocationsLookup
		Set Admin0LocationId = @Admin0, Admin1LocationId = @Admin1, Admin2LocationId = @Admin2
		Where Geohash6 = @ActiveGeohash
	
		print ('Current Active Run ID: ' + cast(@ActiveRunId as varchar(20)))
			
		set @ActiveRunId = @ActiveRunId + 1	
	End
	
	Set @EndTime = SYSDATETIME()
	print ('End time is: ' + cast(@EndTime as varchar(19)))
End