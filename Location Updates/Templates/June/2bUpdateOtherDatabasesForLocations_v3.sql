USE [TestDatabase]
GO

/****** Object:  StoredProcedure [dbo].[2bUpdateOtherDatabasesForLocations_v3]    Script Date: 6/15/2015 1:32:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Molly O'Dwyer
-- Create date: 2/6/2015
-- Update date: 6/10/15 - MLO - Adding transactions,try/catch blocks
-- Description:	Update the other databases with new location information from Universe
-- =============================================

/*
EXEC [2bUpdateOtherDatabasesForLocations_v3] 'Ethiopia', 68  

--Double check update was successful
Select LocationName
From ICRISAT_StarCluster_Data.dbo.Locations
Where LocationTypeId = 68
and CHARINDEX(', Ethiopia', LocationName)>0

*/

alter PROCEDURE [dbo].[2bUpdateOtherDatabasesForLocations_v3] 
	-- Add the parameters for the stored procedure here
	@CountryName varchar(250),
	@LocationTypeId smallint
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   /* Update all other Clusters from Universe */
	--take results of the following select and make that the table to look through
	Declare @tableName varchar(100)
	Declare @U varchar(2000)
	Declare @strDatabase varchar(100)
	
	Declare @DatabaseNames Table (RunId int identity(1,1), DatabaseName varchar(250))
	
	Print 'Updating locations in other databases'
	
	Print 'Getting list of databases'
	
	--Get list of databases
	Begin Transaction
		Begin Try
			INSERT INTO @DatabaseNames (DatabaseName)
			Select name 
			from sys.databases 
			where charindex('StarCluster',name)>0
			and charindex('Base',name) = 0;
		End Try
		Begin Catch
			Print 'Unable to insert into list of databases. Exiting update.'
			Rollback
			Return 1;
		End Catch

		Declare @ActiveRunId int, @MaxRunId int
	
		Select @ActiveRunId = MIN(RunId), @MaxRunId = MAX(RunId)
		From @DatabaseNames 
	
		Print 'Updating databases'
	
		Begin Try
			--Loop through database list and update from Universe
			While @ActiveRunId <= @MaxRunId 
				BEGIN
					Select @strDatabase = [DatabaseName]
					From @DatabaseNames 
					Where RunId = @ActiveRunId 
			
					Set @tableName = @strDatabase + '.dbo.Locations'
			
					--Update locations
					Set @U = 'Update ' + @tableName + CHAR(13)
						+ 'Set LocationName = U.LocationName' + CHAR(13)
						+ 'From ' + @tableName + ' L,' + CHAR(13)
						+ 'Universe.dbo.Locations U' + CHAR(13)
						+ 'Where U.LocationTypeId = ' + cast(@LocationTypeId as varchar(10)) + CHAR(13)
						+ 'and U.LocationId = L.LocationId' + CHAR(13)
						+ ' and Charindex(' + char(39) + ', '+@CountryName + char(39) + ',L.LocationName,0) > 0'  + char(13) 
						+ ' and Charindex(' + char(39) + ', '+@CountryName + ' (' + char(39) + ',L.LocationName,0) = 0'  + char(13) 
				
						print (@U)
				
					Exec (@U)
		
					Print 'Updated locations for ' + @tableName
		
					set @ActiveRunId= @ActiveRunId + 1
				END
		End Try
		Begin Catch
			Print ('Unable to update locations in ' + @tableName + '. Exiting update.')
			Rollback
			Return 1;
		End Catch
	Commit;
END


GO


