USE [TestDatabase]
GO

/****** Object:  StoredProcedure [dbo].[UpdateDatabaseWithNewBoundaries_v3]    Script Date: 6/19/2015 5:43:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Molly O'Dwyer
-- Create date: 12/23/2014
-- Update date: 6/8/2015 
-- Description:	Wrapper Procedure around the procedures that update boundaries with new features
-- Update notes: added a check that the suffix doesn't already exist. If it does, the update ends
--			6/8/15 - MLO - Added transactions and rollbacks. Geohash proc will run independent of the other procs and the other procs will
--							rollback all work done if an error is encountered.
--			6/10/15-mlo- Moved 2b from section 2a to this proc for better control of flow. Move 3b to this proc and renamed. 3A became
--						3B and all but the make valid geometry code was removed. Original 3B became 3A, delete and insert from base admin table
--						Removed explicit transactions.
/*
SET NOCOUNT ON;

--Get list of regions from a list. Can view the Spatial results if needed to help determine region of desired country.
Select LocationId, LocationName, LocationFeature
	From Universe.dbo.Locations
	Where LocationTypeId = 25

--Get list of countinents from a list. Can view the Spatial results if needed to help determine region of desired country.
Select LocationId, LocationName, LocationFeature
	From Universe.dbo.Locations
	Where LocationTypeId = 26	

--Get region country is in
SELECT DISTINCT lr.locationtypeid, lr.relatedlocationtypeid, lr.LocationId, l1.locationname, lr.RelatedLocationId, l2.locationname 
	FROM WorldBank_StarCluster_Data.dbo.LocationRelationships lr
		inner join WorldBank_StarCluster_Data..locations l1 ON l1.locationid=lr.locationid --regions
		inner join WorldBank_StarCluster_Data..locations l2 ON l2.locationid=lr.relatedlocationid --countries
	WHERE lr.relatedlocationtypeid=23 and lr.locationtypeid=25
		and CHARINDEX('Zambia',l2.LocationName)>0
		--or CHARINDEX('Southeastern Asia',l1.LocationName)>0
	ORDER BY l1.locationname, l2.locationname

Declare @LocationProcessData LocationProcessData

INSERT INTO @LocationProcessData
(LocationName, LocationTypeId, LocationFeature, CentroidFeature, PresentationLocationName, Hashkey)
SELECT
	LocationName as [LocationName]
	,LocationTypeId as [LocationTypeId]
	,LocationFeature as [LocationFeature]
	,Centroid as [Centroid]
	,PresentationLocationName as [PresentationLocationName]
	--,Hashkey as [HashKey]
	,'Zambia-' + cast(ID as varchar(10)) as [HashKey]
  FROM [TestDatabase].[dbo].[vAdmin2Zambia]

--Select * from @LocationProcessData 

EXEC UpdateDatabaseWithNewBoundaries_v3 68, 'Zambia', 'Eastern Africa', 'Africa', '2015', @LocationProcessData, 7

*/
-- =============================================
alter PROCEDURE [dbo].[UpdateDatabaseWithNewBoundaries_v3]

	@LocationTypeId smallint,
	@CountryName varchar(250),
	@RegionName varchar(250),
	@ContinentName varchar(250),
	@SuffixToAddToExistingFeatures varchar(20),
	@AdminBoundariesToProcess dbo.LocationProcessData READONLY,
	@HashkeyLength int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET XACT_ABORT,NOCOUNT ON;
	
	Declare @CountryLocationId int
	Declare @StartTime as datetime2(7)
	Declare @EndTime as datetime2(7)
	Declare @RegionLocationId int
	Declare @ContinentLocationId int
	
	Print 'Beginning Location Boundary Update'
	Set @StartTime = SYSDATETIME()
	Print ('Start time is: ' + cast(@StartTime as varchar(19)))
	
	--This gets the LocationId for the country name passed within the where clause
	Select @CountryLocationId = LocationId
	From Universe.dbo.Locations
	Where LocationName = @CountryName
	and LocationTypeId = 23
		
	--Get the LocationId for World Region that country falls within
	Select @RegionLocationId = LocationId
	From Universe.dbo.Locations
	Where LocationName = @RegionName --World Region
	and LocationTypeId = 25

	--Get the LocationId for Continent that country falls within
	Select @ContinentLocationId = LocationId
	From Universe.dbo.Locations
	Where LocationName = @ContinentName  --Continent
	and LocationTypeId = 26
	
	Select @CountryLocationid as Country, @RegionLocationid as Region, @ContinentLocationid as Continent

	--Check if suffix has already been added to the country before
	Declare @S varchar(2000)
		
	Print ('Checking if suffix already exists for ' + @CountryName)
	
	Set @S =
		'Select * ' + char(13)
		+ 'From Universe..Locations ' + char(13)
		+ 'Where LocationTypeId = ' + cast(@LocationTypeId as varchar(5)) + char(13)
		+ 'and (charindex('', ' + @CountryName + ' (' + @SuffixToAddToExistingFeatures + ')'',locationname)>0' + char(13)
		+ 'or charindex('', ' + @CountryName + '(' + @SuffixToAddToExistingFeatures + ')'',locationname)>0)'

		print @S
	Exec (@S)

	Print ('Rowcount: ' + cast(@@rowcount as varchar(10)))

	--see if the above select is 0 or greater than zero. This determines if the update continues
	If @@Rowcount = 0
		Begin
			print 'This is a new suffix! Continuing the update...'
		End
	Else
		Begin
			raiserror('This suffix already exists in the database for %s. Please check the suffix. Exiting update.',1,200, @CountryName)
			return;
		End

	Print 'Starting Section 2A: Update existing boundaries and insert new boundaries'
	
	Begin Transaction --added transaction for section 2a and 2b as this is more difficult to go back and run manually mlo 6/15/15
		--Update Universe Locations table with new boundaries
		Begin Try
			EXECUTE [2aUpdateExistingAndInsertNewAdminBoundaries_v3] 
				@LocationTypeId
				,@CountryName
				,@SuffixToAddToExistingFeatures
				,@AdminBoundariesToProcess 
		End Try
		Begin catch
			Print 'Error occurred in Section 2A. All changes have been rolled back. Update procedure has ended.'
			return 1
		End Catch

		Print 'Starting Section 2B: Update other databases with new locations'
		
		--Update other databases with new boundaries
		Begin Try
			Execute [2bUpdateOtherDatabasesForLocations_v3]
				@CountryName,
				@LocationTypeId
		End Try
		Begin catch
			Print 'Error occurred in Section 2B. All changes have been rolled back. Update procedure has ended.'
			return 1
		End Catch
		
		Print 'Section 2B complete'
	Commit;

	
	Print 'Starting Section 3A: Delete and insert locations to Base Admin'
		
	Begin Try
		EXECUTE [3aDeleteAndInsertToBaseAdmin_v3] 
				@LocationTypeId
				,@CountryName
				,@CountryLocationId
				,@RegionLocationId 
				,@ContinentLocationId
	End Try
	Begin catch
		Print 'Error occurred in Section 3A. All changes have been rolled back. Update procedure has ended.'
		return 1
	End Catch
		
	Print 'Section 3A complete.'

	Print 'Starting Section 3B: Make Geometries Valid'

	Begin Try
		EXECUTE [3bMakeGeometriesValid_v3] 
				@LocationTypeId
				,@CountryName
				,@CountryLocationId
				,@HashkeyLength
	End Try
	Begin catch
		Print 'Error occurred in Section 3B. All changes have been rolled back. Update procedure has ended.'
		return 1
	End Catch

	Print ' Section 3B complete'

	Print 'Starting Section 4: Update Geohashes'
	
	Begin Try
		EXECUTE [4UpdateGeohashes_v3] 
			@CountryLocationId,
			@LocationTypeId,
			@CountryName,
			@HashkeyLength
	 
		Print 'Section 4 complete'
	End Try
	Begin catch
		Print 'Error occurred in Section 4. All changes made during Section 4 have been rolled back. 
				Changes made in previous sections will be retained. This section has ended.'
		return 1
	End Catch

	Print ('Updated completed at: ' + cast(@EndTime as varchar(19)))

END




GO


