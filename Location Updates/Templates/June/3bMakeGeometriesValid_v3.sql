USE [TestDatabase]
GO

/****** Object:  StoredProcedure [dbo].[3bMakeGeometriesValid_v3]    Script Date: 6/19/2015 4:26:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Molly O'Dwyer
-- Create date: 12/18/2014
-- Update date: 6/10/15 - MLO - Added transactions and try/catch blocks, removed call to old 3B so only code left is make geometry valid 
--				6/19/15 - MLO - Added a transaction for checking and converting invalid geographies. Calls function created by Dan to convert geometry to geography.
-- Description:	Check the geometries of base admin features and location features are valid after a new boundary update and if not, make
--				them valid


/*
EXEC dbo.[3bMakeGeometriesValid_v3] 68, 'India', -45498, 6
*/
-- =============================================

alter PROCEDURE [dbo].[3bMakeGeometriesValid_v3]
	-- Add the parameters for the stored procedure here
	@LocationTypeId smallint,
	@CountryName varchar(250),
	@CountryLocationId int,
	@HashkeyLength int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET XACT_ABORT,NOCOUNT ON;
	
	BEGIN TRANSACTION
				  
		Print 'Checking for invalid geometries in BaseAdminFeatures table.'
		
		--Update any geometries that are not valid
		Begin Try
			Update [Universe].[dbo].[BaseAdminFeatures]
			set BaseFeature = BaseFeature.MakeValid()
			Where Admin0LocationId = @CountryLocationId  
			and [BaseFeature].STIsValid() = 0
		End try
		Begin Catch
			Print 'Unable to make base features geometry valid. Exiting update.'
			Rollback
			Return 1;
		End Catch
		Print 'Checking for invalid geometries in Locations table'

		Begin Try
			Update [Universe].[dbo].[Locations]
			set LocationFeature = LocationFeature.MakeValid()
			Where LocationTypeId = @LocationTypeId  
			and Left(Hashkey, @HashkeyLength) = @CountryName + '-'			
			and LocationFeature.STIsValid() = 0	
		End Try
		Begin Catch
			Print 'Unable to make location features geometry valid. Exiting update.'
			Rollback
			Return 1;
		End Catch

		Print 'Invalid geometry check complete.'
	
	COMMIT TRANSACTION;

	Begin Transaction --added 6/19/15, mlo, convert invalid geographies
		Print 'Checking for invalid geographies.'

		Declare @Geom Geometry
		Declare @Valid bit

		Select @Geom = LocationFeature 
			From Universe..Locations
			Where LocationTypeId = @LocationTypeId  
			and Left(Hashkey, @HashkeyLength) = @CountryName + '-'

		SELECT @Valid = [awmDatabase].[dbo].[IsValidGeographyFromGeometry] (@Geom)

		If @Valid = 0
			Begin
				Print 'Invalid geographies exist for this country. Beginning conversion.'

				Declare @ActiveRunId int = 1
				Declare @RunID int = 1, @MaxRunId int
				Declare @Shape geometry
				Declare @ID int

				Create table #BadGeography (RunId int identity(1,1), GeoShape geometry, LocationId int)

				Insert into #BadGeography (GeoShape,LocationId)
					Select LocationFeature, Locationid
					From Universe..Locations
					Where LocationTypeId = 68 --@LocationTypeId  
					and Left(Hashkey, @HashkeyLength) = 'Zambia-' --@CountryName + '-'
				
				Select * from #BadGeography

				Select @ActiveRunId = Min(RunId), @MaxRunId = Max(RunId)
				From #BadGeography

				Begin Try
					While @ActiveRunId <= @MaxRunId
						BEGIN
							Select @Shape = LocationFeature, @ID = LocationId
							From #BadGeography
							Where @RunId = @ActiveRunId

							Print 'Beginning conversion'
							EXEC awmDatabase.dbo.CheckLocationFeatureForValidGeographyAndFixIfNotValid @ID
				

							Set @ActiveRunId = @ActiveRunId + 1
						End
				End Try
				Begin Catch
					Print 'Unable to convert geometry to geography. Error occurred on LocationID ' + @ID
					
				End Catch
			Drop table #BadGeography			
		End
		Else
			Begin
				Print 'All features are valid geographies!'
			End

	Commit transaction;
END


GO


