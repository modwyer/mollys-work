/****** Metadata checks ******/

--Check Locations table
SELEct *
From Universe..Locations
Where LocationTypeId = 68
and CHARINDEX(', Burkina Faso',locationname)>0
and CHARINDEX(', Burkina Faso (',locationname)=0

--Check Base Admin table
SELEct *
From Universe..BaseAdminFeatures 
Where Admin0LocationId = -45373

SELEct l.locationname as AdminName, b.*
From Universe..BaseAdminFeatures B
inner join
Universe..Locations L
on B.Admin1LocationId = L.LocationId
--on B.Admin2LocationId = L.LocationId
Where Admin0LocationId = -45373

	

--Check Geohash table
SELEct *
From Universe..Geohash6LocationsLookup 
Where Admin0LocationId = -45373

SELEct l.locationname as AdminName, g.*
From Universe..Geohash6locationslookup g
inner join
Universe..Locations L
on g.Admin2LocationId = L.LocationId
--on g.Admin2LocationId = L.LocationId
Where Admin0LocationId = -45373

Select *
From Universe..Geohash6locationslookup
Where admin0locationid = -45373
and admin2locationid is null