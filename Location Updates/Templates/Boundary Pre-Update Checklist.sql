/******* Pre-Update checklist ********/
/** Use these script to ID information for the metadata file prior to updating and after the update**/

/** 1. ID region and continent **/
--countries=23 regions=25 continent=26; after finding region for the country, find the continent for the region by 
--adjusting the code using the terms after the '/' and the 2nd charindex for the region name
Use WorldBank_StarCluster_Data;

SELECT DISTINCT lr.locationtypeid, lr.relatedlocationtypeid, lr.LocationId, l1.locationname, lr.RelatedLocationId, l2.locationname 
	FROM dbo.LocationRelationships lr
		inner join locations l1 ON l1.locationid=lr.locationid --regions/continent
		inner join locations l2 ON l2.locationid=lr.relatedlocationid --countries/region
	WHERE lr.relatedlocationtypeid=25 --country/region (25)
		and lr.locationtypeid=26 --region/continent (26)
		and CHARINDEX('Dominican Republic',l2.LocationName)>0
		--and CHARINDEX('Southern Asia',l2.LocationName)>0
	ORDER BY l1.locationname, l2.locationname

/** 2. Find out current status of the database **/

--How many locations? Adjust charindex rows as needed
Select *
From Universe..Locations
Where LocationTypeId = 65 --65 for admin 1
and charindex(', Dominican Republic',locationname)>0
and charindex(', Dominican Republic (',locationname)=0
--and CHARINDEX(', Dominican Republicn',locationname)=0

--How many base admin?
Select l.locationname, b.*
From Universe..BaseAdminFeatures b
inner join
Universe..locations l
on b.admin2locationid = l.locationid
Where Admin0LocationId = -45438

--How many geohashes?
Select *
From Universe..Geohash6LocationsLookup
Where Admin0LocationId = -45438
and Admin2LocationId in (
	Select locationid
	From Universe..Locations
	Where LocationTypeId = 68 --65 for admin 1
	and charindex(', Dominican Republic',locationname)>0
	and charindex(', Dominican Republic (',locationname)=0)

Select Geohash6,Admin0Locationid, Admin1locationid, Admin2locationid,Admin3locationid, Admin4LocationId,Geometry::STGeomFromText(ShapeWKT,4326) as GeohashFeature
From Universe..Geohash6LocationsLookup
Where Admin0LocationId = -45438
Order by Admin1locationid, admin2locationid,admin3locationid