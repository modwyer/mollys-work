create table #Temp(RunID int identity, LocationID int, Latitude real, Longitude real, Elevation real, SpatialMeasure geometry, CentroidSpatialMeasure geometry)

Insert #temp(LocationID, Latitude, Longitude, Elevation, SpatialMeasure, CentroidSpatialMeasure)
select top 10000 LocationID, Latitude, Longitude, Elevation, SpatialMeasure, CentroidSpatialMeasure 
from GlobalGridDataStore..Global5by5MinGrid_full
where locationID between 50000000 and 59999999


Declare @MinRunID int = 1
Declare @MaxRunID int = 500 --at or below 5k
Declare @AbsMaxID int = (select max(runid) from #temp)
while @MinRunID <=@AbsMaxID 
begin

Select A.*, B.LocationName
from #temp A, GlobalGridDataStore..CountryLocations B
where A.CentroidSpatialMeasure.STIntersects(B.SpatialMeasure) = 1
and A.RunID between @MinRunID and @MaxRunID

set @MinRunID = @MinRunID + 500
set @MaxRunID = @MaxRunID + 500
end
