/* Script used to backup the databases prior to updating boundaries in QA*/
--Universe
BACKUP DATABASE [Universe] TO  DISK = N'R:\Backups\Universe_20150128.bak' 
WITH  COPY_ONLY, NOFORMAT, NOINIT,  NAME = N'Universe-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO


--WorldBank_StarCluster_Data
BACKUP DATABASE [WorldBank_StarCluster_Data] TO  DISK = N'R:\Backups\WorldBank_StarCluster_Data_20150128.bak' 
WITH  COPY_ONLY, NOFORMAT, NOINIT,  NAME = N'WorldBank_StarCluster_Data-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO

--ICRISAT_StarCluster_Data
BACKUP DATABASE [ICRISAT_StarCluster_Data] TO  DISK = N'R:\Backups\ICRISAT_StarCluster_Data_20150128.bak' 
WITH  COPY_ONLY, NOFORMAT, NOINIT,  NAME = N'ICRISAT_StarCluster_Data-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO

--UserData_StarCluster_Data
BACKUP DATABASE [UserData_StarCluster_Data] TO  DISK = N'R:\Backups\UserData_StarCluster_Data_20150128.bak' 
WITH  COPY_ONLY, NOFORMAT, NOINIT,  NAME = N'UserData_StarCluster_Data-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO