USE [TestDatabase]
GO

/****** Object:  StoredProcedure [dbo].[2aUpdateExistingAndInsertNewAdminBoundaries_v3]    Script Date: 7/14/2015 1:08:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*
-- =============================================
-- Author:		Dan Allen with Molly
-- Create date: 12/18/2014
-- Updates: 6/8/15 - MLO - Added code for checking if any updates have occurred for this country before, and if so, set all the hashkeys
							for that location to null.
						   Added error handling if the hashkey update fails
						   Wrapped code in transactions
						   Moved section 2b to the main proc for better control of flow
			6/18/15 - MLO - cast @SuffixCheck as varchar in dynamic sql
			7/14/15 - MLO - added variable to dynamic sql to better catch suffixes during pre-check
-- Description:	Update the existing location features for the admin type requested and then insert the new boundaries into universe


Example:
SET NOCOUNT ON;

Declare @LocationProcessData LocationProcessData

INSERT INTO @LocationProcessData
(LocationName, LocationTypeId, LocationFeature, CentroidFeature, PresentationLocationName, Hashkey)
SELECT top 1
	LocationName as [LocationName]
	,LocationTypeId as [LocationTypeId]
	,LocationFeature as [LocationFeature]
	,Centroid as [Centroid]
	,PresentationLocationName as [PresentationLocationName]
	,'Uganda-' + cast(ID as varchar(15)) as [HashKey]
  FROM [TestDatabase].[dbo].[vAdmin2Uganda]

--Select * from @LocationProcessData 

EXEC 2aUpdateExistingAndInsertNewAdminBoundaries_v3 68, 'Uganda', '2015', @LocationProcessData 

Select *
From Universe.dbo.Locations
Where LocationName like '%Uganda%'
and LocationTypeId = 68
*/

-- =============================================
alter PROCEDURE [dbo].[2aUpdateExistingAndInsertNewAdminBoundaries_v3]
	-- Add the parameters for the stored procedure here
	@LocationTypeId smallint,
	@CountryName varchar(250),
	@SuffixToAddToExistingFeatures varchar(20),
	@AdminBoundariesToProcess dbo.LocationProcessData READONLY,
	@SuffixCheck int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET XACT_ABORT,NOCOUNT ON;
	
	Declare @strSQL nvarchar(2000)
	
	--Check if any suffix has already been added to the country before, 6/8/15 mlo
	
		
	Print ('Checking if boundaries have ever been updated before for ' + @CountryName)
	
	Begin try --added try to isolate error occurrance
		Set @strSQL =
			'Select @SuffixCheck = count(*)' + char(13) --added variable 7/14 mlo
			+ 'From Universe..Locations ' + char(13)
			+ 'Where LocationTypeId = ' + cast(@LocationTypeId as varchar(5)) + char(13)
			+ 'and (charindex('', ' + @CountryName + ' (''' + ',locationname)>0' + char(13)
			+ 'or charindex('', ' + @CountryName + '(''' + ',locationname)>0)'

		Print @strSQL
		Exec sp_executesql @strSQL, N'@SuffixCheck int out', @SuffixCheck out --changed execute 7/14 mlo
	End Try
	Begin Catch
		Print ('!!! Error during Suffix check.' + char(13))
		return 1
	End Catch

	Print ('@SuffixCheck value: ' + cast(@SuffixCheck as varchar(10)))
	
	--If suffixes exist change the hashkey of those locations to null - 6/8/15 mlo
	Declare @results int

	Begin Transaction		
		If @SuffixCheck > 0
			Begin
				Print 'Old updates exist. Setting their hashkeys to Null.'
				--Set hashkey to null
				Begin try
					Update [Universe].[dbo].[Locations]
					Set HashKey = Null
						Where LocationTypeId = @LocationTypeId
						and (Charindex(', '+@CountryName+ ' (',LocationName) > 0
							or charindex(', ' + @CountryName + '(',locationname)>0)
						and HashKey is not Null
				End Try
				Begin Catch
					Print ('Unable to set hashkeys to null. Exiting update.' + char(13))
					Rollback
					Return 1;
				End Catch
				
				-- double check all hashkeys were set to null
				Select *
				From Universe..Locations
				Where LocationTypeId = @LocationTypeId
					and (Charindex(', '+@CountryName+ ' (',LocationName) > 0
						or charindex(', ' + @CountryName + '(',locationname)>0)
					and HashKey is not Null
				
				--if not, rollback, otherwise continue
				If @@rowcount > 0
					Begin
						Print 'Not all hashkeys were set to Null. Exiting Section 2A. Any changes in this section will be rolled back'
						rollback
						return 1
					End

				Print 'Successfully set hashkeys to null!'
			End
			Else
				Begin
					Print 'This country has never been updated! Proceeding with the update...'
				End
	Commit;

	--add suffix to locations
	Begin Transaction
		Begin transaction
			If @@error = 0 --added 6/8/15 mlo
				Begin
					Print 'Adding suffix to old locations.'
					--Add suffix to old locations
					Begin try
						Update [Universe].[dbo].[Locations]
							Set LocationName = LocationName + ' ('+@SuffixToAddToExistingFeatures+')'
							Where LocationTypeId = @LocationTypeId
							and Charindex(', '+@CountryName+'',LocationName,0) > 0
							and Charindex(', '+@CountryName+ ' (',LocationName,0) = 0
							and LocationName + ' ('+@SuffixToAddToExistingFeatures+')' 
							not in (Select LocationName 
									From Universe.dbo.Locations 
									Where LocationTypeId = @LocationTypeId)	
					End try
					Begin Catch
						Print 'Unable to add suffix to old locations. Exiting update.'
						Rollback
						Return 1;
					End Catch
				End
		Commit;

		Print 'Adding new boundaries to Locations table'
	
		--Insert the new boundaries into the Locations table
		Begin Transaction --added transaction/try catch 6/8/15 mlo
			Begin Try
				INSERT INTO [Universe].[dbo].[Locations]
						   ([LocationName]
						   ,[LocationTypeId]
						   ,[LocationFeature]
						   ,[CentroidFeature]
						   ,[PresentationLocationName]
						   ,HashKey)
				Select [LocationName]
						   ,[LocationTypeId]
						   ,[LocationFeature]
						   ,[CentroidFeature]
						   ,[PresentationLocationName]
						   ,HashKey
				From @AdminBoundariesToProcess	
				WHERE LocationName not in 
					(Select LocationName 
					From Universe.dbo.Locations 
					Where LocationTypeId = @LocationTypeId)
			End Try
			Begin Catch
				Print 'Unable to insert new boundaries into the Locations table. Exiting update.'
				Rollback
				Return 1;
			End Catch
		Commit;
	Commit;

	Print 'Section 2A complete'

END




GO


