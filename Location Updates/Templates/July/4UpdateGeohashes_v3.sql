USE [TestDatabase]
GO

/****** Object:  StoredProcedure [dbo].[4UpdateGeohashes_v3]    Script Date: 7/13/2015 9:31:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Molly O'Dwyer/Dan Allen
-- Create date: 2/4/2015

-- Description: 

/*
Updates:

3/04/2015 DJA Added in some code to facilitate subsetting of geohash6 features prior to spatial search methods
4/24/15 MLO Added in old code for buffering geohashes outside the polygons.
5/27/15 MLO Cleaned up the buffer section at the end and integrated it with the main geohash update. Added option to find null admin values in geohash table
6/8/15 MLO - Added @HashkeyLength variable to replace len(@CountryName + '-') that was in the code. 
			Added index for hashkeys called UK_Hashkey to speed up script processing.
			When counting remaining geohashes, changed code from @LocationTypeId in another select statement to inner join
6/10/15 - MLO - Wrapped code in transactions and try/catch blocks
7/13/15 - MLO - Added an If/Else to check if index UK_Hashkey exists and to drop and recreate or create depending on the result.

Select *
From universe..locations
Where LocationId = -45498
Exec dbo.[4UpdateGeohashes_v3] -45281, 65, 'Zambia', 7
*/
-- =============================================
alter PROCEDURE [dbo].[4UpdateGeohashes_v3]
	-- Add the parameters for the stored procedure here
	
	@CountryLocationId int,
	@LocationTypeId int,
	@CountryName varchar(40),
	@HashkeyLength int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET XACT_ABORT,NOCOUNT ON;

	--This is to get the bounding box to put into the spatial indexes below
	Declare @MinLong as int
	Declare @MaxLong as int
	Declare @MinLat as int
	Declare @MaxLat as int
	Declare @Env Geometry

	Declare @Geohash6 char(6), @Shape geometry, @ActiveGeohash char(6) 
	Declare @ActiveRunId int, @MaxRunId int	

	Declare @Admin0 int
	Declare @Admin1 int
	Declare @Admin2 int

	Declare @StartTime as datetime2(7)
	Declare @EndTime as datetime2(7)

	Declare @PrintStr varchar(10)
	Declare @strSQL varchar(2000)
	Declare @CountryShape geometry

	Declare @HashLoops Table (RunId int identity(1,1), MinGeohash char(6), MaxGeohash char(6))
	Declare @Neighbors Table (Neighbor char(3))
	Declare @RC int, @Iteration int
	Declare @Corners Table (Geohash3 char(3))
	Declare @GeohashC char(6)


	Set @StartTime = SYSDATETIME()
	print ('Start time is: ' + cast(@StartTime as varchar(19)))
	--create table to hold geohashes within the bounding box
	print 'Creating table #CountrySpecificGeohashes'

	Create Table #CountrySpecificGeohashes 
		(RunId int identity(1,1), Geohash6 char(6), GeoShape geometry, 
		Location0 int, Location1 int, Location2 int, primary key (Geohash6));
	print 'Creating table #NeedToBufferPoints'
	Create Table #NeedToBufferPoints (RunId int identity(1,1), Geohash6 char(6), Shape Geometry, primary key (RunId));
	print 'Creating table #AreaThatIntersectsBuffer'
	Create Table #AreaThatIntersectsBuffer (Geohash6 char(6), AdminLocationId int, Area float);
	print 'Creating table #UpdateForGeohashAdmin'
	Create Table #UpdateForGeohashAdmin (Geohash6 char(6), AdminLocationId int);
	print 'Creating table #CountrySpecificGeohashes'
	CREATE TABLE #BaseAdminFeatures
	(   RunId int identity(1,1),
		[BaseAdminLocationId] [int] NOT NULL,
		[BaseFeature] [geometry] NOT NULL,
		[Admin0LocationId] [int] NOT NULL,
		[Admin1LocationId] [int] NULL,
		[Admin2LocationId] [int] NULL,
	PRIMARY KEY CLUSTERED 
	(
		RunId ASC
	)
	); 
	print 'Creating index UK_Hashkey'

	If exists (                   --Added 7/13 to reduce error during index creation - mlo
		select *
		from universe.sys.indexes
		where name = 'UK_Hashkey'
		and object_id = OBJECT_ID('Universe.dbo.Locations'))
	Begin
		Print 'Index exists. Recreating new index.'
		Drop index UK_Hashkey on Universe..Locations
		Create nonclustered index UK_Hashkey on Universe..Locations (locationtypeid, Hashkey) 
	End
	Else
		Begin
			Print 'Index does not exist. Creating index.'
			Create nonclustered index UK_Hashkey on Universe..Locations (locationtypeid, Hashkey) 
		End

	
	----Get country parameter
	----Get country shape
	----Get country envelope
	Select @Env = LocationFeature.STEnvelope(), @CountryShape = LocationFeature
	From Universe.dbo.Locations
	Where LocationId = @CountryLocationId
	
	print 'Getting bounding box for country'

	--Get the bounding box around the country LocationId that was provided as parameter
	Select @MinLong = floor(Min(Longitude)), @MaxLong = ceiling(Max(longitude)), 
		@MinLat = floor(Min(Latitude)), @MaxLat = ceiling(Max(Latitude))
		From 
			(
			Select @Env.STPointN(1).STX as Longitude, @Env.STPointN(1).STY as Latitude
			UNION ALL
			Select @Env.STPointN(2).STX as Longitude, @Env.STPointN(2).STY as Latitude
			UNION ALL
			Select @Env.STPointN(3).STX as Longitude, @Env.STPointN(3).STY as Latitude
			UNION ALL
			Select @Env.STPointN(4).STX as Longitude, @Env.STPointN(4).STY as Latitude
			) i

	print ('Max Lat: ' + cast(@MaxLat as varchar(50)))
	print ('Min Lat: ' + cast(@MinLat as varchar(50)))
	print ('Max Long: ' + cast(@MaxLong as varchar(50)))
	print ('Min Long: ' + cast(@MinLong as varchar(50)))
	print 'Inserting geohashes within the bounds into #CountrySpecificGeohashes'
		
	Begin Transaction
		set @strSQL = 
		'CREATE SPATIAL INDEX #SPX_CountrySpecificGeohashes 
		ON #CountrySpecificGeohashes
		([geoshape])
		USING  GEOMETRY_GRID 
		WITH (
		bounding_box = ('+ cast(@MinLong as varchar(4)) + ', ' +  cast(@MinLat as varchar(4)) + ', ' +  cast(@MaxLong as varchar(4)) + ', ' + cast(@MaxLat as varchar(4)) + '), '
		+ ' GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
		CELLS_PER_OBJECT = 16);'
	
	
		exec(@strSQL)
		print ('Spatial Index #SPX_CountrySpecificGeohashes Script: ' + char(13) + @strSQL)

		set @strSQL = 
		'CREATE SPATIAL INDEX #SPX_BaseAdminFeatures ON #BaseAdminFeatures
		([BaseFeature])
		USING  GEOMETRY_GRID 
		WITH (
		bounding_box = ('+ cast(@MinLong as varchar(4)) + ', ' +  cast(@MinLat as varchar(4)) + ', ' +  cast(@MaxLong as varchar(4)) + ', ' + cast(@MaxLat as varchar(4)) + '), '
		+ ' GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
		CELLS_PER_OBJECT = 16);'
	
		exec(@strSQL)
		print ('Spatial Index #SPX_BaseAdminFeatures Script: ' + char(13) + @strSQL)

		--create spatial index
		set @strSQL = 
		'CREATE SPATIAL INDEX #SPX_Buffer ON #NeedToBufferPoints
		([Shape])
		USING GEOMETRY_GRID 
		WITH (
		bounding_box = ('+ cast(@MinLong as varchar(4)) + ', ' +  cast(@MinLat as varchar(4)) + ', ' +  cast(@MaxLong as varchar(4)) + ', ' + cast(@MaxLat as varchar(4)) + '), '
		+ ' GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
		CELLS_PER_OBJECT = 16);'

		exec(@strSQL)
		print ('Spatial Index #SPX_Buffer Script: ' + char(13) + @strSQL)

		--Get all geohashes within that envelope (or intersect)
		--insert those selected geohashes into the temp table
		Begin Try
			Insert into #CountrySpecificGeohashes
				(Geohash6, GeoShape)
				Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326)
				From [Universe].[dbo].[GeoHash6LocationsLookup]
				Where Latitude1km between @MinLat and @MaxLat
				and Longitude1km between @MinLong and @MaxLong
			SELECT @Env = [awmDatabase].[dbo].[GetGridCellShapeForBoundingBox] (
			   @MaxLat
			  ,@MinLat
			  ,@MaxLong
			  ,@MinLong)	
		End Try
		Begin Catch
			Print 'Unable to insert geohashes to #CountrySpecificGeohashes table. Exiting Section 4.'
			Rollback
			Return 1;
		End Catch

	Print 'Adding data to #BaseAdminFeatures'

	Begin Try
		INSERT INTO #BaseAdminFeatures
			(BaseAdminLocationId
			  ,[BaseFeature]
			  ,[Admin0LocationId]
			  ,[Admin1LocationId]
			  ,[Admin2LocationId])
		SELECT BaseAdminLocationId
			  ,[BaseFeature]
			  ,[Admin0LocationId]
			  ,[Admin1LocationId]
			  ,[Admin2LocationId]
		FROM [Universe].[dbo].[BaseAdminFeatures]
		WHERE BaseFeature.STIntersects(@Env) = 1
		and Admin0LocationId = @CountryLocationId
	End Try
	Begin Catch
		Print 'Unable to insert geohashes to #BaseAdminFeatures table. Exiting Section 4.'
		Rollback
		Return 1;
	End Catch

	Declare @Count int

	Select @Count = Count(*)
	From #BaseAdminFeatures

	print ('Count of Base Admin Features: ' + cast(@Count as varchar(10)))

	--Loop through all base admin features 
	Select @ActiveRunId = Min(RunId), @MaxRunId = Max(RunId)
	From #BaseAdminFeatures

	print ('Max Run Id: ' + cast(@MaxRunId as varchar(1000)))

	Set @StartTime = SYSDATETIME()
	print ('Start time for the loop through is: ' + cast(@StartTime as varchar(19)))

	Begin Try
		While @ActiveRunId <= @MaxRunId
			BEGIN
				Print ('Active run ID: ' + cast(@ActiveRunId as varchar(20)))

				Select @Shape = B.BaseFeature, @Admin0 = Admin0LocationId, 
				@Admin1 = Admin1LocationId, @Admin2 = Admin2LocationId
				From #BaseAdminFeatures B
				Where B.RunId = @ActiveRunId
		
				--DJA Added in code to help facilitating faster subsetting of geohashes for active shape
				select @Env = @Shape.STEnvelope()
		
				DELETE FROM @Corners
				INSERT INTO @Corners (Geohash3)
					Select Universe.dbo.Geohash_Encode(@Env.STPointN(1).STY,@Env.STPointN(1).STX, 3) 
					UNION ALL
					Select Universe.dbo.Geohash_Encode(@Env.STPointN(2).STY,@Env.STPointN(2).STX, 3) 
					UNION ALL
					Select Universe.dbo.Geohash_Encode(@Env.STPointN(3).STY,@Env.STPointN(3).STX, 3)
					UNION ALL 
					Select Universe.dbo.Geohash_Encode(@Env.STPointN(4).STY,@Env.STPointN(4).STX, 3) 
				
		
				Select @GeohashC = Universe.dbo.Geohash_Encode(@Env.STCentroid().STY,@Env.STCentroid().STX, 6) 		

				DELETE FROM @Neighbors
				INSERT INTO @Neighbors (Neighbor)
					SELECT * 
					FROM [awmDatabase].[dbo].[GetExtendedNeighbors] (left(@GeohashC,3), 0)
		
				Select @RC = COUNT(*)
				From @Corners
				Where Geohash3 not in (Select Neighbor from @Neighbors)
		
				set @Iteration = 1
		
				While @RC > 0 
					BEGIN
						INSERT INTO @Neighbors
						(Neighbor)
						SELECT * 
						FROM [awmDatabase].[dbo].[GetExtendedNeighbors] (left(@GeohashC,3), @Iteration)	
			
						Select @RC = COUNT(*)
						From @Corners
						Where Geohash3 not in (Select Neighbor from @Neighbors)
			
						set @Iteration = @Iteration + 1	
					END
		
				DELETE FROM @HashLoops
				INSERT INTO @HashLoops (MinGeohash, MaxGeohash)
					SELECT Neighbor + '000', Neighbor + 'zzz' 
					FROM @Neighbors 
		
				Print 'Updating Geohash6LocationsLookup table'
			
				Update [Universe].[dbo].[GeoHash6LocationsLookup]
					Set 
						Admin0LocationId = @Admin0,
						Admin1LocationId = @Admin1,
						Admin2LocationId = @Admin2
					Where Geohash6 in ( 
						Select Geohash6
						From #CountrySpecificGeohashes G, 
							@Hashloops H
							Where G.Geohash6 between H.MinGeohash and H.MaxGeohash
							and GeoShape.STWithin(@Shape) = 1 --can @Shape change to @Env and still work? This might avoid the need for the buffer script
						)		
	
				--print ('Current Active Run ID: ' + cast(@ActiveRunId as varchar(20)))
			
				set @ActiveRunId = @ActiveRunId + 1	
			End
		End Try
		Begin Catch
			Print 'Unable to complete the assignment of new locations to geohash table. Exiting Section 4.'
			Rollback
			Return 1;
		End Catch
	Commit;

	Print 'Regular geohash update is complete. Checking for bad geohashes within the country...'

	/**** Optional Section. Buffer the geohash point so that we can intersect it with features (since the point was not within a feature) ***/

	Begin Transaction
		Declare @CountOfRemaining int = 1
		Declare @ActiveIteration int = 1, @MaxIterations int = 5
		Declare @BufferSize float = 0.5
		declare @CountCountryMatch int
		declare @CountAdminMatch int

		--find all geohashes for specified location
		Select @CountCountryMatch = count(*)
			  From [Universe].[dbo].[GeoHash6LocationsLookup] g
			  where
					g.Admin0LocationId = @CountryLocationID
      
		--find all geohashes with specified location where the Admin 2 is in the locations table and the location has a hashkey
		--6/8/15 - MLO - changed from @LocationTypeId in another select statement to inner join
		Select @CountAdminMatch = count(*)
			  From [Universe].[dbo].[GeoHash6LocationsLookup] g
			  inner join Universe.dbo.Locations l
					on (case when @LocationTypeID = 68 then Admin2Locationid else Admin1Locationid end) = l.LocationId
			  Where g.Admin0LocationId = @CountryLocationID
			  and (Left(Hashkey, @HashkeyLength) = @CountryName + '-')
      
		--find how many bad geohashes remaining
		Select @CountOfRemaining = @CountCountryMatch - @CountAdminMatch               
       
	--Loop through bad geohashes and try to assign them to the nearest location
		Begin Try
			While @CountOfRemaining > 0 AND @ActiveIteration < @MaxIterations
				BEGIN
					print ('Count of Remaining geohashes: ' + cast(@CountOfRemaining as varchar(100)))
			
					--Get geohashes not within the polygons. These will be buffered
					Insert into #NeedToBufferPoints (Geohash6, Shape)
						Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326).STBuffer(@BufferSize)
						From [Universe].[dbo].[GeoHash6LocationsLookup]
						Where Admin0LocationId = @CountryLocationId 
						and (((case when @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in 
								(Select LocationId 
								from Universe.dbo.Locations 
								Where LocationTypeId = @LocationTypeId
								and (Left(Hashkey, @HashkeyLength) = @CountryName + '-')))
							or (case when @LocationTypeId = 68 then Admin2LocationId else Admin1Locationid end) is null)

					Select @PrintStr = (Select count(*) from #NeedToBufferPoints)

					print ('Count of geohashes not in a feature that will be buffered: ' + @PrintStr)
					print ('Current Buffer size: ' + cast(@BufferSize as varchar(100)))
					print ('Active Iteration: ' + cast(@ActiveIteration as varchar(100)))

					--Get the intersection of the buffered geohash point with the LocationFeature
					Truncate Table 	#AreaThatIntersectsBuffer
		
					INSERT INTO #AreaThatIntersectsBuffer (Geohash6, AdminLocationId, Area)
						Select B.Geohash6
								, C.LocationId as AdminLocationId
								, C.LocationFeature.STIntersection(B.Shape).STArea() as Area
						From Universe..Locations C, #NeedToBufferPoints B
						Where C.LocationFeature.STIntersects(B.Shape) = 1
						and C.LocationId in (
							Select LocationId 
							from Universe.dbo.Locations 
							Where LocationTypeId = @LocationTypeId
							and Left(Hashkey, @HashkeyLength) = @CountryName + '-')

					Select @PrintStr = (Select count(*) from #AreaThatIntersectsBuffer)
					print ('Count of geohashes whose buffer intersects with a LocationFeature: ' + @PrintStr)

					--Insert the max area from the intersection above in this table; this is what updates Geohash6LocationsLookup
					INSERT INTO #UpdateForGeohashAdmin (Geohash6, AdminLocationId)
						Select B.Geohash6, B.AdminLocationId
						From #AreaThatIntersectsBuffer B,	
							(
							Select Geohash6, Max(Area) as MaxArea
							From #AreaThatIntersectsBuffer
							Group by Geohash6
							) I
						Where B.Geohash6 = I.Geohash6
						and B.Area = I.MaxArea
						order by B.Geohash6, B.area desc

					Select @PrintStr = (Select count(*) from #UpdateForGeohashAdmin)
					print ('Count of geohashes to be updated: ' + @PrintStr)

					--depending on which location type running, update Geohash6LocationsLookup with new location IDs from above table
					if @LocationTypeId = 65
						BEGIN
							Update [Universe].[dbo].[GeoHash6LocationsLookup]
							Set Admin1LocationId = U.AdminLocationId
							From 
								[Universe].[dbo].[GeoHash6LocationsLookup] G,
								#UpdateForGeohashAdmin U
							Where G.Geohash6 = U.Geohash6
								and G.Admin0LocationId = @CountryLocationId
						END	
			
					if @LocationTypeId = 68
						BEGIN
							Update [Universe].[dbo].[GeoHash6LocationsLookup]
							Set Admin2LocationId = U.AdminLocationId
							From 
								[Universe].[dbo].[GeoHash6LocationsLookup] G,
								#UpdateForGeohashAdmin U
							Where G.Geohash6 = U.Geohash6
								and G.Admin0LocationId = @CountryLocationId
						END

					--find out if any remaining geohashes need to be buffered					      
					--find all geohashes with specified location where the Admin 2 is in the locations table and the location has a hashkey
					Select @CountAdminMatch = count(*)
						  From [Universe].[dbo].[GeoHash6LocationsLookup] g
						  inner join Universe.dbo.Locations l
								on (case when @LocationTypeID = 68 then Admin2Locationid else Admin1Locationid end) = l.LocationId
						  Where g.Admin0LocationId = @CountryLocationID
						  and (Left(Hashkey, @HashkeyLength) = @CountryName + '-')
      
					--find how many bad geohashes remaining
					Select @CountOfRemaining = @CountCountryMatch - @CountAdminMatch  
	        
					Truncate table #NeedToBufferPoints     
			
					Set @BufferSize = @BufferSize + .1
					Set @ActiveIteration = @ActiveIteration + 1
				End
			End Try
			Begin Catch
				Print 'Unable to complete the buffer portion of the geohash update. Exiting Section 4.'
				Rollback
				Return 1;
			End Catch
	Commit


	Set @EndTime = SYSDATETIME()
	print ('End time is: ' + cast(@EndTime as varchar(19)))
End




GO


