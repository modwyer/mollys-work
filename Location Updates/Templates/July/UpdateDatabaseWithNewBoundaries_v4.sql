USE [TestDatabase]
GO

/****** Object:  StoredProcedure [dbo].[UpdateDatabaseWithNewBoundaries_v3]    Script Date: 7/14/2015 12:58:38 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Molly O'Dwyer
-- Create date: 12/23/2014
-- Update date: 6/8/2015 
-- Description:	Wrapper Procedure around the procedures that update boundaries with new features
-- Update notes: added a check that the suffix doesn't already exist. If it does, the update ends
--			6/8/15 - MLO - Added transactions and rollbacks. Geohash proc will run independent of the other procs and the other procs will
--							rollback all work done if an error is encountered.
--			6/10/15-mlo- Moved 2b from section 2a to this proc for better control of flow. Move 3b to this proc and renamed. 3A became
--						3B and all but the make valid geometry code was removed. Original 3B became 3A, delete and insert from base admin table
--						Removed explicit transactions.
--			7/14/15 - mlo - Fixed functionality in the suffix check section and added check for truncated values
/*
SET NOCOUNT ON;

Declare @LocationProcessData LocationProcessData
Declare @truncate int

--Check presentationlocationname length, end update if >50 7/14 mlo
Print 'Checking length of PresentationLocationName column.' + char(13)

Select @truncate = count(*)
From Testdatabase.dbo.vAdmin2DominicanRepublic
Where len(presentationlocationname) > 50

Print ('Number of locations that would be truncated: '+ cast(@truncate as varchar(10))+ char(13))

If @truncate > 0
	Begin
		Print '!!!Error! PresentationLocationName is too long. Please shorten the name. Ending update.'
		Return;
	End
	Else
		Begin
			Print 'PresentationLocationName length is good. Continuing update...' + char(13)
			INSERT INTO @LocationProcessData
			(LocationName, LocationTypeId, LocationFeature, CentroidFeature, PresentationLocationName, Hashkey)
				SELECT
					LocationName as [LocationName]
					,LocationTypeId as [LocationTypeId]
					,LocationFeature as [LocationFeature]
					,Centroid as [Centroid]
					,PresentationLocationName as [PresentationLocationName]
					,'Dominican Republic-' + cast(ID as varchar(10)) as [HashKey]
				  FROM [TestDatabase].[dbo].[vAdmin2DominicanRepublic]

			EXEC UpdateDatabaseWithNewBoundaries_v4 68, 'Dominican Republic', 'Carribean', 'North America', '2015', @LocationProcessData, 19
		End

*/
-- =============================================
alter PROCEDURE [dbo].[UpdateDatabaseWithNewBoundaries_v4]

	@LocationTypeId smallint,
	@CountryName varchar(250),
	@RegionName varchar(250),
	@ContinentName varchar(250),
	@SuffixToAddToExistingFeatures varchar(20),
	@AdminBoundariesToProcess dbo.LocationProcessData READONLY,
	@HashkeyLength int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET XACT_ABORT,NOCOUNT ON;
	
	Declare @CountryLocationId int
	Declare @StartTime as datetime2(7)
	Declare @EndTime as datetime2(7)
	Declare @RegionLocationId int
	Declare @ContinentLocationId int
	Declare @SuffixCheck int

	Print 'Beginning Location Boundary Update'
	Set @StartTime = SYSDATETIME()
	Print ('Start time is: ' + cast(@StartTime as varchar(19)))
	
	--This gets the LocationId for the country name passed within the where clause
	Select @CountryLocationId = LocationId
	From Universe.dbo.Locations
	Where LocationName = @CountryName
	and LocationTypeId = 23
		
	--Get the LocationId for World Region that country falls within
	Select @RegionLocationId = LocationId
	From Universe.dbo.Locations
	Where LocationName = @RegionName --World Region
	and LocationTypeId = 25

	--Get the LocationId for Continent that country falls within
	Select @ContinentLocationId = LocationId
	From Universe.dbo.Locations
	Where LocationName = @ContinentName  --Continent
	and LocationTypeId = 26
	
	Select @CountryLocationid as Country, @RegionLocationid as Region, @ContinentLocationid as Continent

	--Check if suffix has already been added to the country before
	Declare @S nvarchar(2000)
		
	Print ('Checking if suffix already exists for ' + @CountryName)
	
	Set @S =
	'Select @SuffixCheck = count(*) ' + char(13) --added @SuffixCheck variable 7/14 mlo
	+ 'From Universe..Locations ' + char(13)
	+ 'Where LocationTypeId = ' + cast(@LocationTypeId as varchar(5)) + char(13)
	+ 'and (charindex('', ' + @CountryName + ' (' + @SuffixToAddToExistingFeatures + ')'',locationname)>0' + char(13)
	+ 'or charindex('', ' + @CountryName + '(' + @SuffixToAddToExistingFeatures + ')'',locationname)>0)'

	Print @S
	Exec sp_executesql @S, N'@SuffixCheck int out', @SuffixCheck out

	Print ('@SuffixCheck value: ' + cast(@SuffixCheck as varchar(10)))
	
	--see if the above select is 0 or greater than zero. This determines if the update continues
	If @SuffixCheck = 0  --adjusted parameter so If would work correctly 7/14 mlo
		Begin
			print 'This is a new suffix! Continuing the update...'
		End
	Else
		Begin
			raiserror('!!!This suffix already exists in the database for %s. Please check the suffix. Exiting update.',1,200, @CountryName)
			return;
		End
	
	Print ('Starting Section 2A: Update existing boundaries and insert new boundaries' + char(13))
	
	Begin Transaction --added transaction for section 2a and 2b as this is more difficult to go back and run manually mlo 6/15/15
		--Update Universe Locations table with new boundaries
		Begin Try
			EXECUTE [2aUpdateExistingAndInsertNewAdminBoundaries_v3] 
				@LocationTypeId
				,@CountryName
				,@SuffixToAddToExistingFeatures
				,@AdminBoundariesToProcess 
				,@SuffixCheck
		End Try
		Begin catch
			Print '!!!Error occurred in Section 2A. All changes have been rolled back. Update procedure has ended.'
			return 1
		End Catch

		Print 'Starting Section 2B: Update other databases with new locations'
		
		--Update other databases with new boundaries
		Begin Try
			Execute [2bUpdateOtherDatabasesForLocations_v3]
				@CountryName,
				@LocationTypeId
		End Try
		Begin catch
			Print '!!!Error occurred in Section 2B. All changes have been rolled back. Update procedure has ended.'
			return 1
		End Catch
		
		Print ('Section 2B complete' + char(13))
	Commit;

	
	Print 'Starting Section 3A: Delete and insert locations to Base Admin'
		
	Begin Try
		EXECUTE [3aDeleteAndInsertToBaseAdmin_v3] 
				@LocationTypeId
				,@CountryName
				,@CountryLocationId
				,@RegionLocationId 
				,@ContinentLocationId
	End Try
	Begin catch
		Print '!!!Error occurred in Section 3A. All changes have been rolled back. Update procedure has ended.'
		return 1
	End Catch
		
	Print ('Section 3A complete.' + char(13))

	Print 'Starting Section 3B: Make Geometries Valid'

	Begin Try
		EXECUTE [3bMakeGeometriesValid_v3] 
				@LocationTypeId
				,@CountryName
				,@CountryLocationId
				,@HashkeyLength
	End Try
	Begin catch
		Print '!!!Error occurred in Section 3B. All changes have been rolled back. Update procedure has ended.'
		return 1
	End Catch

	Print ('Section 3B complete' + char(13))

	Print 'Starting Section 4: Update Geohashes'
	
	Begin Try
		EXECUTE [4UpdateGeohashes_v3] 
			@CountryLocationId,
			@LocationTypeId,
			@CountryName,
			@HashkeyLength
	 
		Print ('Section 4 complete' + char(13))
	End Try
	Begin catch
		Print '!!!Error occurred in Section 4. All changes made during Section 4 have been rolled back. 
				Changes made in previous sections will be retained. This section has ended.'
		return 1
	End Catch
	

	Print ('Updated completed at: ' + cast(@EndTime as varchar(19)))
	
END

GO


