--Check Locations
Declare @CountryName varchar(200) = Uganda

Select *
From Universe.dbo.Locations 
Where LocationTypeId = 68
and charindex(', ' + @CountryName + '',locationname)>0
and charindex(', ' + @CountryName + '(',locationname)=0
and charindex(', ' + @CountryName + ' (',locationname)=0

--Check BaseAdminFeatures
Select *
From Universe.dbo.BaseAdminFeatures 
Where Admin0LocationID = -45330


--Check Geohashes
Select *
From Universe.dbo.Geohash6LocationsLookup 
Where Admin0LocationID = -45301
--run this to ID geohashes with admin 3
and Admin3locationid is not null


--the issue: DRC admin 3 is getting in the Uganda baseadmin
Select L.LocationName,L.LocationFeature, G.Geohash6, Geography::STGeomFromText(ShapeWKT,4326) as Centroid, Geometry::STGeomFromText(ShapeWKT,4326).STY as Lat, Geometry::STGeomFromText(ShapeWKT,4326).STX as Long, G.Admin0locationid, g.admin1locationid,g.admin2locationid,g.admin3locationid
From Geohash6LocationsLookup G
inner join
Locations L
on G.Admin3LocationId = L.LocationId
Where Admin0LocationId = -45301
and Admin3LocationId is not null
and Admin2LocationId = 92161533 --Maracha, Maracha, Uganda
--the fix:
Update Universe..BaseAdminFeatures
Set Admin2LocationID =   --DRC admin 2 location
Where Admin2Locationid = 92161533 -- Maracha, Maracha, Uganda
and Admin3Locationid = 90337524 --Aru, Ituri, Orientale, Democratic Republic of the Congo ; this should definitely map to DRC, not Kenya

--view lat and long of geohash
Select L.LocationName,L.LocationFeature, G.Geohash6, Geography::STGeomFromText(ShapeWKT,4326) as Centroid, Geometry::STGeomFromText(ShapeWKT,4326).STY as Lat, Geometry::STGeomFromText(ShapeWKT,4326).STX as Long, G.Admin0locationid, g.admin1locationid,g.admin2locationid,g.admin3locationid
From Geohash6LocationsLookup G
inner join
Locations L
on G.Admin2LocationId = L.LocationId
Where Admin3LocationId = 90337524 --Aru, Ituri, Orientale, Democratic Republic of the Congo 


--view lat and long of Locations
Declare @CountryName varchar(200) = 'Kaabong, Uganda' --'Democratic Republic of the Congo'
Select [LocationId]
      ,[LocationName]
      ,[CentroidFeature].STY as Latitude --STY must be capitalized, parses out lat
      ,[CentroidFeature].STX as Longitude --STX must be capitalized, parses out long
      ,[LocationFeature]
From Universe.dbo.Locations 
Where LocationTypeId = 68
and charindex(', ' + @CountryName + '',locationname)>0
and charindex(', ' + @CountryName + '(',locationname)=0
and charindex(', ' + @CountryName + ' (',locationname)=0
