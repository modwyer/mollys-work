﻿--Just want to see if we are acting on the right set of data with this where clause
Select	*
From [Universe].[dbo].[Locations]
Where LocationTypeId = 68
and Charindex( ', Ethiopia',LocationName,0) > 0
order by LocationName
and CHARINDEX(', Ethiopia (2014)',LocationName, 0)=0

--STOP   If no error, proceed to next statement
--Change the existing records to a different archive name concatenated with the last year of use
Update [Universe].[dbo].[Locations]
Set LocationName = LocationName + ' (2013)'
Where LocationTypeId = 68
and Charindex( ', Kenya',LocationName,0) > 0

--STOP   If no error, proceed to next statement




--Pull from the master version (universe) with update to name
Update WorldBank_StarCluster_Data.[dbo].[Locations]
Set LocationName = U.LocationName 
From WorldBank_StarCluster_Data.[dbo].[Locations] L, 
Universe.[dbo].[Locations] U
Where U.LocationTypeId = 68
and U.LocationId = L.LocationId
and Charindex( ', Ethiopia',L.LocationName,0) > 0 
 
 select *
From WorldBank_StarCluster_Data.[dbo].[Locations]  
Where LocationTypeId = 68
and Charindex( ', Ethiopia',LocationName,0) > 0


Select Distinct LocationId 
From WorldBank_StarCluster_Data.[dbo].Intersection 
Where LocationId in (Select LocationId 
	From WorldBank_StarCluster_Data.[dbo].[Locations] L
	Where LocationTypeId = 68
	and Charindex( ', Ethiopia (2014)',L.LocationName,0) > 0  )
	
	
Select Distinct LocationId 
From Icrisat_StarCluster_Data.[dbo].Intersection 
Where LocationId in (Select LocationId 
	From Icrisat_StarCluster_Data.[dbo].[Locations] L
	Where LocationTypeId = 68
	and Charindex( ', Ethiopia (2014)',L.LocationName,0) > 0  )	
	
Select Distinct LocationId 
From UserData_StarCluster_Data.[dbo].Intersection 
Where LocationId in (Select LocationId 
	From UserData_StarCluster_Data.[dbo].[Locations] L
	Where LocationTypeId = 68
	and Charindex( ', Ethiopia (2014)',L.LocationName,0) > 0  )		
	
	
Delete From UserData_StarCluster_Data.[dbo].[Locations]
	Where LocationTypeId = 68
	and Charindex( ', Ethiopia (2014)',LocationName,0) > 0	

Delete From WorldBank_StarCluster_Data.[dbo].[Locations]
	Where LocationTypeId = 68
	and Charindex( ', Ethiopia (2014)',LocationName,0) > 0		
	

select *
From WorldBank_StarCluster_Data.[dbo].[Locations]
Where LocationTypeId = 68
and Charindex( ', Ethiopia',LocationName,0) > 0  

--STOP   If no error, proceed to next statement

--Pull from the master version (universe) with update to name
Update ICRISAT_StarCluster_Data.[dbo].[Locations]
Set LocationName = U.LocationName 
From ICRISAT_StarCluster_Data.[dbo].[Locations] L, Universe.[dbo].[Locations] U
Where U.LocationTypeId = 68
and U.LocationId = L.LocationId
and Charindex( ', Ethiopia',L.LocationName,0) > 0  

--STOP   If no error, proceed to next statement

--Pull from the master version (universe) with update to name
Update UserData_StarCluster_Data.[dbo].[Locations]
Set LocationName = U.LocationName 
From UserData_StarCluster_Data.[dbo].[Locations] L, 
Universe.[dbo].[Locations] U
Where U.LocationTypeId = 68
and U.LocationId = L.LocationId
and Charindex( ', Ethiopia',L.LocationName,0) > 0 

--STOP   If no error, proceed to next statement

--Insert the geometry data that you uploaded using SharpGIS utility
INSERT INTO [Universe].[dbo].[Locations]
           ([LocationName]
           ,[LocationTypeId]
           ,[LocationFeature]
           ,[CentroidFeature]
           ,[PresentationLocationName]
           ,HashKey)
SELECT 
		Left([CONSTITUEN],1) + 
	right(lower([CONSTITUEN]),len([CONSTITUEN])-1) 
	+ ', ' + Left([COUNTY_NAM],1) + 
	right(lower([COUNTY_NAM]),len([COUNTY_NAM])-1) + ', Kenya'
	as LocationName
,68 as LocationTypeId
	,[shape]
	,case when [shape].MakeValid().STCentroid()	is null then Shape.MakeValid().STEnvelope().STCentroid() else [shape].MakeValid().STCentroid() end as Centroid
	,	Left([CONSTITUEN],1) + right(lower([CONSTITUEN]),len([CONSTITUEN])-1) 
	+ ', ' + Left([COUNTY_NAM],1) + right(lower([COUNTY_NAM]),len([COUNTY_NAM])-1) + ', Kenya' as [PresentationLocationName]
	,'Kenya-' + cast(Id as varchar(10)) as HashKey
  FROM [TestDatabase].[dbo].[KenyaAdmin2]
 
--STOP   If no error, proceed to next statement
 
--Get the LocationId for Country (admin0) 
--We run this to get the LocationId to manually alter the scripts below
--Run each Location select statement (3) separately. Note the result and update that number in the remaining script

--This gets the LocationId for the country name passed within the where clause
Select LocationId
From Universe.dbo.Locations
Where LocationName = 'Kenya'
and LocationTypeId = 23
-->>>-45344<<<<<
-- Update this number on line ~101, 171, 187, 205, 257, 298, 313

--Get the LocationId for World Region that country falls within
Select LocationId
From Universe.dbo.Locations
Where LocationName = 'Eastern Africa'
and LocationTypeId = 25
-->>>-4842<<<<<
--Update this number on lines ~175

--Get the LocationId for Continent that country falls within
Select LocationId
From Universe.dbo.Locations
Where LocationName = 'Africa'
and LocationTypeId = 26
-->>>-46118<<<<<
--Update this number on lines ~174

--STOP   If no error, proceed to next statement

DELETE      
  FROM [Universe].[dbo].[BaseAdminFeatures]
Where [BaseAdminLevelId] >= 1
  and Admin0LocationId = -45344

--STOP   If no error, proceed to next statement

--we need to get the Admin1LocationIds above our newly added Admin2Locations so we need to do some spatial processing using point and polygon analysis
Declare @Relationship2To1 Table (Admin2LocationId int, Admin1LocationId int)

Insert into @Relationship2To1
(Admin2LocationId,Admin1LocationId)
Select NewAdmin2.LocationId as Admin2LocationId, Admin1.LocationId as Admin1LocationId
From  
(
Select LocationId, CentroidFeature
From Universe.dbo.Locations
Where LocationTypeId = 68 --admin2
and Charindex( ', Kenya',LocationName,0) > 0 
and HashKey like 'Kenya-%'  
) NewAdmin2,
(  
Select LocationId, LocationFeature
From Universe.dbo.Locations
Where LocationTypeId = 65 --admin1
and Charindex( ', Kenya',LocationName,0) > 0  
) Admin1
Where NewAdmin2.CentroidFeature.STWithin(Admin1.LocationFeature) = 1


Insert into @Relationship2To1
(Admin2LocationId,Admin1LocationId)
Select NewAdmin2.LocationId as Admin2LocationId, Admin1.LocationId as Admin1LocationId
From  
(
Select LocationId, LocationFeature
From Universe.dbo.Locations
Where LocationTypeId = 68
and Charindex( ', Kenya',LocationName,0) > 0 
and HashKey like 'Kenya-%'  
) NewAdmin2,
(  
Select LocationId, LocationFeature
From Universe.dbo.Locations
Where LocationTypeId = 65
and Charindex( ', Kenya',LocationName,0) > 0  
) Admin1
Where NewAdmin2.LocationFeature.STIntersects(Admin1.LocationFeature) = 1
and NewAdmin2.LocationId not in (Select Admin2LocationId from @Relationship2To1)


INSERT INTO [Universe].[dbo].[BaseAdminFeatures]
           ([BaseAdminLocationId]
           ,[BaseFeature]
           ,[BaseAdminLevelId]
           ,[Admin0LocationId]
           ,[Admin1LocationId]
           ,[Admin2LocationId]
           ,[ContinentLocationId]
           ,[WorldRegionLocationId])
SELECT L.[LocationId]
      ,L.[LocationFeature]
      ,1 as BaseAdminLevelId
      ,-45344 as Admin0LocationId
      ,R.Admin1LocationId
      ,R.Admin2LocationId
      ,-46118 as [ContinentLocationId]
	  ,-4842 as [WorldRegionLocationId]
  FROM [Universe].[dbo].[Locations] L, 
  @Relationship2To1 R
Where LocationTypeId = 68
and Left(HashKey, LEN('Kenya-')) = 'Kenya-'
and R.Admin2LocationId = L.LocationId

--STOP   If no error, proceed to next statement

--Update any geometries that are not valid
Update [Universe].[dbo].[BaseAdminFeatures]
set BaseFeature = BaseFeature.MakeValid()
Where Admin0LocationId = -45344  
and [BaseFeature].STIsValid() = 0

Update [Universe].[dbo].[Locations]
set LocationFeature = LocationFeature.MakeValid()
Where LocationTypeId = 68
and Left(HashKey,LEN('Kenya-')) = 'Kenya-'
and LocationFeature.STIsValid() = 0

--STOP   If no error, proceed to next statement

Create Table #Features (RunId int identity(1,1), Admin1LocationId int, Shape geometry, primary key (RunId), unique (Admin1LocationId));

Create Table #Geohashes (Geohash6 char(6), Shape geometry, primary key (Geohash6));

--STOP   If no error, proceed to next statement

Declare @Env Geometry

Select @Env = LocationFeature.STEnvelope()
From Universe.dbo.Locations
Where LocationId = -45344 

--This is to get the bounding box to put into the spatial indexes below
Select floor(Min(Longitude)) as MinLong, ceiling(Max(longitude)) as MaxLong, 
floor(Min(Latitude)) as MinLat, ceiling(Max(Latitude)) as MaxLat
From 
	(
	Select @Env.STPointN(1).STX as Longitude, @Env.STPointN(1).STY as Latitude
	UNION ALL
	Select @Env.STPointN(2).STX as Longitude, @Env.STPointN(2).STY as Latitude
	UNION ALL
	Select @Env.STPointN(3).STX as Longitude, @Env.STPointN(3).STY as Latitude
	UNION ALL
	Select @Env.STPointN(4).STX as Longitude, @Env.STPointN(4).STY as Latitude
	) i

--record the results in the bounding boxes below
--STOP   If no error, proceed to next statement

--Build Spatial indexes to help speed up the updates on Geohash lookup table

--If error creating spatial index, simply drop them and redo:
--drop index #SPX_Geohashes on #Geohashes
--drop index #SPX_Features on #Features

CREATE SPATIAL INDEX #SPX_Geohashes ON #Geohashes
(
	[shape]
)USING  GEOMETRY_GRID 
WITH (
--bounding_box = (MinLong, MinLat, MaxLong, MaxLat)
BOUNDING_BOX =(28, -2, 36, 5), 
GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
CELLS_PER_OBJECT = 16);

CREATE SPATIAL INDEX #SPX_Features ON #Features
(
	[shape]
)USING  GEOMETRY_GRID 
WITH (
--bounding_box = (MinLong, MinLat, MaxLong, MaxLat)
BOUNDING_BOX =(28, -2, 36, 5), GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
CELLS_PER_OBJECT = 16);

--STOP   If no error, proceed to next statement

--Convert the wkt from the geohash lookup table into a geometry data type in a temp table
--this may take a minute or two
INSERT INTO #Geohashes
(Geohash6, Shape)
Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326)
From [Universe].[dbo].[GeoHash6LocationsLookup]
Where Admin0LocationId =  -45344
Order by Geohash6

--STOP   If no error, proceed to next statement

--Insert all the features that you added so that you can loop through them and grab the geohash coordinates that are within each feature
Insert into #Features 
(Admin1LocationId, Shape)
Select LocationId, LocationFeature 
	FROM [Universe].[dbo].[Locations]
	Where LocationTypeId = 68 --admin2
	and Left(HashKey, LEN('Kenya-')) = 'Kenya-'

select COUNT(*) from #Features  --this is a doublecheck
--STOP   If no error, proceed to next statement

--This will loop through and update all the geohash lookups to have the new admin2 locationId
--This took about 3 minutes for admin2 for Kenya
Declare @Geohash6 char(6), @Shape geometry, @ActiveAdmin1LocationId int
Declare @ActiveRunId int, @MaxRunId int

Select @ActiveRunId = Min(RunId), @MaxRunId = Max(RunId)
From #Features

While @ActiveRunId <= @MaxRunId
BEGIN
	Select @ActiveAdmin1LocationId = Admin1LocationId ,  @Shape = Shape 
	From #Features 
	Where RunId = @ActiveRunId  
	
	Update [Universe].[dbo].[GeoHash6LocationsLookup]
	Set Admin1LocationId = @ActiveAdmin1LocationId, Admin2LocationId = Null, Admin3LocationId = Null, 
	 Admin4LocationId = Null, Admin5LocationId = Null
	Where Geohash6 in ( 
		Select Geohash6
		From #Geohashes
		Where Shape.STWithin(@Shape) = 1
		)
		and Admin0LocationId = -45344
		

	set @ActiveRunId = @ActiveRunId + 1
END

--STOP   If no errors, proceed to next statement

--Find all the geohashes that are still pointing to old admin2 locations in Uganda
Select LocationName, LocationFeature
From Universe.dbo.Locations
Where LocationTypeId = 68 and Left(Hashkey, LEN('Uganda-')) = 'Uganda-'
UNION ALL
Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326)
From [Universe].[dbo].[GeoHash6LocationsLookup]
Where Admin0LocationId = -45301 
and Admin2LocationId not in 
	(Select LocationId 
	from Universe.dbo.Locations 
	Where LocationTypeId = 68 and Left(Hashkey, LEN('Uganda-')) = 'Uganda-')

--STOP   If no error, proceed to next statement
--now run just the second Select above to make sure there are no extra geohashes pointing to old admin2 locations

--run the following Select to check count	
Select count(*) 
From [Universe].[dbo].[GeoHash6LocationsLookup]
Where Admin0LocationId = -45301 

-------****if no extra geohashes, do not run any script below this line. You are done and can now test! **** -----------	
Create Table #NeedToBufferPoints (RunId int identity(1,1), Geohash6 char(6), Shape Geometry )

--Buffer the geohash point so that we can intersect it with features (since
--the point was not within a feature
Insert into #NeedToBufferPoints
(Geohash6, Shape)
Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326).STBuffer(0.05)
From [Universe].[dbo].[GeoHash6LocationsLookup]
Where Admin0LocationId = -45301 
and Admin2LocationId not in 
	(Select LocationId 
	from Universe.dbo.Locations 
	Where LocationTypeId = 68 and Left(Hashkey, LEN('Uganda-')) = 'Uganda-')


Create Table #AreaThatIntersectsBuffer (Geohash6 char(6), Admin2LocationId int, Area float)

--Get the intersection of the buffered geohash point with the admin2 polygon feature
Truncate Table 	#AreaThatIntersectsBuffer
INSERT INTO #AreaThatIntersectsBuffer
(Geohash6, Admin2LocationId, Area)
Select Geohash6, Admin2LocationId, F.Shape.STIntersection(B.Shape).STArea()
From #Features F, #NeedToBufferPoints B
Where F.Shape.STIntersects(B.Shape) = 1

Create Table #UpdateForGeohashAdmin2 (Geohash6 char(6), Admin2LocationId int)

--Get the max area for each of the intersections and then associate
--the geohash with the locationId that has that max
INSERT INTO #UpdateForGeohashAdmin2
(Geohash6, Admin2LocationId)
Select B.Geohash6, B.Admin2LocationId
From #AreaThatIntersectsBuffer B,	
	(
	Select Geohash6, Max(Area) as MaxArea
	From #AreaThatIntersectsBuffer
	Group by Geohash6
	) I
Where B.Geohash6 = I.Geohash6
and B.Area = I.MaxArea
order by B.Geohash6, B.area desc

--Just take a quick look at the returned records to make sure it looks good before the update
Select *
From 
	[Universe].[dbo].[GeoHash6LocationsLookup] G,
	#UpdateForGeohashAdmin2 U
Where G.Geohash6 = U.Geohash6
	and G.Admin0LocationId = -45301

--Update those pesky geohashs that were not contained by the new features
Update [Universe].[dbo].[GeoHash6LocationsLookup]
Set Admin2LocationId = U.Admin2LocationId
From 
	[Universe].[dbo].[GeoHash6LocationsLookup] G,
	#UpdateForGeohashAdmin2 U
Where G.Geohash6 = U.Geohash6
	and G.Admin0LocationId = -45301