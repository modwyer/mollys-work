USE [TestDatabase]
GO

/****** Object:  StoredProcedure [dbo].[UpdateBaseFeatures_DeleteAndInsertBaseAdmin]    Script Date: 3/13/2015 1:48:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molly O'Dwyer
-- Create date: 3/13/2015
-- Description:	Delete the base admin for a specific country and insert the Locations table locations to BaseAdminFeatures table
/*
Select *
FROM [Universe].[dbo].[BaseAdminFeatures] B
inner join
Universe..Locations L
on B.Admin3LocationId = L.LocationId
Where Admin3LocationId = 90337387
*/


/*
EXEC dbo.[UpdateBaseFeatures_DeleteAndInsertBaseAdmin] 70, 'Democratic Republic of the Congo', -45330, -4841, -46118
*/
-- =============================================

alter PROCEDURE [dbo].[UpdateBaseFeatures_DeleteAndInsertBaseAdmin]
	-- Add the parameters for the stored procedure here
	@LocationTypeId smallint,
	@CountryName varchar(250),
	@CountryLocationId int,
	@RegionLocationId int,
	@ContinentLocationId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Print 'Deleting base features for ' + @CountryName

	DELETE      
	FROM [Universe].[dbo].[BaseAdminFeatures]
	Where Admin0LocationId = @CountryLocationId	

	Print 'Base features deleted: ' + cast(@@rowcount as varchar(20))

	--If updating Admin 3 in BaseAdminFeatures
	if @LocationTypeId = 70
	BEGIN

		Print 'Updating BaseAdminFeatures table Admin 1, 2, and 3 locations'

		--We need to get the Admin2LocationIds above our Admin3Locations so we need to do some spatial processing using point and polygon analysis
		Declare @Relationship2To1 Table (Admin3LocationId int, Admin2LocationId int, Admin1LocationId int)

		--Insert admin 3, admin 2 and 1 IDs where Admin3 Centroid is within the Admin 2 polygon
		--and where Admin 2 Centroid is within the Admin 1 polygon
		Insert into @Relationship2To1
		(Admin3LocationId, Admin2LocationId, Admin1LocationId)
		Select Admin3.LocationId as Admin3LocationId
				, Admin2.LocationId as Admin2LocationId
				, Admin1.LocationId as Admin1LocationId
		From  
			(
			Select LocationId, CentroidFeature, LocationName
			From Universe.dbo.Locations
			Where LocationTypeId = 70 --admin3
			and Charindex(', Democratic Republic of the Congo',LocationName,0) > 0				
			) Admin3,
			(  
			Select LocationId, LocationFeature, CentroidFeature, Locationname
			From Universe.dbo.Locations
			Where LocationTypeId = 68 --admin2
			and Charindex(', Democratic Republic of the Congo',LocationName,0) > 0				
			) Admin2,
			(  
			Select LocationId, LocationFeature, LocationName
			From Universe.dbo.Locations
			Where LocationTypeId = 65 --admin1
			and Charindex(', Democratic Republic of the Congo',LocationName,0) > 0				
			) Admin1
		Where Admin3.CentroidFeature.STWithin(Admin2.LocationFeature) = 1 
		and Admin2.CentroidFeature.STWithin(Admin1.LocationFeature) = 1

	--	', '+@CountryName+''
		
		Print 'Point locations added to relationship table: ' + cast(@@rowcount as varchar(20))
		
		--Insert admin2 and 1 IDs where Admin 2 polygon intersects the Admin 1 polygon and Admin2 ID is not already in the table
		Insert into @Relationship2To1
		(Admin3LocationId, Admin2LocationId, Admin1LocationId)
		Select Admin3.LocationId as Admin3LocationId
				, Admin2.LocationId as Admin2LocationId
				, Admin1.LocationId as Admin1LocationId
		From  
			(
			Select LocationId, CentroidFeature, LocationFeature, LocationName
			From Universe.dbo.Locations
			Where LocationTypeId = 70 --admin3
			and Charindex(', Democratic Republic of the Congo',LocationName,0) > 0				
			) Admin3,
			(  
			Select LocationId, LocationFeature, CentroidFeature, Locationname
			From Universe.dbo.Locations
			Where LocationTypeId = 68 --admin2
			and Charindex(', Democratic Republic of the Congo',LocationName,0) > 0				
			) Admin2,
			(  
			Select LocationId, LocationFeature, LocationName
			From Universe.dbo.Locations
			Where LocationTypeId = 65 --admin1
			and Charindex(', Democratic Republic of the Congo',LocationName,0) > 0				
			) Admin1
		Where Admin3.LocationFeature.STIntersects(Admin2.LocationFeature) = 1
		and Admin2.CentroidFeature.STIntersects(Admin1.LocationFeature) = 1
		and Admin3.LocationId not in (Select Admin3LocationId from @Relationship2To1)
		and Admin2.LocationId not in (Select Admin2LocationId from @Relationship2To1)

		Print 'Polygon locations added to relationship table: ' + cast(@@rowcount as varchar(20))

		Print 'Inserting Admin 2 IDs into base features'
		
		--Insert Admin 1, 2 and 3 IDs from above table and Locations table into the BaseAdminFeatures table as the base admin level
		INSERT INTO [Universe].[dbo].[BaseAdminFeatures]
				   ([BaseAdminLocationId]
				   ,[BaseFeature]
				   ,[BaseAdminLevelId]
				   ,[Admin0LocationId]
				   ,[Admin1LocationId]
				   ,[Admin2LocationId]
				   ,[Admin3LocationId]
				   ,[ContinentLocationId]
				   ,[WorldRegionLocationId])
		SELECT L.[LocationId] as BaseAdminLocationId
			  ,L.[LocationFeature] as BaseFeature
			  ,3 as BaseAdminLevelId
			  ,@CountryLocationId as Admin0LocationId
			  ,R.Admin1LocationId
			  ,R.Admin2LocationId
			  ,R.Admin3LocationId
			  ,@ContinentLocationId as [ContinentLocationId]
			  ,@RegionLocationId as [WorldRegionLocationId]
		  FROM [Universe].[dbo].[Locations] L, 
		  @Relationship2To1 R
		Where L.LocationTypeId = 70 --Admin3
		and Charindex(', Democratic Republic of the Congo',L.LocationName,0) > 0																				
		and R.Admin3LocationId = L.LocationId

		Print 'Admin 1, 2, and 3 LocationIDs added to the BaseAdminFeatures table: ' + cast(@@rowcount as varchar(20))
	END
		
	----If processing Admin 1	
	--If @LocationTypeId = 65
	--BEGIN

	--	Print 'Processing Admin 1 locations'

	--	INSERT INTO [Universe].[dbo].[BaseAdminFeatures]
	--			   ([Admin0LocationId]
	--			   ,[Admin1LocationId]
	--			   ,[ContinentLocationId]
	--			   ,[WorldRegionLocationId])
	--	Select 
	--		LocationId as [BaseAdminLocationId]
	--		,LocationFeature as [BaseFeature]
	--		,1 as BaseAdminLevelId
	--		,@CountryLocationId as Admin0LocationId
	--		,LocationId
	--		,@ContinentLocationId as [ContinentLocationId]
	--		,@RegionLocationId as [WorldRegionLocationId]				
	--	From Universe.dbo.Locations
	--	Where LocationTypeId = 65 --admin1
	--	and Charindex(', '+@CountryName+'',LocationName,0) > 0				
	--	and Charindex(', '+@CountryName+ ' (',LocationName,0) = 0					   
	--END
End

GO

