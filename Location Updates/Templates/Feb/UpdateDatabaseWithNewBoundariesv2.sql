USE [TestDatabase]
GO
/****** Object:  StoredProcedure [dbo].[UpdateDatabaseWithNewBoundaries]    Script Date: 12/23/2014 12:09:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Molly O'Dwyer
-- Create date: 12/23/2014
-- Update date: 2/17/15
-- Description:	Wrapper Procedure around the procedures that update boundaries with new features
/*
SET NOCOUNT ON;

--Get list of regions from a list. Can view the Spatial results if needed to help determine region of desired country.
Select LocationId, LocationName, LocationFeature
	From Universe.dbo.Locations
	Where LocationTypeId = 25

--Get list of countinents from a list. Can view the Spatial results if needed to help determine region of desired country.
Select LocationId, LocationName, LocationFeature
	From Universe.dbo.Locations
	Where LocationTypeId = 26	

Declare @LocationProcessData LocationProcessData

INSERT INTO @LocationProcessData
(LocationName, LocationTypeId, LocationFeature, CentroidFeature, PresentationLocationName, Hashkey)
SELECT
	LocationName as [LocationName]
	,LocationTypeId as [LocationTypeId]
	,LocationFeature as [LocationFeature]
	,Centroid as [Centroid]
	,PresentationLocationName as [PresentationLocationName]
	,Hashkey as [HashKey]
	--,'India-' + cast(ID as varchar(15)) as [HashKey]
  FROM [TestDatabase].[dbo].[vAdmin1India]

--Select * from @LocationProcessData 

EXEC UpdateDatabaseWithNewBoundariesv2 65, 'India', 'Southern Asia', 'Asia', '2014', 0, @LocationProcessData 

*/
-- =============================================
alter PROCEDURE [dbo].[UpdateDatabaseWithNewBoundariesv2]

	@LocationTypeId smallint,
	@CountryName varchar(250),
	@RegionName varchar(250),
	@ContinentName varchar(250),
	@SuffixToAddToExistingFeatures varchar(20),
	@IsLocationTypeBase bit,
	@AdminBoundariesToProcess dbo.LocationProcessData READONLY

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Declare @CountryLocationId int
	Declare @StartTime as datetime2(7)
	Declare @EndTime as datetime2(7)
	
	Print 'Beginning Location Boundary Update'
	Set @StartTime = SYSDATETIME()
	Print ('Start time is: ' + cast(@StartTime as varchar(19)))
	
	Select @CountryLocationId = LocationId
	From Universe.dbo.Locations
	Where LocationName = @CountryName
	and LocationTypeId = 23
	
	Print 'Starting Section 2A: Update existing boundaries and insert new boundaries'
	
	EXECUTE [2aUpdateExistingAndInsertNewAdminBoundaries] 
	   @LocationTypeId
	  ,@CountryName
	  ,@SuffixToAddToExistingFeatures
	  ,@AdminBoundariesToProcess 
	
	Print 'Section 2 complete'
	Print 'Starting Section 3A: Update Base Admins'
	
	EXECUTE [3aUpdateBaseFeaturesAfterBoundaryUpdate] 
	   @LocationTypeId
	  ,@CountryName
	  ,@RegionName
	  ,@ContinentName
	  ,@SuffixToAddToExistingFeatures
	  ,@IsLocationTypeBase
	
	Print 'Section 3 complete'
	Print 'Starting Section 4: Update Geohashes'
	
	EXECUTE [4UpdateGeohashes_LoopBaseFeatures] 
	   @CountryLocationId
	 
	Print 'Section 4 complete'
	Print ('Updated completed at: ' + cast(@EndTime as varchar(19)))

END
