USE [TestDatabase]
GO
/****** Object:  StoredProcedure [dbo].[2bUpdateOtherDatabasesForLocations]    Script Date: 2/6/2015 11:42:16 ******/
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Molly O'Dwyer
-- Create date: 2/6/2015
-- Update date: 2/17/15
-- Description:	Update the other databases with new location information from Universe
-- =============================================

/*
EXEC [2bUpdateOtherDatabasesForLocations] 'Ethiopia', 68, '2014'  

--Double check update was successful
Select LocationName
From ICRISAT_StarCluster_Data.dbo.Locations
Where LocationTypeId = 68
and CHARINDEX(', Ethiopia', LocationName)>0

*/

Alter PROCEDURE [dbo].[2bUpdateOtherDatabasesForLocations] 
	-- Add the parameters for the stored procedure here
	@CountryName varchar(250),
	@LocationTypeId smallint
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	

   /* Update all other Clusters from Universe */
	--take results of the following select and make that the table to look through
	Declare @tableName varchar(100)
	Declare @U varchar(2000)
	Declare @strDatabase varchar(100)
	
	Declare @DatabaseNames Table (RunId int identity(1,1), DatabaseName varchar(250))
	
	Print 'Updating locations in other databases'
	
	Print 'Getting list of databases'
	
	--Get list of databases
	INSERT INTO @DatabaseNames 
	(DatabaseName)
	Select name 
	from sys.databases 
	where charindex('StarCluster',name)>0
	and charindex('Base',name) = 0;
	
	
	Declare @ActiveRunId int, @MaxRunId int
	
	Select @ActiveRunId = MIN(RunId), @MaxRunId = MAX(RunId)
	From @DatabaseNames 
	
	Print 'Updating databases'
	
	--Loop through database list and update from Universe
	While @ActiveRunId <= @MaxRunId 
	BEGIN
			Select @strDatabase = [DatabaseName]
			From @DatabaseNames 
			Where RunId = @ActiveRunId 
			
			Set @tableName = @strDatabase + '.dbo.Locations'
			
			--Update locations
			Set @U = 'Update ' + @tableName + CHAR(13)
				+ 'Set LocationName = U.LocationName' + CHAR(13)
				+ 'From ' + @tableName + ' L,' + CHAR(13)
				+ 'Universe.dbo.Locations U' + CHAR(13)
				+ 'Where U.LocationTypeId = ' + cast(@LocationTypeId as varchar(10)) + CHAR(13)
				+ 'and U.LocationId = L.LocationId' + CHAR(13)
				+ ' and Charindex(' + char(39) + ', '+@CountryName + char(39) + ',L.LocationName,0) > 0'  + char(13) 
				+ ' and Charindex(' + char(39) + ', '+@CountryName + ' (' + char(39) + ',L.LocationName,0) = 0'  + char(13) 
				
				print (@U)
				

			Exec (@U)
		
			Print 'Updated locations for ' + @tableName
		
			set @ActiveRunId= @ActiveRunId + 1
	END

END
GO
