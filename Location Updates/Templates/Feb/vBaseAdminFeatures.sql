Create view vBaseAdminFeatures 
as
Select A0.LocationName as Admin0Name, A1.LocationName as Admin1Name, A2.LocationName as Admin2Name, B.*
From Universe.dbo.BaseAdminFeatures B
inner join
Universe.dbo.Locations A0
on B.Admin0LocationId = A0.LocationId
inner join
Universe.dbo.Locations A1
on B.Admin1LocationId =A1.LocationId
inner join
Universe.dbo.Locations A2
on B.Admin2LocationId = A2.LocationId

