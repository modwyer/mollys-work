USE [TestDatabase]
GO
/****** Object:  StoredProcedure [dbo].[3bDeleteAndInsertToBaseAdmin]    Script Date: 2/6/2015 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Molly O'Dwyer
-- Create date: 2/6/2015
-- Description:	Delete the base admin for a specific country and insert the new locations to BaseAdminFeatures table
/*
Select *
FROM [Universe].[dbo].[BaseAdminFeatures]
Where Admin0LocationId = -45303
*/


/*
EXEC dbo.[3bDeleteAndInsertToBaseAdmin] 68, 'Ethiopia', 'Eastern Africa', 'Africa', -45303, -4842, -46118
*/
-- =============================================

Alter PROCEDURE [dbo].[3bDeleteAndInsertToBaseAdmin]
	-- Add the parameters for the stored procedure here
	@LocationTypeId smallint,
	@CountryName varchar(250),
	@RegionName varchar(250),
	@ContinentName varchar(250),
	@CountryLocationId int,
	@RegionLocationId int,
	@ContinentLocationId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Print 'Deleting base features for ' + @CountryName

	DELETE      
	FROM [Universe].[dbo].[BaseAdminFeatures]
	Where Admin0LocationId = @CountryLocationId	

	Print 'Base features deleted'

	--If processing Admin 2
	if @LocationTypeId = 68
	BEGIN

		Print 'Processing Admin 2 locations'

		--Only do this if it is admin level 2
		--We need to get the Admin1LocationIds above our newly added Admin2Locations so we need to do some spatial processing using point and polygon analysis
		Declare @Relationship2To1 Table (Admin2LocationId int, Admin1LocationId int)

		--Insert admin 2 and 1 IDs where Admin2 Centroid is within the Admin 1 polygon
		Insert into @Relationship2To1
		(Admin2LocationId,Admin1LocationId)
		Select NewAdmin2.LocationId as Admin2LocationId, Admin1.LocationId as Admin1LocationId
		From  
			(
			Select LocationId, CentroidFeature
			From Universe.dbo.Locations
			Where LocationTypeId = 68 --admin2
			and Charindex(', '+@CountryName+'',LocationName,0) > 0				
			and Charindex(', '+@CountryName+ ' (',LocationName,0) = 0
			and HashKey like (@CountryName+'-%')
			) NewAdmin2,
			(  
			Select LocationId, LocationFeature
			From Universe.dbo.Locations
			Where LocationTypeId = 65 --admin1
			and Charindex(', '+@CountryName+'',LocationName,0) > 0				
			and Charindex(', '+@CountryName+ ' (',LocationName,0) = 0
			and Charindex(', '+@CountryName+ '(',LocationName,0) = 0
			) Admin1
		Where NewAdmin2.CentroidFeature.STWithin(Admin1.LocationFeature) = 1 

		Print 'Point locations added to relationship table'
		
		--Insert admin2 and 1 IDs where Admin 2 polygon intersects the Admin 1 polygon and Admin2 ID is not already in the table
		Insert into @Relationship2To1
		(Admin2LocationId,Admin1LocationId)
		Select NewAdmin2.LocationId as Admin2LocationId, Admin1.LocationId as Admin1LocationId
		From  
			(
			Select LocationId, LocationFeature
			From Universe.dbo.Locations
			Where LocationTypeId = 68 --admin2
			and Charindex(', '+@CountryName+'',LocationName,0) > 0				
			and Charindex(', '+@CountryName+ ' (',LocationName,0) = 0
			and HashKey like (@CountryName+'-%') 
			) NewAdmin2,
			(  
			Select LocationId, LocationFeature, LocationName 
			From Universe.dbo.Locations
			Where LocationTypeId = 65 --admin1
			and Charindex(', '+@CountryName+'',LocationName,0) > 0				
			and Charindex(', '+@CountryName+ ' (',LocationName,0) = 0
			and Charindex(', '+@CountryName+ '(',LocationName,0) = 0
			) Admin1
		Where NewAdmin2.LocationFeature.STIntersects(Admin1.LocationFeature) = 1
		and NewAdmin2.LocationId not in (Select Admin2LocationId from @Relationship2To1)

		Print 'Polygon locations added to relationship table'
		Print 'Inserting Admin 2 IDs into base features'
		
		--Insert Admin 2 IDs from above table and Locations table into the BaseAdminFeatures table as the base admin level
		INSERT INTO [Universe].[dbo].[BaseAdminFeatures]
				   ([BaseAdminLocationId]
				   ,[BaseFeature]
				   ,[BaseAdminLevelId]
				   ,[Admin0LocationId]
				   ,[Admin1LocationId]
				   ,[Admin2LocationId]
				   ,[ContinentLocationId]
				   ,[WorldRegionLocationId])
		SELECT L.[LocationId]
			  ,L.[LocationFeature]
			  ,2 as BaseAdminLevelId
			  ,@CountryLocationId as Admin0LocationId
			  ,R.Admin1LocationId
			  ,R.Admin2LocationId
			  ,@ContinentLocationId as [ContinentLocationId]
			  ,@RegionLocationId as [WorldRegionLocationId]
		  FROM [Universe].[dbo].[Locations] L, 
		  @Relationship2To1 R
		Where LocationTypeId = 68 --Admin2
		and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-' 																				
		and R.Admin2LocationId = L.LocationId

	END
		
	--If processing Admin 1	
	If @LocationTypeId = 65
	BEGIN

		Print 'Processing Admin 1 locations'

		INSERT INTO [Universe].[dbo].[BaseAdminFeatures]
				   ([BaseAdminLocationId]
				   ,[BaseFeature]
				   ,[BaseAdminLevelId]
				   ,[Admin0LocationId]
				   ,[Admin1LocationId]
				   ,[ContinentLocationId]
				   ,[WorldRegionLocationId])
		Select 
			LocationId as [BaseAdminLocationId]
			,LocationFeature as [BaseFeature]
			,1 as BaseAdminLevelId
			,@CountryLocationId as Admin0LocationId
			,LocationId
			,@ContinentLocationId as [ContinentLocationId]
			,@RegionLocationId as [WorldRegionLocationId]				
		From Universe.dbo.Locations
		Where LocationTypeId = 65 --admin1
		and Charindex(', '+@CountryName+'',LocationName,0) > 0				
		and Charindex(', '+@CountryName+ ' (',LocationName,0) = 0					   
	END
End
