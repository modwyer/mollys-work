USE [TestDatabase]
GO

/****** Object:  StoredProcedure [dbo].[2aUpdateExistingAndInsertNewAdminBoundaries]    Script Date: 5/21/2015 4:13:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Dan Allen with Molly
-- Create date: 12/18/2014
-- Update date: 2/17/15
-- Description:	Update the existing location features for the admin type requested and then insert the new boundaries into universe

/*
Example:
SET NOCOUNT ON;

Declare @LocationProcessData LocationProcessData

INSERT INTO @LocationProcessData
(LocationName, LocationTypeId, LocationFeature, CentroidFeature, PresentationLocationName, Hashkey)
SELECT top 1
	LocationName as [LocationName]
	,LocationTypeId as [LocationTypeId]
	,LocationFeature as [LocationFeature]
	,Centroid as [Centroid]
	,PresentationLocationName as [PresentationLocationName]
	,'Uganda-' + cast(ID as varchar(15)) as [HashKey]
  FROM [TestDatabase].[dbo].[vAdmin2Uganda]

--Select * from @LocationProcessData 

EXEC 2aUpdateExistingAndInsertNewAdminBoundaries_v3 68, 'Uganda', '2015', @LocationProcessData 

Select *
From Universe.dbo.Locations
Where LocationName like '%Uganda%'
and LocationTypeId = 68
*/

-- =============================================
CREATE PROCEDURE [dbo].[2aUpdateExistingAndInsertNewAdminBoundaries_v3]
	-- Add the parameters for the stored procedure here
	@LocationTypeId smallint,
	@CountryName varchar(250),
	@SuffixToAddToExistingFeatures varchar(20),
	@AdminBoundariesToProcess dbo.LocationProcessData READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	BEGIN TRANSACTION;

	Declare @strSQL varchar(2000)
	
	Select *
	From Universe..Locations
	Where LocationTypeId = @LocationTypeId
	and Charindex(', '+@CountryName+ ' (' +@PriorYear +')',LocationName,0) > 0
	and HashKey is not Null

	--Print 'Setting old hashkeys to Null'
	
	--Set hashkey to null
	--Update [Universe].[dbo].[Locations]
	--Set HashKey = Null
	--Where LocationTypeId = @LocationTypeId
	--and Charindex(', '+@CountryName+ ' (' +@PriorYear +')',LocationName,0) > 0
	--and HashKey is not Null
	
	Print 'Adding suffix to old location'
	
	--Add suffix to old locations
	Update [Universe].[dbo].[Locations]
		Set LocationName = LocationName + ' ('+@SuffixToAddToExistingFeatures+')'
		Where LocationTypeId = @LocationTypeId
		and Charindex(', '+@CountryName+'',LocationName,0) > 0
		and Charindex(', '+@CountryName+ ' (',LocationName,0) = 0
	 --and LocationName + ' ('+@SuffixToAddToExistingFeatures+')' 
		--not in (Select LocationName 
		--		From Universe.dbo.Locations 
		--		Where LocationTypeId = @LocationTypeId)	
	--the above commented out section is not needed now there is a check for existing suffixes in the previous proc
		
Print 'Adding new boundaries to Locations table'
	
	--Insert the new boundaries into the Locations table
	INSERT INTO [Universe].[dbo].[Locations]
			   ([LocationName]
			   ,[LocationTypeId]
			   ,[LocationFeature]
			   ,[CentroidFeature]
			   ,[PresentationLocationName]
			   ,HashKey)
	Select [LocationName]
			   ,[LocationTypeId]
			   ,[LocationFeature]
			   ,[CentroidFeature]
			   ,[PresentationLocationName]
			   ,HashKey
	From @AdminBoundariesToProcess	
	WHERE LocationName not in 
		(Select LocationName 
		From Universe.dbo.Locations 
		Where LocationTypeId = @LocationTypeId)
	
	Print 'Starting Section 2B:Update other databases with new locations'
	--Update other databases with new boundaries
	Execute [2bUpdateOtherDatabasesForLocations]
		@CountryName,
		@LocationTypeId
		
	Print 'Section 2B complete'
	Commit Transaction;	

END


GO


