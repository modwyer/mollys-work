Declare @CountryName varchar(40) = 'India'
Declare @CountryLocationId int = -45498
Declare @LocationTypeId int = 65
Declare @PrintStr varchar(10)
Declare @strSQL varchar(2000)
Declare @CountryShape geometry
Declare @Env Geometry
Declare @MinLong as int
Declare @MaxLong as int
Declare @MinLat as int
Declare @MaxLat as int

--create tables: #CountrySpecificGeohashes, #NeedToBufferPoints, #AreaThatIntersectsBuffer, #UpdateForGeohashAdmin
Create Table #CountrySpecificGeohashes (RunId int identity(1,1), Geohash6 char(6), GeoShape geometry, 
	Location0 int, Location1 int, Location2 int, primary key (Geohash6));
Create Table #NeedToBufferPoints (RunId int identity(1,1), Geohash6 char(6), Shape Geometry, primary key (RunId));
Create Table #AreaThatIntersectsBuffer (Geohash6 char(6), AdminLocationId int, Area float);
Create Table #UpdateForGeohashAdmin (Geohash6 char(6), AdminLocationId int);
--Buffer the geohash point so that we can intersect it with features (since the point was not within a feature)

--Get country parameter
--Get country shape
--Get country envelope
Select @Env = LocationFeature.STEnvelope(), @CountryShape = LocationFeature
From Universe.dbo.Locations
Where LocationId = @CountryLocationId

print 'Getting bounding box for country'

--Get the bounding box around the country LocationId that was provided as parameter
Select @MinLong = floor(Min(Longitude)), @MaxLong = ceiling(Max(longitude)), 
	@MinLat = floor(Min(Latitude)), @MaxLat = ceiling(Max(Latitude))
	From 
		(
		Select @Env.STPointN(1).STX as Longitude, @Env.STPointN(1).STY as Latitude
		UNION ALL
		Select @Env.STPointN(2).STX as Longitude, @Env.STPointN(2).STY as Latitude
		UNION ALL
		Select @Env.STPointN(3).STX as Longitude, @Env.STPointN(3).STY as Latitude
		UNION ALL
		Select @Env.STPointN(4).STX as Longitude, @Env.STPointN(4).STY as Latitude
		) i

	print ('Max Lat: ' + cast(@MaxLat as varchar(50)))
	print ('Min Lat: ' + cast(@MinLat as varchar(50)))
	print ('Max Long: ' + cast(@MaxLong as varchar(50)))
	print ('Min Long: ' + cast(@MinLong as varchar(50)))

print 'Inserting geohashes within the bounds into #CountrySpecificGeohashes'
--Get all geohashes within that envelope (or intersect)
--insert those selected geohashes into the temp table	

--create #CountrySpecificGeohashes spatial index
set @strSQL = 
	'CREATE SPATIAL INDEX #SPX_CountrySpecificGeohashes ON #CountrySpecificGeohashes
	([geoshape])
	USING  GEOMETRY_GRID 
	WITH (
	bounding_box = ('+ cast(@MinLong as varchar(4)) + ', ' +  cast(@MinLat as varchar(4)) + ', ' +  cast(@MaxLong as varchar(4)) + ', ' + cast(@MaxLat as varchar(4)) + '), '
	+ ' GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
	CELLS_PER_OBJECT = 16);'
	
exec(@strSQL)
print ('Spatial Index #SPX_CountrySpecificGeohashes Script: ' + char(13) + @strSQL)
	
--create #NeedToBufferPoints spatial index
set @strSQL = 
	'CREATE SPATIAL INDEX #SPX_Buffer ON #NeedToBufferPoints
	([Shape])
	USING GEOMETRY_GRID 
	WITH (
	bounding_box = ('+ cast(@MinLong as varchar(4)) + ', ' +  cast(@MinLat as varchar(4)) + ', ' +  cast(@MaxLong as varchar(4)) + ', ' + cast(@MaxLat as varchar(4)) + '), '
	+ ' GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
	CELLS_PER_OBJECT = 16);'

exec(@strSQL)
print ('Spatial Index #SPX_Buffer Script: ' + char(13) + @strSQL)

--insert from geohash to #CountrySpecificGeohashes
Insert into #CountrySpecificGeohashes
	(Geohash6, GeoShape)
	Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326)
	From [Universe].[dbo].[GeoHash6LocationsLookup]
	Where Latitude1km between @MinLat and @MaxLat
	and Longitude1km between @MinLong and @MaxLong
	
--Get geohashes not within the polygons. These will be buffered
--insert from geohashes to #NeedToBufferPoints where location is old location
Insert into #NeedToBufferPoints
(Geohash6, Shape)
	Select g.Geohash6, Geometry::STGeomFromText(g.ShapeWKT,4326).STBuffer(0.5)
	From [Universe].[dbo].[GeoHash6LocationsLookup] g
	inner join
	Universe..Locations l
	on g.admin1locationid = l.locationid
	Where Admin0LocationId = @CountryLocationId 
	and CHARINDEX(' (201',l.locationname)>0
	and (case when @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in
		(Select LocationId
		from Universe.dbo.Locations 
		Where LocationTypeId = @LocationTypeId
		and CHARINDEX('' + @CountryName + '-''',Hashkey)>0)
		--and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-')    chose the line above for simplification
	--186 records for India admin1 on QA

--insert geohashes to #NeedToBufferPoints with country admin0 but null admin1
Insert into #NeedToBufferPoints
(Geohash6, Shape)
	Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326)--.STBuffer(0.5)
	From [Universe].[dbo].[GeoHash6LocationsLookup]
	Where Admin0LocationId = @CountryLocationId 
	and Admin1LocationId is null
	and geohash6 not in (Select Geohash6 from #NeedToBufferPoints)

Select @PrintStr = (Select count(*) from #NeedToBufferPoints)
print ('Count of geohashes not in a feature that will be buffered: ' + @PrintStr)


--Get the intersection of the buffered geohash point with the admin2 polygon feature
Truncate Table 	#AreaThatIntersectsBuffer
--insert from #CountrySpecificGeohashes & #NeedToBufferPoints to #AreaThatIntersectsBuffer
--no spatial index needed
INSERT INTO #AreaThatIntersectsBuffer
(Geohash6, AdminLocationId, Area)
Select C.Geohash6, C.Location2, C.GeoShape.STIntersection(B.Shape).STArea()
From #CountrySpecificGeohashes C, #NeedToBufferPoints B
Where C.GeoShape.STIntersects(B.Shape) = 1

--Get the max area for each of the intersections and then associate
--the geohash with the locationId that has that max
--insert from #AreaThatIntersectsBuffer to #UpdateForGeohashAdmin
INSERT INTO #UpdateForGeohashAdmin
(Geohash6, AdminLocationId)
Select B.Geohash6, B.AdminLocationId
From #AreaThatIntersectsBuffer B,	
	(
	Select Geohash6, Max(Area) as MaxArea
	From #AreaThatIntersectsBuffer
	Group by Geohash6
	) I
Where B.Geohash6 = I.Geohash6
and B.Area = I.MaxArea
order by B.Geohash6, B.area desc

Set @PrintStr = 'Select * from #UpdateForGeohashAdmin'
Exec @PrintStr
--Select @PrintStr = (Select * from #UpdateForGeohashAdmin)
print 'Geohashes in the intersection: ' + @PrintStr

--Update those pesky geohashs that were not contained by the new features
if @LocationTypeId = 65
	BEGIN
		Print 'Working on Admin 1 update'
		Update [Universe].[dbo].[GeoHash6LocationsLookup]
		Set Admin1LocationId = U.AdminLocationId
		From 
			[Universe].[dbo].[GeoHash6LocationsLookup] G,
			#UpdateForGeohashAdmin U
		Where G.Geohash6 = U.Geohash6
			and G.Admin0LocationId = @CountryLocationId
	END	
			
if @LocationTypeId = 68
	BEGIN
		Print 'Working on Admin 2 update'
		Update [Universe].[dbo].[GeoHash6LocationsLookup]
		Set Admin2LocationId = U.AdminLocationId
		From 
			[Universe].[dbo].[GeoHash6LocationsLookup] G,
			#UpdateForGeohashAdmin U
		Where G.Geohash6 = U.Geohash6
			and G.Admin0LocationId = @CountryLocationId
	END

--check your work
Select @PrintStr = (Select count(*)
		From Universe.dbo.GeoHash6LocationsLookup
		Where Admin0LocationId = @CountryLocationId 
		and (case when @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in 
			(Select LocationId 
			from Universe.dbo.Locations 
			Where LocationTypeId = 68 
			and CHARINDEX('' + @CountryName + '-''',Hashkey)>0)
			
print ('Remaining geohashes: ' + @PrintStr)

Select @PrintStr = (Select count(*)
		From Universe.dbo.GeoHash6LocationsLookup
		Where Admin0LocationId = @CountryLocationId 
		and Admin1LocationId is null)

Print ('Remaining geohashes with Admin 1 nulls: ' + @PrintStr)



--Extend it one more time to see if we can capture them
Declare @CountOfRemaining int = 1
Declare @ActiveIteration int = 1, @MaxIterations int = 5
Declare @BufferSize float = 0.1
       
Select @CountOfRemaining = count(*)
From [Universe].[dbo].[GeoHash6LocationsLookup]
Where Admin0LocationId = @CountryLocationId
and (case when @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in        
        (Select LocationId
        from Universe.dbo.Locations
        Where LocationTypeId = @LocationTypeId
		and CHARINDEX('' + @CountryName + '-''',Hashkey)>0)
        --and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-')                      
       
print ('Count of Remaining geohashes: ' + cast(@CountOfRemaining as varchar(100)))
       
While @CountOfRemaining > 0 AND @ActiveIteration < @MaxIterations
	BEGIN	
        Select @CountOfRemaining = count(*)
        From [Universe].[dbo].[GeoHash6LocationsLookup]
        Where Admin0LocationId = @CountryLocationId
        and (case when @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in         
            (Select LocationId
            from Universe.dbo.Locations
            Where LocationTypeId = @LocationTypeId
			and CHARINDEX('' + @CountryName + '-''',Hashkey)>0)
            --and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-')          
               
        print ('Geohashes now remaining: ' + cast(@CountOfRemaining as varchar(100)))
                   
        INSERT INTO #AreaThatIntersectsBuffer
        Select C.Geohash6, C.Location2, C.GeoShape.STIntersection(B.Shape.STBuffer(@BufferSize)).STArea()
        From #CountrySpecificGeohashes C, #NeedToBufferPoints B
        Where C.GeoShape.STIntersects(B.Shape) = 0
        and C.GeoShape.STIntersects(B.Shape.STBuffer(@BufferSize)) = 1
        and C.Geohash6 not in (Select Distinct Geohash6 From #AreaThatIntersectsBuffer Where Area > 0)
             
        print ('Current Buffer size: ' + cast(@BufferSize as varchar(100)))
        print ('Active Iteration: ' + cast(@ActiveIteration as varchar(100)))
             
	--Get the max area for each of the intersections and then associate
	--the geohash with the locationId that has that max
	--insert from #AreaThatIntersectsBuffer to #UpdateForGeohashAdmin
	INSERT INTO #UpdateForGeohashAdmin
	(Geohash6, AdminLocationId)
	Select B.Geohash6, B.AdminLocationId
	From #AreaThatIntersectsBuffer B,	
		(
		Select Geohash6, Max(Area) as MaxArea
		From #AreaThatIntersectsBuffer
		Group by Geohash6
		) I
	Where B.Geohash6 = I.Geohash6
	and B.Area = I.MaxArea
	order by B.Geohash6, B.area desc

	Set @PrintStr = 'Select * from #UpdateForGeohashAdmin'
	Exec @PrintStr
	--Select @PrintStr = (Select * from #UpdateForGeohashAdmin)
	print 'Geohashes in the intersection: ' + @PrintStr

	--Update those pesky geohashs that were not contained by the new features
	if @LocationTypeId = 65
		BEGIN
			Update [Universe].[dbo].[GeoHash6LocationsLookup]
			Set Admin1LocationId = U.AdminLocationId
			From 
				[Universe].[dbo].[GeoHash6LocationsLookup] G,
				#UpdateForGeohashAdmin U
			Where G.Geohash6 = U.Geohash6
				and G.Admin0LocationId = @CountryLocationId
		END	
			
	if @LocationTypeId = 68
	BEGIN
		Update [Universe].[dbo].[GeoHash6LocationsLookup]
		Set Admin2LocationId = U.AdminLocationId
		From 
			[Universe].[dbo].[GeoHash6LocationsLookup] G,
			#UpdateForGeohashAdmin U
		Where G.Geohash6 = U.Geohash6
			and G.Admin0LocationId = @CountryLocationId
	END
		
	Select @PrintStr = (Select count(*)
			From Universe.dbo.GeoHash6LocationsLookup
			Where Admin0LocationId = @CountryLocationId 
			and (case when @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in 
				(Select LocationId 
				from Universe.dbo.Locations 
				Where LocationTypeId = 68 
				and CHARINDEX('' + @CountryName + '-''',Hashkey)>0)
				--and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-')) 
	print ('Remaining geohashes: ' + @PrintStr)
             
			set @BufferSize = @BufferSize + .5
			set @ActiveIteration = @ActiveIteration + 1
	END  