Declare @Env Geometry
Declare @CompareShape geometry
Declare @ActivePointShape geometry
Declare @PointNum int
Declare @MinPoint int, @MaxPoint int
Declare @LocationName varchar(50)

--get envelope
Select @Env = LocationFeature.STEnvelope()
	From Universe..Locations
	Where LocationId = -45373

--drop table #BFNeighborShapes
Create table #BFNeighborShapes (Shape geometry, Locationname varchar(50))
Insert into #BFNeighborShapes (Shape, Locationname)
	Select locationfeature, LocationName
	From Universe..Locations 
	Where locationtypeid = 68
	and charindex(', Burkina Faso',locationname)=0
	and locationfeature.STIntersects(@Env)=1

--get aggregate geometry of all neighboring polygons within the envelope
Select @CompareShape = awmDatabase.dbo.UnionAggregate(Shape)
From #BFNeighborShapes

Select @CompareShape

Set @PointNum = 1
Select @MaxPoint = max(Shape.STNumPoints())
From TestDatabase..BurkinaFasoAdmin2

Select @ActivePointShape = Shape.STPointN(@PointNum)
From TestDatabase..BurkinaFasoAdmin2

Print ('Max points for selected country: ' + cast(@MaxPoint as varchar(10)))
Print ('Active point is: ' + cast(@PointNum as varchar(10)))

--loop through points in country polygons and see if they are within the big neighbor polygon
While @PointNum <= @MaxPoint
	Begin

		If @ActivePointShape.STWithin(@CompareShape) = 1
			Begin
				Print ('Oops! This point is in a neighboring country! Point: ' + cast(@PointNum as varchar(10)))
				
				--find out which polygon that point is part of
				Select @LocationName = Locationname
				From #BFNeighborShapes
				Where @ActivePointShape.STWithin(Shape) = 1

				Print ('Neighboring polygon is ' + @LocationName)

				Select @LocationName = admin2Name + ', ' + admin1Name
				From Testdatabase..BurkinaFasoAdmin2
				Where @ActivePointShape.STTouches(Shape)=1

				Print ('Admin1 polygon: ' + @LocationName)
			End
		
		--increment point number
		Set @PointNum = @PointNum + 1

		--find new active point
		Select @ActivePointShape = Shape.STPointN(@PointNum)
		From TestDatabase..BurkinaFasoAdmin2


	End


