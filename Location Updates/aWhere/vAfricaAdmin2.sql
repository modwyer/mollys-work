USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAfricaAdmin2]    Script Date: 7/1/2015 11:23:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


alter view [dbo].[vAfricaAdmin2]
as
	(Select *
	From Universe..locations l, testdatabase..vAfricaContinent a
	where locationtypeid = 68
	and l.locationfeature.STIntersects(a.Continent) = 1
	and charindex('(',locationname)=0
	and l.LocationFeature.STGeometryType() <> 'GeometryCollection'
	)

GO


