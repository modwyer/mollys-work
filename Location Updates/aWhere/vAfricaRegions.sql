USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAfricaRegions]    Script Date: 7/1/2015 11:23:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


alter view [dbo].[vAfricaRegions]
as
	(Select *
	From Universe..locations l, testdatabase..vAfricaContinent a
	where locationtypeid = 25
	and l.locationfeature.STIntersects(a.Continent) = 1
	and l.LocationFeature.STGeometryType() <> 'GeometryCollection'
	)
	
	 


GO


