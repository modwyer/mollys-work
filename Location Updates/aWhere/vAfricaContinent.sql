USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAfricaContinent]    Script Date: 7/1/2015 11:23:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


alter view [dbo].[vAfricaContinent]
as
	(select LocationFeature as Continent
	from universe..locations
	where locationtypeid = 26
	and CHARINDEX('Africa',locationname)>0
	and LocationFeature.STGeometryType() <> 'GeometryCollection')

GO


