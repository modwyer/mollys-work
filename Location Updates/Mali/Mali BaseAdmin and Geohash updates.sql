/****************** MALI UPDATES *****************************/
--Run: Delete excess BaseAdmin features for Mali
DELETE      
FROM [Universe].[dbo].[BaseAdminFeatures]
Where Admin0LocationId = -45370
and BaseAdminLevelId <4
--348 rows affected

--View: Check the change looks good in spatial view and table
Select *
FROM [Universe].[dbo].[BaseAdminFeatures]
Where Admin0LocationId = -45370

--Run: Update geohashes for Mali
 Exec [4UpdateGeohashes_LoopBaseFeaturesFaster] -45370