--Admin 1
Declare @LocationProcessData LocationProcessData

INSERT INTO @LocationProcessData
(LocationName, LocationTypeId, LocationFeature, CentroidFeature, PresentationLocationName, Hashkey)
SELECT
	Admin1 + ', Zambia' as [LocationName]
	,LocationTypeId as [LocationTypeId]
	,Shape as [LocationFeature]
	,Centroid as [Centroid]
	,Admin1 + ', Zambia' as [PresentationLocationName]
	,'Zambia-' + cast(ID as varchar(10)) as [HashKey]
  FROM [TestDatabase].[dbo].[vAdmin1Zambia]

--Select * from @LocationProcessData 

EXEC UpdateDatabaseWithNewBoundaries_v3 65, 'Zambia', 'Eastern Africa', 'Africa', '2015', @LocationProcessData, 7 --length of country name + dash (-)

--Admin 2
Declare @LocationProcessData LocationProcessData

INSERT INTO @LocationProcessData
(LocationName, LocationTypeId, LocationFeature, CentroidFeature, PresentationLocationName, Hashkey)
SELECT
	Admin2 +', ' + Admin1 + ', Zambia' as [LocationName]
	,LocationTypeId as [LocationTypeId]
	,Shape as [LocationFeature]
	,Centroid as [Centroid]
	,Admin2 +', ' + Admin1 + ', Zambia' as [PresentationLocationName]
	,'Zambia-' + cast(ID as varchar(10)) as [HashKey]
  FROM [TestDatabase].[dbo].[vAdmin2Zambia]

--Select * from @LocationProcessData 

EXEC UpdateDatabaseWithNewBoundaries_v3 68, 'Zambia', 'Eastern Africa', 'Africa', '2015', @LocationProcessData, 7