USE [TestDatabase]
GO

/****** Object:  View [dbo].[vZambiaSurroundingCountryShape]    Script Date: 05/04/2015 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




alter view [dbo].[vZambiaSurroundingCountryShape]
as
	(Select LocationId,LocationName,LocationTypeId, LocationFeature, Hashkey
	from Universe.dbo.Locations
	Where LocationTypeId = 68
		and (CHARINDEX(', Namibia', LocationName)>0
			or charindex(', Angola',LocationName)>0
			or CHARINDEX(', Zimbabwe',LocationName)>0
			or CHARINDEX(', Mozambique',LocationName)>0
			or CHARINDEX(', Malawi',LocationName)>0
			or CHARINDEX(', Tanzania',LocationName)>0
			or CHARINDEX(', Democratic Republic of the Congo',LocationName)>0
			or CHARINDEX(', Botswana',LocationName)>0)
		and charindex(', Tanzania (',LocationName)=0
		and charindex(', Democratic Republic of the Congo (',LocationName)=0
	)


GO


