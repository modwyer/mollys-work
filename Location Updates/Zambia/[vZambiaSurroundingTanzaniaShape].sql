USE [TestDatabase]
GO

/****** Object:  View [dbo].[vZambiaSurroundingCountryShape]    Script Date: 05/04/2015 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




create view [dbo].[vZambiaSurroundingTanzaniaShape]
as
	(Select LocationId,LocationName,LocationTypeId, LocationFeature, Hashkey
	from Universe.dbo.Locations
	Where LocationTypeId = 68
		and (CHARINDEX(', Tanzania',LocationName)>0)
		and charindex(', Tanzania (',LocationName)=0
	)


GO


