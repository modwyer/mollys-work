/*** Troubleshoot missing Zambia polygon, Chavuma ***/
 --Loaded 100 points in Chavuma
 --Behavoir in the platform: table and charts show data but map has no polygon at Admin 2. Admin 1 polygon shows up.
 --Correct hierarchy: Admin 2: 91788981; Admin 1: 91787015; Admin 0: -45281


--Star exists? YES
	select *
	From WorldBank_Starcluster_data..STars -- StarID: 8534
	order by starname

--Dataset exists on the star? YES
	Select *
	From WorldBank_Starcluster_data..Datasets
	where starid = 8534 --Dataset ID: 39645

--Is the polygon valid geometry? YES
	Select *
	From Universe..Locations
	Where Locationid = 92183803 --chavuma
	and Locationfeature.STIsValid() = 0

--Is the polygon valid geography? YES
Declare @Geom Geometry

Select @Geom = LocationFeature
	From Universe..Locations
	Where Locationid = 92183803 

SELECT [awmDatabase].[dbo].[IsValidGeographyFromGeometry] (@Geom)

/** BASE FEATURES TABLE **/
--Is the BaseAdminFeatures table hierarchy correct? YES
	Select *
	From Universe..BaseAdminFeatures
	Where Admin2locationid = 92183803 --chavuma
	--91787015 northwestern (admin 1); -45281 Zambia

--Is the old location in the base admin table? NO
	Select *
	From Universe..BaseAdminFeatures
	Where Admin2locationid = 90298068 --Chavuma (2015)

/** GEOHASH TABLE **/
--Is the geohash table hierarchy correct? YES
	Select * 
	From Universe..Geohash6locationslookup
	Where admin2locationid = 92183803 --91788981 --Chavuma 2799 rows
	--then add this line to see if any are not pointing to the correct admin 1
	and Admin1locationid <> 91787015 --North-Western (admin 1)

--Does the old location show up in the geohash table? NO
	Select * 
	From Universe..Geohash6locationslookup
	Where admin2locationid = 90298068 --Chavuma (2015)
-- Does the old Admin 1 location show up in the geohash table? NO
	Select *
	From Universe..Geohash6locationslookup
	Where admin1locationid = 90006459

/** LOCATIONRELATIONSHIPS TABLE **/
--Doest the new location show up in the LocationRelationships table as a relatedlocation? YES
	Select *
	From WorldBank_Starcluster_Data..LocationRelationships
	Where Relatedlocationid = 92183803 --chavuma (1 row)
	--with relatedlocationtype 68; 
--Doest the old location show up in the LocationRelationships table as a relatedlocation? YES
	Select *
	From WorldBank_Starcluster_Data..LocationRelationships
	Where Relatedlocationid = 90298068 --Chavuma (2015) (1 row)
--both old and new have a parent id the same (the new admin 1), 91787015 North-western
92183797	North-Western, Zambia
90006459	North-Western, Zambia (2015)
--Doest the new location show up in the LocationRelationships table as a location? YES
	Select *
	From WorldBank_Starcluster_Data..LocationRelationships
	Where locationid = 92183803 --chavuma (430 rows)
	--with locationtype 68 & relatedlocationtype 1
--What locations is it the parent of? Points loaded today and some loaded the day before
	Select lr.*, l.locationname
	From WorldBank_Starcluster_Data..LocationRelationships lr
	inner join
	Universe..Locations l
	on lr.relatedlocationid = l.locationid
	Where lr.locationid = 92183803 --chavuma
--Doest the related locations have a relation other than with type 68? YES 
	Select *
	From WorldBank_Starcluster_Data..LocationRelationships
	Where locationid not in (-45281, -4842, -46118, 91787015, 91788981) --(correct country, continent, region, admin 1, admin 2)
	and relatedlocationid in
		(Select distinct relatedlocationid
		From WorldBank_Starcluster_Data..LocationRelationships
		Where locationid = 91788981 --chavuma (220 rows)
		) --0 rows

--Doest the old location show up in the LocationRelationships table as a relatedlocation? YES
	Select *
	From WorldBank_Starcluster_Data..LocationRelationships
	Where Relatedlocationid = 90298068 --Chavuma (2015)
--Doest the old location show up in the LocationRelationships table as a location? YES
	Select *
	From WorldBank_Starcluster_Data..LocationRelationships
	Where locationid = 90298068 --chavuma (2015) (7 rows)
	--with locationtype 68 & relatedlocationtype 1
--What locations is it the parent of? Points loaded prior to today (very first test load)
	Select lr.*, l.locationname
	From WorldBank_Starcluster_Data..LocationRelationships lr
	inner join
	Universe..Locations l
	on lr.relatedlocationid = l.locationid
	Where lr.locationid = 90298068 --chavuma (2015)
--Doest the old related locations have a relation other than with type 68? YES 
	Select *
	From WorldBank_Starcluster_Data..LocationRelationships
	Where --locationid not in (-45281, -4842, -46118, 91787015, 91788981) --(correct country, continent, region, admin 1, admin 2)
	relatedlocationid in
		(Select distinct relatedlocationid
		From WorldBank_Starcluster_Data..LocationRelationships
		Where locationid = 90298068 --chavuma (2015)  (220 rows)
		) --7 rows

--Do all geohashes within the location polygon point to the correct admin 0, 1, and 2? YES
Select geohash6, Latitude1km, Longitude1km, admin0locationid, admin1locationid, admin2locationid
From Universe..geohash6locationslookup, Locations
where geometry::STGeomFromText(ShapeWKT, 4326).STWithin(locationfeature) = 1
and locationid = 92183803