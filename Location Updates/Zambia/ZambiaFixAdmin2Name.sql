Declare @LocID int

--Update Universe
--find the id for Shibuyunji
select @LocID = Locationid
from universe..locations
where locationtypeid = 68
and locationname like 'Shibuyunji, Central, Zambia'

--91789067
--update Shibuyunji's admin 1 name to lusaka instead of central
Update Universe..locations
Set Locationname = 'Shibuyunji, Lusaka, Zambia'
Where Locationid = @LocID

--Update worldBank
Update WorldBank_StarCluster_Data..locations
Set Locationname = 'Shibuyunji, Lusaka, Zambia'
Where Locationid = @LocID

--for checking after update
--select *
--from universe..locations
--where locationtypeid = 68
--and locationname like 'Shibuyunji, %Zambia'