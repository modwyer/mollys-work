USE [TestDatabase]
GO

/****** Object:  View [dbo].[vZambiaAdmin2Shapes]    Script Date: 02/20/2015 10:25:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




alter view [dbo].[vZambiaAdmin2Shapes]
as
	(Select LocationId,LocationName,LocationTypeId, LocationFeature, Hashkey
	from Universe.dbo.Locations
	Where LocationTypeId = 68
	and CHARINDEX(', Zambia', LocationName)>0
	--and CHARINDEX(', Zambia (',LocationName)=0
	)


GO


