 
 --Set Dire Dawa Admin1 to Dire Dawa LocationId
 Update Universe.[dbo].GeoHash6LocationsLookup
  set Admin1LocationId = 
	  (
	  select LocationId
	  From Universe.dbo.Locations
	  Where LocationTypeId = 65
	  and LocationName = 'Dire Dawa, Ethiopia' 
	  )  
  Where Admin2LocationId = 
	  (
	  select LocationId
	  From Universe.dbo.Locations
	  Where LocationTypeId = 68
	  and LocationName = 'Dire Dawa, Dire Dawa, Ethiopia'
	  )
	  and Admin1LocationId <> 
	  (
	  select LocationId
	  From Universe.dbo.Locations
	  Where LocationTypeId = 65
	  and LocationName = 'Dire Dawa, Ethiopia' 
	  )
  
  
  --Set Addis Ababa Admin0 LocationID to Ethiopia
  --Update Universe.[dbo].GeoHash6LocationsLookup
  --set Admin0LocationId = -45303  --Ethiopia
  --Where Admin1LocationId = 90007145  --Addis Ababa, Ethiopia
  --and Admin0LocationId = -45287 --Somalia
  
  
  --Set Addis Ababa Admin0 LocationID to Ethiopia
 Update Universe.[dbo].GeoHash6LocationsLookup
  set Admin0LocationId = 
	  (
	  select LocationId
	  From Universe.dbo.Locations
	  Where LocationTypeId = 23
	  and LocationName = 'Ethiopia' 
	  )  
  Where Admin1LocationId = 
	  (
	  select LocationId
	  From Universe.dbo.Locations
	  Where LocationTypeId = 65
	  and LocationName = 'Addis Ababa, Ethiopia'
	  )
	  and Admin0LocationId = 
	  (
	  select LocationId
	  From Universe.dbo.Locations
	  Where LocationTypeId = 23
	  and LocationName = 'Somalia' 
	  )
    
 
 ----Set Region 14 admin1 to Addis Ababa LocationID
 --Update Universe.[dbo].GeoHash6LocationsLookup
 -- set Admin1LocationId = 90007145  --Addis Ababa, Ethiopia
 -- Where Admin2LocationId = 91565351  --Region 14, Addis Ababa, Ethiopia
 -- and Admin1LocationId = 90007154  --Oromia, Ethiopia
  
 --Set Region 14 admin1 to Addis Ababa LocationID 
 Update Universe.[dbo].GeoHash6LocationsLookup
  set Admin1LocationId = 
	  (
	  select LocationId
	  From Universe.dbo.Locations
	  Where LocationTypeId = 65
	  and LocationName = 'Addis Ababa, Ethiopia' 
	  )  
  Where Admin2LocationId = 
	  (
	  select LocationId
	  From Universe.dbo.Locations
	  Where LocationTypeId = 68
	  and LocationName = 'Region 14, Addis Ababa, Ethiopia'
	  )
	  and Admin1LocationId = 
	  (
	  select LocationId
	  From Universe.dbo.Locations
	  Where LocationTypeId = 65
	  and LocationName = 'Oromia, Ethiopia'
	  )

  
  
 --Set old geohashes to Region 14 
 --zone 5, unknown, Addis Abbaba, Ethiopia
 Update Universe.[dbo].GeoHash6LocationsLookup
 set Admin2LocationId = 
	(
	Select LocationId
	From Universe.dbo.Locations
	Where LocationTypeId = 68
	and LocationName = 'Region 14, Addis Ababa, Ethiopia'
	)
 Where Admin2LocationId = 
	 (
	 Select LocationId
	From Universe.dbo.Locations
	Where LocationTypeId = 68
	and LocationName = 'Unknown, Addis Ababa, Ethiopia (2014)'
	)
	OR Admin2LocationId =
	 (
	 Select LocationId
	From Universe.dbo.Locations
	Where LocationTypeId = 68
	and LocationName = 'Zone 5, Addis Ababa, Ethiopia (2014)'
	)
 
 
----Set Harari admin 2 to Harari admin 1
--  Update Universe.[dbo].GeoHash6LocationsLookup
--  set Admin1LocationId = 90007153  --Harari People, Ethiopia
--  Where Admin2LocationId = 91565328  --Harari, Harari People, Ethiopia
  
  
  
 Update Universe.[dbo].GeoHash6LocationsLookup
  set Admin1LocationId = 
	  (
	  select LocationId
	  From Universe.dbo.Locations
	  Where LocationTypeId = 65
	  and LocationName = 'Harari People, Ethiopia' 
	  )  
  Where Admin2LocationId = 
	  (
	  select LocationId
	  From Universe.dbo.Locations
	  Where LocationTypeId = 68
	  and LocationName = 'Hareri, Harari People, Ethiopia'
	  )
	  and Admin1LocationId <> 
	  (
	  select LocationId
	  From Universe.dbo.Locations
	  Where LocationTypeId = 65
	  and LocationName = 'Harari People, Ethiopia' 
	  )  
	  
 
 --Set Admin 2 LocationIDs that point to Somalia to Ethiopia 
Update Universe.[dbo].GeoHash6LocationsLookup
  set Admin0LocationId = (Select LocationId 
				From universe.dbo.Locations
				Where LocationName = 'Ethiopia' 
				and LocationTypeId = 23)
  Where Admin0LocationId = (Select LocationId 
				From universe.dbo.Locations
				Where LocationName = 'Somalia' 
				and LocationTypeId = 23) 
  and Geohash6 in 
  (Select G.Geohash6
	From Universe.dbo.GeoHash6LocationsLookup G
	INNER JOIN
	Universe.dbo.Locations L
	ON G.Admin2LocationId = L.LocationId
	Where G.Admin0LocationId = (Select LocationId 
				From universe.dbo.Locations
				Where LocationName = 'Somalia' 
				and LocationTypeId = 23)
	and CHARINDEX(', Ethiopia (',L.LocationName)>0)
 
  /*
 Select *
 From Universe.dbo.Locations
 Where LocationTypeId = 65
 and CHARINDEX('Harari People', LocationName)>0
 --and LocationId = 90007154

Select *
 From Universe.dbo.GeoHash6LocationsLookup
 Where Admin0LocationId = -45287 
 and Admin2LocationId is not null

 
 
--get name of admin 1 that admin 2 points to 
Select L.LocationName as Admin1Name, G.*
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin1LocationId = L.LocationId
Where G.Admin2LocationId = 91565328

--get name of admin 2
Select L.LocationName as Admin2Name, G.*
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin2LocationId = L.LocationId
Where G.Admin2LocationId = 91565328

--get name of admin0 that admin 2 points to
Select L.LocationName as Admin0Name, G.*
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin0LocationId = L.LocationId
Where G.Admin2LocationId = 90310722

--get name of admin 1 that points to Admin0 
Select L.LocationName as Admin1Name, G.*
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin1LocationId = L.LocationId
Where G.Admin0LocationId = -45287 --Somalia

--get name of admin 1 that points to Admin0 
Select L.LocationName as Admin2Name, G.*
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin2LocationId = L.LocationId
Where G.Admin0LocationId = -45287
*/