	Declare @CountryLocationId int
	Declare @LocationTypeId int = 68
	Declare @CountryName varchar(100) = 'Ethiopia'
	Declare @RegionName varchar(100) = 'Middle Africa'
	Declare @ContinentName varchar (100) = 'Africa'
	Declare @SuffixToAddToExistingFeatures varchar(50) = '2014'
	Declare @IsLocationTypeBase bit = 1
	 

	Select @CountryLocationId = LocationId
	From Universe.dbo.Locations
	Where LocationName = @CountryName
	and LocationTypeId = 23

EXECUTE TestDatabase.dbo.[UpdateBaseFeaturesAfterBoundaryUpdate] 
	   @LocationTypeId
	  ,@CountryName
	  ,@RegionName
	  ,@ContinentName
	  ,@SuffixToAddToExistingFeatures
	  ,@IsLocationTypeBase
	  
EXECUTE Testdatabase.dbo.[UpdateGeohashes] 
	   @CountryLocationId
	  ,@LocationTypeId
	  ,@CountryName
