/*
Select *
From Universe.dbo.Locations
Where LocationTypeId = 68
and CHARINDEX('West Shewa, Oromia, Ethiopia (',LocationName)>0
--and CHARINDEX('Ethiopia (',LocationName)=0
order by LocationName


--91934684	Kelem Wellega, Oromia, Ethiopia mapping to West Wellega
--91934713	West Wellega, Oromia, Ethiopia showing a polygon covering west and Kelem Wellega

Select L.LocationName as Admin1Name, G.*
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin1LocationId = L.LocationId
Where G.Admin2LocationId = 91934684
order by Admin1Name
*/
--Script 1: set all admin 1 for Kelem Wellega to Oromia (currently pointing to Oromia and Gambela Peoples)
Update Universe.[dbo].GeoHash6LocationsLookup
  set Admin1LocationId = 90007154 --Oromia, Ethiopia
  Select * from Universe.[dbo].GeoHash6LocationsLookup
  Where Admin2LocationId = 91934684 --Kelem Wellega
  and Admin1LocationId = 90007152 --Gambela Peoples, Ethiopia
  
  
--Script 2: set all admin 1 for West Wellega to Oromia (currently pointing to Oromia and Benshangul-Gumaz)
Update Universe.[dbo].GeoHash6LocationsLookup
  set Admin1LocationId = 90007154 --Oromia, Ethiopia
  Where Admin2LocationId = 91934713 --West Wellega
  and Admin1LocationId = 90007150 --Benshangul-Gumaz, Ethiopia
  
--Script 3: fix some goehashes pointing to old West Wellega boundary so the new boundaries will show in the platform
	--first coordinate fix
	Update Universe.[dbo].GeoHash6LocationsLookup
	  set Admin0LocationId = (Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Ethiopia' 
							and LocationTypeId = 23), 
				Admin2LocationId = (
							Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Kelem Wellega, Oromia, Ethiopia' 
							and LocationTypeId = 68
							)
	  Where Admin0LocationId = (Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Somalia' 
							and LocationTypeId = 23)
		  and Admin2LocationID = (Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'West Wellega, Oromia, Ethiopia (2014)' 
							and LocationTypeId = 68) 
	  and Geohash6 in 
	  (Select G.Geohash6
		  From Universe.dbo.GeoHash6LocationsLookup G
		  INNER JOIN
		  Universe.dbo.Locations L
		  ON G.Admin2LocationId = L.LocationId
		  Where G.Admin0LocationId = (Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Somalia' 
							and LocationTypeId = 23)
		  and CHARINDEX(', Ethiopia (',L.LocationName)>0)  
		
	--second coordinate fix
	Update Universe.[dbo].GeoHash6LocationsLookup
	  set       Admin2LocationId = (
							Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'West Wellega, Oromia, Ethiopia' 
							and LocationTypeId = 68
											  )
	  Where Admin2LocationID = (Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Kelem Wellega, Oromia, Ethiopia' 
							and LocationTypeId = 68) 
	  and Geohash6 =  (
				SELECT [Universe].[dbo].[geohash_encode] (
				   9.3224
				   ,35.2976
				  ,6)  
			)

--third coordinate fix
--Set Admin 2 LocationIDs that point to Somalia to Ethiopia 
Update Universe.[dbo].GeoHash6LocationsLookup
  set       Admin2LocationId = (
                        Select LocationId 
                        From universe.dbo.Locations
                        Where LocationName = 'West Wellega, Oromia, Ethiopia' 
                        and LocationTypeId = 68
                                          )
  Where Admin2LocationID = (Select LocationId 
                        From universe.dbo.Locations
                        Where LocationName = 'West Wellega, Oromia, Ethiopia (2014)' 
                        and LocationTypeId = 68) 
  and Geohash6 =  (
            SELECT [Universe].[dbo].[geohash_encode] (
               9.2
               , 35.7333 
              ,6)  
        )

--Script 4: set all admin 1 for Southern to Tigray (current pointing to Tigray, Afar, Amhara)
Update Universe.[dbo].GeoHash6LocationsLookup
  set Admin1LocationId = 90007147 --Tigray, Ethiopia
  Where Admin2LocationId = 91934706 --Southern, Tigray, Ethiopia
  and Admin1LocationId = 90007149 --Amhara, Ethiopia
  or Admin1LocationId = 90007148 --Afar, Ethiopia
  
--Script 5: fix some goehashes pointing to old Western boundary so the new boundaries will show in the platform 
Update Universe.[dbo].GeoHash6LocationsLookup
	  set Admin0LocationId = (Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Ethiopia' 
							and LocationTypeId = 23), 
				Admin2LocationId = (
							Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Western, Tigray, Ethiopia' 
							and LocationTypeId = 68
							)
	  Where Admin0LocationId = (Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Somalia' 
							and LocationTypeId = 23)
		  and Admin2LocationID = (Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Western, Tigray, Ethiopia (2014)' 
							and LocationTypeId = 68) 
	  and Geohash6 in 
	  (Select G.Geohash6
		  From Universe.dbo.GeoHash6LocationsLookup G
		  INNER JOIN
		  Universe.dbo.Locations L
		  ON G.Admin2LocationId = L.LocationId
		  Where G.Admin0LocationId = (Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Somalia' 
							and LocationTypeId = 23)
		  and CHARINDEX(', Ethiopia (',L.LocationName)>0)
		  
--Script 6: fix some goehashes pointing to old Zone 2, Afar boundary so the new boundaries will show in the platform 
Update Universe.[dbo].GeoHash6LocationsLookup
	  set Admin0LocationId = (Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Ethiopia' 
							and LocationTypeId = 23), 
				Admin2LocationId = (
							Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Zone 2, Afar, Ethiopia' 
							and LocationTypeId = 68
							)
	  Where Admin0LocationId = (Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Somalia' 
							and LocationTypeId = 23)
		  and Admin2LocationID = (Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Zone 2, Afar, Ethiopia (2014)' 
							and LocationTypeId = 68) 
	  and Geohash6 in 
	  (Select G.Geohash6, Admin1LocationId, Admin2LocationId
		  From Universe.dbo.GeoHash6LocationsLookup G
		  INNER JOIN
		  Universe.dbo.Locations L
		  ON G.Admin2LocationId = L.LocationId
		  Where G.Admin0LocationId = (Select LocationId 
							From universe.dbo.Locations
							Where LocationName = 'Somalia' 
							and LocationTypeId = 23)
		  and CHARINDEX(', Ethiopia (',L.LocationName)>0)