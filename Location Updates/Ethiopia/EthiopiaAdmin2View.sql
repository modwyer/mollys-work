use TestDatabase;

Create view vAdmin1_2Ethiopia
as 
SELECT 
	Zone_Name as Admin2
	,68 as LocationTypeId
	,[Shape] as Shape
	,[OBJECTID] as Id
	,case when [Shape].MakeValid().STCentroid() is null then [Shape].MakeValid().STEnvelope().STCentroid() else [Shape].MakeValid().STCentroid() end as Centroid
  FROM [TestDatabase].[dbo].[EthiopiaAdmin2Zones21915] 
 
 
 --find all admin 1 names where the admin 2 centroid is within the admin1 locationfeature 

 create view vAdmin2Ethiopia
 as
 Select v.Admin2 + ', ' + l.LocationName as LocationName
	, v.Centroid
	, v.Shape as LocationFeature
	, v.Id
	, v.LocationTypeId
	, v.Admin2 + ', ' + l.LocationName as PresentationLocationName
	,'Ethiopia-' + CAST(Id as varchar(11)) as Hashkey
 from vAdmin1_2Ethiopia v, Universe.dbo.Locations l
 where v.Centroid.STWithin(l.LocationFeature) = 1
 and l.LocationTypeId = 65 --admin1
 and Charindex(', Ethiopia',L.LocationName,0) > 0 
 
 