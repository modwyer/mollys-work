/*
Select *
From Universe.dbo.BaseAdminFeatures B,
Universe.dbo.Locations L
Where L.LocationTypeId = 68
and B.Admin0LocationId in 
	(Select LocationId 
	From Universe.dbo.Locations 
	WHere LocationTypeId = 23 
	and LocationName = 'Somalia') -- In ('Ethiopia','Somalia'))
and L.LocationId = B.Admin2LocationId
and CHARINDEX(', Ethiopia',L.LocationName)>0



Select *
From Universe.dbo.BaseAdminFeatures B,
Universe.dbo.Locations L
Where L.LocationTypeId = 68
and B.Admin0LocationId in 
	(Select LocationId 
	From Universe.dbo.Locations 
	WHere LocationTypeId = 23 
	and LocationName = 'Somalia') -- In ('Ethiopia','Somalia'))
and L.LocationId = B.Admin2LocationId
and CHARINDEX(', Ethiopia',L.LocationName)>0



Select BaseFeature
From Universe.dbo.BaseAdminFeatures B,
Universe.dbo.Locations L
Where L.LocationTypeId = 68
and B.Admin0LocationId in 
	(Select LocationId 
	From Universe.dbo.Locations 
	WHere LocationTypeId = 23 
	and LocationName = 'Ethiopia') -- In ('Ethiopia','Somalia'))
and L.LocationId = B.Admin2LocationId
Union all
Select LocationFeature
From Universe.dbo.Locations
Where LocationId = (Select LocationId From Universe.dbo.Locations WHere LocationTypeId = 23 and LocationName = 'Ethiopia')
*/

Delete
From Universe.dbo.BaseAdminFeatures 
Where baseAdminLocationId in 
(
Select BaseAdminLocationId
From Universe.dbo.BaseAdminFeatures B,
Universe.dbo.Locations L
Where L.LocationTypeId = 68
and B.Admin0LocationId in 
	(Select LocationId 
	From Universe.dbo.Locations 
	WHere LocationTypeId = 23 
	and LocationName = 'Somalia') -- In ('Ethiopia','Somalia'))
and L.LocationId = B.Admin2LocationId
and CHARINDEX(', Ethiopia',L.LocationName)>0
)