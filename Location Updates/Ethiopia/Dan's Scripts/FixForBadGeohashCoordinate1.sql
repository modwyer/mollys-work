Select *
FROM [Universe].[dbo].[GeoHash6LocationsLookup]
WHERE Geohash6 
IN (
SELECT Geohash6 FROM [UserData_StarCluster_Data].[dbo].[GetGeohash6Neighbors] (
  'sc8dp3'
  , 8.79791
  ,34.7814   
  )
  )
  
  

 --Set Admin 2 LocationIDs that point to Somalia to Ethiopia 
Update Universe.[dbo].GeoHash6LocationsLookup
  set Admin0LocationId = (Select LocationId 
				From universe.dbo.Locations
				Where LocationName = 'Ethiopia' 
				and LocationTypeId = 23), 
		Admin2LocationId = (
				Select LocationId 
				From universe.dbo.Locations
				Where LocationName = 'Kelem Wellega, Oromia, Ethiopia' 
				and LocationTypeId = 68
							)
  Where Admin0LocationId = (Select LocationId 
				From universe.dbo.Locations
				Where LocationName = 'Somalia' 
				and LocationTypeId = 23)
	and Admin2LocationID = (Select LocationId 
				From universe.dbo.Locations
				Where LocationName = 'West Wellega, Oromia, Ethiopia (2014)' 
				and LocationTypeId = 68) 
  and Geohash6 in 
  (Select G.Geohash6
	From Universe.dbo.GeoHash6LocationsLookup G
	INNER JOIN
	Universe.dbo.Locations L
	ON G.Admin2LocationId = L.LocationId
	Where G.Admin0LocationId = (Select LocationId 
				From universe.dbo.Locations
				Where LocationName = 'Somalia' 
				and LocationTypeId = 23)
	and CHARINDEX(', Ethiopia (',L.LocationName)>0)  
  
  
  
  Select *
From Universe.Dbo.Locations L
Where LocationId = 91934684