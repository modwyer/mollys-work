Polygon               Old Name                           Old ID                  New ID                 New Name
1       Western Tigray, Tigray    90310747            91934722            Western, Tigray
2       Southern Tigray, Tigray   90310746            91934706            Southern, Tigray
3       Gamo Gofa, SNNPR         90310730            91934668            Gamo Gofa, SNNPR


Select *
from universe.dbo.locations
Where LocationId = 90310747

select *
FROM [Universe].[dbo].[BaseAdminFeatures]
WHERE Admin2LocationId = 90310747


Update [Universe].[dbo].GeoHash6LocationsLookup
SET Admin2LocationId = 
	(Select LocationId From 
	Universe.dbo.Locations
	Where LocationName = 'Western, Tigray, Ethiopia'
	and LocationTypeId = 68)
FROM [Universe].[dbo].GeoHash6LocationsLookup
WHERE Admin2LocationId = 
	(Select LocationId From 
	Universe.dbo.Locations
	Where LocationName = 'Western Tigray, Tigray, Ethiopia (2014)'
	and LocationTypeId = 68)


Update [Universe].[dbo].GeoHash6LocationsLookup
SET Admin2LocationId = 
	(Select LocationId From 
	Universe.dbo.Locations
	Where LocationName = 'Southern, Tigray, Ethiopia'
	and LocationTypeId = 68)
FROM [Universe].[dbo].GeoHash6LocationsLookup
WHERE Admin2LocationId = 
	(Select LocationId From 
	Universe.dbo.Locations
	Where LocationName = 'Southern Tigray, Tigray, Ethiopia (2014)'
	and LocationTypeId = 68)
	
	
Update [Universe].[dbo].GeoHash6LocationsLookup
SET Admin2LocationId = 
	(Select LocationId From 
	Universe.dbo.Locations
	Where LocationName = 'Gamo Gofa, SNNPR, Ethiopia'
	and LocationTypeId = 68)
FROM [Universe].[dbo].GeoHash6LocationsLookup
WHERE Admin2LocationId = 
	(Select LocationId From 
	Universe.dbo.Locations
	Where LocationName = 'Gamo Gofa, SNNPR, Ethiopia (2014)'
	and LocationTypeId = 68)
	

