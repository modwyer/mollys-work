



SELECT [Universe].[dbo].[geohash_encode] (
   9.2, 35.7333 
  ,6)
GO


Select *
FROM [Universe].[dbo].[GeoHash6LocationsLookup]
WHERE Geohash6 
IN (
SELECT Geohash6 FROM [UserData_StarCluster_Data].[dbo].[GetGeohash6Neighbors] (
  'sc9km4'
  , 9.2, 35.7333   
  )
  )
  
  

 --Set Admin 2 LocationIDs that point to Somalia to Ethiopia 
Update Universe.[dbo].GeoHash6LocationsLookup
  set 	Admin2LocationId = (
				Select LocationId 
				From universe.dbo.Locations
				Where LocationName = 'West Wellega, Oromia, Ethiopia' 
				and LocationTypeId = 68
							)
  Where Admin2LocationID = (Select LocationId 
				From universe.dbo.Locations
				Where LocationName = 'West Wellega, Oromia, Ethiopia (2014)' 
				and LocationTypeId = 68) 
  and Geohash6 =  (
		SELECT [Universe].[dbo].[geohash_encode] (
		   9.2
		   , 35.7333 
		  ,6)  
	  ) 
  
  
  
  Select *
From Universe.Dbo.Locations L
Where LocationId = 90310782



90310782 --Bad