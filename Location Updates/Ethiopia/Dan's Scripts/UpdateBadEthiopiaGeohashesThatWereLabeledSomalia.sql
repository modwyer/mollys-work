/*
--Check for somalia encoded geohashes that are in Ethiopia
Select L.LocationName as Admin1Name, G.Admin0LocationId
      , G.Admin1LocationId, G.Admin2LocationId, G.Geohash6
      , cast([Latitude1km] as varchar(250)) + ', ' + cast([Longitude1km] as varchar(250)) as LatLong
    , Geometry::STGeomFromText([ShapeWKT],4326), G.ShapeWKT as WKT
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin2LocationId = L.LocationId
Where G.Admin0LocationId = (Select LocationId 
                                          From universe.dbo.Locations
                                          Where LocationName = 'Somalia' 
                                          and LocationTypeId = 23)
and CHARINDEX(', Ethiopia (',L.LocationName)>0
*/

Create Table #MismatchedGeohashes (Geohash6 char(6), Shape geometry, Location0 int, Location1 int, Location2 int, primary key (Geohash6))
INSERT INTO #MismatchedGeohashes
(Geohash6, Shape)
Select G.Geohash6
    , Geometry::STGeomFromText([ShapeWKT],4326)
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin2LocationId = L.LocationId
Where --G.Admin0LocationId = (Select LocationId 
                                          --From universe.dbo.Locations
                                          --Where LocationName = 'Somalia' 
                                          --and LocationTypeId = 23)
and CHARINDEX(', Ethiopia (',L.LocationName)>0



Select G.Geohash6
    , Geometry::STGeomFromText([ShapeWKT],4326)
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin2LocationId = L.LocationId
Where L.LocationTypeId = 68
and CHARINDEX(', Ethiopia',L.LocationName)>0

Update #MismatchedGeohashes
set Location0 = L.LocationId
From #MismatchedGeohashes M,
Universe.dbo.Locations L
Where L.LocationTypeId = 23
and L.LocationFeature.STIntersects(M.Shape) = 1 


Update #MismatchedGeohashes
set Location1 = L.LocationId
From #MismatchedGeohashes M,
Universe.dbo.Locations L
Where L.LocationTypeId = 65
and L.LocationFeature.STIntersects(M.Shape) = 1

Update #MismatchedGeohashes
set Location2 = L.LocationId
From #MismatchedGeohashes M,
Universe.dbo.Locations L
Where L.LocationTypeId = 68
and L.LocationFeature.STIntersects(M.Shape) = 1

Update Universe.dbo.GeoHash6LocationsLookup
Set Admin0LocationId = Location0,
Admin1LocationId = Location1,
Admin2LocationId = Location2
From #MismatchedGeohashes M,
Universe.dbo.GeoHash6LocationsLookup G
Where G.Geohash6 = M.Geohash6



Select *
From Universe.dbo.Locations
Where LocationId = 90310741

Select *
From Universe.dbo.BaseAdminFeatures B,
Universe.dbo.Locations L
Where L.LocationTypeId = 68
and B.Admin0LocationId in 
	(Select LocationId 
	From Universe.dbo.Locations 
	WHere LocationTypeId = 23 
	and LocationName = 'Ethiopia') -- In ('Ethiopia','Somalia'))
and L.LocationId = B.Admin2LocationId
and CHARINDEX(', Ethiopia',L.LocationName)>0


Select *
From Universe.dbo.BaseAdminFeatures B,
Universe.dbo.Locations L
Where L.LocationTypeId = 68
and B.Admin0LocationId in 
	(Select LocationId 
	From Universe.dbo.Locations 
	WHere LocationTypeId = 23 
	and LocationName = 'Somalia') -- In ('Ethiopia','Somalia'))
and L.LocationId = B.Admin2LocationId
and CHARINDEX(', Ethiopia',L.LocationName)>0


Select BaseFeature
From Universe.dbo.BaseAdminFeatures
Where Admin2LocationId = 90310741
Union all
Select LocationFeature
From Universe.dbo.Locations
Where LocationId = (Select LocationId From Universe.dbo.Locations WHere LocationTypeId = 23 and LocationName = 'Ethiopia')