Select *
From Universe..Locations
Where LocationTypeId = 68
and CHARINDEX('Ethiopia',LocationName)>0


/*

90310782	West Wellega, Oromia, Ethiopia
90310729	Derashe Special Woreda, SNNPR, Ethiopia

*/

Select L.LocationName as Admin1Name, B.*
From Universe..BaseAdminFeatures B
inner join
Universe.dbo.Locations L
ON B.Admin0LocationId = L.LocationId
Where B.Admin2LocationId = 90310782


Select L.LocationName as Admin1Name, B.*
From Universe..Geohash6LocationsLookup B
inner join
Universe.dbo.Locations L
ON B.Admin0LocationId = L.LocationId
Where B.Admin2LocationId = 90310782