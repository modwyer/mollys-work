USE [TestDatabase]
GO

/****** Object:  StoredProcedure [dbo].[4UpdateGeohashes_LoopBaseFeatures]    Script Date: 02/19/2015 10:40:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Molly O'Dwyer/Dan Allen
-- Create date: 2/4/2015

-- Description: 

/*
Select *
From universe..locations
Where LocationId = -45301
Exec [4UpdateGeohashes_LoopBaseFeatures] -45303 --Ethiopia
*/
-- =============================================
CREATE PROCEDURE [dbo].[4UpdateGeohashes_LoopBaseFeatures]
	-- Add the parameters for the stored procedure here
	
	@CountryLocationId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare @Env Geometry

--This is to get the bounding box to put into the spatial indexes below
Declare @MinLong as int
Declare @MaxLong as int
Declare @MinLat as int
Declare @MaxLat as int

Declare @Geohash6 char(6), @Shape geometry, @ActiveGeohash char(6) 
Declare @ActiveRunId int, @MaxRunId int	

Declare @Admin0 int
Declare @Admin1 int
Declare @Admin2 int

Declare @StartTime as datetime2(7)
Declare @EndTime as datetime2(7)

Declare @strSQL varchar(2000)
Declare @CountryShape geometry


Set @StartTime = SYSDATETIME()
print ('Start time is: ' + cast(@StartTime as varchar(19)))
--create table to hold geohashes within the bounding box
print 'Creating table #CountrySpecificGeohashes'

Create Table #CountrySpecificGeohashes 
	(RunId int identity(1,1), Geohash6 char(6), GeoShape geometry, 
	Location0 int, Location1 int, Location2 int, primary key (RunId));

CREATE TABLE #BaseAdminFeatures
(   RunId int identity(1,1),
	[BaseAdminLocationId] [int] NOT NULL,
	[BaseFeature] [geometry] NOT NULL,
	[Admin0LocationId] [int] NOT NULL,
	[Admin1LocationId] [int] NULL,
	[Admin2LocationId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	RunId ASC
)
); 


--Get country parameter
--Get country shape
--Get country envelope
Select @Env = LocationFeature.STEnvelope(), @CountryShape = LocationFeature
From Universe.dbo.Locations
Where LocationId = @CountryLocationId

print 'Getting bounding box for country'

--Get the bounding box around the country LocationId that was provided as parameter
Select @MinLong = floor(Min(Longitude)), @MaxLong = ceiling(Max(longitude)), 
	@MinLat = floor(Min(Latitude)), @MaxLat = ceiling(Max(Latitude))
	From 
		(
		Select @Env.STPointN(1).STX as Longitude, @Env.STPointN(1).STY as Latitude
		UNION ALL
		Select @Env.STPointN(2).STX as Longitude, @Env.STPointN(2).STY as Latitude
		UNION ALL
		Select @Env.STPointN(3).STX as Longitude, @Env.STPointN(3).STY as Latitude
		UNION ALL
		Select @Env.STPointN(4).STX as Longitude, @Env.STPointN(4).STY as Latitude
		) i

print ('Max Lat: ' + cast(@MaxLat as varchar(50)))
print ('Min Lat: ' + cast(@MinLat as varchar(50)))
print ('Max Long: ' + cast(@MaxLong as varchar(50)))
print ('Min Long: ' + cast(@MinLong as varchar(50)))
print 'Inserting geohashes within the bounds into #CountrySpecificGeohashes'
--Get all geohashes within that envelope (or intersect)
--insert those selected geohashes into the temp table	

	set @strSQL = 
	'CREATE SPATIAL INDEX #SPX_CountrySpecificGeohashes 
	ON #CountrySpecificGeohashes
	([geoshape])
	USING  GEOMETRY_GRID 
	WITH (
	bounding_box = ('+ cast(@MinLong as varchar(4)) + ', ' +  cast(@MinLat as varchar(4)) + ', ' +  cast(@MaxLong as varchar(4)) + ', ' + cast(@MaxLat as varchar(4)) + '), '
	+ ' GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
	CELLS_PER_OBJECT = 16);'
	
	
	exec(@strSQL)
	print ('Spatial Index #SPX_CountrySpecificGeohashes Script: ' + char(13) + @strSQL)

	set @strSQL = 
	'CREATE SPATIAL INDEX #SPX_BaseAdminFeatures ON #BaseAdminFeatures
	([BaseFeature])
	USING  GEOMETRY_GRID 
	WITH (
	bounding_box = ('+ cast(@MinLong as varchar(4)) + ', ' +  cast(@MinLat as varchar(4)) + ', ' +  cast(@MaxLong as varchar(4)) + ', ' + cast(@MaxLat as varchar(4)) + '), '
	+ ' GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
	CELLS_PER_OBJECT = 16);'
	
	exec(@strSQL)
	print ('Spatial Index #SPX_BaseAdminFeatures Script: ' + char(13) + @strSQL)

Insert into #CountrySpecificGeohashes
	(Geohash6, GeoShape)
	Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326)
	From [Universe].[dbo].[GeoHash6LocationsLookup]
	Where Latitude1km between @MinLat and @MaxLat
	and Longitude1km between @MinLong and @MaxLong
	
SELECT @Env = [awmDatabase].[dbo].[GetGridCellShapeForBoundingBox] (
   @MaxLat
  ,@MinLat
  ,@MaxLong
  ,@MinLong)	
  
INSERT INTO #BaseAdminFeatures
	(BaseAdminLocationId
	  ,[BaseFeature]
      ,[Admin0LocationId]
      ,[Admin1LocationId]
      ,[Admin2LocationId])
	
SELECT BaseAdminLocationId
	  ,[BaseFeature]
      ,[Admin0LocationId]
      ,[Admin1LocationId]
      ,[Admin2LocationId]
  FROM [Universe].[dbo].[BaseAdminFeatures]
WHERE BaseFeature.STIntersects(@Env) = 1
and Admin0LocationId in (
select LocationId
From Universe.dbo.Locations 
Where LocationTypeId = 23
and LocationFeature.STIntersects(@CountryShape) = 1
)

Declare @Count int

Select @Count = Count(*)
From #BaseAdminFeatures

print @Count

--Loop through all base admin features 
Select @ActiveRunId = Min(RunId), @MaxRunId = Max(RunId)
From #BaseAdminFeatures

print ('Max Run Id: ' + cast(@MaxRunId as varchar(1000)))

Set @StartTime = SYSDATETIME()
print ('Start time for the loop through is: ' + cast(@StartTime as varchar(19)))

While @ActiveRunId <= @MaxRunId
		
	BEGIN
	
		Select @Shape = B.BaseFeature, @Admin0 = Admin0LocationId, 
		@Admin1 = Admin1LocationId, @Admin2 = Admin2LocationId
		From #BaseAdminFeatures B
		Where B.RunId = @ActiveRunId
		
		
		Update [Universe].[dbo].[GeoHash6LocationsLookup]
		Set 
			Admin0LocationId = @Admin0,
			Admin1LocationId = @Admin1,
			Admin2LocationId = @Admin2
		Where Geohash6 in ( 
			Select Geohash6
			From #CountrySpecificGeohashes
			Where GeoShape.STWithin(@Shape) = 1
			)		
	
		print ('Current Active Run ID: ' + cast(@ActiveRunId as varchar(20)))
			
		set @ActiveRunId = @ActiveRunId + 1	
	End
	
	Set @EndTime = SYSDATETIME()
	print ('End time is: ' + cast(@EndTime as varchar(19)))
End

GO


