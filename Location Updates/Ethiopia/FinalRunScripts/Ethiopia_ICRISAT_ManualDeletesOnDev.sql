

--droppepd LocationTypeId because LocationId is unique
Delete
--Select *
From ICRISAT_StarCluster_Data.dbo.LocationRelationships
Where LocationId in
(Select LocationId From ICRISAT_StarCluster_Data.dbo.Locations L
Where LocationTypeId = 68
 and Charindex(', Ethiopia',L.LocationName,0) > 0
 and Charindex(', Ethiopia (',L.LocationName,0) = 0
)
or RelatedLocationId in 
(Select LocationId From ICRISAT_StarCluster_Data.dbo.Locations L
Where LocationTypeId = 68
 and Charindex(', Ethiopia',L.LocationName,0) > 0
 and Charindex(', Ethiopia (',L.LocationName,0) = 0
)


Delete	
--Select *
From ICRISAT_StarCluster_Data.dbo.StarData_ExtendedString
Where LocationId in
(Select LocationId From ICRISAT_StarCluster_Data.dbo.Locations L
Where LocationTypeId = 68
 and Charindex(', Ethiopia',L.LocationName,0) > 0
 and Charindex(', Ethiopia (',L.LocationName,0) = 0
)

Delete	
--Select *
From ICRISAT_StarCluster_Data.dbo.StarData_Float
Where LocationId in
(Select LocationId From ICRISAT_StarCluster_Data.dbo.Locations L
Where LocationTypeId = 68
 and Charindex(', Ethiopia',L.LocationName,0) > 0
 and Charindex(', Ethiopia (',L.LocationName,0) = 0
)

Delete	
--Select *
From ICRISAT_StarCluster_Data.dbo.StarData_SQLVariant
Where LocationId in
(Select LocationId From ICRISAT_StarCluster_Data.dbo.Locations L
Where LocationTypeId = 68
 and Charindex(', Ethiopia',L.LocationName,0) > 0
 and Charindex(', Ethiopia (',L.LocationName,0) = 0
)

Delete 
--Select *
From ICRISAT_StarCluster_Data.dbo.Locations 
Where LocationTypeId = 68
 and Charindex(', Ethiopia',LocationName,0) > 0
 and Charindex(', Ethiopia (',LocationName,0) = 0

Update ICRISAT_StarCluster_Data.dbo.Locations 
Set LocationName = PresentationLocationName 
--Select * from ICRISAT_StarCluster_Data.dbo.Locations 
Where LocationTypeId = 68
and Charindex(', Ethiopia',LocationName,0) > 0