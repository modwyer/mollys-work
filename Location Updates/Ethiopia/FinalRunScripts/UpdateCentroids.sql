--Update centroids
Update [Universe].[dbo].[Locations]  
Set CentroidFeature = [LocationFeature].STCentroid()
--Select * from [Universe].[dbo].[Locations]
Where LocationTypeId = 68
and CHARINDEX(' Ethiopia',LocationName)>0
and CHARINDEX(' Ethiopia (',LocationName)=0 
and CHARINDEX(' Ethiopia(',LocationName)=0


--then see if any centroids are null

Update [Universe].[dbo].[Locations]    
set [CentroidFeature] = LocationFeature.STEnvelope().STCentroid()
--Select * from [Universe].[dbo].[Locations] 
WHERE [LocationFeature] is not null
and [CentroidFeature] is null  
and LocationTypeId = 68
and CHARINDEX(' Ethiopia',LocationName)>0
and CHARINDEX(' Ethiopia (',LocationName)=0 
and CHARINDEX(' Ethiopia(',LocationName)=0
