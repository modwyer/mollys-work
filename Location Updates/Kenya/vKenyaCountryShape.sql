USE [TestDatabase]
GO

/****** Object:  View [dbo].[KenyaCountryShape]    Script Date: 02/02/2015 13:48:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create view [dbo].[KenyaCountryShape]
as
	(Select LocationId,LocationName,LocationTypeId, LocationFeature, Hashkey
	from Universe.dbo.Locations
	Where LocationTypeId = 23
	and CHARINDEX('Kenya', LocationName)>0)
GO


