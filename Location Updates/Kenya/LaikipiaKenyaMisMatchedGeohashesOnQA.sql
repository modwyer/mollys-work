Select *
From Locations
Where LocationTypeId = 65
and CHARINDEX('Laikipia, Kenya',LocationName)>0
and CHARINDEX(', Kenya (',LocationName)=0
order by LocationName asc

/*
90717147 Baringo, Kenya   admin1
91261487 Baringo south, Baringo, Kenya
91261488 Tiaty, Baringo, Kenya

90717184 Laikipia, Kenya admin1
91261424 Ndaragwa, Nyandarua, Kenya
91261623	Laikipia east, Laikipia, Kenya
91261489	Laikipia north, Laikipia, Kenya
91261622	Laikipia west, Laikipia, Kenya
*/


Select G.*,L.LocationName
From Universe.dbo.GeoHash6LocationsLookup G
inner join
Universe..Locations L
on G.Admin1LocationId = L.LocationId
Where G.Admin2LocationId = 91261623 --Laikipia east, Laikipia, Kenya
and G.Admin1LocationId <> 90717184 --Laikipia, Kenya
--result is 16 geohashes pointing to Nyeri, Kenya admin 1
--LocationIDs are different on Prod but the results are the same

Select G.*,L.LocationName 
From Universe.dbo.GeoHash6LocationsLookup G
inner join
Universe..Locations L
on G.Admin1LocationId = L.LocationId
Where G.Admin2LocationId = 91261489 --Laikipia north, Laikipia, Kenya
and G.Admin1LocationId <> 90717184 --Laikipia, Kenya
--result is 3 geohashes pointing to Samburu, Kenya, 5 geohashes pointing to Meru, Kenya, and 10 geohashes pointing to Isiolo, Kenya


Select G.*,L.LocationName 
From Universe.dbo.GeoHash6LocationsLookup G
inner join
Universe..Locations L
on G.Admin1LocationId = L.LocationId
Where G.Admin2LocationId = 91261622 --Laikipia west, Laikipia, Kenya
and G.Admin1LocationId <> 90717184 --Laikipia, Kenya
--result is 7 geohashes pointing to Nyandarua, Kenya admin 1 and 16 pointing to Baringo, Kenya admin1

Select G.*,L.LocationName 
From Universe.dbo.GeoHash6LocationsLookup G
inner join
Universe..Locations L
on G.Admin1LocationId = L.LocationId
Where G.Admin2LocationId = 91261424 --91261424 Ndaragwa, Nyandarua, Kenya
--result is 2 geohashes point to Nyeri, Kenya and 1 points to Laikipia, Kenya admin1

Select G.*,L.LocationName 
From Universe.dbo.GeoHash6LocationsLookup G
inner join
Universe..Locations L
on G.Admin1LocationId = L.LocationId
Where G.Admin2LocationId = 91261487 --Baringo south, Baringo, Kenya
and G.Admin1LocationId <> 90717147 --Baringo, Kenya
-- result is 4 geohashes pointing to Laikipia, Kenya


Select G.*,L.LocationName 
From Universe.dbo.GeoHash6LocationsLookup G
inner join
Universe..Locations L
on G.Admin1LocationId = L.LocationId
Where G.Admin2LocationId = 91261488 --Tiaty, Baringo, Kenya
and G.Admin1LocationId <> 90717147 --Baringo, Kenya
--result is 1 geohash points to Samburu, Kenya, 1 points to Laikipia, Kenya, 7 point to Turkana, Kenya


--Test on prod:
Select G.*,L.LocationName 
From Universe.dbo.GeoHash6LocationsLookup G
inner join
Universe..Locations L
on G.Admin1LocationId = L.LocationId
Where Admin2LocationId = 91910909 --Laikipia east, Laikipia, Kenya; different LocationId than what's on QA
and G.Admin1LocationId <> 90717184 --Laikipia, Kenya
