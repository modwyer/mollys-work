USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAdmin2India]    Script Date: 5/15/2015 11:55:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

alter view [dbo].[vAdmin2India]
 as
 Select v.Admin2 + ', ' + l.LocationName as LocationName
	, v.Centroid
	, v.Shape as LocationFeature
	, v.Id
	, v.LocationTypeId
	, v.Admin2 + ', ' + l.LocationName as PresentationLocationName
	,'India-' + CAST(Id as varchar(11)) as Hashkey
 from vAdmin1_2India v, Universe.dbo.Locations l
 where v.Centroid.STWithin(l.LocationFeature) = 1
 and l.LocationTypeId = 65 --admin1
 and Charindex(', India',L.LocationName,0) > 0 
 and l.HashKey is not null
GO


