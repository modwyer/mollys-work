/**** Update for QA India issue of geography filter showing admin 2 locations under incorrect admin 1 locations. ****/
--Get locationids for testing
select locationid, locationname
from universe..locations
where locationname like '%, India'
and locationtypeid = 65

select locationid, locationname
from universe..locations
where locationname like '%, India'
and locationtypeid = 68

/*
92173851	Andhra Pradesh, India
92174559	Yanam, Puducherry, India

92173858	Daman and Diu, India
92174097	Gir-Somnath, Gujarat, India

92173861	Gujarat, India
92174023	Daman, Daman and Diu, India

92173867	Kerala, India
92174263	Mahe, Puducherry, India

92173876	Puducherry, India

*/

select l.locationname, lr.*
from WorldBank_StarCluster_Data..locationrelationships lr
inner join
universe..locations l
on lr.relatedlocationid = l.locationid
where lr.locationid = 92173876 --Yanam


--Manually update each location.
--Yanam and Andhra Pradesh to Puducherry
Update WorldBank_StarCluster_Data..locationrelationships
Set locationid = 92173876 --Puducherry
where relatedlocationid = 92174559 --Yanam
and locationid = 92173851 --Andhra Pradesh

--Gir-Somnath and Daman and Diu to Gujarat
Update WorldBank_StarCluster_Data..locationrelationships
Set locationid = 92173861 --Gujarat
where relatedlocationid = 92174097 --Gir-Somnath
and locationid = 92173858 --Daman and Diu

--Daman and Gujarat to Daman and Diu
Update WorldBank_StarCluster_Data..locationrelationships
Set locationid = 92173858 --Daman and diu
where relatedlocationid = 92174023 --Daman
and locationid = 92173861 --Gujarat

--Mahe and Kerala to Puducherry
Update WorldBank_StarCluster_Data..locationrelationships
Set locationid = 92173876 --Puducherry
where relatedlocationid = 92174263 --Mahe
and locationid = 92173867 --Kerala


--Compare names and make sure admin 1 in relatedname matches locationname.
select l1.locationname as locationname, lr.locationid,l2.locationname as relatedname, lr.relatedlocationid
from WorldBank_StarCluster_Data..locationrelationships lr
inner join
universe..locations l1
on lr.locationid = l1.locationid
inner join
universe..locations l2
on lr.relatedlocationid = l2.LocationId
where lr.RelatedLocationId in (
	select locationid
	from universe..locations
	where locationtypeid = 68
	and locationname like '%, India')
and lr.RelatedLocationtypeid = 68
and lr.locationtypeid = 65