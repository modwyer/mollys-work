USE [TestDatabase]
GO

/****** Object:  StoredProcedure [dbo].[2aUpdateExistingAndInsertNewAdminBoundaries_vIndia]    Script Date: 5/15/2015 10:18:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Dan Allen with Molly
-- Create date: 12/18/2014
-- Update date: 2/17/15
-- Description:	Update the existing location features for the admin type requested and then insert the new boundaries into universe

/*
Example:
SET NOCOUNT ON;

Declare @LocationProcessData LocationProcessData

INSERT INTO @LocationProcessData
(LocationName, LocationTypeId, LocationFeature, CentroidFeature, PresentationLocationName, Hashkey)
SELECT 
	LocationName as [LocationName]
	,LocationTypeId as [LocationTypeId]
	,LocationFeature as [LocationFeature]
	,Centroid as [Centroid]
	,PresentationLocationName as [PresentationLocationName]
	,'India-' + cast(ID as varchar(15)) as [HashKey]
  FROM [TestDatabase].[dbo].[vAdmin1India]

--Select * from @LocationProcessData 

EXEC 2aUpdateExistingAndInsertNewAdminBoundaries_vIndia 65, 'India', '2015', @LocationProcessData 


*/

-- =============================================
CREATE PROCEDURE [dbo].[2aUpdateExistingAndInsertNewAdminBoundaries_vIndia]
	-- Add the parameters for the stored procedure here
	@LocationTypeId smallint = 65,
	@CountryName varchar(250) = 'India',
	@SuffixToAddToExistingFeatures varchar(20) = '2015',
	@AdminBoundariesToProcess dbo.LocationProcessData READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	BEGIN TRANSACTION;

	Declare @strSQL varchar(2000)
	
	Print 'Adding suffix to old locations'
	
	--Add suffix to old locations
	Update [Universe].[dbo].[Locations]
	Set LocationName = LocationName + ' ('+@SuffixToAddToExistingFeatures+')'
	Select * From Universe..Locations
	 Where LocationTypeId = @LocationTypeId
	 and Charindex(', '+@CountryName+'',LocationName,0) > 0
	 and Charindex(', '+@CountryName+ ' (',LocationName,0) = 0
	 and LocationName + ' ('+@SuffixToAddToExistingFeatures+')' 
		not in (Select LocationName 
				From Universe.dbo.Locations 
				Where LocationTypeId = @LocationTypeId)	
		
Print 'Adding new boundaries to Locations table'
	
	--Insert the new boundaries into the Locations table
	INSERT INTO [Universe].[dbo].[Locations]
			   ([LocationName]
			   ,[LocationTypeId]
			   ,[LocationFeature]
			   ,[CentroidFeature]
			   ,[PresentationLocationName]
			   ,HashKey)
	Select [LocationName]
			   ,[LocationTypeId]
			   ,[LocationFeature]
			   ,[CentroidFeature]
			   ,[PresentationLocationName]
			   ,HashKey
	From @AdminBoundariesToProcess	
	WHERE LocationName not in 
		(Select LocationName 
		From Universe.dbo.Locations 
		Where LocationTypeId = @LocationTypeId)
	
	--Print 'Starting Section 2B:Update other databases with new locations'
	----Update other databases with new boundaries
	--Execute [2bUpdateOtherDatabasesForLocations]
	--	@CountryName,
	--	@LocationTypeId
		
	--Print 'Section 2B complete'
	Commit Transaction;	

END


GO


