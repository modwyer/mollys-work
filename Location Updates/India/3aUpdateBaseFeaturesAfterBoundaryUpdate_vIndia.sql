USE [TestDatabase]
GO

/****** Object:  StoredProcedure [dbo].[3aUpdateBaseFeaturesAfterBoundaryUpdate_vIndia]    Script Date: 5/15/2015 11:19:22 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		Molly O'Dwyer
-- Create date: 12/18/2014
-- Update date: 5/15/15
-- Description:	Update the base admin features after a new boundary update


/*
EXEC dbo.[3aUpdateBaseFeaturesAfterBoundaryUpdate_vIndia] 65, 'India', 'Southern Asia', 'Asia'
*/
-- =============================================

alter PROCEDURE [dbo].[3aUpdateBaseFeaturesAfterBoundaryUpdate_vIndia]
	-- Add the parameters for the stored procedure here
	@LocationTypeId smallint,
	@CountryName varchar(250),
	@RegionName varchar(250),
	@ContinentName varchar(250)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	BEGIN TRANSACTION;
	
	Declare @CountryLocationId int
	Declare @RegionLocationId int
	Declare @ContinentLocationId int

	--This gets the LocationId for the country name passed within the where clause
	Select @CountryLocationId = LocationId
	From Universe.dbo.Locations
	Where LocationName = @CountryName
	and LocationTypeId = 23

	--Get the LocationId for World Region that country falls within
	Select @RegionLocationId = LocationId
	From Universe.dbo.Locations
	Where LocationName = @RegionName --World Region
	and LocationTypeId = 25

	--Get the LocationId for Continent that country falls within
	Select @ContinentLocationId = LocationId
	From Universe.dbo.Locations
	Where LocationName = @ContinentName  --Continent
	and LocationTypeId = 26

	Print 'Base Location type is Admin 2'
		
	Print 'Starting Section 3B: Delete and insert locations to Base Admin'
				
	EXECUTE [3bDeleteAndInsertToBaseAdminV2] 
			@LocationTypeId
			,@CountryName
			,@CountryLocationId
			,@RegionLocationId 
			,@ContinentLocationId
				
	Print ' Section 3B complete'
		
	  
	Print 'Checking for invalid geometries'
	--Update any geometries that are not valid
	Update [Universe].[dbo].[BaseAdminFeatures]
	set BaseFeature = BaseFeature.MakeValid()
	Where Admin0LocationId = @CountryLocationId  
	and [BaseFeature].STIsValid() = 0

	Update [Universe].[dbo].[Locations]
	set LocationFeature = LocationFeature.MakeValid()
	Where LocationTypeId = @LocationTypeId  
	and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-' 			
	and LocationFeature.STIsValid() = 0	
		
	Print 'Done with invalid geometries'
	
	COMMIT TRANSACTION;
END




GO


