Select L.LocationName, B.Admin0LocationId, B.Admin1LocationId, B.Admin2LocationId, BaseFeature,LocationFeature
From Universe..BaseAdminFeatures B
inner join
Universe..Locations L
on B.Admin1LocationId = L.LocationId
Where Admin2LocationId = 91736363


Select L.LocationName, B.Admin0LocationId, B.Admin1LocationId, B.Admin2LocationId,LocationFeature
	, Geometry::STGeomFromText(B.ShapeWKT,4326) as GeohashFeature
	, Latitude1km, Longitude1km
From Universe..Geohash6LocationsLookup B
inner join
Universe..Locations L
on B.Admin2LocationId = L.LocationId
Where Admin0LocationId = -45498
and charindex('Lakshadweep, India',L.Locationname)>0
and charindex('Lakshadweep, India (',L.Locationname)=0

Select *
From Universe..Locations
Where LocationTypeID in (65, 68)
and charindex(', India', LocationName)>0
and charindex(', India (', LocationName)=0
and charindex(', Indian', LocationName)=0