USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAdmin1_0709Fix]    Script Date: 7/9/2015 8:34:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


alter view [dbo].[vAdmin1_0709Fix]
as 
SELECT 
	NAME_1 + ', India' as LocationName
	,65 as LocationTypeId
	,[Shape] as LocationFeature
	,[ID] as Id
	,case when [Shape].MakeValid().STCentroid() is null then [Shape].MakeValid().STEnvelope().STCentroid() else [Shape].MakeValid().STCentroid() end as Centroid
	,NAME_1 + ', India' as PresentationLocationName
	,Name_0 + '-' + cast(ID as varchar(10)) as Hashkey
  FROM [TestDatabase].[dbo].[IndiaAdmin1_0709Fix]

GO


