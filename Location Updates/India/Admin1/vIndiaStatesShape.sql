USE [TestDatabase]
GO

/****** Object:  View [dbo].[vIndiaStatesShape]    Script Date: 02/13/2015 09:51:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Create view [dbo].[vIndiaStatesShape]
as
	(Select LocationId,LocationName,LocationTypeId, LocationFeature, Hashkey
	from Universe.dbo.Locations
	Where LocationTypeId = 65
	and CHARINDEX(', India', LocationName)>0)

GO


