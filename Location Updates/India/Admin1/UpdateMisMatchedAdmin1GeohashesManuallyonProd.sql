/**** Script to Update Geohash table where admin 1 is incorrect. Template ****/
--Created: 6/9/15
--Author: Molly O'Dwyer

Declare @Admin1 int
Declare @Admin2 int
Declare @Admin0 int = -45498 --change this to the country being worked on
Declare @newAdmin int
Declare @ActiveRunId int, @MaxRunId int	
Declare @LocationName varchar(50)

Create table #IdUpdates (RunId int identity(1,1), Locationname varchar(50), Admin1Locationid int, Admin2Locationid int, newAdminLocationid int)

--values are formatted in Excel and pasted into the value area
Insert into #IdUpdates (Locationname, Admin1Locationid, Admin2Locationid, newAdminLocationid)
--paste values in the space below, replacing the text from ( to )
Values ('Karimganj, Assam, India', '92446755', '92447281', '92446726'), 
('Sindhudurg, Maharashtra, India', '92446733', '92447286', '92446743'), 
('Srikakulam, Andhra Pradesh, India', '92446748', '92446865', '92446724'), 
('Gurdaspur, Punjab, India', '92446737', '92447222', '92446750'), 
('Kokrajhar, Assam, India', '92446758', '92447112', '92446726'), 
('Gir-Somnath, Daman and Diu, India', '92446734', '92447274', '92446731'), 
('Kishanganj, Bihar, India', '92446758', '92446832', '92446727'), 
('Thiruvallur, Tamil Nadu, India', '92446724', '92447254', '92446753'), 
('Mamit, Mizoram, India', '92446755', '92446858', '92446746'), 
('Pashchim Champaran, Bihar, India', '92446756', '92446859', '92446727'), 
('East Jaintia Hills, Meghalaya, India', '92446726', '92446870', '92446745'), 
('Darjeeling, West Bengal, India', '92446752', '92447157', '92446758')

Select @MaxRunId = max(RunId), @ActiveRunId = Min(RunId)
From #IdUpdates

--Run through each location ID and change admin 1 to new Admin 1 from the table
While @ActiveRunId <=@MaxRunId
	Begin

		Select @Admin1 = Admin1Locationid, @Admin2 = Admin2Locationid, @newAdmin = newAdminLocationid, @LocationName = Locationname
		From #IdUpdates
		Where RunId = @ActiveRunId

		Update Universe..Geohash6locationslookup
		Set Admin1LocationId = @newAdmin
		Where Admin0LocationId = @Admin0
		and Admin1LocationId = @Admin1
		and Admin2LocationId = @Admin2

		

		Print('Admin 1 for location ' + @LocationName + ' has been updated.')
		Print ('Old Admin 1: ' + cast(@Admin1 as varchar(10)) + char(13) + 'New Admin 1: ' + cast(@newAdmin as varchar(10)))

		Set @ActiveRunId = @ActiveRunid + 1
	End

--set 4 remaining nulls to a location id
Update Universe..Geohash6locationslookup
Set Admin2Locationid = 92447259 --North Andaman
Where Admin0LocationId = @Admin0
and Admin1LocationId = 92446723 --Andaman & Nicobar Islands
and admin2locationid is null

Drop table #IdUpdates