/******  India troubleshooting on Dev for geohashes, base admin, and location points *****/




/* These needed new point locations in the test dataset due to centroid location being outside the actual polygon
--rural maps to urban because of centroid location
90271199	Bangalore Rural, Karnataka, India
90271200	Bangalore Urban, Karnataka, India

--
90271692	Dadra and Nagar Haveli, Dadra and Nagar Haveli, India
90271693	Daman, Daman and Diu, India
90271130	Valsad, Gujarat, India


--Puducherry maps to Villupuram - why?
90271409	Puducherry, Puducherry, India
90271507	Villupuram, Tamil Nadu, India

--Raipur maps to Mahasamund b/c of centroid location
90271689	Raipur, Chhattisgarh, India
90271687	Mahasamund, Chhattisgarh, India

--Sundargarh maps to Sambalpur b/c of centroid location
90271406	Sundargarh, Orissa, India
90271404	Sambalpur, Orissa, India

--Uttar mapping to Dinajpur in Bangladesh b/c of centroid location
90271613	Uttar Dinajpur, West Bengal, India
80259082	Dinajpur, Rajshahi, Bangladesh

--Nagaon maps to Karbi b/c of centroid issue
90271632	Nagaon, Assam, India
90271627	Karbi Anglong, Assam, India

--Phek maps to Ukhrul, possible centroid issue?? Near the border but looks like it's in the right poly
90271373	Phek, Nagaland, India
90271352	Ukhrul, Manipur, India

--East maps to Senapati, b/c of centroid issue
90271348	East Imphal, Manipur, India
90271349	Senapati, Manipur, India

--*Yanam maps to East, possible centroid issue? Near the border but looks like it's in the right poly
90271410	Yanam, Puducherry, India
90271260	East Godavari, Andhra Pradesh, India
*/

Select L.LocationName, B.*
From Universe..BaseAdminFeatures B
inner join
Universe..Locations L
on B.Admin2LocationId = L.LocationId
Where B.Admin1LocationId = 91715090

--Updates to Base Admin Table after the above location fixes and initial geohash update
/* Dadra and Haveli a2 maps to Gujarat a1 in base admin
91715090 Gujarat (a1)
90271692 Dadra and Haveli (a2)
91715086	Dadra and Nagar Haveli, India
*/
Update Universe..BaseAdminFeatures
Set Admin1LocationId = 91715086
Where Admin2LocationId = 90271692

------------

Select L.LocationName, B.*
From Universe..BaseAdminFeatures B
inner join
Universe..Locations L
on B.Admin3LocationId = L.LocationId
Where B.Admin2LocationId = 90271513

/*Rajasthan maps to Haryana a1
91715091	Haryana, India
91715107	Rajasthan, India
90271443    Hanumangarh, Rajasthan, India maps to haryana a1
*/
Update Universe..BaseAdminFeatures
Set Admin1LocationId = 91715107
Where Admin2LocationId = 90271443

/*Agra maps to Rajasthan a1
90271513 Agra, Uttar Pradesh, India maps to Rajasthan a1
91715107	Rajasthan, India
91715111	Uttar Pradesh, India
*/
Update Universe..BaseAdminFeatures
Set Admin1LocationId = 91715111
Where Admin2LocationId = 90271513