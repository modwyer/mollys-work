USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAdmin1Fix_072015]    Script Date: 7/20/2015 10:10:18 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[vAdmin1Fix_072015]
as 
SELECT 
	NAME_1 + ', India' as LocationName
	,65 as LocationTypeId
	,[Shape] as LocationFeature
	,[ID] as Id
	,case when [Shape].MakeValid().STCentroid() is null then [Shape].MakeValid().STEnvelope().STCentroid() else [Shape].MakeValid().STCentroid() end as Centroid
	,NAME_1 + ', India' as PresentationLocationName
	,'India-' + cast(ID as varchar(10)) as Hashkey
  FROM [TestDatabase].[dbo].[IndiaAdmin1Fix_072015]


GO


