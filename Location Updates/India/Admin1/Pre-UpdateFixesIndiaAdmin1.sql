
--For dev and QA, but not Prod. Remove hashkey from current locations
Update Universe..Locations
Set Hashkey = Null
Where LocationTypeId = 65
and charindex(', India', locationname)>0
and charindex(', Indian', locationname)=0
and HashKey is not null

Update WorldBank_StarCluster_Data..Locations
Set Hashkey = Null
Where LocationTypeId = 65
and charindex(', India', locationname)>0
and charindex(', Indian', locationname)=0
and HashKey is not null

Update UserData_StarCluster_Data..Locations
Set Hashkey = Null
Where LocationTypeId = 65
and charindex(', India', locationname)>0
and charindex(', Indian', locationname)=0
and HashKey is not null

Update ICRISAT_StarCluster_Data..Locations
Set Hashkey = Null
Where LocationTypeId = 65
and charindex(', India', locationname)>0
and charindex(', Indian', locationname)=0
and HashKey is not null