USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAdmin1India]    Script Date: 02/27/2015 12:44:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[vAdmin1India]
as 
SELECT 
	NAME_1 + ', India' as LocationName
	,65 as LocationTypeId
	,[Shape] as Shape
	,[OBJECTID] as Id
	,case when [Shape].MakeValid().STCentroid() is null then [Shape].MakeValid().STEnvelope().STCentroid() else [Shape].MakeValid().STCentroid() end as Centroid
	,NAME_1 + ', India' as PresentationLocationName
	,'India-' + CAST(Id as varchar(8)) as Hashkey
  FROM [TestDatabase].[dbo].[IndiaAdmin1]
GO

