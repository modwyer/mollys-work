/**** Script to Update Geohash table where admin 1 is incorrect. DEV ****/
--Created: 6/9/15
--Author: Molly O'Dwyer

Declare @Admin1 int
Declare @Admin2 int
Declare @Admin0 int = -45498 --change this to the country being worked on
Declare @newAdmin1 int
Declare @ActiveRunId int, @MaxRunId int	
Declare @LocationName varchar(50)

Create table #IdUpdates (RunId int identity(1,1), Locationname varchar(50), Admin1Locationid int, Admin2Locationid int, newAdmin1Locationid int)

--values are formatted in Excel and pasted into the value area
Insert into #IdUpdates (Locationname, Admin1Locationid, Admin2Locationid, newAdmin1Locationid)
Values ('Kancheepuram, Tamil Nadu, India', '91738053', '91736295', '91738082'), 
('Gajapati, Odisha, India', '91738053', '91736207', '91738077'), 
('Mamit, Mizoram, India', '91738055', '91736393', '91738075'), 
('Dakshin Dinajpur, West Bengal, India', '91738056', '91736142', '91738087'), 
('Sindhudurg, Maharashtra, India', '91738062', '91736572', '91738072'), 
('Uttara Kannada, Karnataka, India', '91738062', '91736651', '91738068'), 
('Kolhapur, Maharashtra, India', '91738062', '91736340', '91738072'), 
('Barmer, Rajasthan, India', '91738063', '91736067', '91738080'), 
('Gir-Somnath, Daman and Diu, India', '91738063', '91736218', '91738060'), 
('Uttarkashi, Uttarakhand, India', '91738065', '91736652', '91738086'), 
('Leh, Jammu and Kashmir, India', '91738065', '91736368', '91738066'), 
('Kangra, Himachal Pradesh, India', '91738066', '91736297', '91738065'), 
('Pathankot, Punjab, India', '91738066', '91736476', '91738079'), 
('South Goa, Goa, India', '91738068', '91736593', '91738062'), 
('North Goa, Goa, India', '91738068', '91736453', '91738062'), 
('Kannur, Kerala, India', '91738068', '91736301', '91738069'), 
('Kanniyakumari, Tamil Nadu, India', '91738069', '91736300', '91738082'), 
('Thirunelveli, Tamil Nadu, India', '91738069', '91736617', '91738082'), 
('Dakshina Kannada, Karnataka, India', '91738069', '91736143', '91738068'), 
('Kodagu, Karnataka, India', '91738069', '91736334', '91738068'), 
('Navsari, Gujarat, India', '91738072', '91736440', '91738063'), 
('Thiruvananthapuram, Kerala, India', '91738082', '91736619', '91738069'), 
('Chittoor, Andhra Pradesh, India', '91738082', '91736133', '91738053'), 
('Purnia, Bihar, India', '91738087', '91736498', '91738056')

Select @MaxRunId = max(RunId), @ActiveRunId = Min(RunId)
From #IdUpdates

--Run through each location ID and change admin 1 to new Admin 1 from the table
While @ActiveRunId <=@MaxRunId
	Begin

		Select @Admin1 = Admin1Locationid, @Admin2 = Admin2Locationid, @newAdmin1 = newAdmin1Locationid, @LocationName = Locationname
		From #IdUpdates
		Where RunId = @ActiveRunId

		Update Universe..Geohash6locationslookup
		Set Admin1LocationId = @newAdmin1
		Where Admin0LocationId = @Admin0
		and Admin1LocationId = @Admin1
		and Admin2LocationId = @Admin2

		Print('Admin 1 for location ' + @LocationName + ' has been updated.')
		Print ('Old Admin 1: ' + cast(@Admin1 as varchar(10)) + char(13) + 'New Admin 1: ' + cast(@newAdmin1 as varchar(10)))

		Set @ActiveRunId = @ActiveRunid + 1
	End

Drop table #IdUpdates