
/* script to update location names for Telangana admin 2 and 3 post-update */

--Update Admin 2 Telangana names
Update Universe..Locations
Set LocationName = REPLACE(LocationName, 'Andhra Pradesh', 'Telangana'),
	PresentationLocationName = REPLACE(PresentationLocationName, 'Andhra Pradesh', 'Telangana')
Where LocationId in
(
Select distinct B.Admin2LocationId
	From Universe..BaseAdminFeatures B
	inner join
	Universe..Locations L
	On B.Admin1LocationId = L.LocationId
	inner join
	Universe..Locations A
	On B.Admin2LocationId = A.LocationId
	inner join
	Universe..Locations C
	On B.Admin3LocationId = C.LocationId
	Where B.Admin0LocationId = -45498
	and B.Admin1LocationId = 91715114)

--Update Admin 3 Telangana names
Update Universe..Locations
Set LocationName = REPLACE(LocationName, 'Andhra Pradesh', 'Telangana'),
	PresentationLocationName = REPLACE(PresentationLocationName, 'Andhra Pradesh', 'Telangana')
Where LocationId in
(
Select distinct B.Admin3LocationId
	From Universe..BaseAdminFeatures B
	inner join
	Universe..Locations L
	On B.Admin1LocationId = L.LocationId
	inner join
	Universe..Locations A
	On B.Admin2LocationId = A.LocationId
	inner join
	Universe..Locations C
	On B.Admin3LocationId = C.LocationId
	Where B.Admin0LocationId = -45498
	and B.Admin1LocationId = 91715114)

/*

SElect *, REPLACE(LocationName, 'Andhra Pradesh', 'Telangana') as NewName,
	REPLACE(PresentationLocationName, 'Andhra Pradesh', 'Telangana') as NewPresentationName
From Universe..Locations
Where LocationId in
(
Select distinct B.Admin3LocationId
	From Universe..BaseAdminFeatures B
	inner join
	Universe..Locations L
	On B.Admin1LocationId = L.LocationId
	inner join
	Universe..Locations A
	On B.Admin2LocationId = A.LocationId
	inner join
	Universe..Locations C
	On B.Admin3LocationId = C.LocationId
	Where B.Admin0LocationId = -45498
	and B.Admin1LocationId = 91715114)
	
	*/