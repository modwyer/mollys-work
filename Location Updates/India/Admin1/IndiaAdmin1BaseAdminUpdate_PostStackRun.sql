/*** Process to prep Admin 1 Base Admins prior to update.***/



/* Step 1 Find Base Admin and delete any that are not the base admin */
--India base admin is 3

Delete
From Universe..BaseAdminFeatures
Where Admin0LocationId = -45498
and BaseAdminLevelId<3

/* Step 2 Update the Admin 1 locationIDs for the base admins based on when the Centroid of the base admin is within an admin 1 */

Update Universe..BaseAdminFeatures
set Admin1LocationId = U.LocationId 
From Universe..BaseAdminFeatures B,
	(
	Select 
		L.LocationId
		,B.BaseAdminLocationId
	From TestDatabase..vIndiaBaseAdmins B
	, Universe..Locations L
	where B.Centroid.STWithin(L.LocationFeature)=1
	and L.LocationTypeId = 65
	and charindex(', India',L.LocationName)>0
	and charindex(', India (',L.LocationName)=0
	)  U
Where B.BaseAdminLocationId = U.BaseAdminLocationId


/* Step 3 Test that the change worked */
--Looking to see if all new Admin 1 locations are in base admin and if there are any old locations still in base admin
Select L.LocationName as Admin1, A.LocationName as Admin2, C.LocationName as Admin3, B.Admin0LocationId,B.Admin1LocationId,B.Admin2LocationId,B.Admin3LocationId
From Universe..BaseAdminFeatures B
inner join
Universe..Locations L
On B.Admin1LocationId = L.LocationId
inner join
Universe..Locations A
On B.Admin2LocationId = A.LocationId
inner join
Universe..Locations C
On B.Admin3LocationId = C.LocationId
Where B.Admin0LocationId = -45498
order by L.LocationName, A.LocationName, C.LocationName

/* Step 4 Check Telangana, the new Admin 1 */
Select *
From Universe..Locations
Where LocationTypeId = 68
and CHARINDEX('Telangana, India',LocationName)>0
and CHARINDEX(', India (',LocationName)=0
and CHARINDEX('United States',LocationName)=0

/* Step 5 The following locations has old locations in the BaseAdmin table that needed updating. These may need updating in QA and prod but should 
     double check before updating. */

Update Universe..BaseAdminFeatures
Set Admin1LocationId = 91715079 --Andaman & Nicobar Islands
Where Admin1LocationId = 90004105

Update Universe..BaseAdminFeatures
Set Admin1LocationId = 91715080 --Andhra Pradesh
Where Admin1LocationId = 90004116

Update Universe..BaseAdminFeatures
Set Admin1LocationId = 91715090  --Gujarat
Where Admin1LocationId = 90004108

Update Universe..BaseAdminFeatures
Set Admin1LocationId = 91715096  --Kerala
Where Admin1LocationId = 90004114

Update Universe..BaseAdminFeatures
Set Admin1LocationId = 91715097  --Lakshadweep
Where Admin1LocationId = 90004115

Update Universe..BaseAdminFeatures
Set Admin1LocationId = 91715113  --West Bengal
Where Admin1LocationId = 90004133



/* Miscellaneous queries used to get to the above.

Select *
From Universe..BaseAdminFeatures
Where Admin1LocationId = 90004105


Select L.LocationName 
	,L.LocationId
	,Centroid
	,B.BaseAdminLocationId
	,B.BaseAdminLevelId
	,BaseFeature
	,Admin0LocationId
	,Admin1LocationId
	,Admin2LocationId
	,Admin3LocationId
From TestDatabase..vIndiaBaseAdmins B
, Universe..Locations L
where B.Centroid.STWithin(L.LocationFeature)=1
and L.LocationTypeId = 65
and charindex(', India',L.LocationName)>0
and charindex(', India (',L.LocationName)=0

--checking view for invalid features
Select *
From TestDatabase..vIndiaBaseAdmins B
Where Centroid.STIsValid() =0

--update invalid features to valid
Update Universe..Locations
set LocationFeature = LocationFeature.MakeValid()
Where LocationFeature.STIsValid() =0
and LocationTypeId = 65
and charindex(', India',LocationName)>0

--checking Locations for invalid features
Select *
From Universe..Locations L
Where LocationFeature.STIsValid() =0
and L.LocationTypeId = 65
and charindex(', India',L.LocationName)>0


Universe..Locations L
where B.Centroid.STWithin(L.LocationFeature)=1
and L.LocationTypeId = 65
and charindex(', India',L.LocationName)>0


*/