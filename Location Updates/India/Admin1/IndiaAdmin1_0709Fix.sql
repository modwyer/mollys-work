/* BACKGROUND
Replace 2 admin 1 polygons that had major boundary issues that were affecting the use of the shapefile. There were 2 separate 
instances of this: Gujarat and Daman and Diu on the west side of the country and Puducherry and Andhra Pradesh on the east side.
There are 2 shapefiles, one with Gujarat and Daman and Diu and the other with Puducherry and Andhra Pradesh. These need to replace 
the current Admin 1 boundaries for Gujuarat, Daman and Diu, Andhra Pradesh and Puducherry.

Puducherry is split into 2 polygons located in different sections along the coast. Only 1 of those polygons was modified but the
shapefile includes all 4 polygons. The one polygon is completely surrounded by Andhra Pradesh. The other 3 do not border Andhra Pradesh.
Daman and Diu is also split into 2 polygons. Both border either the ocean or Gujarat so no other polygons could be effected. In both
locations only the ocean border and the border between the 2 locations in question were modified. No internal boundaries with neighboring 
locations were modified.

No location names were changed in Admin 1 but the location names of Yanam and Gir-Somnath and Mahe Admin 2 will need to be updated to the correct Admin 1 name.
BaseAdminFeatures table will also need to have the Admin 1 ID updated for those admin 2 locations.
*/

/**********************************************************************************************************************/

/*** Create View and data from view ***/
Create table #LocationProcessData (RunId int identity(1,1), LocationName varchar(200), LocationTypeId int, 
			LocationFeature geometry, CentroidFeature geometry, PresentationLocationName varchar(50), Hashkey varchar(10))

INSERT INTO #LocationProcessData (LocationName, LocationTypeId, LocationFeature, CentroidFeature, PresentationLocationName, Hashkey)
	SELECT
	LocationName as [LocationName]
	,LocationTypeId as [LocationTypeId]
	,LocationFeature as [LocationFeature]
	,Centroid as [Centroid]
	,PresentationLocationName as [PresentationLocationName]
	,Hashkey as [HashKey]
	FROM [TestDatabase].[dbo].[vAdmin1Fix_072015]

/*** Update Location features ***/
Print 'Updating locationfeature in Universe'

--Update locationfeature in Universe
Update Universe..Locations 
Set LocationFeature = F.LocationFeature
From Universe..Locations L,#LocationProcessData F
Where l.locationtypeid = 65 
and L.LocationName = F.LocationName

Select * from #LocationProcessData


/*** Make new polys valid ***/
--Check for bad geometries and make valid
Print 'Updating valid geometries in Universe'

Update [Universe].[dbo].[Locations]
set LocationFeature = LocationFeature.MakeValid()
Where LocationTypeId = 65 			
and LocationFeature.STIsValid() = 0	
and LocationName in (
	Select LocationName
	From #LocationProcessData)

Print 'Updating valid geographies in Universe'
Declare @ActiveRunId int = 1
Declare @RunID int = 1, @MaxRunId int
Declare @Shape geometry
Declare @ID int

--drop table #GeographyToCheck
Create table #GeographyToCheck (RunId int identity(1,1), GeoShape geometry, LocationId int)

Print 'Insert to geometry to check'
Insert into #GeographyToCheck (GeoShape,LocationId)
	Select LocationFeature, Locationid
	From Universe..Locations
	Where LocationTypeId = 65 
	and LocationName in (
		Select LocationName
		From #LocationProcessData)
				
Select * from #GeographyToCheck

Select @ActiveRunId = Min(RunId), @MaxRunId = Max(RunId)
From #GeographyToCheck

		Begin Try
			While @ActiveRunId <= @MaxRunId
				BEGIN
					Select @Shape = GeoShape, @ID = LocationId
					From #GeographyToCheck
					Where @RunId = @ActiveRunId

					Print 'Beginning conversion'
					EXEC awmDatabase.dbo.CheckLocationFeatureForValidGeographyAndFixIfNotValid @ID
				
					Set @ActiveRunId = @ActiveRunId + 1
				End
		End Try
		Begin Catch
			Print 'Unable to convert geometry to geography. Error occurred on LocationID ' + @ID
					
		End Catch

/*** Updating ICRISAT DB since it was not updated with the last conversion ***/
Print 'Updating location features in other databases'
Update [ICRISAT_StarCluster_Data].dbo.Locations
Set LocationFeature = U.LocationFeature
From [ICRISAT_StarCluster_Data].dbo.Locations L, Universe..Locations U
Where L.LocationTypeId = 65
and L.LocationName in (
	Select LocationName
	From #LocationProcessData)

/************************************************************************************************************/

/*** Update Base features for Gir-Somnath to point to the correct Admin 1 ***/
Declare @newID int, @oldID int, @Admin2 int

Print 'Update base features for Gir-Somnath'
Select @newID = Locationid
From Universe..Locations
Where Locationtypeid = 65
and locationname like 'Gujarat, India'

Select @oldId = Locationid
From Universe..Locations
Where Locationtypeid = 65
and locationname like 'Daman and Diu, India'

Select @Admin2 = Locationid
From Universe..locations
where locationtypeid = 68
and locationname like 'Gir-Somnath,%'

Print 'Gir-Somnath Admin 1 old ID: ' + cast(@oldID as varchar(10)) + char(13) + 
'Gir-Somnath Admin 1 new ID: ' + cast(@newID as varchar(10)) + char(13) +
'Gir-Somnath Admin 2 ID: ' + cast(@Admin2 as varchar(10))

Update Universe..BaseAdminFeatures
Set Admin1LocationId = @newID
Where Admin0Locationid = -45498
and Admin1locationid = @oldID
and Admin2locationid = @Admin2

Update Universe..geohash6locationslookup
Set Admin1locationid = @newID
Where admin2locationid = @Admin2

/*** Update Base features for Yanam to point to the correct Admin 1 ***/
Declare @newID int, @oldID int, @Admin2 int
Print 'Update base features for Yanam'
Select @newID = Locationid
From Universe..Locations
Where Locationtypeid = 65
and locationname like 'Puducherry, India'

Select @oldId = Locationid
From Universe..Locations
Where Locationtypeid = 65
and locationname like 'Andhra Pradesh, India'

Select @Admin2 = Locationid
From Universe..locations
where locationtypeid = 68
and locationname like 'Yanam, Andhra Pradesh, India'

Print 'Yanam Admin 1 old ID: ' + cast(@oldID as varchar(10)) + char(13) + 
'Yanam Admin 1 new ID: ' + cast(@newID as varchar(10)) + char(13) +
'Yanam Admin 2 ID: ' + cast(@Admin2 as varchar(10))

Update Universe..BaseAdminFeatures
Set Admin1LocationId = @newID
Where Admin0Locationid = -45498
and Admin1locationid = @oldID
and Admin2locationid = @Admin2

Update Universe..geohash6locationslookup
Set Admin1locationid = @newID
Where admin2locationid = @Admin2

/*** Update Base features for Mahe to point to the correct Admin 1 ***/
Declare @newID int, @oldID int, @Admin2 int
Print 'Update base features for Mahe'
Select @newID = Locationid
From Universe..Locations
Where Locationtypeid = 65
and locationname like 'Puducherry, India'

Select @oldId = Locationid
From Universe..Locations
Where Locationtypeid = 65
and locationname like 'Kerala, India'

Select @Admin2 = Locationid
From Universe..locations
where locationtypeid = 68
and locationname like 'Mahe, Kerala, India'

Print 'Mahe Admin 1 old ID: ' + cast(@oldID as varchar(10)) + char(13) + 
'Mahe Admin 1 new ID: ' + cast(@newID as varchar(10)) + char(13) +
'Mahe Admin 2 ID: ' + cast(@Admin2 as varchar(10))

Update Universe..BaseAdminFeatures
Set Admin1LocationId = @newID
Where Admin0Locationid = -45498
and Admin1locationid = @oldID
and Admin2locationid = @Admin2

Update Universe..geohash6locationslookup
Set Admin1locationid = @newID
Where admin2locationid = @Admin2

/*** Update Base features for Daman to point to the correct Admin 1 ***/
Declare @newID int, @oldID int, @Admin2 int
Print 'Update base features for Daman'
Select @newID = Locationid
From Universe..Locations
Where Locationtypeid = 65
and locationname like 'Daman and Diu, India'

Select @oldId = Locationid
From Universe..Locations
Where Locationtypeid = 65
and locationname like 'Gujarat, India'

Select @Admin2 = Locationid
From Universe..locations
where locationtypeid = 68
and locationname like 'Daman, Gujarat, India'

Print 'Daman Admin 1 old ID: ' + cast(@oldID as varchar(10)) + char(13) + 
'Daman Admin 1 new ID: ' + cast(@newID as varchar(10)) + char(13) +
'Daman Admin 2 ID: ' + cast(@Admin2 as varchar(10))

Update Universe..BaseAdminFeatures
Set Admin1LocationId = @newID
Where Admin0Locationid = -45498
and Admin1locationid = @oldID
and Admin2locationid = @Admin2

Update Universe..geohash6locationslookup
Set Admin1locationid = @newID
Where admin2locationid = @Admin2

/******************************************************************************************/
/*** Update Admin 2 location names ***/
--Update Gir-Somnath location and presentation location name to Gujarat
Print 'Update Gir-Somnath location name'
Update Universe..locations
Set LocationName = 'Gir-Somnath, Gujarat, India', PresentationLocationName = 'Gir-Somnath, Gujarat, India'
Where LocationName = 'Gir-Somnath, Daman and Diu, India'

Update WorldBank_starcluster_data..locations
Set LocationName = 'Gir-Somnath, Gujarat, India', PresentationLocationName = 'Gir-Somnath, Gujarat, India'
Where LocationName = 'Gir-Somnath, Daman and Diu, India'

Update Userdata_starcluster_data..locations
Set LocationName = 'Gir-Somnath, Gujarat, India', PresentationLocationName = 'Gir-Somnath, Gujarat, India'
Where LocationName = 'Gir-Somnath, Daman and Diu, India'

Update ICRISAT_starcluster_data..locations
Set LocationName = 'Gir-Somnath, Gujarat, India', PresentationLocationName = 'Gir-Somnath, Gujarat, India'
Where LocationName = 'Gir-Somnath, Daman and Diu, India'

/*** Update Yanam location and presentation location name to Puducherry ***/
Print 'Update Yanam location name'
Update Universe..locations
Set LocationName = 'Yanam, Puducherry, India', PresentationLocationName = 'Yanam, Puducherry, India'
Where LocationName = 'Yanam, Andhra Pradesh, India'

Update WorldBank_starcluster_data..locations
Set LocationName = 'Yanam, Puducherry, India', PresentationLocationName = 'Yanam, Puducherry, India'
Where LocationName = 'Yanam, Andhra Pradesh, India'

Update Userdata_starcluster_data..locations
Set LocationName = 'Yanam, Puducherry, India', PresentationLocationName = 'Yanam, Puducherry, India'
Where LocationName = 'Yanam, Andhra Pradesh, India'

Update ICRISAT_starcluster_data..locations
Set LocationName = 'Yanam, Puducherry, India', PresentationLocationName = 'Yanam, Puducherry, India'
Where LocationName = 'Yanam, Andhra Pradesh, India'

/*** Update Mahe location and presentation location name to Puducherry ***/
Print 'Update Mahe location name'
Update Universe..locations
Set LocationName = 'Mahe, Puducherry, India', PresentationLocationName = 'Mahe, Puducherry, India'
Where LocationName = 'Mahe, Kerala, India'

Update WorldBank_starcluster_data..locations
Set LocationName = 'Mahe, Puducherry, India', PresentationLocationName = 'Mahe, Puducherry, India'
Where LocationName = 'Mahe, Kerala, India'

Update Userdata_starcluster_data..locations
Set LocationName = 'Mahe, Puducherry, India', PresentationLocationName = 'Mahe, Puducherry, India'
Where LocationName = 'Mahe, Kerala, India'

Update ICRISAT_starcluster_data..locations
Set LocationName = 'Mahe, Puducherry, India', PresentationLocationName = 'Mahe, Puducherry, India'
Where LocationName = 'Mahe, Kerala, India'

/*** Update Daman location and presentation location name to Daman and Diu ***/
Print 'Update Daman location name'
Update Universe..locations
Set LocationName = 'Daman, Daman and Diu, India', PresentationLocationName = 'Daman, Daman and Diu, India'
Where LocationName = 'Daman, Gujarat, India'

Update WorldBank_starcluster_data..locations
Set LocationName = 'Daman, Daman and Diu, India', PresentationLocationName = 'Daman, Daman and Diu, India'
Where LocationName = 'Daman, Gujarat, India'

Update Userdata_starcluster_data..locations
Set LocationName = 'Daman, Daman and Diu, India', PresentationLocationName = 'Daman, Daman and Diu, India'
Where LocationName = 'Daman, Gujarat, India'

Update ICRISAT_starcluster_data..locations
Set LocationName = 'Daman, Daman and Diu, India', PresentationLocationName = 'Daman, Daman and Diu, India'
Where LocationName = 'Daman, Gujarat, India'
/************************************************************************************************************/
--test before geohash update
select *
from universe..locations
where locationtypeid = 68
and locationname like 'Gir-Somnath,%'
--91736218 Dev
--92174097 QA

select *
from universe..locations
where locationtypeid = 68
and locationname like 'Yanam,%'
--91736680 Dev
--92174559 QA

select *
from universe..locations
where locationtypeid = 68
and locationname like 'Mahe,%'
--91736384 Dev
--92174263 QA

select *
from universe..locations
where locationtypeid = 68
and locationname like 'Daman,%'
-- Dev
--92174128 QA Diu
--92174023 QA Daman

/*** Run geohash update ***/
--Gujarat
Exec dbo.[4UpdateGeohashesForSinglePolygon] 92173861, 65, 'India', 6
Go --8 min on dev, 19 min on QA

--Daman and Diu
Exec dbo.[4UpdateGeohashesForSinglePolygon] 92173858, 65, 'India', 6
Go --2 min on dev, 3.5 min on QA

--Andhra Pradesh
Exec dbo.[4UpdateGeohashesForSinglePolygon] 92173851, 65, 'India', 6
Go --3 min on dev, 5.5 min on QA

--Puducherry
Exec dbo.[4UpdateGeohashesForSinglePolygon] 92173876, 65, 'India', 6
Go --3 min on dev, 4.5 on QA

--Kerala
Exec dbo.[4UpdateGeohashesForSinglePolygon] 92173867, 65, 'India', 6
Go --2.5 min on dev, 4 min on QA
/*******************************************************************************************************/
/* Clean up LocationRelationships table */
--Delete was used on Dev but was not needed on QA
--Delete
select *
from WorldBank_StarCluster_Data..locationrelationships
where relatedlocationtypeid = 68
and locationid in ( --old location ids
	Select locationid
	from universe..locations
	where locationtypeid = 65
	and CHARINDEX(', India (201',locationname)>0)
and relatedlocationid not in ( --new location ids
	Select locationid
	from universe..locations
	where locationtypeid = 68
	and CHARINDEX(', India (201',locationname)>0)

--Update used on QA but not needed on Dev
--Manually update each location that relates to an incorrect admin 1
--Yanam and Andhra Pradesh to Puducherry
Update WorldBank_StarCluster_Data..locationrelationships
Set locationid = 92173876 --Puducherry
where relatedlocationid = 92174559 --Yanam
and locationid = 92173851 --Andhra Pradesh

--Gir-Somnath and Daman and Diu to Gujarat
Update WorldBank_StarCluster_Data..locationrelationships
Set locationid = 92173861 --Gujarat
where relatedlocationid = 92174097 --Gir-Somnath
and locationid = 92173858 --Daman and Diu

--Daman and Gujarat to Daman and Diu
Update WorldBank_StarCluster_Data..locationrelationships
Set locationid = 92173858 --Daman and diu
where relatedlocationid = 92174023 --Daman
and locationid = 92173861 --Gujarat

--Mahe and Kerala to Puducherry
Update WorldBank_StarCluster_Data..locationrelationships
Set locationid = 92173876 --Puducherry
where relatedlocationid = 92174263 --Mahe
and locationid = 92173867 --Kerala
/******************************************************************************************************/
--Select * from universe..locations where locationtypeid = 65 and locationname like 'Kerala, India'
/*** Spatial checks - Use as necessary ***/
Declare @Admin1 geometry
Print 'Checking for Andhra Pradesh. Should contain 14 rows.'
Select @Admin1 = LocationFeature
From Universe..Locations
Where Locationtypeid = 65
and locationname like 'Andhra Pradesh, India'

Select @Admin1

Select *
From Universe..Locations
Where Locationtypeid = 68
and Locationfeature.STIntersects(@Admin1) = 1
and locationname like '%Andhra Pradesh, India'

/****/
Declare @Admin1 geometry
Print 'Checking for Puducherry. Should contain 4 rows.'
Select @Admin1 = LocationFeature
From Universe..Locations
Where Locationtypeid = 65
and locationname like 'Puducherry, India'

Select @Admin1

Select *
From Universe..Locations
Where Locationtypeid = 68
and Locationfeature.STIntersects(@Admin1) = 1
and locationname like '%Puducherry, India'

/****/
Declare @Admin1 geometry
Print 'Checking for Gujarat. Should contain 33 rows.'
Select @Admin1 = LocationFeature
From Universe..Locations
Where Locationtypeid = 65
and locationname like 'Gujarat, India'

Select @Admin1

Select *
From Universe..Locations
Where Locationtypeid = 68
and Locationfeature.STIntersects(@Admin1) = 1
and locationname like '%Gujarat, India'

/****/
Declare @Admin1 geometry
Print 'Checking for Daman and Diu. Should contain 2 rows.'
Select @Admin1 = LocationFeature
From Universe..Locations
Where Locationtypeid = 65
and locationname like 'Daman and Diu, India'

Select @Admin1 

Select *
From Universe..Locations
Where Locationtypeid = 68
and Locationfeature.STIntersects(@Admin1) = 1
and locationname like '%Daman and Diu, India'

/****/
Declare @Admin1 geometry
Print 'Checking for Kerala. Should contain 14 rows.'
Select @Admin1 = LocationFeature
From Universe..Locations
Where Locationtypeid = 65
and locationname like 'Kerala, India'

Select @Admin1 

Select *
From Universe..Locations
Where Locationtypeid = 68
and Locationfeature.STIntersects(@Admin1) = 1
and locationname like '%Kerala, India'


