

Create view vIndiaBaseAdmins
as
Select B.BaseAdminLocationId
	,B.BaseAdminLevelId
	,BaseFeature
	,Admin0LocationId
	,Admin1LocationId
	,Admin2LocationId
	,Admin3LocationId
	,case when B.BaseFeature.MakeValid().STCentroid() is null then B.BaseFeature.MakeValid().STEnvelope().STCentroid() else B.BaseFeature.MakeValid().STCentroid() end as Centroid
From Universe..BaseAdminFeatures B
where Admin0LocationId = -45498
