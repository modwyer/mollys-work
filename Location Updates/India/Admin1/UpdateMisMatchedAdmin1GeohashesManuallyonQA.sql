/**** Script to Update Geohash table where admin 1 is incorrect. QA ****/
--Created: 6/9/15
--Author: Molly O'Dwyer

Declare @Admin1 int
Declare @Admin2 int
Declare @Admin0 int = -45498 --change this to the country being worked on
Declare @newAdmin1 int
Declare @ActiveRunId int, @MaxRunId int	
Declare @LocationName varchar(50)

Create table #IdUpdates (RunId int identity(1,1), Locationname varchar(50), Admin1Locationid int, Admin2Locationid int, newAdmin1Locationid int)

--values are formatted in Excel and pasted into the value area
Insert into #IdUpdates (Locationname, Admin1Locationid, Admin2Locationid, newAdmin1Locationid)
Values ('Thiruvallur, Tamil Nadu, India', '92173851', '92174497', '92173880'), 
('Sindhudurg, Maharashtra, India', '92173860', '92174451', '92173870'), 
('Gir-Somnath, Daman and Diu, India', '92173861', '92174097', '92173858'), 
('Uttarkashi, Uttarakhand, India', '92173863', '92174531', '92173884'), 
('Srikakulam, Andhra Pradesh, India', '92173875', '92174478', '92173851'), 
('Kannur, Kerala, India', '92173876', '92174180', '92173867'), 
('Nagapattinam, Tamil Nadu, India', '92173876', '92174304', '92173880'), 
('Viluppuram, Tamil Nadu, India', '92173876', '92174538', '92173880'), 
('Karimganj, Assam, India', '92173882', '92174189', '92173853')

Select @MaxRunId = max(RunId), @ActiveRunId = Min(RunId)
From #IdUpdates

--Run through each location ID and change admin 1 to new Admin 1 from the table
While @ActiveRunId <=@MaxRunId
	Begin

		Select @Admin1 = Admin1Locationid, @Admin2 = Admin2Locationid, @newAdmin1 = newAdmin1Locationid, @LocationName = Locationname
		From #IdUpdates
		Where RunId = @ActiveRunId

		Update Universe..Geohash6locationslookup
		Set Admin1LocationId = @newAdmin1
		Where Admin0LocationId = @Admin0
		and Admin1LocationId = @Admin1
		and Admin2LocationId = @Admin2

		Print('Admin 1 for location ' + @LocationName + ' has been updated.')
		Print ('Old Admin 1: ' + cast(@Admin1 as varchar(10)) + char(13) + 'New Admin 1: ' + cast(@newAdmin1 as varchar(10)))

		Set @ActiveRunId = @ActiveRunid + 1
	End

Drop table #IdUpdates