/**** Script to Update Geohash table where admin 1 is incorrect. QA ****/
--Created: 6/9/15
--Author: Molly O'Dwyer

Declare @Admin1 int
Declare @Admin2 int
Declare @Admin0 int = -45498 --change this to the country being worked on
Declare @newAdmin int
Declare @ActiveRunId int, @MaxRunId int	
Declare @LocationName varchar(50)

Create table #IdUpdates (RunId int identity(1,1), Locationname varchar(50), Admin1Locationid int, Admin2Locationid int, newAdminLocationid int)

--values are formatted in Excel and pasted into the value area
Insert into #IdUpdates (Locationname, Admin1Locationid, Admin2Locationid, newAdminLocationid)
Values ('North & Middle Andaman, Andaman and Nicobar, India', '92173850', '0', '92174327')

Select @MaxRunId = max(RunId), @ActiveRunId = Min(RunId)
From #IdUpdates

--Run through each location ID and change admin 1 to new Admin 1 from the table
While @ActiveRunId <=@MaxRunId
	Begin

		Select @Admin1 = Admin1Locationid, @Admin2 = Admin2Locationid, @newAdmin = newAdminLocationid, @LocationName = Locationname
		From #IdUpdates
		Where RunId = @ActiveRunId

		Update Universe..Geohash6locationslookup
		Set Admin2LocationId = @newAdmin
		Where Admin0LocationId = @Admin0
		and Admin1LocationId = @Admin1
		and Admin2LocationId is null

		Print('Admin 1 for location ' + @LocationName + ' has been updated.')
		Print ('Old Admin 1: ' + cast(@Admin1 as varchar(10)) + char(13) + 'New Admin 1: ' + cast(@newAdmin as varchar(10)))

		Set @ActiveRunId = @ActiveRunid + 1
	End

Drop table #IdUpdates