USE [TestDatabase]
GO

/****** Object:  View [dbo].[vIndiaSurroundingCountryShape]    Script Date: 02/13/2015 17:17:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




Alter view [dbo].[vIndiaSurroundingCountryShape]
as
	(Select LocationId,LocationName,LocationTypeId, LocationFeature, Hashkey
	from Universe.dbo.Locations
	Where LocationTypeId = 65
	and (CHARINDEX(', Burma', LocationName)>0
	or charindex(', Bhutan',LocationName)>0
	or CHARINDEX(', Bangladesh',LocationName)>0
	or CHARINDEX(', China',LocationName)>0
	or CHARINDEX(', Nepal',LocationName)>0
	or CHARINDEX(', Pakistan',LocationName)>0))


GO


