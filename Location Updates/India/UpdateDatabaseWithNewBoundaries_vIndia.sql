USE [TestDatabase]
GO

/****** Object:  StoredProcedure [dbo].[UpdateDatabaseWithNewBoundaries_vIndia]    Script Date: 5/15/2015 10:09:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		Molly O'Dwyer
-- Create date: 12/23/2014
-- Update date: 2/17/15
-- Description:	Wrapper Procedure around the procedures that update boundaries with new features
/*
SET NOCOUNT ON;

--Get list of regions from a list. Can view the Spatial results if needed to help determine region of desired country.
Select LocationId, LocationName, LocationFeature
	From Universe.dbo.Locations
	Where LocationTypeId = 25

--Get list of countinents from a list. Can view the Spatial results if needed to help determine region of desired country.
Select LocationId, LocationName, LocationFeature
	From Universe.dbo.Locations
	Where LocationTypeId = 26	

Declare @LocationProcessData LocationProcessData

INSERT INTO @LocationProcessData
(LocationName, LocationTypeId, LocationFeature, CentroidFeature, PresentationLocationName, Hashkey)
SELECT
	LocationName as [LocationName]
	,LocationTypeId as [LocationTypeId]
	,LocationFeature as [LocationFeature]
	,Centroid as [Centroid]
	,PresentationLocationName as [PresentationLocationName]
	,'India-' + cast(ID as varchar(10)) as [HashKey]
  FROM [TestDatabase].[dbo].[vAdmin2India]

--Select * from @LocationProcessData 

EXEC UpdateDatabaseWithNewBoundaries_vIndia 65, 'India', 'Southern Asia', 'Asia', '2015', @LocationProcessData 

*/
-- =============================================
alter PROCEDURE [dbo].[UpdateDatabaseWithNewBoundaries_vIndia]

	@LocationTypeId smallint,
	@CountryName varchar(250),
	@RegionName varchar(250),
	@ContinentName varchar(250),
	@SuffixToAddToExistingFeatures varchar(20),
	@AdminBoundariesToProcess dbo.LocationProcessData READONLY

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Declare @CountryLocationId int
	Declare @StartTime as datetime2(7)
	Declare @EndTime as datetime2(7)
	
	Print 'Beginning Location Boundary Update'
	Set @StartTime = SYSDATETIME()
	Print ('Start time is: ' + cast(@StartTime as varchar(19)))
	
	Select @CountryLocationId = LocationId
	From Universe.dbo.Locations
	Where LocationName = @CountryName
	and LocationTypeId = 23
	
	Print 'Starting Section 2A: Update existing boundaries and insert new boundaries'
	
	EXECUTE [2aUpdateExistingAndInsertNewAdminBoundaries_vIndia] 
	   @LocationTypeId
	  ,@CountryName
	  ,@SuffixToAddToExistingFeatures
	  ,@AdminBoundariesToProcess 
	
	Print 'Starting Section 2B:Update other databases with new locations'
	--Update other databases with new boundaries
	Execute [2bUpdateOtherDatabasesForLocations_vIndia]
		@LocationTypeId,
		@CountryName
		

	Print 'Section 2 complete'
	Print 'Starting Section 3A: Update Base Admins'
	
	EXECUTE [3aUpdateBaseFeaturesAfterBoundaryUpdate_vIndia] 
	   @LocationTypeId
	  ,@CountryName
	  ,@RegionName
	  ,@ContinentName

	Print 'Section 3 complete. Please run the geohash update.'
	--Print 'Starting Section 4: Update Geohashes'
	
	--EXECUTE [4UpdateGeohashes_LoopBaseFeaturesFaster] 
	--   @CountryLocationId
	 
	--Print 'Section 4 complete'
	Print ('Updated completed at: ' + cast(@EndTime as varchar(19)))

END







GO


