USE [TestDatabase]
GO

/****** Object:  StoredProcedure [dbo].[4UpdateGeohashes_LoopBaseFeaturesFasterv2_vIndia]    Script Date: 5/15/2015 11:45:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		Molly O'Dwyer/Dan Allen
-- Create date: 2/4/2015

-- Description: 

/*
Updates:

3/04/2015 DJA Added in some code to facilitate subsetting of geohash6 features prior to spatial search methods
4/24/15 MLO Added in old code for buffering geohashes outside the polygons.

Select *
From universe..locations
Where LocationId = -45498
Exec dbo.[4UpdateGeohashes_LoopBaseFeaturesFasterv2_vIndia] -45498, 65, 'India'
*/
-- =============================================
alter PROCEDURE [dbo].[4UpdateGeohashes_LoopBaseFeaturesFasterv2_vIndia]
	-- Add the parameters for the stored procedure here
	
	@CountryLocationId int,
	@LocationTypeId int,
	@CountryName varchar(40)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare @Env Geometry

--This is to get the bounding box to put into the spatial indexes below
Declare @MinLong as int
Declare @MaxLong as int
Declare @MinLat as int
Declare @MaxLat as int

Declare @Geohash6 char(6), @Shape geometry, @ActiveGeohash char(6) 
Declare @ActiveRunId int, @MaxRunId int	

--Declare @Admin0 int
--Declare @Admin1 int
--Declare @Admin2 int

Declare @StartTime as datetime2(7)
Declare @EndTime as datetime2(7)

Declare @PrintStr varchar(10)
Declare @strSQL varchar(2000)
Declare @CountryShape geometry

--Declare @HashLoops Table (RunId int identity(1,1), MinGeohash char(6), MaxGeohash char(6))
--Declare @Neighbors Table (Neighbor char(3))
--Declare @RC int, @Iteration int
--Declare @Corners Table (Geohash3 char(3))
--Declare @GeohashC char(6)


Set @StartTime = SYSDATETIME()
print ('Start time is: ' + cast(@StartTime as varchar(19)))
--create table to hold geohashes within the bounding box
print 'Creating table #CountrySpecificGeohashes'

Create Table #CountrySpecificGeohashes 
	(RunId int identity(1,1), Geohash6 char(6), GeoShape geometry, 
	Location0 int, Location1 int, Location2 int, primary key (Geohash6));
	Create Table #NeedToBufferPoints (RunId int identity(1,1), Geohash6 char(6), Shape Geometry, primary key (RunId));
Create Table #AreaThatIntersectsBuffer (Geohash6 char(6), AdminLocationId int, Area float);
Create Table #UpdateForGeohashAdmin (Geohash6 char(6), AdminLocationId int);

--CREATE TABLE #BaseAdminFeatures
--(   RunId int identity(1,1),
--	[BaseAdminLocationId] [int] NOT NULL,
--	[BaseFeature] [geometry] NOT NULL,
--	[Admin0LocationId] [int] NOT NULL,
--	[Admin1LocationId] [int] NULL,
--	[Admin2LocationId] [int] NULL,
--PRIMARY KEY CLUSTERED 
--(
--	RunId ASC
--)
--); 


--Get country parameter
--Get country shape
--Get country envelope
Select @Env = LocationFeature.STEnvelope(), @CountryShape = LocationFeature
From Universe.dbo.Locations
Where LocationId = @CountryLocationId

print 'Getting bounding box for country'

--Get the bounding box around the country LocationId that was provided as parameter
Select @MinLong = floor(Min(Longitude)), @MaxLong = ceiling(Max(longitude)), 
	@MinLat = floor(Min(Latitude)), @MaxLat = ceiling(Max(Latitude))
	From 
		(
		Select @Env.STPointN(1).STX as Longitude, @Env.STPointN(1).STY as Latitude
		UNION ALL
		Select @Env.STPointN(2).STX as Longitude, @Env.STPointN(2).STY as Latitude
		UNION ALL
		Select @Env.STPointN(3).STX as Longitude, @Env.STPointN(3).STY as Latitude
		UNION ALL
		Select @Env.STPointN(4).STX as Longitude, @Env.STPointN(4).STY as Latitude
		) i

print ('Max Lat: ' + cast(@MaxLat as varchar(50)))
print ('Min Lat: ' + cast(@MinLat as varchar(50)))
print ('Max Long: ' + cast(@MaxLong as varchar(50)))
print ('Min Long: ' + cast(@MinLong as varchar(50)))
print 'Inserting geohashes within the bounds into #CountrySpecificGeohashes'
--Get all geohashes within that envelope (or intersect)
--insert those selected geohashes into the temp table	

	set @strSQL = 
	'CREATE SPATIAL INDEX #SPX_CountrySpecificGeohashes 
	ON #CountrySpecificGeohashes
	([geoshape])
	USING  GEOMETRY_GRID 
	WITH (
	bounding_box = ('+ cast(@MinLong as varchar(4)) + ', ' +  cast(@MinLat as varchar(4)) + ', ' +  cast(@MaxLong as varchar(4)) + ', ' + cast(@MaxLat as varchar(4)) + '), '
	+ ' GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
	CELLS_PER_OBJECT = 16);'
	
	
	exec(@strSQL)
	print ('Spatial Index #SPX_CountrySpecificGeohashes Script: ' + char(13) + @strSQL)

	--set @strSQL = 
	--'CREATE SPATIAL INDEX #SPX_BaseAdminFeatures ON #BaseAdminFeatures
	--([BaseFeature])
	--USING  GEOMETRY_GRID 
	--WITH (
	--bounding_box = ('+ cast(@MinLong as varchar(4)) + ', ' +  cast(@MinLat as varchar(4)) + ', ' +  cast(@MaxLong as varchar(4)) + ', ' + cast(@MaxLat as varchar(4)) + '), '
	--+ ' GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
	--CELLS_PER_OBJECT = 16);'
	
	--exec(@strSQL)
	--print ('Spatial Index #SPX_BaseAdminFeatures Script: ' + char(13) + @strSQL)

	--create spatial index
	set @strSQL = 
	'CREATE SPATIAL INDEX #SPX_Buffer ON #NeedToBufferPoints
	([Shape])
	USING GEOMETRY_GRID 
	WITH (
	bounding_box = ('+ cast(@MinLong as varchar(4)) + ', ' +  cast(@MinLat as varchar(4)) + ', ' +  cast(@MaxLong as varchar(4)) + ', ' + cast(@MaxLat as varchar(4)) + '), '
	+ ' GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), 
	CELLS_PER_OBJECT = 16);'

	exec(@strSQL)
	print ('Spatial Index #SPX_Buffer Script: ' + char(13) + @strSQL)

Insert into #CountrySpecificGeohashes
	(Geohash6, GeoShape)
	Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326)
	From [Universe].[dbo].[GeoHash6LocationsLookup]
	Where Latitude1km between @MinLat and @MaxLat
	and Longitude1km between @MinLong and @MaxLong

/*
SELECT @Env = [awmDatabase].[dbo].[GetGridCellShapeForBoundingBox] (
   @MaxLat
  ,@MinLat
  ,@MaxLong
  ,@MinLong)	
 
  
INSERT INTO #BaseAdminFeatures
	(BaseAdminLocationId
	  ,[BaseFeature]
      ,[Admin0LocationId]
      ,[Admin1LocationId]
      ,[Admin2LocationId])
	
SELECT BaseAdminLocationId
	  ,[BaseFeature]
      ,[Admin0LocationId]
      ,[Admin1LocationId]
      ,[Admin2LocationId]
  FROM [Universe].[dbo].[BaseAdminFeatures]
WHERE BaseFeature.STIntersects(@Env) = 1
and Admin0LocationId in (
select LocationId
From Universe.dbo.Locations 
Where LocationTypeId = 23
and LocationFeature.STIntersects(@CountryShape) = 1
)


Declare @Count int

Select @Count = Count(*)
From #BaseAdminFeatures

print @Count

--Loop through all base admin features 
Select @ActiveRunId = Min(RunId), @MaxRunId = Max(RunId)
From #BaseAdminFeatures

print ('Max Run Id: ' + cast(@MaxRunId as varchar(1000)))

Set @StartTime = SYSDATETIME()
print ('Start time for the loop through is: ' + cast(@StartTime as varchar(19)))

While @ActiveRunId <= @MaxRunId
		
	BEGIN
	
		Select @Shape = B.BaseFeature, @Admin0 = Admin0LocationId, 
		@Admin1 = Admin1LocationId, @Admin2 = Admin2LocationId
		From #BaseAdminFeatures B
		Where B.RunId = @ActiveRunId
		
		--DJA Added in code to help facilitating faster subsetting of geohashes for active shape
		select @Env = @Shape.STEnvelope()
		
		DELETE FROM @Corners
		INSERT INTO @Corners
		(Geohash3)
		Select Universe.dbo.Geohash_Encode(@Env.STPointN(1).STY,@Env.STPointN(1).STX, 3) 
		UNION ALL
		Select Universe.dbo.Geohash_Encode(@Env.STPointN(2).STY,@Env.STPointN(2).STX, 3) 
		UNION ALL
		Select Universe.dbo.Geohash_Encode(@Env.STPointN(3).STY,@Env.STPointN(3).STX, 3)
		UNION ALL 
		Select Universe.dbo.Geohash_Encode(@Env.STPointN(4).STY,@Env.STPointN(4).STX, 3) 
				
		
		Select @GeohashC = Universe.dbo.Geohash_Encode(@Env.STCentroid().STY,@Env.STCentroid().STX, 6) 		

		DELETE FROM @Neighbors
		INSERT INTO @Neighbors
		(Neighbor)
		SELECT * 
		FROM [awmDatabase].[dbo].[GetExtendedNeighbors] (left(@GeohashC,3), 0)
		
		Select @RC = COUNT(*)
		From @Corners
		Where Geohash3 not in (Select Neighbor from @Neighbors)
		
		set @Iteration = 1
		
		While @RC > 0 
		BEGIN
			INSERT INTO @Neighbors
			(Neighbor)
			SELECT * 
			FROM [awmDatabase].[dbo].[GetExtendedNeighbors] (left(@GeohashC,3), @Iteration)	
			
			Select @RC = COUNT(*)
			From @Corners
			Where Geohash3 not in (Select Neighbor from @Neighbors)
			
			set @Iteration = @Iteration + 1	
		END
		
		DELETE FROM @HashLoops
		INSERT INTO @HashLoops
		(MinGeohash, MaxGeohash)
		SELECT Neighbor + '000', Neighbor + 'zzz' 
		FROM @Neighbors 
		
		
		Update [Universe].[dbo].[GeoHash6LocationsLookup]
		Set 
			Admin0LocationId = @Admin0,
			Admin1LocationId = @Admin1,
			Admin2LocationId = @Admin2
		Where Geohash6 in ( 
			Select Geohash6
			From #CountrySpecificGeohashes G, 
				@Hashloops H
				Where G.Geohash6 between H.MinGeohash and H.MaxGeohash
				and GeoShape.STWithin(@Shape) = 1
			)		
	
		print ('Current Active Run ID: ' + cast(@ActiveRunId as varchar(20)))
			
		set @ActiveRunId = @ActiveRunId + 1	
	End
*/	
--Buffer the geohash point so that we can intersect it with features (since
--the point was not within a feature
--Get geohashes not within the polygons. These will be buffered
Insert into #NeedToBufferPoints
(Geohash6, Shape)
Select Geohash6, Geometry::STGeomFromText(ShapeWKT,4326).STBuffer(0.5)
From [Universe].[dbo].[GeoHash6LocationsLookup]
Where Admin0LocationId = @CountryLocationId 
and (case when @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in 
	(Select LocationId 
	from Universe.dbo.Locations 
	Where LocationTypeId = @LocationTypeId
	and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-') 
		
Select @PrintStr = (Select count(*) from #NeedToBufferPoints)
print ('Count of geohashes not in a feature that will be buffered: ' + @PrintStr)


--Get the intersection of the buffered geohash point with the admin2 polygon feature
Truncate Table 	#AreaThatIntersectsBuffer
INSERT INTO #AreaThatIntersectsBuffer
(Geohash6, AdminLocationId, Area)
Select C.Geohash6, C.Location2, C.GeoShape.STIntersection(B.Shape).STArea()
From #CountrySpecificGeohashes C, #NeedToBufferPoints B
Where C.GeoShape.STIntersects(B.Shape) = 1

--Extend it one more time to see if we can capture them
Declare @CountOfRemaining int = 1
Declare @ActiveIteration int = 1, @MaxIterations int = 5
Declare @BufferSize float = 0.1
       
Select @CountOfRemaining = count(*)
From [Universe].[dbo].[GeoHash6LocationsLookup]
Where Admin0LocationId = @CountryLocationId
and (case when @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in        
        (Select LocationId
        from Universe.dbo.Locations
        Where LocationTypeId = @LocationTypeId
        and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-')                      
       
print ('Count of Remaining geohashes: ' + cast(@CountOfRemaining as varchar(100)))
       
While @CountOfRemaining > 0 AND @ActiveIteration < @MaxIterations
BEGIN
			
        Select @CountOfRemaining = count(*)
        From [Universe].[dbo].[GeoHash6LocationsLookup]
        Where Admin0LocationId = @CountryLocationId
        and (case when @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in         
            (Select LocationId
            from Universe.dbo.Locations
            Where LocationTypeId = @LocationTypeId
            and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-')          
               
        print ('Geohashes now remaining: ' + cast(@CountOfRemaining as varchar(100)))
                   
        INSERT INTO #AreaThatIntersectsBuffer
        Select C.Geohash6, C.Location2, C.GeoShape.STIntersection(B.Shape.STBuffer(@BufferSize)).STArea()
        From #CountrySpecificGeohashes C, #NeedToBufferPoints B
        Where C.GeoShape.STIntersects(B.Shape) = 0
        and C.GeoShape.STIntersects(B.Shape.STBuffer(@BufferSize)) = 1
        and C.Geohash6 not in (Select Distinct Geohash6 From #AreaThatIntersectsBuffer Where Area > 0)
             
        print ('Current Buffer size: ' + cast(@BufferSize as varchar(100)))
        print ('Active Iteration: ' + cast(@ActiveIteration as varchar(100)))
             
	--Get the max area for each of the intersections and then associate
	--the geohash with the locationId that has that max
	INSERT INTO #UpdateForGeohashAdmin
	(Geohash6, AdminLocationId)
	Select B.Geohash6, B.AdminLocationId
	From #AreaThatIntersectsBuffer B,	
		(
		Select Geohash6, Max(Area) as MaxArea
		From #AreaThatIntersectsBuffer
		Group by Geohash6
		) I
	Where B.Geohash6 = I.Geohash6
	and B.Area = I.MaxArea
	order by B.Geohash6, B.area desc

	--Select @PrintStr = (Select * from #UpdateForGeohashAdmin)
	--print ('Geohashes in the intersection: ' + @PrintStr)

	--Update those pesky geohashs that were not contained by the new features
	if @LocationTypeId = 65
	BEGIN
		Update [Universe].[dbo].[GeoHash6LocationsLookup]
		Set Admin1LocationId = U.AdminLocationId
		From 
			[Universe].[dbo].[GeoHash6LocationsLookup] G,
			#UpdateForGeohashAdmin U
		Where G.Geohash6 = U.Geohash6
			and G.Admin0LocationId = @CountryLocationId
	END	
			
	if @LocationTypeId = 68
	BEGIN
		Update [Universe].[dbo].[GeoHash6LocationsLookup]
		Set Admin2LocationId = U.AdminLocationId
		From 
			[Universe].[dbo].[GeoHash6LocationsLookup] G,
			#UpdateForGeohashAdmin U
		Where G.Geohash6 = U.Geohash6
			and G.Admin0LocationId = @CountryLocationId
	END
		
	Select @PrintStr = (Select count(*)
			From Universe.dbo.GeoHash6LocationsLookup
			Where Admin0LocationId = @CountryLocationId 
			and (case when + @LocationTypeId = 68 then Admin2LocationId else Admin1LocationId end) not in 
				(Select LocationId 
				from Universe.dbo.Locations 
				Where LocationTypeId = 68 and Left(Hashkey, LEN(@CountryName + '-')) = @CountryName + '-')) 
	print ('Remaining geohashes: ' + @PrintStr)
             
			set @BufferSize = @BufferSize + .5
			set @ActiveIteration = @ActiveIteration + 1
	END  

	Set @EndTime = SYSDATETIME()
	print ('End time is: ' + cast(@EndTime as varchar(19)))
End




GO


