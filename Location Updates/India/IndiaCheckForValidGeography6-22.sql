--Check for valid geography for base admin and admin 1
Declare @Geom Geometry
	Declare @Valid bit
	Declare @HashkeyLength int = 6
	Declare @CountryName varchar(50) = 'India'
	Declare @LocationTypeId int = 65
	Declare @Admin1 int
	Declare @LocationId int
	Declare @MaxID int
	Declare @MinID int = 1
	Declare @ActiveRunID int

	Create table #MissingPolys (RunId int identity(1,1), LocationId int, LocationFeature geometry, Admin1Locationid int)

	Insert into #MissingPolys (LocationId, LocationFeature, Admin1Locationid)
	Select BaseAdminLocationid, BaseFeature, Admin1locationid
	From Universe..BaseAdminFeatures
	Where BaseAdminlocationid in ('92446784', '92446785', '92446786', '92446787', '92446768', '92446788', '92446760', '92446759', '92446778', 
		'92446791', '92446789', '92446790', '92446792', '92446793', '92446761', '92446762', '92446763', '92446764', '92446765', 
		'92446766', '92446767', '92446769', '92446770', '92446775', '92446776', '92446777', '92446779', '92446780', '92446781', 
		'92446782', '92446783', '92446774', '92446771', '92446772', '92446773', '92446794')

	Select @MaxID = max(Runid), @MinId = min(RunId)
	From #MissingPolys

	Set @ActiveRunID = @MinId

	While @ActiveRunID <= @MaxId
		Begin	
			Print ('Active ID: ' + cast(@ActiveRunId as varchar(10)))
			Select @Geom = LocationFeature, @LocationId = Locationid, @Admin1 = Admin1locationid
			From #MissingPolys
			Where @ActiveRunid = Runid

			SELECT @Valid = [awmDatabase].[dbo].[IsValidGeographyFromGeometry] (@Geom)

			Print ('Location ID: ' + cast(@Locationid as varchar(10))+ char(13) + 'Valid geometry (1= valid, 0= not valid): ' + cast(@Valid as varchar(10)))
			
			Select @Geom = LocationFeature
			From Universe..Locations
			Where @Admin1 = Locationid
			
			SELECT @Valid = [awmDatabase].[dbo].[IsValidGeographyFromGeometry] (@Geom)
			
			Print ('Admin 1 ID: ' + cast(@Admin1 as varchar(10)) + char(13) + 'Valid geometry (1= valid, 0= not valid): ' + cast(@Valid as varchar(10)))

			Set @ActiveRunId = @ActiveRunId + 1
		End

	Drop table #MissingPolys

--Check for valid geography for locations table admin 2
Declare @Geom Geometry
	Declare @Valid bit
	Declare @HashkeyLength int = 6
	Declare @CountryName varchar(50) = 'India'
	Declare @LocationTypeId int = 65
	Declare @LocationName varchar(50)
	Declare @LocationId int
	Declare @MaxID int
	Declare @MinID int = 1
	Declare @ActiveRunID int

	Create table #MissingPolys (RunId int identity(1,1), LocationId int, LocationFeature geometry, LocationName varchar(50))

	Insert into #MissingPolys (LocationId, LocationFeature, LocationName)
	Select Locationid, LocationFeature, LocationName
	From Universe..Locations
	Where locationid in ('92446784', '92446785', '92446786', '92446787', '92446768', '92446788', '92446760', '92446759', '92446778', 
		'92446791', '92446789', '92446790', '92446792', '92446793', '92446761', '92446762', '92446763', '92446764', '92446765', 
		'92446766', '92446767', '92446769', '92446770', '92446775', '92446776', '92446777', '92446779', '92446780', '92446781', 
		'92446782', '92446783', '92446774', '92446771', '92446772', '92446773', '92446794')

	Select @MaxID = max(Runid), @MinId = min(RunId)
	From #MissingPolys

	Set @ActiveRunID = @MinId

	While @ActiveRunID <= @MaxId
		Begin	
			Print ('Active ID: ' + cast(@ActiveRunId as varchar(10)))
			Select @Geom = LocationFeature, @LocationName = LocationName, @LocationId = Locationid
			From #MissingPolys
			Where @ActiveRunid = Runid

			SELECT @Valid = [awmDatabase].[dbo].[IsValidGeographyFromGeometry] (@Geom)

			Print ('Location ID: ' + cast(@Locationid as varchar(10))+ char(13) + 'Location name: ' + @LocationName + char(13) + 'Valid geometry (1= valid, 0= not valid): ' + cast(@Valid as varchar(10)))

			Set @ActiveRunId = @ActiveRunId + 1
		End

	Drop table #MissingPolys

--Check for valid geometry in Locations
Select *
From [Universe].[dbo].[Locations]
Where locationid in ('92446784', '92446785', '92446786', '92446787', '92446768', '92446788', '92446760', '92446759', '92446778', 
'92446791', '92446789', '92446790', '92446792', '92446793', '92446761', '92446762', '92446763', '92446764', '92446765', '92446766', 
'92446767', '92446769', '92446770', '92446775', '92446776', '92446777', '92446779', '92446780', '92446781', '92446782', '92446783', 
'92446774', '92446771', '92446772', '92446773', '92446794')
and LocationFeature.STIsValid() = 0	

--Check for invalid geometry in Base admin features
Select *
From [Universe].[dbo].Baseadminfeatures
Where BaseAdminLocationId in ('92446784', '92446785', '92446786', '92446787', '92446768', '92446788', '92446760', '92446759', '92446778', 
'92446791', '92446789', '92446790', '92446792', '92446793', '92446761', '92446762', '92446763', '92446764', '92446765', '92446766', 
'92446767', '92446769', '92446770', '92446775', '92446776', '92446777', '92446779', '92446780', '92446781', '92446782', '92446783', 
'92446774', '92446771', '92446772', '92446773', '92446794')
and BaseFeature.STIsValid() = 0	