use universe

Select distinct l.locationname, b.admin1locationid--, Geometry::STGeomFromText(ShapeWKT,4326)--, b.admin2locationid, l.locationname
From Universe..geohash6locationslookup b
inner join
Universe..Locations l
on b.Admin1locationid = l.locationid
Where Admin0LocationId = -45498
--and charindex(' (2014)',l.locationname)=0
order by l.locationname--b.admin1locationid
--and charindex('(2015)', l.locationname)=0

--set admin1locationid to null where the id points to an old location; also set associated admin2locationid to null
Update Universe..Geohash6LocationsLookup
Set Admin1LocationId = null, Admin2LocationId = null, Admin3LocationId = null
--Select l.locationname, g.* from Geohash6LocationsLookup g
--inner join
--Locations l
--on g.admin1locationid = l.locationid
Where Admin0Locationid = -45498
 and Admin1locationid in (
	Select Locationid
	From Universe..Locations
	Where Locationtypeid = 65
	and CHARINDEX(', India (',locationname)>0
	--and CHARINDEX(', India (', locationname)=0
	)


--set admin1locationid to null where the id points to an old location; also set associated admin2locationid to null
Update Universe..Geohash6LocationsLookup
Set Admin1LocationId = null, Admin2LocationId = null, Admin3LocationId = null
--Select l.locationname, g.* from Geohash6LocationsLookup g
--inner join
--Locations l
--on g.admin2locationid = l.locationid
Where Admin0Locationid = -45498
 and Admin2locationid in (
	Select Locationid
	From Universe..Locations
	Where Locationtypeid = 68
	and CHARINDEX(', India (',locationname)>0
	)



Select distinct l.locationname, b.admin1locationid, b.admin2locationid--, Geometry::STGeomFromText(ShapeWKT,4326)--, l.locationname
From Universe..baseadminfeatures b
inner join
Universe..Locations l
on b.Admin2locationid = l.locationid
Where Admin0LocationId = -45498
--and charindex(' (2014)',l.locationname)=0
order by b.admin2locationid