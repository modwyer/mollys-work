
--Make invalid geometries valid for India admin 2
Update TestDatabase..IndiaAdmin2
SET Shape = Geometry::STGeomFromText(awmDatabase.dbo.MakeValidGeographyFromGeometry(Shape).ToString(),4326)
--Select * From TestDatabase..IndiaAdmin2
Where awmDatabase.dbo.IsValidGeographyFromGeometry(Shape) = 0

--Make invalid geometries valid for India admin 1
Update TestDatabase..IndiaAdmin1
SET Shape = Geometry::STGeomFromText(awmDatabase.dbo.MakeValidGeographyFromGeometry(Shape).ToString(),4326)
--Select * From TestDatabase..IndiaAdmin1
Where awmDatabase.dbo.IsValidGeographyFromGeometry(Shape) = 0