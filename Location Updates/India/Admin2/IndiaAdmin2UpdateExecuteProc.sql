Declare @LocationProcessData LocationProcessData

INSERT INTO @LocationProcessData
(LocationName, LocationTypeId, LocationFeature, CentroidFeature, PresentationLocationName, Hashkey)
SELECT
	LocationName as [LocationName]
	,LocationTypeId as [LocationTypeId]
	,LocationFeature as [LocationFeature]
	,Centroid as [Centroid]
	,PresentationLocationName as [PresentationLocationName]
	,'India-' + cast(ID as varchar(10)) as [HashKey]
  FROM [TestDatabase].[dbo].[vAdmin2India]

EXEC UpdateDatabaseWithNewBoundariesv2 68, 'India', 'Southern Asia', 'Asia', '2015', @LocationProcessData 