USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAdmin2BurkinaFaso]    Script Date: 6/24/2015 10:19:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




alter view [dbo].[vAdmin2BurkinaFaso]
as 
SELECT 
	v.Admin2 + ', ' + l.locationname as LocationName
	, v.Centroid
	,[Shape] as LocationFeature
	,v.Id
	,v.LocationTypeId
	,v.Admin2 + ', ' + l.LocationName as PresentationLocationName
	,'Burkina Faso-' + CAST(v.Id as varchar(13)) as Hashkey
	FROM vAdmin1_2BurkinaFaso v, Universe..Locations l
	where v.Centroid.STWithin(l.LocationFeature) = 1
	and l.LocationTypeId = 65 --admin1
	and Charindex(', Burkina Faso',L.LocationName,0) > 0 
	and Charindex(', Burkina Faso (',L.LocationName,0) = 0 
	and l.HashKey is not null



GO


