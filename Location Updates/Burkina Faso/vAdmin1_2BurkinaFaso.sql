USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAdmin1_2BurkinaFaso]    Script Date: 6/24/2015 10:24:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[vAdmin1_2BurkinaFaso]
as 
SELECT 
	admin2Name as Admin2
	, admin1Name as Admin1
	,68 as LocationTypeId
	,[Shape] as Shape
	,[ID] as Id
	, [awmDatabase].[dbo].[GetGeometryForCoordinate] (INSIDE_Y, INSIDE_X) as Centroid --use this when you import the centroid x and y columns with the table
	--,case when (case when [Shape].MakeValid().STCentroid() is null 
	--then [Shape].MakeValid().STEnvelope().STCentroid() 
	--else [Shape].MakeValid().STCentroid() end).STIntersects(Shape) = 1 then 
	--(case when [Shape].MakeValid().STCentroid() is null 
	--then [Shape].MakeValid().STEnvelope().STCentroid() 
	--else [Shape].MakeValid().STCentroid() end)
	--else Shape.STPointN(1) end as Centroid
  FROM [TestDatabase].[dbo].[BurkinaFasoAdmin2] 


GO


