USE [TestDatabase]
GO

/****** Object:  View [dbo].[vBurkinaFasoSurroundingCountryShape]    Script Date: 5/14/2015 11:45:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE view [dbo].[vBurkinaFasoSurroundingCountryShape]
as
	(Select LocationId,LocationName,LocationTypeId, LocationFeature, Hashkey
	from Universe.dbo.Locations
	Where LocationTypeId = 68
		and (CHARINDEX(', Ghana', LocationName)>0
			or charindex(', Togo',LocationName)>0
			or CHARINDEX(', Benin',LocationName)>0
			or CHARINDEX(', Niger',LocationName)>0
			or CHARINDEX(', Mali',LocationName)>0
			or CHARINDEX(', C�te d''Ivoire',LocationName)>0)
		
	)



GO


