--Admin 1
Declare @LocationProcessData LocationProcessData

INSERT INTO @LocationProcessData
(LocationName, LocationTypeId, LocationFeature, CentroidFeature, PresentationLocationName, Hashkey)
SELECT
	Admin1 + ', Burkina Faso' as [LocationName]
	,LocationTypeId as [LocationTypeId]
	,Shape as [LocationFeature]
	,Centroid as [Centroid]
	,Admin1 + ', Burkina Faso' as [PresentationLocationName]
	,'Burkina Faso-' + cast(ID as varchar(10)) as [HashKey]
  FROM [TestDatabase].[dbo].[vAdmin1BurkinaFaso]

--Select * from @LocationProcessData 

EXEC UpdateDatabaseWithNewBoundaries_v3 65, 'Burkina Faso', 'Middle Africa', 'Africa', '2015', @LocationProcessData, 13

--Admin 2
Declare @LocationProcessData LocationProcessData

INSERT INTO @LocationProcessData
(LocationName, LocationTypeId, LocationFeature, CentroidFeature, PresentationLocationName, Hashkey)
SELECT
	Admin2 +', ' + Admin1 + ', Burkina Faso' as [LocationName]
	,LocationTypeId as [LocationTypeId]
	,Shape as [LocationFeature]
	,Centroid as [Centroid]
	,Admin2 +', ' + Admin1 + ', Burkina Faso' as [PresentationLocationName]
	,'Burkina Faso-' + cast(ID as varchar(10)) as [HashKey]
  FROM [TestDatabase].[dbo].[vAdmin2BurkinaFaso]

--Select * from @LocationProcessData 

EXEC UpdateDatabaseWithNewBoundaries_v3 68, 'Burkina Faso', 'Middle Africa', 'Africa', '2015', @LocationProcessData, 13