USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAdmin1BurkinaFaso]    Script Date: 6/22/2015 4:57:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Create view [dbo].[vAdmin1BurkinaFaso]
as 
SELECT 
	Admin1Name + ', Burkina Faso' as LocationName
	,65 as LocationTypeId
	,[Shape] as LocationFeature
	,[OBJECTID] as Id
	,case when [Shape].MakeValid().STCentroid() is null then [Shape].MakeValid().STEnvelope().STCentroid() else [Shape].MakeValid().STCentroid() end as Centroid
	,Admin1Name + ', Burkina Faso'  as PresentationLocationName
	,'Burkina Faso' + CAST(Id as varchar(8)) as Hashkey
  FROM [TestDatabase].[dbo].[BurkinaFasoAdmin1]


GO


