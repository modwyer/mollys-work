SELECT TOP 1000 Geometry::STGeomFromText([ShapeWKT], 4326) as centroid
  FROM [Universe].[dbo].[GeoHash6LocationsLookup]
  where admin0locationid = -45498
  union all
  select locationfeature 
  from [Universe].[dbo].locations 
  where locationid = -45498