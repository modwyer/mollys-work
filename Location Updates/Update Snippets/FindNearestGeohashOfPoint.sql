Select geohash6, latitude1km, longitude1km, Geometry::STGeomFromText(ShapeWKT,4326) as Shape, Admin0locationid, admin1locationid, admin2locationid, continentlocationid, worldregionlocationid
From Universe..GeoHash6LocationsLookup
Where Geohash6 in (
	Select lr.relatedlocationid
	From WorldBank_STarcluster_data..Locationrelationships lr
	inner join
	Universe..Locations l
	on lr.relatedlocationid = l.locationid
	Where lr.locationid = 91788981)
	




