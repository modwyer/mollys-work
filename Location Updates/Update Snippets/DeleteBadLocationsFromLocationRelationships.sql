
Delete 
--Select *
From WorldBank_StarCluster_Data..LocationRelationships
Where RelatedLocationId in (
  SELECT 
		  LR.[RelatedLocationId]
  FROM [WorldBank_StarCluster_Data].[dbo].[LocationRelationships] LR
  INNER JOIN 
  Universe.dbo.Locations L
  ON LR.RelatedLocationId = L.LocationId
  Where LR.relatedLocationTypeId <> L.LocationTypeId
  )
and RelatedLocationTypeId not in (65, 68) --65 and 68 types have been checked and they map correctly so we don�t want to delete the ones in there.


--shows 65 and 68 location types match and should be excluded
Select l.locationname as RelatedName,l.locationtypeid as LocationsType,lr.locationid
		, lr.relatedlocationid, l2.locationname as ParentName, lr.relatedlocationtypeid
From WorldBank_StarCluster_Data..LocationRelationships lr
inner join
Universe..locations l
on lr.RelatedLocationId = l.locationid
Where RelatedLocationId in (
  SELECT 
		 distinct LR.[RelatedLocationId]
  FROM [WorldBank_StarCluster_Data].[dbo].[LocationRelationships] LR
  INNER JOIN 
  Universe.dbo.Locations L
  ON LR.RelatedLocationId = L.LocationId
  Where LR.relatedLocationTypeId <> L.LocationTypeId
  )
and LR.relatedLocationTypeId <> L.LocationTypeid



