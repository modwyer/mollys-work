Select *
From universe.dbo.locations
where locationId = 92446739


USE [BMGF_Constellation]
GO

SELECT distinct [StarId]
      ,[DatasetId]
      --,[RecordId]
      --,[AttributeId]
      ,[LocationId]
      --,[TemporalId]
      --,[ContextId]
      --,[EntityId]
      --,[CalculationId]
      --,[ExtensionId]
      --,[IntersectionMeasure]
  FROM bmgf_Constellation.[dbo].[Intersection]
Where LocationId --= 92446739

in (

  SELECT 
		Distinct LR.[RelatedLocationId]
  FROM [WorldBank_StarCluster_Data].[dbo].[LocationRelationships] LR
  INNER JOIN 
  Universe.dbo.Locations L
  ON LR.RelatedLocationId = L.LocationId
  Where LR.relatedLocationTypeId <> L.LocationTypeId
  )



