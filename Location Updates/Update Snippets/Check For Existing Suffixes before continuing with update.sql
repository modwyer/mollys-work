Declare @LocationTypeId smallint = 65
Declare @CountryName varchar(250) = 'Tanzania'
Declare	@RegionName varchar(250)
Declare	@ContinentName varchar(250)
Declare	@SuffixToAddToExistingFeatures varchar(20) = '2015'
Declare @S int

--check if the desired suffix is already used for this country
Set @S =
	(Select * 
	From Universe..Locations 
	Where locationtypeid = 65 
	and (charindex(', Tanzania(2014)',locationname)>0
		or charindex(', Tanzania (2014)',locationname)>0)
	)

Select @S

If @@rowcount = 0
	Begin
		print 'This is a new suffix! Continuing the update...'
	End
Else
	raiserror('This suffix already exists in the database for %s. Please check the suffix. Ending update.',1,200, @CountryName)
	return







--get subset of locations for the desired country and put that into a temp table to be worked on (this will then be used 
--to update the real locations table)
