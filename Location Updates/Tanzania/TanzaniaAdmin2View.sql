--create the view first

Create view vAdmin1_2Tanzania
as 
SELECT 
	Left([District_N],1) + right(lower([District_N]),len([District_N])-1) as Admin2
	,68 as LocationTypeId
	,[Shape] as Shape
	,[ID] as Id
	,case when [Shape].MakeValid().STCentroid() is null then [Shape].MakeValid().STEnvelope().STCentroid() else [Shape].MakeValid().STCentroid() end as Centroid
  FROM [TestDatabase].[dbo].[Tanzania_Districts] 
 
 --find all admin 1 names where the admin 2 centroid is within the admin1 locationfeature 

 Create view vAdmin2Tanzania
 as
 Select v.Admin2 + ', ' + l.LocationName as LocationName
	, v.Centroid
	, v.Shape as LocationFeature
	, v.Id
	, v.LocationTypeId
	, v.Admin2 + ', ' + l.LocationName as PresentationLocationName
 from vAdmin1_2Tanzania v, Universe.dbo.Locations l
 where v.Centroid.STWithin(l.LocationFeature) = 1
 and l.LocationTypeId = 65 --admin2
 and Charindex(', Tanzania',L.LocationName,0) > 0 
 and Charindex(', Tanzania (2013)',L.LocationName,0) = 0 
 

 



         
Create view vTanzaniaCentroids
as
Select
	case when [Tanzania_Counties].MakeValid().STCentroid() is null then [Tanzania_Counties].MakeValid().STEnvelope().STCentroid() else [Tanzania_Counties].MakeValid().STCentroid() end as Centroid
	from TestDatabase.dbo.TanzaniaAdmin2
	

 --this checks to see if the two admin levels match up
 Select v.Admin2, v.shape
 from vAdmin1Admin2Tanzania v
 UNION ALL 
 Select l.LocationName, l.LocationFeature 
 From Universe.dbo.Locations l
 where l.LocationTypeId = 65 --admin1
 and Charindex( ', Tanzania',L.LocationName,0) > 0 
 and CHARINDEX(', Tanzania(2006)',L.LocationName,0)=0
 
 
 
 

