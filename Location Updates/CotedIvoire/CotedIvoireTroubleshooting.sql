Select *
From Universe..Locations
Where LocationTypeId = 68
and charindex(', C�te d',LocationName)>0

Select *
From Universe..Locations
Where LocationId = 80259059

Select *
From Universe..Locations
Where LocationTypeId = 23
order by LocationName asc

/*
90756882	C�te d Ivoire
-45378	Cote d'Ivoire
80259087	Adzop�, Agn�by, C�te d'Ivoire

*/

Select L.LocationName as Admin1Name, G.Admin0LocationId
	, G.Admin1LocationId, G.Admin2LocationId, G.Geohash6
	, cast([Latitude1km] as varchar(250)) + ', ' + cast([Longitude1km] as varchar(250)) as LatLong
    , Geometry::STGeomFromText([ShapeWKT],4326), G.ShapeWKT as WKT
From Universe.dbo.GeoHash6LocationsLookup G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin0LocationId = L.LocationId
Where G.Admin2LocationId = 80259087
Order by Admin1Name

Select L.LocationName as Admin1Name, G.Admin0LocationId
	, G.Admin1LocationId, G.Admin2LocationId
From Universe.dbo.BaseAdminFeatures G
INNER JOIN
Universe.dbo.Locations L
ON G.Admin0LocationId = L.LocationId
Where G.Admin0LocationId = 90756882
Order by Admin1Name


Select *
From Universe..GeoHash6LocationsLookup
Where Admin0LocationId = -45378

Select *
From Universe..BaseAdminFeatures
Where Admin0LocationId = -45378