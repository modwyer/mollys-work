select *
from worldbank_starcluster_data..locations 
where locationtypeid = 68
and locationname like '%Dominican Republic%'

Update Universe..Locations
set locationname = left(locationname, len(locationname)-7) 
where locationtypeid = 68
and charindex(', Dominican Republic (', locationname) >0

Update worldbank_starcluster_data..Locations
set locationname = left(locationname, len(locationname)-7) 
where locationtypeid = 68
and charindex(', Dominican Republic (', locationname) >0