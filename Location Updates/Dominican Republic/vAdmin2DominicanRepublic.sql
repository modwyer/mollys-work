USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAdmin2DominicanRepublic]    Script Date: 7/14/2015 3:02:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

alter view [dbo].[vAdmin2DominicanRepublic]
as 
SELECT 
	v.Admin2 + ', ' + l.locationname as LocationName
	, v.Centroid
	,[Shape] as LocationFeature
	,v.Id
	,v.LocationTypeId
	,v.Admin2 + ', ' + v.Admin1 + ', DR' as PresentationLocationName
	,'Dominican Republic-' + CAST(v.Id as varchar(13)) as Hashkey
	FROM vAdmin1_2DominicanRepublic v, Universe..Locations l
	where v.Centroid.STWithin(l.LocationFeature) = 1
	and l.LocationTypeId = 65 --admin1
	and Charindex(', Dominican Republic',L.LocationName,0) > 0 
	and Charindex(', Dominican Republic (',L.LocationName,0) = 0 
	and l.HashKey is not null

GO


