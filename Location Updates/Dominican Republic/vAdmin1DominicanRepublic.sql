USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAdmin1DominicanRepublic]    Script Date: 7/10/2015 11:06:09 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




alter view [dbo].[vAdmin1DominicanRepublic]
as 
SELECT 
	TOPONIMIA + ', Dominican Republic' as LocationName
	,65 as LocationTypeId
	,[Shape] as LocationFeature
	,[OBJECTID] as Id
	,case when [Shape].MakeValid().STCentroid() is null then [Shape].MakeValid().STEnvelope().STCentroid() else [Shape].MakeValid().STCentroid() end as Centroid
	,TOPONIMIA + ', Dominican Republic' as PresentationLocationName
	,'Dominican Republic-' + CAST(Id as varchar(8)) as Hashkey
  FROM [TestDatabase].[dbo].[DominicanRepublicAdmin1]



GO


