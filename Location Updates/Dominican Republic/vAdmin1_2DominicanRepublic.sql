USE [TestDatabase]
GO

/****** Object:  View [dbo].[vAdmin1_2DominicanRepublic]    Script Date: 7/10/2015 11:08:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




alter view [dbo].[vAdmin1_2DominicanRepublic]
as 
SELECT 
	Municip as Admin2
	, Province as Admin1
	,68 as LocationTypeId
	,[Shape] as Shape
	,[ID] as Id
	, [awmDatabase].[dbo].[GetGeometryForCoordinate] (INSIDE_Y, INSIDE_X) as Centroid --use this when you import the centroid x and y columns with the table
	--,case when (case when [Shape].MakeValid().STCentroid() is null 
	--then [Shape].MakeValid().STEnvelope().STCentroid() 
	--else [Shape].MakeValid().STCentroid() end).STIntersects(Shape) = 1 then 
	--(case when [Shape].MakeValid().STCentroid() is null 
	--then [Shape].MakeValid().STEnvelope().STCentroid() 
	--else [Shape].MakeValid().STCentroid() end)
	--else Shape.STPointN(1) end as Centroid
  FROM [TestDatabase].[dbo].[DominicanRepublicAdmin2] 



GO


