/***** PLAT-3435 - Bug 'Regional auth does not work' Research *****/

--Do any countries from universe exist in the relationships table as a relatedlocation? no
Select *
from Universe..locations
where locationtypeid = 62 --WT country
and locationid in (
	Select relatedlocationid
	from Universe..LocationRelationships
	where relatedLocationtypeid = 62
	and locationtypeid = 63
	)

--Do any countries from universe exist in the relationships table as a relatedlocation? no
Select *
from Universe..locations
where locationtypeid = 23 --REgular country
and locationid in (
	Select relatedlocationid
	from Universe..LocationRelationships
	where relatedLocationtypeid = 23
	and locationtypeid = 63
	)

--Do any countries exist in prod as a child in the relationships table? Nope
Select *
from Universe..LocationRelationships
where relatedLocationtypeid = 62

--Do any regions exist as parents in the relationships table and if so, what is their child? location type 23 countries
Select *
from Universe..LocationRelationships
where Locationtypeid = 63

--Bolivia; doesn't exist in relationship table
Select *
from Universe..LocationRelationships
where relatedLocationid = -988136

--region id for bolivia
select *
from universe..locations
where locationid = -2620117

--point -13,-66 - No WT region relationship
Select *
from Universe..LocationRelationships
where relatedLocationid = 60130706	