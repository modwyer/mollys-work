/****** Compare and regenerate the spatial measures and centroid measures to see if they are the same as the original ***/
--Drop table #Points

--create table to hold points that are known to not fall within a grid on data3
Create table #Points (ID int, Latitude float, Longitude float, PointGeom geometry, LocationName varchar(20), LocationID int)
Insert into #Points (ID, Latitude, Longitude, PointGeom, LocationName, LocationID)
	Values (1, 7.13333, -1.75, awmDatabase.dbo.GetGeometryForCoordinate(7.13333, -1.75), 'Abofour', 10170857)
			, (2, 6.25, -0.25, awmDatabase.dbo.GetGeometryForCoordinate(6.25, -0.25), 'Akwapim', 10158176)
			, (3, 6.5, -1.35, awmDatabase.dbo.GetGeometryForCoordinate(6.5, -1.35), 'Beposo', 10124983)
			, (4, 7.01667, -2, awmDatabase.dbo.GetGeometryForCoordinate(7.01667, -2), 'Nsuta', 10152773)
			, (5, 5.8, -0.75, awmDatabase.dbo.GetGeometryForCoordinate(5.8, -0.75), 'Nkawkaw', 10160463)
			, (6, 9.5, -1.6, awmDatabase.dbo.GetGeometryForCoordinate(9.5, -1.6), 'Kubeng', 10166649)

--Select * from #Points

--Select l.SpatialMeasure, l.locationname
--From Universe..Locations l, #Points p --GlobalGridDataStore..Global5by5minGrid_Full l, #Points p
--where l.locationid = p.LocationID
--or l.LocationName = 'GridX: -17, GridY: 78'
--union all
--Select PointGeom, p.LocationName from #Points p, GlobalGridDataStore..Global5by5minGrid_Full l
--where p.locationid = l.locationid

--create a table to hold grids where the locationid matches the point locationids in #Points
Create table #Grids (LocationId int, LocationName varchar(50), SpatialMeasure geometry, LocationMeasure varchar(max)
						, CentroidSpatialMeasure geometry, CentroidLocationMeasure varchar(8000), GeneratedPolygon geometry
						, GeneratedCentroid geometry)

Insert into #Grids (LocationID, LocationName, SpatialMeasure, LocationMeasure, CentroidSpatialMeasure, CentroidLocationMeasure)
	Select LocationID, LocationName, SpatialMeasure, LocationMeasure, CentroidSpatialMeasure, CentroidLocationMeasure
	From Universe..locations
	where locationtypeid = 8
	and locationid in (
		Select LocationID
		from #Points
		)

--find distance from point to grid and area of grid
Select p.LocationId
		, p.Latitude
		, p.Longitude
		, p.LocationName
		, p.PointGeom.STDistance(g.SpatialMeasure) as DistanceToGrid
		,g.locationname as GridName
		, g.spatialmeasure.STArea() as GridArea
		, g.spatialmeasure
		, g.locationmeasure
		, g.centroidspatialmeasure
		, g. centroidlocationmeasure
From #Points p, #Grids g
Where p.locationid = g.locationid 

Select * from #Grids

--Add column to hold generated polygon and insert generated polygon values
Update #Grids
Set GeneratedPolygon = geometry::STPolyFromText(LocationMeasure,4326)
	where Locationid is not null

--delete from #Grids where locationid is null

--Get area of original and generated polygons
Select LocationId, SpatialMeasure.STArea() as SpatialMeasureArea, GeneratedPolygon.STArea () as GeneratedPolygonArea
from #Grids

--Get difference of areas for comparison
Select LocationId, (GeneratedPolygon.STArea () - SpatialMeasure.STArea()) as AreaDifference 
from #Grids

-- insert generated centroid values
Update #Grids
Set GeneratedCentroid = GeneratedPolygon.STCentroid()
	where Locationid is not null

--Get distance between original centroid and generated centroid
Select CentroidSpatialMeasure, GeneratedCentroid, GeneratedCentroid.STDistance(CentroidSpatialMeasure) as Distance
From #Grids


--visualize the original and generated polygons and centroids at the same time using union all
Select SpatialMeasure
from #Grids
where locationid = 10124983
union all
Select GeneratedPolygon
from #Grids
where locationid = 10124983
union all
Select CentroidSpatialMeasure
from #Grids
where locationid = 10124983
union all
Select GeneratedCentroid
from #Grids
where locationid = 10124983









--------------------------------------------------------------------
drop table #SpatialTest
Create table #SpatialTest (RunID int identity, SpatialMeasure geometry, LocationMeasure varchar(max), GeneratedSpatial geometry)
Insert into #SpatialTest (SpatialMeasure, LocationMeasure)
	Select SpatialMeasure, LocationMeasure
	from universe..locations
	where LocationName = 'GridX: -17, GridY: 78'
	or LocationName = 'GridX: -17, GridY: 79'
	or LocationName = 'GridX: -17, GridY: 80'
	or LocationName = 'GridX: -17, GridY: 81'
	union all
	Select CentroidSpatialMeasure, CentroidLocationMeasure
	From Universe..locations
	where LocationName = 'GridX: -17, GridY: 78'
	or LocationName = 'GridX: -17, GridY: 79'
	or LocationName = 'GridX: -17, GridY: 80'
	or LocationName = 'GridX: -17, GridY: 81'

Select * from #SpatialTest

Update #SpatialTest
Set GeneratedSpatial = geometry::STPolyFromText(LocationMeasure,4326)
Where LocationMeasure like 'Polygon%'

Update #SpatialTest
Set GeneratedSpatial = geometry::STCentroid(LocationMeasure,4326)
Where LocationMeasure like 'Point%'


------------------------------------------
Declare @Lat float = 7.13333
Declare @Long float = -1.75
Declare @Point geometry

Set @Point = geometry::STGeomFromText('POINT (' + cast(@Long as varchar(20)) + ' ' + cast(@Lat as varchar(20)) + ')', 4326)

Select l.SpatialMeasure
From Universe..Locations l
where l.locationtypeid = 8
and locationid = 10124983 --l.locationtypeid = 8
--and @Point.STIntersects(l.SpatialMeasure) = 1
union all
Select @Point





