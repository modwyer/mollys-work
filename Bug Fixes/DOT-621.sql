--compare dev to prod
Select spatialmeasure, locationname
from universe..locations
where locationtypeid = 8
and locationid not in (
	select locationid
	from universe_prod..locations
	where locationtypeid = 8
	)
union all
select spatialmeasure, locationname
from universe..locations
where locationtypeid = 23

--compare prod to deb
Select spatialmeasure, locationname
from universe_prod..locations
where locationtypeid = 1
and locationid not in (
	select locationid
	from universe..locations
	where locationtypeid = 1
	)
union all
select spatialmeasure, locationname
from universe_prod..locations
where locationtypeid = 23

--find the locations both have in common
select l2.locationid, l2.locationname, l2.spatialmeasure
	from universe..locations l1
inner join
universe_prod..locations l2
on l1.locationid = l2.locationid
where l1.locationtypeid = 8
and l2.locationtypeid = 8

	and locationid not in (
		Select locationid
		from universe..locations
		where locationtypeid = 1
		)

union all
Select SpatialMeasure, locationname
from Universe..locations
where locationtypeid = 23