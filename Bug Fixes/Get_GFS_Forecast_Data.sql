USE [WT_Forecast]
GO

/****** Object:  StoredProcedure [dbo].[Get_GFS_Forecast_Data]    Script Date: 8/25/2015 1:56:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jim Hancock
-- Create date: 4/9/2014
-- Description:	Get GFS forecast data from passed in parameters
--
-- 04/22/2014 JNH Changed to use new Date column in GFSHourly
--
-- There will be no duplicates passed in for Lat, lon, start, end
-- This will validate locations, dates, and offset
--
/*
Declare @GFS_Forecast_Input_List dbo.GFS_Forecast_Input_List
Declare @Return_Code int
	Insert into @GFS_Forecast_Input_List
	Values (1,42, 114, ' 1/1/2012', ' 4/25/2014', 4)
			,(2,38, -95, '4/24/2014 5:00', ' 4/27/2014 14:00', -2)
			,(3,5, -61, '4/24/2014', '5/2/2014', 6)
			,(4,10, -5, '4/24/2014', '5/2/2014', 8)
			,(5,47, 2, '4/24/2014', '5/2/2014', -4)
			,(6,22, 78, '4/24/2014', '5/2/2014', 1)
			,(7,0, 0, '4/24/2014', '5/2/2014', 2)
			,(8,-34, 21, '4/16/2014', '4/20/2014', 2)
Exec @Return_Code = Get_GFS_Forecast_Data @GFS_Forecast_Input_List, '6BB5EC48-D895-42AD-B4A7-C6002EC0A399', 'aWhere Beta'
Select @Return_Code

*/

--Need to run this on DATA3 when ready
-- EXEC sp_serveroption @server = 'awm_Users',@optname = 'remote proc transaction promotion', @optvalue = 'false' ;

-- =============================================
CREATE PROCEDURE [dbo].[Get_GFS_Forecast_Data]
 
	@GFS_Forecast_Input_List dbo.GFS_Forecast_Input_List readonly
	,@UserId varchar(100) = NULL
	,@ApplicationName nvarchar(256) = NULL
	,@AuthServer varchar(5) = 'DEV'

AS
BEGIN

	SET NOCOUNT ON;
	
		Declare @Errormsg Varchar(200)
		
--Set @Errormsg = 'Start' +  ', ' + (CONVERT( VARCHAR(24), GETDATE(), 121)) RAISERROR (@Errormsg, 0, 1) WITH NOWAIT

	
	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#GFS_Forecast_Input_List')) 
	BEGIN DROP TABLE #GFS_Forecast_Input_List END
	
	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#UserList')) 
	BEGIN DROP TABLE #UserList END
	
	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#T2')) 
	BEGIN DROP TABLE #T2 END

	Create Table #GFS_Forecast_Input_List(ID int NOT NULL ,
											[Latitude] [real] NOT NULL,
											[Longitude] [real] NOT NULL,
											[StartDate] [smalldatetime] NOT NULL,
											[EndDate] [smalldatetime] NOT NULL,
											StartTemporalid int,
											EndTemporalid int,
											Timezone [smallint] NULL,
											LocationId int NULL,
											SpatialMeasure geometry,
											--CONSTRAINT [PK_ID] PRIMARY KEY CLUSTERED 
											 PRIMARY KEY CLUSTERED 
											(ID ASC
											))
		--CREATE SPATIAL INDEX Do not do this!

declare @value varchar(50); 
declare @cmd varchar(2000);
set @value = NEWID()
set @value = @value + 'StartEndTemporal'
 
SET @cmd = 'CREATE INDEX [' + @value + '] ON #GFS_Forecast_Input_List(StartTemporalid, EndTemporalid)';
EXEC(@cmd);

set @value = NEWID()
set @value = @value + 'Timezone'
SET @cmd = 'CREATE INDEX [' + @value + '] ON #GFS_Forecast_Input_List([Timezone])INCLUDE ([ID],[LocationId])';
EXEC(@cmd);

	
	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#USerList')) 
	BEGIN DROP TABLE #USerList END
	
	Create table #UserList(CountryId int, StartTemporalid int, EndTemporalid int)			
											
	Insert into #GFS_Forecast_Input_List(ID,Latitude, Longitude, StartDate, EndDate, Timezone)											
		Select ID, Latitude, Longitude, StartDate, EndDate, UTC_Offset from @GFS_Forecast_Input_List

--Set @Errormsg = 'Insert into #GFS_Forecast_Input_List' +  ', ' + (CONVERT( VARCHAR(24), GETDATE(), 121)) RAISERROR (@Errormsg, 0, 1) WITH NOWAIT
--Select * from @GFS_Forecast_Input_List		
		
	--Validation
		--Lat lon
		If(Select COUNT(1) from #GFS_Forecast_Input_List 
			where Latitude > 90	
				or Latitude < -90 
				or Longitude > 180 
				or Longitude < -180 ) > 0 Begin
		Print 'Invalid Latitude or Longitude'
		Return 1
		End
		
		--Start and end dates
		--If(Select COUNT(1) from #GFS_Forecast_Input_List 
		--	where StartDate > DATEADD(DAY, 8, cast(convert(varchar(25), Getdate(), 102) as datetime) ) 
		--		or StartDate < cast(convert(varchar(25), Getdate(), 102) as datetime) 
		--		or EndDate > DATEADD(DAY, 8, cast(convert(varchar(25), Getdate(), 102) as datetime) ) 
		--		or EndDate < cast(convert(varchar(25), Getdate(), 102) as datetime) 
		--		) > 0 Begin
		--Print 'Invalid StartDate or EndDate'
		--Return 2
		--End
		
		Update #GFS_Forecast_Input_List Set SpatialMeasure = geometry::STGeomFromText('POINT (' + CAST(Longitude as varchar(20)) + ' ' + CAST(Latitude as varchar(20)) +  ')', 4326)
--Set @Errormsg = 'Insert into #GFS_Forecast_Input_List SpatialMeasure' +  ', ' + (CONVERT( VARCHAR(24), GETDATE(), 121)) RAISERROR (@Errormsg, 0, 1) WITH NOWAIT		
		Update #GFS_Forecast_Input_List Set LocationId = l.locationid 
			from #GFS_Forecast_Input_List g 
				inner join Universe..Locations l on g.SpatialMeasure.STIntersects(l.SpatialMeasure) = 1
			where l.LocationTypeId in (8) -- was 74, need to go 5 min?
		
--Set @Errormsg = 'Insert into #GFS_Forecast_Input_List LocationId' +  ', ' + (CONVERT( VARCHAR(24), GETDATE(), 121)) RAISERROR (@Errormsg, 0, 1) WITH NOWAIT					
		Update #GFS_Forecast_Input_List Set Timezone = n.Timezone
		from #GFS_Forecast_Input_List g inner join GFSMasterTable5by5MinGrids n on g.LocationId = n.LocationId
		where g.Timezone is null	
		
		Update #GFS_Forecast_Input_List Set Timezone = 0 where Timezone is null
		
				--UTC_Offset
		If(Select COUNT(1) from #GFS_Forecast_Input_List 
			where Timezone > 12 or Timezone < -11) > 0 Begin
		Print 'Invalid UTC_Offset'
		Return 3
		End		

		--LatLon has valid Locationid?
		If(Select COUNT(1) from #GFS_Forecast_Input_List 
			where SpatialMeasure is null
		) > 0 Begin
		Print 'Invalid StartDate or EndDate'
		Return 4
		End
--Select * from #GFS_Forecast_Input_List		
		Update #GFS_Forecast_Input_List 
			Set StartDate = DATEADD(HOUR, Timezone, StartDate)
			,EndDate = DATEADD(HOUR, Timezone, EndDate)
		
		Update #GFS_Forecast_Input_List 
			Set StartTemporalid = WT_Processing.dbo.GetTemporalIdFromDate([StartDate], 2,0)
			,EndTemporalid = WT_Processing.dbo.GetTemporalIdFromDate([endDate], 2,0)
		
		--User validation
		If @AuthServer = 'DEV' or @AuthServer = 'PROD' Begin
		Insert into #UserList
		EXEC  awm_users.awm_users.dbo.ReturnUserAccessRights @UserId, @ApplicationName
		End
		
		If @AuthServer = 'QA' Begin
		Insert into #UserList
		EXEC  awm_usersQA.awm_users.dbo.ReturnUserAccessRights @UserId, @ApplicationName
		End
		
		--Don't get locations without permissions
		Delete #GFS_Forecast_Input_List where LocationId not in (Select LocationId from #UserList)
		
		--Don't get dates without permissions
		Update #GFS_Forecast_Input_List Set StartTemporalid = u.StartTemporalid
		from [GlobalGridDataStore].[dbo].[Global5by5minGrid_Full] g inner join #GFS_Forecast_Input_List f 
			on g.LocationId = f.LocationId inner join #UserList u on g.WTCountryLocationId = u.CountryId
			where f.StartTemporalid < u.StartTemporalid
		
		Update #GFS_Forecast_Input_List Set EndTemporalid = u.EndTemporalid
		from [GlobalGridDataStore].[dbo].[Global5by5minGrid_Full] g inner join #GFS_Forecast_Input_List f 
			on g.LocationId = f.LocationId inner join #UserList u on g.CountryLocationId = u.CountryId
			where f.EndTemporalid > u.EndTemporalid
		
		-- Now implement the 14 day window rule
		
		Update #GFS_Forecast_Input_List Set StartDate = Universe.dbo.GetDateFromTemporalId(StartTemporalid, 2)
			, EndDate = Universe.dbo.GetDateFromTemporalId(EndTemporalid, 2)

		Update 	#GFS_Forecast_Input_List Set EndDate = DATEADD(DAY, 14, StartDate)
			where DATEDIFF(DAY, StartDate, EndDate)	> 14
		
		Update #GFS_Forecast_Input_List Set StartTemporalid = Universe.dbo.GetTemporalIdFromDate(StartDate,2,0)
		Update #GFS_Forecast_Input_List Set EndTemporalid = Universe.dbo.GetTemporalIdFromDate(EndDate,2,0)
		
		--Select * from #GFS_Forecast_Input_List --debug
--Set @Errormsg = 'Main Select' +  ', ' + (CONVERT( VARCHAR(24), GETDATE(), 121)) RAISERROR (@Errormsg, 0, 1) WITH NOWAIT		
		
		Select f.ID
			, f.Latitude
			, f.Longitude
			, DATEADD(hh,F.TimeZone,h.Date) as Date --h.Date --universe.dbo.GetDateFromTemporalId(h.TemporalID,2) as [Date]
			, f.TimeZone,	
			f.locationid,
			TMAX	= AVG(Case when h.AttributeID = 1 THEN IntersectionMeasure END)
			,TMIN	= AVG(Case when h.AttributeID = 2 THEN IntersectionMeasure END)
			,ACPCP	= AVG(Case when h.AttributeID = 4 THEN IntersectionMeasure END)
			,DSWRF	= AVG(Case when h.AttributeID = 6 THEN IntersectionMeasure END)
			,RH		= AVG(Case when h.AttributeID = 12 THEN IntersectionMeasure END)
			,CRAIN	= AVG(Case when h.AttributeID = 16 THEN IntersectionMeasure END)
			,SUNSD	= AVG(Case when h.AttributeID = 17 THEN IntersectionMeasure END)
			,TCDC	= AVG(Case when h.AttributeID = 18 THEN IntersectionMeasure END)
			,MWS	= AVG(Case when h.AttributeID = 88 THEN IntersectionMeasure END)
			into #T2
		from #GFS_Forecast_Input_List f inner join Universe..Locationrelationships lr on  lr.RelatedLocationId = f.LocationId 
		inner join GFSHourly h with (nolock) on h.LocationID = lr.LocationId
			WHERE DATEADD(hh,F.TimeZone,h.Date) between 
			f.StartDate and
			 f.EndDate
			and lr.LocationTypeId = 74 and RelatedLocationTypeId = 8
			group by f.ID, f.Latitude, f.Longitude, h.Date, f.TimeZone,f.locationid
			
		
--Set @Errormsg = 'CRAIN Start' +  ', ' + (CONVERT( VARCHAR(24), GETDATE(), 121)) RAISERROR (@Errormsg, 0, 1) WITH NOWAIT		
	
			Update #T2 Set CRAIN = a.CRAIN 
				From  #T2 t Inner join (
										Select f.ID, f.Latitude, f.Longitude
										,DATEADD(hh,F.Timezone,h.Date) as Date -- universe.dbo.GetDateFromTemporalId(h.TemporalID,2) as [Date]
										, f.TimeZone,	
										f.locationid as Locationid
										,CRAIN = AVG(Case when h.AttributeID = 16 THEN IntersectionMeasure END)
										from #GFS_Forecast_Input_List f inner join Universe..Locationrelationships lr on  lr.RelatedLocationId = f.LocationId 
										inner join GFSHourly h with (nolock) on h.LocationID = lr.LocationId
										WHERE DATEADD(hh,F.TimeZone,h.Date) between 
										f.StartDate and f.EndDate
										and h.AttributeID = 16
										group by f.ID, f.Latitude, f.Longitude, h.Date, f.TimeZone,f.locationid
										) a on a.Date = t.date and a.Locationid = t.locationid
		
		--Now set it back to local time
			Update #T2 Set [Date] = DATEADD(Hour, -Timezone, [Date])
		
		
		Select [#T2].ID, [#T2].Latitude, [#T2].Longitude, [#T2].Date, [#T2].TimeZone, [#T2].locationid, [#T2].TMAX, [#T2].TMIN, [#T2].ACPCP, [#T2].DSWRF, [#T2].RH, [#T2].CRAIN, [#T2].SUNSD, [#T2].TCDC, [#T2].MWS 
		from #T2
		order by ID, [Date], Locationid
--Select ID, TMAX, TMIN, ACPCP, DSWRF, RH, CRAIN, SUNSD, TCDC, MWS from #GFS_Forecast_Input_List

	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#GFS_Forecast_Input_List')) 
	BEGIN DROP TABLE #GFS_Forecast_Input_List END
	
	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#T2')) 
	BEGIN DROP TABLE #T2 END
	
	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE ID = OBJECT_ID(N'tempdb..#UserList')) 
	BEGIN DROP TABLE #UserList END
	
	
	
--Set @Errormsg = 'End' +  ', ' + (CONVERT( VARCHAR(24), GETDATE(), 121)) RAISERROR (@Errormsg, 0, 1) WITH NOWAIT		
END


GO


