
--Pivot table
--For table with Country, Year, SalesAmount
SELECT   [Country], [2005], [2006], [2007], [2008]
FROM   [dbo].[PivotExample] 
PIVOT
(
       SUM(SalesAmount)
       FOR [Year] IN ([2005], [2006], [2007], [2008])
) AS P


--Unpivot table
--For table with Country, 2005, 2006, 2007, 2008
SELECT   Country, Year, SalesAmount 
FROM [dbo].[UnpivotExample]
UNPIVOT
(
       SalesAmount
       FOR [Year] IN ([2005], [2006], [2007], [2008])
) AS P