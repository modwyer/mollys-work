/*** Script to change metadata; not good for bulk changes ***/

/*** Run the following 3 lines to get the XML doc to determine node path; use DLE prefix ***/
use WorldBank_StarCluster_Data;
declare @sid int = 207;
select * 
	from Stars 
	where starId = @sid;


/*** Run this script to update the star; fill in relevant info in the xml section ***/
update Stars 
set EntryDocument.modify(N'declare namespace DLE="http://tempuri.org/DataLibraryEntry.xsd";
		replace value of (/DLE:LibraryEntry/DLE:DataSource/DLE:Owner/DLE:Name)[1]
		with "CountrySTAT"
		')
	where StarId = @sid
	;

--doublecheck only, not needed for script
select * from Stars where starId = @sid;

