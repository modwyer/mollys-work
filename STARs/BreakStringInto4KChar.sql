use awm_Users
	--concat usernames
	declare @userListCsv nvarchar(max);
	declare @list varchar(max) = ''
		Select @list = @list + ', ' + LoweredUserName
		from aspnet_Users
	
		Set @userListCsv = substring(@list,3, len(@list)-3)
		Select len(@userListCsv) as ListCount, @userListCsv as UserList

	--break list up to less than 4000 char
	While len(@userListCsv) > 4000
		Begin
			--if len(@userListCsv) > 4000
			--		Begin
						declare @firstGroup nvarchar(4000)
						declare @secondGroup nvarchar(4000)
						--set first group
						Set @firstGroup = left(@userListCsv,charindex(', ',@userListCsv, 3900)-1)
						--set second group
						Set @secondGroup = right(@userListCsv, len(@userListCsv) - len(@firstGroup)-2)				
			--		End
			--Else
			--	Set @firstGroup = @userListCsv
	--double check results
	Select len(@firstGroup) as FirstGroupCount, @firstGroup as FirstGroupList
	Select len(@secondGroup) as SecondGroupCount, @secondGroup as SecondGroupList


