﻿-- spawn-more-overlords.sql
-- creates a new role and assigns all aWhere users that role for the apps platform
-- mms, 2/24/2014
-- apps site to SQL instance mapping: { appsdev => data1dev\ss08std, appsqa => data1qa, apps => data1 }
-- change role name at the top to the role name in starcraft.sql or starcraft2.sql
-- you can safely run this script multiple times all its operations are idempotent
-- if you see any red text under Messages in SSMS call matt

use awm_Users;
declare @roleName nvarchar(4000) = 'Data-DRC-Household-Survey-002';


-- do not modify below this line
begin try
	set transaction isolation level read committed;
	begin transaction;
	set xact_abort on;
	raiserror('Working on role %s...', 1, 99,@roleName) with nowait;
	declare @appName varchar(256) = 'aWhere Beta';
	declare @roleExists int;
	exec @roleExists = dbo.aspnet_Roles_RoleExists @appName, @roleName;
	--select @roleExists as RoleExists;
	if (@roleExists = 0)
	begin
		declare @status int;
		exec @status = dbo.aspnet_Roles_CreateRole @appName, @roleName;
		if (@status > 0)
		begin
			raiserror('Error creating role, code returned %d', 18, 100, @status) with nowait;
		end
	end
	raiserror('Confirmed role exists, continuing. Making an aWhere user list...', 1, 101) with nowait;
	declare @userListCsv nvarchar(4000);
	set @userListCsv = 
		(select awmDatabase.dbo.ConcatOrderedDistinct(LoweredUserName) 
		from aspnet_Users 
		where CHARINDEX('@awhere.com',LoweredUserName) > 0);
	--select LEN(@userListCsv) as UserListLen, @userListCsv as UserList;
	declare @lastStatus int;
	declare @tstamp datetime = CURRENT_TIMESTAMP;
	exec @lastStatus = dbo.aspnet_UsersInRoles_AddUsersToRoles_aWhere @appName, @userListCsv,@roleName,@tstamp;
	if (@lastStatus > 0)
	begin
		raiserror('Error adding users to role, code returned was %d', 18, 110, @lastStatus) with nowait;
	end
	commit transaction;
	raiserror('Success! Comitted all our work.', 1, 201) with nowait;
end try
begin catch
	raiserror('Caught error, rolling back', 1, 202) with nowait;
	rollback transaction;
end catch
