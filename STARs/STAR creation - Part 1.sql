﻿-- starcraft2.sql, drops new stars into the world bank cluster for data analyst use. mms 2/14/2014
-- v0.1: encode the starId, merge the role, add to big constellation, nothing more.


--   _______                        _______									___________ 
--	|  _____|_____         _____   / _____7_____          _______ _____    /__  ___  _/
--	 \-\    /_  __\  <\   <_  __\ |-/     <_  __\    <\  /_  ___//_  __\     | |  | |
--	  \ \   \v| |    /_\   | | \\ | |      | | \\    /_\   | |_  \v| |       | |  | |
--	   \ \    | |   //_\\  | | // \ \      | | //   //_\\  | _/    | |       | |  | |
--	    \ \   | |  / ___ \ | |< \  \ \     | |< \  / ___ \ | |     | |     __| |__| |__
--	 /\__\ \  |/  /_/   \_\|_| \_\  \ \____|_| \_\/_/   \_\|_|     |/     /___________/
--	/______/                         \____/
--												- a67sm @ ign, mms

-- >>> StarCraft II is the World Bank edition
-- >>> Use for any data from that organization or general third-party public access stuff
-- >>> FAOSTAT, UN, ADB, etc

use WorldBank_StarCluster_Data;

-- >>> change these variables:

declare @newStarName varchar(100) = 'Ghana Boundary Update'; -- pretty much anything you want, is a display name
declare @newStarRole varchar(100) = 'Data-Ghana-Boundary-Update'; -- must keep 'Data-' prefix, no spaces allowed


-- >>> if the sql Messages window below mentions anything about a zerg rush, call matt
-- >>> if no zerg rush, launch the user admin tool and permission people for that role

-- don't change anything past this point!!!!!

declare @worldBankClusterId int = 1;
declare @starTypeId int = 1;

declare @s dbo.StarList;
insert into @s(StarName,StarTypeId,StarClusterId)
values (@newStarName, @starTypeId, @worldBankClusterId);
exec dbo.EncodeStars @s;

declare @sId int;
set @sId = (select StarId from Stars where StarName = @newStarName and StarTypeId = @starTypeId and StarClusterId = @worldBankClusterId)

if (@sId is null)
begin
	raiserror('ZERG RUSH KEKEKE', 17, 0) with nowait;
end

merge into Universe.dbo.StarRoles target
using (
	select @sId as StarId, @newStarRole as RoleName
) source
on target.StarId = source.StarId
when not matched then insert(StarId, RoleName)
values (source.StarId, source.RoleName)
when matched then update
set RoleName = source.RoleName
;
select * from Universe.dbo.StarRoles where StarId = @sId;

declare @bmgf int = 2;
merge into BMGF_Constellation.dbo.ConstellationStars target
using (
	select @bmgf as ConstellationId, @sId as StarId
) source
on target.ConstellationId = source.ConstellationId and target.StarId = source.StarId
when not matched then insert(ConstellationId, StarId)
values (source.ConstellationId, source.StarId)
;
select * from BMGF_Constellation.dbo.ConstellationStars where StarId = @sId;