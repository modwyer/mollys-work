USE [WorldBank_StarCluster_Data]
GO
/****** Object:  StoredProcedure [dbo].[ClearStar]    Script Date: 02/09/2015 16:07:16 ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Declare @List as [GenericKeyList]

INSERT INTO @List
([GenericKeyId])
Values(3530)

Select * From @List

EXEC [ClearStar] @List

*/

ALTER PROCEDURE [dbo].[ClearStar]
	@starIds dbo.GenericKeyList readonly
as
begin
	declare @deleteRuns table(RunId int identity primary key, StarId smallint);
	insert into @deleteRuns(StarId)
	select gkl.GenericKeyId
	from @starIds gkl
	;

	declare @i int, @i_max int;
	select @i = MIN(RunId), @i_max = MAX(RunId)
	from @deleteRuns
	;
	while @i <= @i_max
	begin
		declare @starId int = (select StarId from @deleteRuns where RunId = @i);
		declare @starDatasets dbo.GenericKeyList;
		insert into @starDatasets
		select DatasetId
		from Datasets
		where StarId = @starId
		;
		declare @n int = (select COUNT(GenericKeyId) from @starDatasets);
		if (@n = 0)
		begin
			set @i = @i+1;
			continue;
		end
		exec dbo.ClearDataset @starDatasets;
		set @i = @i+1;
	end

	delete Stars
	from Stars S
	inner join @starIds gkl on S.StarId = gkl.GenericKeyId
	;

	RETURN 0;
end