-- ================================================
/*

*/
-- ================================================
Use TestDatabase;
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Molly O'Dwyer
-- Create date: 5/18/15
-- Description:	This procedure creates a temp table to hold a list of role names that need to be permissioned for all users. It loops 
-- through the names and calls the procedure that assigns the users to the role.
/*
Exec CreateMulitpleRoleNamesBasedOnTableOfNames 'TestDatabase', 'StarRoleNamesTable'
*/

-- =============================================
alter PROCEDURE dbo.CreateMulitpleRoleNamesBasedOnTableOfNames
	-- Add the parameters for the stored procedure here
	@databaseName varchar(100),
	@tableName varchar(100)

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @ActiveRunId int, @MaxRunId int	
	Declare @roleName nvarchar(4000)
	Declare @insertData varchar(2000)
	--Declare @tableName varchar(100) = 'StarRoleNamesTable'
	--Declare @databaseName varchar(100) = 'TestDatabase'
	
	--Create table #StarRoleNames (RunId int identity(1,1), RoleName varchar(4000))

	Set @insertData = 
		'Insert into #StarRoleNames (RoleName)' + char(13)
		+ 'Select RoleName ' + char(13)
		+ 'From ' + @databaseName + '.dbo.' + @tableName

Print @insertData

	Exec @insertData

	Select @ActiveRunId = Min(RunId), @MaxRunId = Max(RunId)
	From #StarRoleNames

	While @ActiveRunId <= @MaxRunId
		
		BEGIN

			Set @roleName = (
			Select RoleName
			From #StarRoleNames
			Where RunId = @ActiveRunId
			)

			EXEC Assign_All_Current_Users_To_Rolev2 @roleName
				

			Set @ActiveRunId = @ActiveRunId + 1
		End
		
	drop table #StarRoleNames

END
GO
