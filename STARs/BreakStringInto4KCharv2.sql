use awm_Users
	--concat usernames
	declare @userListCsv nvarchar(max);
	declare @list varchar(max) = ''
		Select @list = @list + ', ' + LoweredUserName
		from aspnet_Users
	
		Set @userListCsv = substring(@list,3, len(@list)-3)
		Select len(@userListCsv) as OriginalListCount, @userListCsv as OriginalUserList

		
	--break list up to less than 4000 char
	declare @shortUserListCsv nvarchar(max)

	While len(@userListCsv) > 0
		Begin
			If len(@userListCsv) > 4000
				Begin
					Print 'List is more than 4000 characters'
					Set @shortUserListCsv = left(@userListCsv,charindex(', ',@userListCsv, 3900)-1)
					--set short chunk to the original list minus the previous short chunk
					Set @userListCsv = right(@userListCsv, len(@userListCsv) - len(@shortUserListCsv)-2) 
					--Select len(@shortUserListCsv) as ShortListLength, @shortUserListCsv as ShortList
					--Select len(@userListCsv) as NewListLength, @userListCsv as NewList
				End
			Else
				Begin
					Set @shortUserListCsv = @userListCsv
					Set @userListCsv = right(@userListCsv, len(@userListCsv) - len(@shortUserListCsv))
				End

		--exec @lastStatus = dbo.aspnet_UsersInRoles_AddUsersToRoles_aWhere @appName, @shortUserListCsv,@roleName,@tstamp;
		--if (@lastStatus > 0)
		--	begin
		--		raiserror('Error adding users to role, code returned was %d', 18, 110, @lastStatus) with nowait;
		--	end
		Select len(@shortUserListCsv) as ShortListLength, @shortUserListCsv as ShortList
		Select len(@userListCsv) as NewListLength, @userListCsv as NewList
	End
		
				
