/*** Script to clear a star that won't clear on its own ***/

use WorldBank_StarCluster_Data;

/*** use select to find star Id
To clear dev or QA or prod, must be connected to \Platform ***/
select
	s.StarId, 
	s.StarName, 
	EntryDocument.value(N'
		declare namespace DLE="http://tempuri.org/DataLibraryEntry.xsd";
		/DLE:LibraryEntry[1]/DLE:Title[1]
		', 'varchar(250)'
	) as StarTitle
from
	WorldBank_StarCluster_Data.dbo.Stars s
;


/*** Clear the desired star; to clear more than one star add ,(starID), (starID) on the second line ***/
declare @list dbo.GenericKeyList;
insert into @list values (5835);
exec dbo.ClearStar @list;

/*
Select StarName, StarID
From WorldBank_StarCluster_Data..Stars
Order by StarName
*/