/* This section processes a table of role names by using RunId and assigns the active roleName based on the runId then calls a proc to
create the roles */
use TestDatabase
Declare @ActiveRunId int, @MaxRunId int	
Declare @roleName nvarchar(4000)

--Create table #StarRoleNames (RunId int identity(1,1), RoleName varchar(4000))

 
	Insert into #StarRoleNames (RoleName)
	Select RoleName
	From TestDatabase..StarRoleNamesTable

	Select @ActiveRunId = Min(RunId), @MaxRunId = Max(RunId)
	From #StarRoleNames

	While @ActiveRunId <= @MaxRunId
		
		BEGIN

			Set @roleName = (
			Select RoleName
			From #StarRoleNames
			Where RunId = @ActiveRunId
			)

			EXEC Assign_All_Current_Users_To_Rolev2 @roleName
				

			Set @ActiveRunId = @ActiveRunId + 1
		End

truncate table #StarRoleNames



