/*** Script to change the name of an already creaded STAR ***/

select * from Stars;

set transaction isolation level serializable
begin transaction
declare @newNames table(StarId int primary key, NewStarName varchar(40));

insert into @newNames values
(250, 'Courtney Test'),
(252, 'Hannah Test'),
(253, 'Yizhe Test'),
(254, 'Rhi Test'),
(255, 'Molly Test'),
(256, 'DS Test 001'),
(316, 'DS Test 002'),
(317, 'DS Test 003'),
(318, 'DS Test 004')
;

select * from @newNames;

update Stars
set StarName = n.NewStarName
from Stars s
inner join @newNames n on s.StarId=n.StarId
;


/*** use rollback first to test script then use commit to make changes permanent ***/
Select * From Stars;
rollback; --comment out when running commit

/*** Run commit after verifying rollback ***/
--commit;

