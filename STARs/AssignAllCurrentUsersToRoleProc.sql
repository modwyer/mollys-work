use awm_Users;
GO

/****** Object:  StoredProcedure [dbo].[Assign_All_Current_Users_To_Role]    Script Date: 5/7/2015 10:48:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Molly O'Dwyer
-- Create date: 5/7/15
-- Description:	Creates a new role and assigns all users in the system to that role so they can see the datasets on the star
-- apps site to SQL instance mapping: { appsdev => data1dev\ss08std, appsqa => data1qa, apps => data1 }
-- change role name at the top to the role name in STAR creation - Part 1.sql
-- you can safely run this script multiple times all its operations are idempotent
-- default @appName is 'aWhere Beta'
/*

EXEC Assign_All_Current_Users_To_Role 'Data-CountrySTAT-Ghana-001'

*/
-- =============================================
alter PROCEDURE [dbo].Assign_All_Current_Users_To_Role

		@roleName nvarchar(4000)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
-- do not modIFy below this line
	
	BEGIN try
		set transaction isolation level read committed;
		BEGIN transaction;
		set xact_abort on;

		raiserror('Working on role %s...', 1, 99,@roleName) with nowait;
		declare @appName varchar(256) = 'aWhere Beta';
		declare @roleExists int;

		EXEC @roleExists = awm_Users.dbo.aspnet_Roles_RoleExists @appName, @roleName;
		--select @roleExists as RoleExists;
		
		IF (@roleExists = 0)
			BEGIN
				raiserror('Role does not exist, quiting procedure', 18, 100) with nowait;
				Return 1
			END
				
		--concat usernames
		declare @userListCsv nvarchar(max);
		declare @list varchar(max) = ''
				
		Select @list = @list + ', ' + LoweredUserName
		from awm_users.dbo.aspnet_Users
	
		Set @userListCsv = substring(@list,3, len(@list)-3)
		--check results
		Select len(@userListCsv) as OriginalListCount, @userListCsv as OriginalUserList

		
		--break list up to less than 4000 char
		declare @shortUserListCsv nvarchar(max)
		declare @lastStatus int;
		declare @tstamp datetime = CURRENT_TIMESTAMP;
	
		While len(@userListCsv) > 0
		BEGIN
			IF len(@userListCsv) > 4000
				BEGIN
					Print 'List is more than 4000 characters'
					Set @shortUserListCsv = left(@userListCsv,charindex(', ',@userListCsv, 3900)-1)
					--set short chunk to the original list minus the previous short chunk
					Set @userListCsv = right(@userListCsv, len(@userListCsv) - len(@shortUserListCsv)-2) 
				END
			Else
				BEGIN
					Set @shortUserListCsv = @userListCsv
					Set @userListCsv = right(@userListCsv, len(@userListCsv) - len(@shortUserListCsv))
				END


			exec @lastStatus = awm_Users.dbo.aspnet_UsersInRoles_AddUsersToRoles_aWhere @appName, @shortUserListCsv,@roleName,@tstamp;
		
			IF (@lastStatus > 0)
			BEGIN
				raiserror('Error adding users to role, code returned was %d', 18, 110, @lastStatus) with nowait;
			END

			--check results
			Select len(@shortUserListCsv) as ShortListLength, @shortUserListCsv as ShortList
			Select len(@userListCsv) as NewListLength, @userListCsv as NewList

			commit transaction;
			raiserror('Success! Comitted all our work.', 1, 201) with nowait;
		End
	END try
	
	BEGIN catch
		raiserror('Caught error, rolling back', 1, 202) with nowait;
		rollback transaction;
	END catch
End
GO


