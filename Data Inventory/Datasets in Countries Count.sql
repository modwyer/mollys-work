/*** Script to get counts of the number of datasets for each country ***/

WITH countryData(StarId,CountryName) AS
(
	SELECT DISTINCT StarID, loc.LocationName 
	FROM IntersectionAsVariant ixn 
	inner join Locations loc ON ixn.LocationId = loc.LocationId
	WHERE ixn.StarId in (67,68,69,73,74,75,83,91,146,76,77,79,105,106,108,109,110,128) --comment out this line to see all in cluster
	and loc.LocationTypeId = 23 -- countries; 25=regions, 65=admin 1, 23=countries
)
SELECT
	cd.CountryName, count(cd.CountryName) AS DataCount
FROM
	countryData cd
GROUP BY cd.CountryName
ORDER BY 
	DataCount DESC, cd.CountryName
;

/*** Script to get a list of locations per star ***/
SELECT DISTINCT StarID, loc.LocationName 
	FROM IntersectionAsVariant ixn 
	inner join Locations loc on ixn.LocationId = loc.LocationId
	WHERE ixn.StarId in (67,68,69,73,74,75,83,91,146,76,77,79,105,106,108,109,110,128)
	and loc.LocationTypeId = 23 -- countries
	ORDER BY StarID
	;
		
/*** Get concatenated list of countries in each star ***/
SELECT StarId, awmDatabase.dbo.ConcatOrderedDistinct(loc.LocationName)
	FROM IntersectionAsVariant ixn 
	inner join Locations loc on ixn.LocationId = loc.LocationId
	WHERE ixn.StarId in (67,68,69,73,74,75,83,91,146,76,77,79,105,106,108,109,110,128) --or for a single star use ixn.StarId = @starId; before select use declar @starId as int = star#
	and loc.LocationTypeId = 23 -- countries
	GROUP BY StarId
	;