﻿/*** Script to pull entire data inventory on production from WorldBank and ICRISAT Star Clusters ***/
/*** Will need to run this twice, once for WorldBank and once for ICRISAT ***/

--use WorldBank_StarCluster_Data;
use ICRISAT_StarCluster_Data;
with cte1(StarId,StarRoleName,StarNameInLoader,DatasetType,DatasetName,UpdatedOn,Timespan,Geography,DatasetSource,ShortDescription,DatasetKeywords)
as
(
select
	s.StarId,
	sr.RoleName,
	s.StarName as StarNameInLoader,
	EntryDocument.value(N'
		declare namespace DLE="http://tempuri.org/DataLibraryEntry.xsd";
		/DLE:LibraryEntry[1]/DLE:Type[1]
		', 'varchar(250)'
	) as DatasetType,
	EntryDocument.value(N'
		declare namespace DLE="http://tempuri.org/DataLibraryEntry.xsd";
		/DLE:LibraryEntry[1]/DLE:Title[1]
		', 'varchar(250)'
	) as DatasetName,
	EntryDocument.value(N'
		declare namespace DLE="http://tempuri.org/DataLibraryEntry.xsd";
		/DLE:LibraryEntry[1]/DLE:UpdatedOn[1]
		', 'varchar(250)'
	) as DateUpdated,
	EntryDocument.value(N'
		declare namespace DLE="http://tempuri.org/DataLibraryEntry.xsd";
		/DLE:LibraryEntry[1]/DLE:Timespan[1]
		', 'varchar(250)'
	) as Timespan,
	EntryDocument.value(N'
		declare namespace DLE="http://tempuri.org/DataLibraryEntry.xsd";
		/DLE:LibraryEntry[1]/DLE:Geography[1]
		', 'varchar(250)'
	) as [Geography],
	EntryDocument.value(N'
		declare namespace DLE="http://tempuri.org/DataLibraryEntry.xsd";
		/DLE:LibraryEntry[1]/DLE:DataSource/DLE:Owner/DLE:Name[1]
		', 'varchar(250)'
	) as DataSource,
	EntryDocument.value(N'
		declare namespace DLE="http://tempuri.org/DataLibraryEntry.xsd";
		/DLE:LibraryEntry[1]/DLE:Description[1]
		', 'varchar(250)'
	) as ShortDescription,
	EntryDocument.value(N'
		declare namespace DLE="http://tempuri.org/DataLibraryEntry.xsd";
		/DLE:LibraryEntry[1]/DLE:Keywords[1]
		', 'varchar(250)'
	) as DatasetKeywords
	--,awmDatabase.dbo.ConcatOrderedDistinct(lt.LocationTypeName + '/' + tt.TemporalTypeName) as ResolutionsFound
from
	Stars s
inner join
	Universe.dbo.StarRoles sr
	on sr.StarId = s.StarId
where
	EntryDocument is not null
),
cte2(StarId, ResolutionsFound) as
(
select
	s.StarId,
	awmDatabase.dbo.ConcatOrderedDistinct(lt.LocationTypeName + '/' + tt.TemporalTypeName) as ResolutionsFound
from
	Stars s
inner join
	Datasets d
	on s.StarId = d.StarId
inner join
	LocationTypes lt
	on d.BaseLocationTypeId = lt.LocationTypeId
inner join
	TemporalTypes tt
	on d.BaseTemporalTypeId = tt.TemporalTypeId
where
	s.EntryDocument is not null
	and CHARINDEX('StarStructure_',d.DatasetName) = 0
group by
	s.StarId
),
cte3(StarId,RawIxnCount,BasePairCount,AttrCount) as (
	select 
		d.StarId, 
		COUNT(ixn.IntersectionMeasure) as RawIxnCount,
		COUNT(distinct ixn.LocationId + ' ' + ixn.TemporalId) as BasePairCount,
		COUNT(distinct AttributeId) as AttrCount
	from Intersection ixn
	inner join Datasets d on d.DatasetId = ixn.DatasetId
	where ixn.CalculationId = 0 or ixn.CalculationId is null
	group by d.StarId
),
cte4(StarId,WeirdRatio) as (   --Shaw Density
	select c3.StarId, (c3.RawIxnCount * 1.0) / (c3.BasePairCount * c3.AttrCount)
	from cte3 c3
)
select c1.*, c2.ResolutionsFound, c3.RawIxnCount as ValidINX, c3.BasePairCount as LocationTimePairs, c3.AttrCount as AttributeCount
from cte1 c1
inner join cte2 c2 on c1.StarId = c2.StarId
inner join cte3 c3 on c1.StarId = c3.StarId
;