/*** Data Inventory Exploration, Part 2 ***/

/*** Query 3a - Africa
: Every year with data per country, listed in multiple rows ***/
--////Can only use 1 database at a time. Uncomment ICRISAT and comment out WorldBank and copy the results together////

use WorldBank_StarCluster_Data;
--Use ICRISAT_StarCluster_Data;
Select LocationName, LocationId
From Locations
Where LocationTypeId = 68
and CHARINDEX(', SNNPR, Ethiopia', LocationName)>0

Declare @Africa geometry = 
	(Select LocationFeature
	From Locations
	Where LocationId = 91525954)
Select l.LocationName
	, t.TemporalName as YearWithData
From IntersectionAsVariant ixn
inner join Temporal t on ixn.TemporalId = t.TemporalId
inner join Locations l on ixn.LocationId = l.LocationId
Where @Africa.STContains(CentroidFeature)=1
	and t.TemporalTypeId = 8
	and l.LocationTypeId = 68--23
group by  l.LocationName, t.TemporalName
Order by l.LocationName, t.TemporalName


--Get only Africa countries
Declare @Africa geometry = (Select LocationFeature From Locations Where LocationId = -46118)-- Africa Id: -46118
Select LocationId, LocationName, LocationFeature, CentroidFeature
From locations l
Where l.LocationTypeId = 23
and @Africa.STContains(CentroidFeature)=1



/*** Query 3b: List of years with data per country in Africa, Concatenated to single row ***/
--////Can only use 1 database at a time. Uncomment ICRISAT and comment out WorldBank and copy the results together////

use WorldBank_StarCluster_Data;
--Use ICRISAT_StarCluster_Data;

Declare @Africa geometry = (Select LocationFeature From Locations Where LocationId = -46118)

Select l.LocationName
	, awmDatabase.dbo.ConcatOrderedDistinct(t.TemporalName) as YearsWithdata
From IntersectionAsVariant ixn
	inner join Temporal t on ixn.TemporalId = t.TemporalId
	inner join Locations l on ixn.LocationId = l.LocationId
Where @Africa.STContains(CentroidFeature)=1
	and t.TemporalTypeId = 8
	and l.LocationTypeId = 23
Group by  l.LocationName
Order by l.LocationName

/*** Query 1c - Africa: Range of years per country in Africa  ***/
--////Can only use 1 database at a time. Uncomment ICRISAT and comment out WorldBank and copy the results together////

use WorldBank_StarCluster_Data;
--use ICRISAT_StarCluster_Data;

Declare @Africa geometry = (Select LocationFeature From Locations Where LocationId = -46118)
	
Select --s.StarName
	 l.LocationName
	, min(t.TemporalName) as MinYear
	, MAX(t.TemporalName) as MaxYear
	
From IntersectionAsVariant ixn
inner join Temporal t on ixn.TemporalId = t.TemporalId
--inner join Stars s on ixn.StarId = s.StarId
inner join Locations l on ixn.LocationId = l.LocationId

Where t.TemporalTypeId = 8
and l.LocationTypeId = 23
and @Africa.STContains(CentroidFeature)=1
group by l.LocationName
order by  l.LocationName asc  --s.StarName asc,



--count how many datasets share each attribute name
Select a.AttributeName
	,count(d.DatasetId) as DatasetCount
From Datasets d
inner join Attributes a on d.DatasetId = a.DatasetId
Group by  a.AttributeName
order by DatasetCount desc

select * from Attributes
Select * from Datasets


/***   ***/
--list of datasets that contain a specific attribute by attribute name or attribute id
Select a.AttributeName
	,d.PresentationDatasetName
From Attributes a
inner join Datasets d on d.DatasetId = a.DatasetId
Where AttributeName like '%Exports%' ---or use = 'text' without % to get exact; comment out entire line to get all attributes
Group by  a.AttributeName, d.PresentationDatasetName
order by a.AttributeName


/***   ***/
--List of locations where each attribute occurs, country level

Select a.AttributeName
	,l.LocationName

From Attributes a
inner join IntersectionAsVariant ixn on a.AttributeId = ixn.AttributeId
inner join Locations l on ixn.LocationId = l.LocationId
Where l.LocationTypeId = 23
group by a.AttributeName, l.LocationName
