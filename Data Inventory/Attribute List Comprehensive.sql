/*** Script for attribute list for Client Services ***/
/*** Only uncomment one of the use statements below! ***/

/*** Uncomment for "pro loaded" stuff: ***/
use WorldBank_StarCluster_Data;

/***  Uncomment for Icrisat's big project ***/
--use ICRISAT_StarCluster_Data;

declare @metaTitles table(StarId int primary key, StarName varchar(250), StarTitle nvarchar(250));
insert into @metaTitles
select
	s.StarId, 
	s.StarName, 
	EntryDocument.value(N'
		declare namespace DLE="http://tempuri.org/DataLibraryEntry.xsd";
		/DLE:LibraryEntry[1]/DLE:Title[1]
		', 'varchar(250)'
	) as StarTitle
from
	dbo.Stars s
;

--select * from @metaTitles;

select
	mt.StarId,mt.StarName,mt.StarTitle,
	dbo.AttributeDisplayPathFromNode(a.Node) as FolderPath,
	a.AttributeName,
	a.AttributeDescription,
	a.AttributeId
from 
	Attributes a
inner join
	Datasets d on d.DatasetId = a.DatasetId
inner join
	@metaTitles mt on mt.StarId = d.StarId
where
	a.IsFolder = 0
;