/*** Script to find number of loads attempted per dataset, aka STAR ***/

use Universe;
select
	d.PresentationDatasetName,
	COUNT(d.DatasetId) as LoadCount
from
	Datasets d
inner join
	Stars s on d.StarId = s.StarId
where
	d.PresentationDatasetName is not null
	and s.StarClusterId = 1
group by
	d.PresentationDatasetName
;