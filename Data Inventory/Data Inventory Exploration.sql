/*** Queries to explore the data in the database that is on production ***/

/*** Query 1a: For each star list the range of years (min and max) per Star ***/
--////Can only use 1 database at a time. Uncomment ICRISAT and comment out WorldBank and copy the results together////

use WorldBank_StarCluster_Data;
--Use ICRISAT_StarCluster_Data;
Select ixn.StarId
	,s.StarName
	,d.PresentationDatasetName
	, min(t.TemporalName) as MinYear
	, MAX(t.TemporalName) as MaxYear
From IntersectionAsVariant ixn
	inner join Temporal t on ixn.TemporalId = t.TemporalId
	inner join Datasets d on ixn.DatasetId = d.DatasetId
	inner join Stars s on ixn.StarId = s.StarId
Where t.TemporalTypeId = 8
Group by ixn.StarId, s.StarName, d.PresentationDatasetName
Order by ixn.StarId

--Select * from Datasets
--select * from IntersectionAsVariant


/*** Query 1b: For each country list the range of years (min and max) per Star   ***/
--////Can only use 1 database at a time. Uncomment ICRISAT and comment out WorldBank and copy the results together////

Use WorldBank_StarCluster_Data;
--use ICRISAT_StarCluster_Data;
Select s.StarName
	 ,l.LocationName
	, min(t.TemporalName) as MinYear
	, MAX(t.TemporalName) as MaxYear
From IntersectionAsVariant ixn
	inner join Temporal t on ixn.TemporalId = t.TemporalId
	inner join Stars s on ixn.StarId = s.StarId
	inner join Locations l on ixn.LocationId = l.LocationId
Where t.TemporalTypeId = 8
	and l.LocationTypeId = 23
Group by s.StarName, l.LocationName
Order by s.StarName asc, l.LocationName asc

/*** Query 1b - Africa: For each country list the range of years (min and max) per Star   ***/
--////Can only use 1 database at a time. Uncomment ICRISAT and comment out WorldBank and copy the results together////

Declare @Africa geometry = (Select LocationFeature From Locations Where LocationId = -46118)--Location ID for Africa

Select s.StarName
	 ,l.LocationName
	, min(t.TemporalName) as MinYear
	, MAX(t.TemporalName) as MaxYear
From IntersectionAsVariant ixn
	inner join Temporal t on ixn.TemporalId = t.TemporalId
	inner join Stars s on ixn.StarId = s.StarId
	inner join Locations l on ixn.LocationId = l.LocationId
Where @Africa.STContains(CentroidFeature) = 1
	and t.TemporalTypeId = 8
	and l.LocationTypeId = 23
Group by s.StarName, l.LocationName
order by s.StarName asc, l.LocationName asc


/*** Query 1c: Find the year range (min and max) for each attribute grouped by starname ***/
--////Can only use 1 database at a time. Uncomment ICRISAT and comment out WorldBank and copy the results together////

use WorldBank_StarCluster_Data;
--use ICRISAT_StarCluster_Data;
Select s.StarName
	,a.AttributeId
	, a.AttributeName
	, min(t.TemporalName) as MinYear
	, MAX(t.TemporalName) as MaxYear
From IntersectionAsVariant ixn
	inner join Temporal t on ixn.TemporalId = t.TemporalId
	inner join Stars s on ixn.StarId = s.StarId
	inner join Attributes a on ixn.AttributeId = a.AttributeId
Where t.TemporalTypeId = 8
Group by s.StarName, a.AttributeId, a.AttributeName
Order by s.StarName asc, a.AttributeName asc

select * from Attributes order by AttributeDescription asc


/*** Query 2a: Count how many stars have the same attribute name ***/
--////Can only use 1 database at a time. Uncomment ICRISAT and comment out WorldBank and copy the results together////

use WorldBank_StarCluster_Data;
--use ICRISAT_StarCluster_Data;

Select a.AttributeName
	,count(d.StarId) as DatasetCount
From Datasets d
	inner join Attributes a on d.DatasetId = a.DatasetId
Group by a.AttributeName
Order by DatasetCount desc

--select * from Attributes
--Select * from Datasets

/*** Query 2b: Count how many datasets have the same attribute name ***/
--////Can only use 1 database at a time. Uncomment ICRISAT and comment out WorldBank and copy the results together////

use WorldBank_StarCluster_Data;
--use ICRISAT_StarCluster_Data;

Select a.AttributeName
	,count(d.DatasetId) as DatasetCount
From Datasets d
inner join Attributes a on d.DatasetId = a.DatasetId
Group by  a.AttributeName
order by DatasetCount desc



/*** Query 2c: count how many attributes occur in each dataset ***/
--////Can only use 1 database at a time. Uncomment ICRISAT and comment out WorldBank and copy the results together////

Use WorldBank_StarCluster_Data;
--Use ICRISAT_StarCluster_Data;

Select d.PresentationDatasetName as DatasetName
	,count(a.AttributeId) as AttributeCount
From Datasets d
	inner join Attributes a on d.DatasetId = a.DatasetId
Group by  d.PresentationDatasetName
Order by AttributeCount desc
