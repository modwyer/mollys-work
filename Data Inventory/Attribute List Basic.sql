/*** Script to see complete list of attributes from Universe db ***/
SELECT 
	 sc.StarClusterId
	,sc.StarClusterName
	,s.StarId
	,s.StarName
	,a.[DatasetId]
	,[PresentationDatasetName] 
	,[AttributeId]
	,[AttributeName]
	,[AttributeDescription]                 
FROM 
	[Universe].[dbo].[Attributes] a 
inner join 
	[Universe].[dbo].[Datasets] d
	on a.DatasetId = d.DatasetId
inner join 
	Universe.dbo.Stars s
	on d.StarId = s.StarId
inner join 
	Universe.dbo.StarClusters sc
	on s.StarClusterId = sc.StarClusterId
Where 
	d.PresentationDatasetName Is not null
	AND a.IsFolder = 0
	-- StarCluster IDs: 1 => WorldBank/ThirdParty, 3 => UserData, 5 => ICRISAT
order by 
	a.AttributeId
;
 